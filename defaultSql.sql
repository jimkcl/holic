-- JI update_170622_1745
drop table mainSlide;
drop table alarm;

drop table vReply;
drop table videoLectureURL;
drop table videoLecture;
drop table videoLecturePackage;

drop table videoURL;
drop table video;
drop table videoPackage;

drop table askReply;
drop table askBoard;
drop table noticeBoardImg;
drop table noticeBoard;

drop table lessonEvaluation;
drop table lessonUser;
drop table lesson;
drop table tLike;
drop table evaluation;
drop table tImg;
drop table teacherDetail;

drop table declaration;
drop table blockUser;
drop table userPayment;
drop table userDetail;
delete from userLogin;






SHOW ENGINE INNODB STATUS;
SET FOREIGN_KEY_CHECKS=0;

select version();

use holic;


-- ****** sample ******
-- normal user 
delete from uSerLoginSequence;
insert into userLoginSequence values ( sequence );

insert into userLogin values ('user1', 'user1', 
	(select sequence from userLoginSequence order by sequence desc limit 1));

insert into userDetail (userKey, userWechat, userNick, userMail) 
	values((select sequence from userLoginSequence order by sequence desc limit 1), 
			'user1', 'user1', 'user1@user1.user1');


-- teacher
delete from uSerLoginSequence;
insert into userLoginSequence values ( sequence );

insert into userLogin values ('teacher1', 'teacher1', 
	(select sequence from userLoginSequence order by sequence desc limit 1));

insert into userDetail (userKey, userWechat, userNick, userMail, userType) 
	values((select sequence from userLoginSequence order by sequence desc limit 1), 
			'teacher1', 'teacher1', 'teacher1@teacher1.teacher1', 5);

insert into teacherDetail (userKey, userName) values (
	(select userKey from userLogin where userID='teacher1'), 'teacher1');


-- Operator
delete from uSerLoginSequence;
insert into userLoginSequence values ( sequence );

insert into userLogin values ('oper1', 'oper1', 
	(select sequence from userLoginSequence order by sequence desc limit 1));

insert into userDetail (userKey, userWechat, userNick, userMail, userType) 
	values((select sequence from userLoginSequence order by sequence desc limit 1), 
			'operator1', 'operator1', 'operator1@operator1.operator1', 8);


-- administrator
delete from uSerLoginSequence;
insert into userLoginSequence values ( sequence );

insert into userLogin values ('ddz7uBIYvKSFpWvMIbGJSw==', '1f3ce40415a2081fa3eee75fc39fff8e56c22270d1a978a7249b592dcebd20b4', 
	(select sequence from userLoginSequence order by sequence desc limit 1));

insert into userDetail (userKey, userWechat, userNick, userMail, userType) 
	values((select sequence from userLoginSequence order by sequence desc limit 1), 
			'kMV4LDcBlcZaRW3DqnJkwg==', 'admin', 'CsQK0X2cM8AVPeH5SNVBJwgzrRPAlkSDttGRCzhLhec=', 9);






-- ****** User ******

-- UserLogin
drop table userLogin;

delete from userLogin;


create table userLogin(
	userID			VARCHAR(130) not null unique,
	userPassword	VARCHAR(130) not null,
	userKey			VARCHAR(130) primary key
);

select * from userLogin;




-- UserLogin Sequnce
create table userLoginSequence ( sequence int(6) auto_increment primary key );

insert into userLoginSequence values ( sequence );

delete from userLoginSequence;

select * from userLoginSequence;






-- UserDetail
drop table userDetail;


create table userDetail(
	userKey			VARCHAR(130) primary key not null,
	userWechat		VARCHAR(130) unique not null,
	userNick		VARCHAR(130) unique not null,
	userMail		VARCHAR(130) unique not null,
	userSchool		VARCHAR(130),
	userPost		VARCHAR(6),
	userAddress1	VARCHAR(150),
	userAddress2	VARCHAR(150),
	userGender		INT(1),
	userBirth		VARCHAR(10),
	userType		INT(2) default 1,
	userDate		DATETIME default CURRENT_TIMESTAMP,
	
	foreign key (userKey) references UserLogin(userKey) 
	on update cascade on delete cascade
);

select * from userDetail;






-- userPayment
drop table userPayment;


create table userPayment(
	userKey		VARCHAR(130) not null primary key,
	pDate		DATETIME not null,
	
	foreign key (userKey) references userDetail(userKey) 
	on update cascade on delete cascade
);

select * from userPayment;






-- blockUser
drop table blockUser;


create table blockUser(
	userKey		VARCHAR(130) not null primary key,
	bDate		DATETIME not null,
	
	foreign key (userKey) references userDetail(userKey) 
	on update cascade on delete cascade
);

select * from blockUser;






-- declaration
drop table declaration;


create table declaration(
	userKey1	VARCHAR(130) not null,
	userKey2	VARCHAR(130) not null,
	reason		VARCHAR(30) not null,
	
	foreign key (userKey1) references userDetail(userKey) 
	on update cascade on delete cascade,
	
	foreign key (userKey2) references userDetail(userKey) 
	on update cascade on delete cascade
);

select * from declaration;






-- ****** Teacher ******

-- TeacherDetail
drop table teacherDetail;


create table teacherDetail(
	userKey		VARCHAR(130) primary key not null,
	userName	VARCHAR(15),
	tProfile	VARCHAR(255),
	tListening	INT(3),
	tRead		INT(3),
	tWriting	INT(3),
	tGrade		INT(1),
	tUpdateDate	DATETIME default CURRENT_TIMESTAMP,
	
	foreign key (userKey) references userDetail(userKey) 
	on update cascade on delete cascade
);

select * from teacherDetail;






-- TImg
drop table tImg;


create table tImg(
	userKey	 VARCHAR(130) not null primary key,
	gradePath VARCHAR(255),
	provePath VARCHAR(255),
	
	foreign key (userKey) references userDetail(userKey) 
	on update cascade on delete cascade
);

select * from tImg;





-- evaluation
drop table evaluation;


create table evaluation(
	tUserKey	VARCHAR(130) not null,
	userKey		VARCHAR(130) not null,
	eContent	VARCHAR(255) not null,
	eDate		DATETIME default CURRENT_TIMESTAMP,
	
	foreign key (tUserKey) references teacherDetail(userKey) 
	on update cascade on delete cascade,
	
	foreign key (userKey) references userDetail(userKey) 
	on update cascade on delete cascade
);

select * from evaluation;






-- tLike
drop table tLike;


create table tLike(
	tUserKey	VARCHAR(130) not null,
	UserKey		VARCHAR(130) not null,
	
	foreign key (UserKey) references UserDetail(UserKey) 
	on update cascade on delete cascade,
	
	foreign key (UserKey) references TeacherDetail(UserKey) 
	on update cascade on delete cascade
);

select * from tLike;






-- ****** Lesson ******
drop table lesson;


create table lesson(
	lIdx		INT(6) not null primary key,
	userKey		VARCHAR(130) not null,
	lTitle		VARCHAR(20) not null,
	lAddress	VARCHAR(255) not null,
	lDate		DATETIME default CURRENT_TIMESTAMP,
	lPersonnel	INT(6) not null,
	lAbility	VARCHAR(200),
	lMaterials	VARCHAR(200),
	lType		INT(1) default 0,
	
	foreign key (userKey) references userDetail(userKey) 
	on update cascade on delete cascade
);

select * from lesson;






-- lessonUser
drop table lessonUser;


create table lessonUser(
	lIdx		INT(6) not null,
	userKey		VARCHAR(130) not null,
	
	foreign key (userKey) references userDetail(userKey) 
	on update cascade on delete cascade,
	
	foreign key (lIdx) references lesson(lIdx) 
	on update cascade on delete cascade
);

select * from lessonUser;






-- lessonEvaluation
drop table lessonEvaluation;


create table lessonEvaluation(
	lIdx		INT(6) not null,
	userKey		VARCHAR(130) not null,
	lContent	VARCHAR(255) not null,
	lDate		DATETIME default CURRENT_TIMESTAMP,
	
	foreign key (lIdx) references lesson(lIdx) 
	on update cascade on delete cascade,
	
	foreign key (userKey) references userDetail(userKey) 
	on update cascade on delete cascade
);


select * from lessonEvaluation;






-- ****** Board ******

-- noticeBoard
drop table noticeBoard;


create table noticeBoard(
	idx			INT(4) primary key auto_increment,
	userKey		VARCHAR(130) not null,
	userNick	VARCHAR(15) not null,
	aTitle		VARCHAR(30) not null,
	aContent	Text not null,
	aDate		DATETIME default CURRENT_TIMESTAMP,
	aCount		INT(4) default 0,
	aType		INT(2) not null,
	
	foreign key (userKey) references UserDetail(userKey) 
	on update cascade on delete cascade,
	
	foreign key (userNick) references UserDetail(userNick) 
	on update cascade on delete cascade
);

select * from noticeBoard;






-- noticeBoardImg
drop table noticeBoardImg;


create table noticeBoardImg(
	idx		INT(4) not null,
	path	VARCHAR(255) not null,
	
	foreign key (idx) references noticeBoard(idx) 
	on update cascade on delete cascade
);

select * from noticeBoardImg;






-- AskBoard
drop table askBoard;


create table askBoard(
	idx			INT(4) primary key auto_increment,
	userKey		VARCHAR(130) not null,
	userNick	VARCHAR(15) not null,
	aTitle		VARCHAR(30) not null,
	aContent	Text not null,
	aDate		DATETIME default CURRENT_TIMESTAMP,
	aCount		INT(4) default 0,
	aVisibility	INT(1) default 0,
	
	foreign key (userKey) references UserDetail(userKey) 
	on update cascade on delete cascade,
	
	foreign key (userNick) references UserDetail(userNick) 
	on update cascade on delete cascade
);

select * from askBoard;






-- askReply
drop table askReply;


create table askReply(
	idx			INT(4) primary key auto_increment,
	bIdx		INT(4) not null ,
	userKey		VARCHAR(130) not null,
	userNick	VARCHAR(15) not null,
	rContent	VARCHAR(255) not null,
	rDate		DATETIME default CURRENT_TIMESTAMP,
	rOpenCheck	INT(1) default 0,
	
	foreign key (userKey) references userDetail(userKey) 
	on update cascade on delete cascade,
	
	foreign key (bIdx) references AskBoard(idx) 
	on update cascade on delete cascade,
	
	foreign key (userNick) references UserDetail(userNick) 
	on update cascade on delete cascade
);

select * from askReply;






-- ****** video ******

-- videoPackage
drop table videoPackage;


create table videoPackage(
	vPackage	VARCHAR(20) not null primary key
);

select * from videoPackage;






-- video
drop table video;


create table video(
	vIdx		INT(6) primary key auto_increment,
	userKey		VARCHAR(130) not null,
	vTitle		VARCHAR(20) not null,
	vContent	Text not null,
	vPath		VARCHAR(255) not null,
	rDate		DATETIME default CURRENT_TIMESTAMP,
	vType		INT(1) default 1,
	
	foreign key (userKey) references userDetail(userKey) 
	on update cascade on delete cascade
);

select * from video;






-- videoURL
drop table videoURL;


create table videoURL(
	idx			INT(6) primary key auto_increment,
	vIdx		INT(6) not null,
	vPackage	VARCHAR(20),
	vURL		VARCHAR(255) not null,
	
	foreign key (vIdx) references video(vIdx) 
	on update cascade on delete cascade,
	
	foreign key (vPackage) references videoPackage(vPackage) 
	on update cascade on delete cascade
);

select * from videoURL;






-- videoLecturePackage
drop table videoLecturePackage;


create table videoLecturePackage(
	vPackage	VARCHAR(20) not null primary key
);

select * from videoLecturePackage;






-- videoLecture
drop table videoLecture;


create table videoLecture(
	vIdx		INT(6) primary key auto_increment,
	userKey		VARCHAR(130) not null,
	tUserKey	VARCHAR(130),
	vPackage	VARCHAR(20),
	vTitle		VARCHAR(40) not null,
	vContent	Text not null,
	vDate		DATETIME default CURRENT_TIMESTAMP,
	vScore		INT(2) default 0,
	vScoreCount	INT(6) default 0,
	vType		INT(1) default 0,
	vCount		INT(6) default 0,
	
	foreign key (userKey) references UserDetail(userKey) 
	on update cascade on delete cascade,
	
	foreign key (tUserKey) references teacherDetail(userKey) 
	on update cascade on delete cascade,
	
	foreign key (vPackage) references videoLecturePackage(vPackage) 
	on update cascade on delete cascade
);

select * from videoLecture;




insert ignore videoLecture (userKey,vTitle,vContent) 
	values ((select userKey from userLogin order by userKey desc limit 1),1,1);






-- videoLectureURL
drop table videoLectureURL;


create table videoLectureURL(
	idx			INT(6) primary key auto_increment,
	vIdx		INT(6) not null,
	vName		VARCHAR(50) not null,
	vURL		VARCHAR(255) not null,
	
	foreign key (vIdx) references videoLecture(vIdx) 
	on update cascade on delete cascade
);

select * from videoLectureURL;






-- vReply
drop table vReply;


create table vReply(
	vIdx		INT(6) not null,
	userKey		VARCHAR(130) not null,
	vrComment	VARCHAR(150) not null,
	vrScore		INT(1) not null,
	vrDate		DATETIME default CURRENT_TIMESTAMP,
	
	foreign key (vIdx) references videoLecture(vIdx) 
	on update cascade on delete cascade,
	
	foreign key (userKey) references userDetail(userKey) 
	on update cascade on delete cascade
);

select * from vReply;






-- ****** Etc ******
-- coupon
drop table coupon;


create table coupon(
	serial		VARCHAR(19) not null primary key,
	validity	Date not null,
	term		INT(2) not null
);

select * from coupon;






-- alarm
drop table alarm;


create table alarm(
	userKey		VARCHAR(130) not null,
	aContent	VARCHAR(200) not null,
	aDate		DATETIME default CURRENT_TIMESTAMP,
	aCheck		INT(1) default 0
);

select * from alarm;






-- MainSlide
drop table MainSlide;


create table MainSlide(
	idx		INT(6) primary key auto_increment,
	imgPath	VARCHAR(255) not null
);

select * from MainSlide;





