// 회원가입 javascript

		var button = true;
		var userId = "";
		var userPassword1 = "";
		var userPassword = "";
		var userNick = "";
		var userWechat = "";
		var userMail1 = "";
		var userMail2 = "";
		var patten = /.*[\s]$/g;
		
		$(document).ready(function(){
			
			//검증부분
			$("#userId").blur(function(){
				userId = document.getElementById('userId').value;
				var patten = /^[a-zA-Z][a-zA-Z0-9_]*$/;
				if(userId.length < 4){
					$(".error_id").html("<p class='error' style='color:#ff6666'>아이디는 4자리이상이여야합니다</p>");
					$("#userId").val("");
				}else if(!patten.test(userId)){
					$(".error_id").html("<p class='error' style='color:#ff6666'>아이디는 영어,숫자로 _만 가능합니다</p>");
					$("userId").focus();
				}else{
					$.ajax({
						type:'post',
						url:'/user/joinIdCheck',
						data:{
							userId:userId
						},
						async:false,//동기방식
						dataType:'text',
						success:function(result){
							if(result == "Id_Yes"){
								$(".Check_error").html("<strong>아이디가 중복됩니다.</strong>");
								$("#userId").val("");
								$("#userId").focus();
							}else if(result == "Id_No"){
								$(".Check_error").html("<strong>사용가능한 아이디입니다.</strong>");
								$(".error_id").html("");
							}else if(result == "Id_null"){
								$(".Check_error").html("<strong>아이디가 공백이면 안됩니다</strong>");	
								$("#userId").focus();
							}	
						}
					})
				}
			});
			
			$('#userNick').blur(function(){
				userNick = document.getElementById('userNick').value;
				if(userNick.length>0){
					$.ajax({
						type:'post',
						url:'/user/joinNicCheck',
						data:{
							userNick:userNick
						},
						async:false,
						dataType:'text',
						success:function(result){
							if(result == "Nick_Yes"){
								$(".Check_error").html("<strong>닉넴이 중복됩니다.</strong>");
								$("#userNick").val("");
								$("#userNick").focus();
							}else if(result == "Nick_No"){
				
								$(".Check_error").html("<strong>사용가능한 닉네임입니다.</strong>");
							}else if(result == "Nike_null"){
	
								$(".Check_error").html("<strong>닉넴임이 공백이면 안됩니다</strong>");	
								$("#userNick").focus();
							}
						}
					});
				}else{
					$(".error_Nike").html("<p class='error_text' style='color:#ff6666'>1자이상이여야합니다</p>")
				}
				
			});
			
			$("#userWechat").blur(function(){
				userWechat = document.getElementById('userWechat').value;
				var WechatPatten1 = /.*[a-zA-Z0-9_.-]/;
				if(userWechat.length <3){
					$(".error_Wechat").html("<p class='error_text' style='color:#ff6666'>2자리이상이여야합니다</p>");
				}else if(!WechatPatten1.test(userWechat)){
					$(".error_Wechat").html("<p class='error_text' style='color:#ff6666'>위쳇은 영어랑숫자 ( _-.특수문자 )로만구성되여야합니다</p>");
				}else{
					$.ajax({
						type:'post',
						url:'/user/joinWechatCheck',
						data:{
							userWechat:userWechat
						},
						async:false,
						dataType:'text',
						success:function(result){
							
							if(result == "Wechat_Yes"){
								
								$(".Check_error").html("<strong>Wechat이  중복됩니다.</strong>");
								$("#userWechat").val("");
								$("#userWechat").focus();
							}else if(result == "Wechat_No"){
								
								$(".Check_error").html("<strong>사용가능한 Wechat입니다.</strong>");
							}else if(result == "Nike_null"){
								
								$(".Check_error").html("<strong>Wechat이 공백이면 안됩니다</strong>");	
								$("#userWechat").focus();
							}
						}
					});
				}
			});
			
			$("#userMail2").blur(function(){
				var patten = /([a-zA-Z0-9._-]{1,20})@([a-zA-Z0-9._-]{1,10})\.([a-zA-Z0-9._-]{1,6})/;
				var userMail = document.getElementById('userMail1').value+"@"+document.getElementById('userMail2').value;
				console.log(userMail);
				console.log(patten.test(userMail));
				if(patten.test(userMail)){
					
					$.ajax({
						type:'post',
						url:'/user/joinMailCheck',
						data:{
							userMail:userMail
						},
						async:false,
						dataType:'text',
						success:function(result){
							if(result == "Mail_Yes"){
							
								$(".Check_error").html("<strong>Email이  중복됩니다.</strong>");
								$("#userWechat").val("");
								$("#userWechat").focus();
							}else if(result == "Mail_No"){
								$(".Check_error").html("<strong>사용가능한 Email입니다.</strong>");
								$(".error_Email").html("");
							}else if(result == "Mail_null"){
								
								$(".Check_error").html("<strong>Email이 공백이면 안됩니다</strong>");	
								$("#userWechat").focus();
							}
						}
						
					});
				}else{
					$(".Check_error").html("<strong>정확한 Email입력해주세요</strong>");
				}		
			}); 
			
			//password
			$("#userPassword1").blur(function(){
				if(document.getElementById('userPassword1').value.length < 8){
					$(".error_pass1").html("<p class='error' style='color:#ff6666'>비번은 8자리이상이여야합니다</p>");
				}	
				$("#userPassword1").focus(function(){
					$(".error_pass1").html("<p class='error' style='color:#ff6666'></p>");
				});
			});
			
			
			//button cliclk
			$("#loginSubmit").click(function(){
				/* event.preventDefault(); */
				//var formData = new FormData("#isOk");
				//var formData = $('#isOk').serialize();
				var idResult ='';
				var nickResult = '';
				var wechatResult = '';
				var mailResult = '';
				var userMail = document.getElementById('userMail1').value+"@"+document.getElementById('userMail2').value;
				userId = document.getElementById('userId').value;
				userPassword = document.getElementById('userPassword').value;
				userNick = document.getElementById('userNick').value;
				userWechat = document.getElementById('userWechat').value;
				$(".Check_error").html("");
				/* $.ajax({
					type:'post',
					url:'/user/join',
					data:formData,
					dataType:'text',
					success:function(result){
						alert(result);
					},error: function(){
						alert('2222');
					}
				}); */ 
				
				
				//다시한번검증하는 부분 
				
					//아이디
					userId = document.getElementById('userId').value;
					var patten = /^[a-zA-Z][a-zA-Z0-9_]*$/;
					if(userId.length < 4){
						$(".error_id").html("<p class='error' style='color:#ff6666'>아이디는 4자리이상이여야합니다</p>");
						$("#userId").val("");
						return false;
					}else if(!patten.test(userId)){
						$(".error_id").html("<p class='error' style='color:#ff6666'>아이디는 영어,숫자로 _만 가능합니다</p>");
						$("userId").focus();
						return false;
					}else{
						$.ajax({
							type:'post',
							url:'/user/joinIdCheck',
							data:{
								userId:userId
							},
							async:false,//동기방식
							dataType:'text',
							success:function(result){
								if(result == "Id_Yes"){
									idResult = "Id_Yes";
									$(".Check_error").html("<strong>아이디가 중복됩니다.</strong>");
									$("#userId").focus();
									return false;
								}else if(result == "Id_No"){
									idResult = "Id_No";
									$(".Check_error").html("<strong>사용가능한 아이디입니다.</strong>");
									$(".error_id").html("");
									return false;
								}else if(result == "Id_null"){
									idResult = "Id_null";
									$(".Check_error").html("<strong>아이디가 공백이면 안됩니다</strong>");	
									$("#userId").focus();
									return false;
								}	
							}
						})
					}
					
					//pass
						userPassword1 = document.getElementById('userPassword1').value;
						userPassword = document.getElementById('userPassword').value;
						console.log(userPassword);
						if(userPassword1.length < 8){
							$(".Check_error").html("<strong>비번은 8자리이상이여야합니다.</strong>");
							$("#userPassword1").val("");
							$("#userPassword").val("");
							$("#userPassword1").focus();
							return false;
						}	
						$("#userPassword1").focus(function(){
							$(".error_pass1").html("<p class='error' style='color:#ff6666'></p>");
						});
						if(userPassword1 != userPassword){
							$("#userPassword").focus();
							$(".Check_error").html("<strong>비번이 동일하지 않습니다</strong>");
							return false;
						}
			
			
					//닉넴
					userNick = document.getElementById('userNick').value;
					if(userNick.length>0){
						$.ajax({
							type:'post',
							url:'/user/joinNicCheck',
							data:{
								userNick:userNick
							},
							async:false,
							dataType:'text',
							success:function(result){
								if(result == "Nick_Yes"){
									nickResult = "Nick_Yes";
									$(".Check_error").html("<strong>닉넴이 중복됩니다.</strong>");
									$("#userNick").val("");
									$("#userNick").focus();
									return false;
								}else if(result == "Nick_No"){
									nickResult = "Nick_No";
									$(".Check_error").html("<strong>사용가능한 닉네임입니다.</strong>");
									return false;
								}else if(result == "Nike_null"){
									nickResult = "Nike_null";
									$(".Check_error").html("<strong>닉넴임이 공백이면 안됩니다</strong>");	
									$("#userNick").focus();
									return false;
								}
							}
						});
					}else{
						$(".error_Nike").html("<p class='error_text' style='color:#ff6666'>1자이상이여야합니다</p>")
						return false;
					}
					
				
				
					//위쳇
					userWechat = document.getElementById('userWechat').value;
					var WechatPatten1 = /.*[a-zA-Z0-9_.-]/;
					if(userWechat.length <3){
						$(".error_Wechat").html("<p class='error_text' style='color:#ff6666'>2자리이상이여야합니다</p>");
						return false;
					}else if(!WechatPatten1.test(userWechat)){
						$(".error_Wechat").html("<p class='error_text' style='color:#ff6666'>위쳇은 영어랑숫자 ( _-.특수문자 )로만구성되여야합니다</p>");
						return false;
					}else{
						$.ajax({
							type:'post',
							url:'/user/joinWechatCheck',
							data:{
								userWechat:userWechat
							},
							async:false,
							dataType:'text',
							success:function(result){
								
								if(result == "Wechat_Yes"){
									wechatResult = "Wechat_Yes";
									$(".Check_error").html("<strong>Wechat이  중복됩니다.</strong>");
									$("#userWechat").val("");
									$("#userWechat").focus();
									return false;
								}else if(result == "Wechat_No"){
									wechatResult = "Wechat_No";
									$(".Check_error").html("<strong>사용가능한 Wechat입니다.</strong>");
									return false;
								}else if(result == "Nike_null"){
									wechatResult = "Wecha_tNull";
									$(".Check_error").html("<strong>Wechat이 공백이면 안됩니다</strong>");	
									$("#userWechat").focus();
									return false;
								}
							}
						});
					}
			
				
					//email
					var patten = /([a-zA-Z0-9._-]{1,20})@([a-zA-Z0-9._-]{1,10})\.([a-zA-Z0-9._-]{1,6})/;
					var userMail = document.getElementById('userMail1').value+"@"+document.getElementById('userMail2').value;
					console.log(userMail);
					console.log(patten.test(userMail));
					if(patten.test(userMail)){
						
						$.ajax({
							type:'post',
							url:'/user/joinMailCheck',
							data:{
								userMail:userMail
							},
							async:false,
							dataType:'text',
							success:function(result){
								if(result == "Mail_Yes"){
									mailResult = "Mail_Yes";
									$(".Check_error").html("<strong>Email이  중복됩니다.</strong>");
									$("#userWechat").val("");
									$("#userWechat").focus();
									return false;
								}else if(result == "Mail_No"){
									mailResult = "Mail_No";
									$(".Check_error").html("");
									$(".error_Email").html("");
									return false;
								}else if(result == "Mail_null"){
									mailResult = "Mail_null";
									$(".Check_error").html("<strong>Email이 공백이면 안됩니다</strong>");	
									$("#userWechat").focus();
									return false;
								}
							}
							
						});
					}else{
						$(".Check_error").html("<strong>정확한 Email입력해주세요</strong>");
						return false;
					}		
				
				// 전체부분을 보내는 부분
				$.ajax({
					type:'post',
					url:'/user/join',
					data:{
						userMail:userMail,
						userId:userId,
						userPassword:userPassword,
						userNick:userNick,
						userWechat:userWechat
					},
					async:false,
					dataType:'text',
					success:function(result){
						if(result == "Join_Ok"){
							alert("가입 축하드립니다.");
							self.location="/user/login";
						}
					}	
				});
				
			});
			
			$(".useOk").click(function(){
				if(button){
					$(".useOk_text").slideDown("slow");
					button = false;
				}else{
					$(".useOk_text").slideUp("slow");
					button = true;
				}
			});
			
		});
		
		function uIdCheck(){
			var idPatten1 = /^[a-zA-Z]/;
			var idPatten2 = /^[a-zA-Z][a-zA-Z0-9_]*$/;
			userId = document.getElementById('userId').value;
			var idLength = userId.length;
			
			$(".error_id").html("");
			if(!idPatten1.test(userId)){
				$(".error_id").html("<p class='error' style='color:#ff6666'>아이디첫글자는 영어로 작성해주세요</p>");
				$("#userId").val("");
				return false;
			}
			
			if(!idPatten2.test(userId)){
				$(".error_id").html("<p class='error' style='color:#ff6666'>아이디는 영어,숫자로만 가능합니다</p>");
				return false;
			}
			
			if(idLength>11){
				$(".error_id").html("<p class='error' style='color:#ff6666'>아이디는 12자리이하여야합니다</p>");
				$("#userId").val(document.getElementById('userId').value.substr(0,11));
				return false;
			}
		}
		
		function passCheck1(){
			/* var passPatten = /.*([a-zA-Z]|[!@#$%^*+=-]|[0-9])$/; */
			if(document.getElementById('userPassword1').value.length >=8){
				$(".error_pass1").html("<p class='error' style='color:#66cc66'>사용가능한 비밀번호입니다.</p>");
			}
			if(patten.test(document.getElementById('userPassword1').value)){
				$(".error_pass1").html("<p class='error' style='color:#ff6666'>공백을 포함할수없습니다</p>");
				$("#userPassword1").val("");
			}
		}
		
		function passCheck(){
			
			var passPatten = /^[a-zA-Z](.*[a-zA-Z]|.*[!@#$%^*+=-]|.*[0-9]).{2,10}$/;
			userPassword1 = document.getElementById('userPassword1').value;
			userPassword = document.getElementById('userPassword').value;
			if(userPassword1 == userPassword){
				$("#userPassword").css({background:"#66cc66"});
				$(".error_pass").html("<p class='error_text' style='color:#66cc66'>입력하신 비번이 동일합니다</p>");
			}else{
				$("#userPassword").css({background:"#ff6666"});
				$(".error_pass").html("<p class='error_text' style='color:#ff6666'>입력하신 비번이 동일하지 않습니다</p>");
			}
		}
		
		function NickCheck(){
			userNick = $("#userNick").val();
			if(patten.test(userNick)){
				$(".error_Nike").html("<p class='error_text' style='color:#ff6666'>닉넥입은 공백있으면 안됩니다</p>");
				$("#userNick").val("")
			}else{
				$(".error_Nike").html("<p class='error_text' style='color:#ff6666'></p>");
			}
			
			
		}
		
		function WeChatCheck(){
			var WechatPatten1 = /.*[a-zA-Z0-9_.-]$/;
			userWechat = document.getElementById('userWechat').value;
			if(!WechatPatten1.test(userWechat)){
				$(".error_Wechat").html("<p class='error_text' style='color:#ff6666'>위쳇은 영어랑숫자 ( _-.특수문자 )로만구성되여야합니다</p>");
			}
			else{
				$(".error_Wechat").html("<p class='error_text' style='color:#ff6666'></p>");
			}
			
			if(patten.test(userWechat)){
				$(".error_Wechat").html("<p class='error_text' style='color:#ff6666'>공백이있으면 안됩니다</p>");
				$("#userWechat").val("");
			}
			
		}
		
		function Email1Check(){
			var mailPatten = /^[a-zA-Z0-9_.-]([^!@#]|[a-zA-Z0-9_.-]){1,19}/;
			var userMail1 = $("#userMail1").val(); 
			console.log(userMail1);
			console.log(mailPatten.test(userMail1));
			if(!mailPatten.test(userMail1)){
				$(".error_Email").html("<p class='error_text' style='color:#ff6666'>숫자, 영어로구성된 제대로된 Email 입력해주세요</p>");
				/* $("#userEmail1").val(userEmail1.substr(1,userEmail1.length-2)); */
			}
		}
		function Email2Check(){
			var mailPatten = /^[a-zA-Z0-9_.-]([a-zA-Z0-9_.-]{3,10})\.+([a-zA-Z0-9_.-]{1,6})%/g;
			var userMail2 = $("#userMail2").val(); 
			console.log(userMail2);
			console.log(mailPatten.test(userMail2));
			
		}
		 