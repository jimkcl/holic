/**
 * 
 */

function checkImageType(fileName){
	var pattern=/jpg|gif|png|jpeg/i; 
	
	return fileName.match(pattern);
}

function getFileInfo(fullName){
	var fileName, imgsrc, getLink;
	var fileLink; //uuid와 원래파일이름에 적용
	
	if(checkImageType(fullName)){
		var num = fullName.indexOf("teacher");
		var length = fullName.length();
		alert(num);
		alert(length);
		alert(fullName.substr(num));
		fileLink=fullName.substr(14); //s_를 뺀 뒤부터 나오는 uuid+원래파일이름
		var prefix=fullName.substr(0, 12);
		var suffix=fullName.substr(14);
		getLink="/displayFile?fileName="+prefix+suffix;
	} else{ //이미지 파일이 아닐경우
		imgsrc="/resources/img/back.png";
		fileLink=fullName.substr(12); //이미지 파일이 아닌 경우는 s_가 생기지 않으므로..
		getLink="/displayFile?fileName="+fullName;
	}
	
	fileName=fileLink.substr(fileLink.indexOf("_")+1);
	
	return {fileName:fileName, imgsrc:imgsrc, getLink:getLink, fullName:fullName}; // /board/create.jsp의 handlebars의 속성명과 같아야 함.
}
