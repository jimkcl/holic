/// userPage 즉 회원가입후 마이페지를 말함

$(document).ready(function(){
	var button_check = true;
	
	$("#submit_button").click(function(){
		//성별 입력
		$(".weChat_check").css("display","inline-block");
		
		if(button_check){
			$(this).html("수정확인");
			$(".button_click_on").css({display:'block'});
			$(".button_click_no").css({display:'none'});
			
			button_check = false;
		}else{
			button_check = true;
			$(this).html("정보수정");
			
			var userGender = $(":input:radio[name=1]:checked").val();
			var birth_check = $(".birth_check").val();
			
			if(birth_check == "no"){
				var birth_year = $("select[name=birth_year]").val();
				var birth_month = $("select[name=birth_month]").val();
				var birth_day = $("select[name=birth_day]").val();
				var userBirth = birth_year+"-"+birth_month+"-"+birth_day;	
			}else if(birth_check == "ok"){
				var userBirth = $(".ok_birth").val();
			}
			
			var area =$("select[name=area]").val();
			var school1 = $(".school1").val();
			var school2 = $(".school2").val();
			var userSchool = area+"+"+school1+"+"+school2;
			var userPost = $("#sample6_postcode").val();
			var userAddress1 = $("#sample6_address").val();
			var userAddress2 = $("#sample6_address2").val();
			
			if(userGender=="no"){
				userGender=0;
			}else if(userGender == "man"){
				userGender=1;
			}else if(userGender == "women"){
				userGender=2;
			}
			
			/*
			console.log(userBirth);
			console.log(userSchool);
			console.log(userPost);
			console.log(userAddress1);
			console.log(userAddress2);
			*/
			
			$.ajax({
				type:'post',
				url:'/user/userPage',
				dataType:'text',
				data:{
					userSchool : userSchool,
					userPost : 0,
					userAddress1 : userAddress1,
					userAddress2 : userAddress2,
					userGender : userGender,
					userBirth : userBirth
				},
				success:function(data){
					alert("수정하였습니다.");
					location.reload();
				}
			});
		}
	});
});

//////////   강사쪽 자바스크립트
$.fn.setPreview = function(opt){
    var defaultOpt = {
        inputFile: $(this),
        img: null,
        w: 200,
        h: 200
    };
    $.extend(defaultOpt, opt); //앞에 것을 뒤에것으로 업데이트시킴
 
    var previewImage = function(){
    	
        if (!defaultOpt.inputFile || !defaultOpt.img) return;
        
        var inputFile = defaultOpt.inputFile.get(0);
        var img       = defaultOpt.img.get(0);
        
        if(checkImg(inputFile.value)){
        	// FileReader
	        if (window.FileReader) {
	            // image 파일만
	            if (!inputFile.files[0].type.match(/image\//)) return;
	 
	            // preview
	            try {
	                var reader = new FileReader();
	                reader.onload = function(e){
	                    img.src = e.target.result;
	                    img.style.width  = defaultOpt.w+'px';
	                    img.style.height = defaultOpt.h+'px';
	                    img.style.display = '';
	                }
	                reader.readAsDataURL(inputFile.files[0]);
	            } catch (e) {
	                // exception...
	            }
	        // img.filters (MSIE)
	        } else if (img.filters) {
	            inputFile.select();
	            inputFile.blur();
	            
	            var imgSrc = document.selection.createRange().text;
	 
	            img.style.width  = defaultOpt.w+'px';
	            img.style.height = defaultOpt.h+'px';
	            img.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(enable='true',sizingMethod='scale',src=\""+imgSrc+"\")";           
	            img.style.display = '';
	        // no support
	        //} else {
	            // Safari5, ...
	        }
        }else{
        	alert("사진파일을 업로드해주세요");
        }
	};
    
    var checkImg = function(imgName){
    	var pattern=/jpg|gif|png|jpeg/i; 
		return pattern.test(imgName);	
    }
 
    // onchange
    $(this).change(function(){
        previewImage();
    });
};
		 
		 
$(document).ready(function(){
    var opt = {
        img: $('#prove_img'),
        h: 240
    };
 
    $('#provePath_file').setPreview(opt);
    
    var opt = {
    	img:$("#grade_img"),
    	h:240
    };
    
    $('#gradePath_file').setPreview(opt);
});
