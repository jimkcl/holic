<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<!-- HanJungIl - 작성 - 170610 ~ 170630 -->
<%@ page session="false" contentType="text/html; charset=UTF-8"%>
<html>
<head>
<title>Holic</title>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/modern-business.css">
<style type="text/css">
body { margin-top: -25px !important; }
iframe { border: 0; }
</style>
<script>
$(document).ready(function(){
	$('.carousel').carousel({ interval: 3000 });
});
</script>
</head>
<body>
	<jsp:include page="default/header.jsp" />
		<!-- Header Carousel -->
	    <header id="myCarousel" class="carousel slide">
	        <!-- Indicators -->
	        <ol class="carousel-indicators">
	            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
	            <li data-target="#myCarousel" data-slide-to="1"></li>
	            <li data-target="#myCarousel" data-slide-to="2"></li>
	        </ol>
	
	        <!-- Wrapper for slides -->
	        <div class="carousel-inner">
	            <div class="item active">
	                <div class="fill" style="background-image:url('/img/mainSlider/bg_170614.jpg');"></div>
	                <div class="carousel-caption">
	                    <!-- <h2>메인슬라이드</h2> -->
	                </div>
	            </div>
	            <div class="item">
	                <div class="fill" style="background-image:url('/img/mainSlider/teacher_list01.jpg');"></div>
	                <div class="carousel-caption">
	                    <!-- <h2>Caption 2</h2> -->
	                </div>
	            </div>
	            <div class="item">
	                <div class="fill" style="background-image:url('/img/mainSlider/event85_galaxyTabS3_06.png');"></div>
	                <div class="carousel-caption">
	                    <!-- <h2>Caption 3</h2> -->
	                </div>
	            </div>
	        </div>
	
	        <!-- Controls -->
	        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
	            <span class="icon-prev"></span>
	        </a>
	        <a class="right carousel-control" href="#myCarousel" data-slide="next">
	            <span class="icon-next"></span>
	        </a>
	    </header>
		<section id="main">
			<div class="mainSection">
				
			    <!-- Page Content -->
			    <div class="container">
					<div class="row">
						<div class="col-lg-12">
			                <h2 class="page-header">공지/문의</h2>
			            </div>
			            <div class="col-md-4 col-sm-6">
			                <a href="portfolio-item.html">
			                </a>
			            </div>
			            <c:if test="${ true }">
			            	<div style="display: inline-block;">
			            		<h1 style="margin: 60px 0;">등록된 공지가 없습니다.</h1>
			            	</div>
			            </c:if>
					</div>
					
					<!-- Portfolio Section -->
			        <div class="row">
			            <div class="col-lg-12">
			                <h2 class="page-header">최신 강의동영상</h2>
			            </div>
			            
			            <c:forEach items="${ videoLectureList }" var="list" varStatus="status">
			            	<div class="col-md-4 col-sm-6" style="margin-bottom: 20px;">
			            		<div style="background-color: #f5f5f5;">
					                <a href="portfolio-item.html">
					                    <!-- <img class="img-responsive img-portfolio img-hover" src="http://placehold.it/700x450" alt=""> -->
					                    <iframe class="img-responsive img-portfolio img-hover" 
					                    	width="700px" height="450px" src="${ list.VContent }"
					                    	style="margin-bottom: 15px;"></iframe>
					                </a>
					                
									<div style="padding: 10px;">
						                <h4 style="margin-top: 0;">
						                	<a href="/videoLecture/videoLectureDetail/${ list.VIdx }">${ list.VTitle }</a>
						                </h4>
					                    
					                    <a href="/videoLecture/videoLectureList?type=vPackage&inputText=${ list.VPackage }">${ list.VPackage }</a>
										
					                    <div style="text-align: right; float: right;">
											<p style="font-size: 30px; margin: 0 0 -10px 0;">${ list.VScore / 10 }</p>
						                    <c:forEach begin="1" end="${ list.VScore / 10 % 5 }">
												<span style="font-size: 20px; color: #FFCB10;">★</span>
											</c:forEach>
											<c:forEach begin="${ list.VScore / 10 % 5 + 1 }" end="5">
												<span style="font-size: 20px; color: #FFCB10;">☆</span>
											</c:forEach>
					                    </div>
					                    
										<div style="font-size: 11px; margin-top: 25px;">
											<fmt:parseDate var="data" value="${ fn:substring(list.VDate,0,19) }" pattern="yyyy-MM-dd HH:mm:ss"/>
											<fmt:formatDate value="${ data }" pattern="yyyy년 MM월 dd일 hh시 mm분 ss초"/>
					                    </div>
				                    </div>
			            		</div>
				            </div>
				            
				            <c:if test="${ status.count == 3 }">
				            	<div style="display: table; width: 100%;"></div>
				            </c:if>
			            </c:forEach>
			            <c:if test="${ fn:length(videoLectureList) == 0 }">
			            	<div style="display: inline-block;">
			            		<h1 style="margin: 60px 0;">등록된 강의 영상이 없습니다.</h1>
			            	</div>
			            </c:if>
			        </div>
			        <!-- /.row -->
					
			        <!-- Marketing Icons Section -->
			        <!--
			        <div class="row">
			            <div class="col-lg-12">
			                <h1 class="page-header">최신 스터디</h1>
			            </div>
			            <div class="col-md-4">
			                <div class="panel panel-default">
			                    <div class="panel-heading">
			                        <h4><i class="fa fa-fw fa-check"></i> Bootstrap v3.3.7</h4>
			                    </div>
			                    <div class="panel-body">
			                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, optio corporis quae nulla aspernatur in alias at numquam rerum ea excepturi expedita tenetur assumenda voluptatibus eveniet incidunt dicta nostrum quod?</p>
			                        <a href="#" class="btn btn-default">Learn More</a>
			                    </div>
			                </div>
			            </div>
			            <div class="col-md-4">
			                <div class="panel panel-default">
			                    <div class="panel-heading">
			                        <h4><i class="fa fa-fw fa-gift"></i> Free &amp; Open Source</h4>
			                    </div>
			                    <div class="panel-body">
			                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, optio corporis quae nulla aspernatur in alias at numquam rerum ea excepturi expedita tenetur assumenda voluptatibus eveniet incidunt dicta nostrum quod?</p>
			                        <a href="#" class="btn btn-default">Learn More</a>
			                    </div>
			                </div>
			            </div>
			            <div class="col-md-4">
			                <div class="panel panel-default">
			                    <div class="panel-heading">
			                        <h4><i class="fa fa-fw fa-compass"></i> Easy to Use</h4>
			                    </div>
			                    <div class="panel-body">
			                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, optio corporis quae nulla aspernatur in alias at numquam rerum ea excepturi expedita tenetur assumenda voluptatibus eveniet incidunt dicta nostrum quod?</p>
			                        <a href="#" class="btn btn-default">Learn More</a>
			                    </div>
			                </div>
			            </div>
			        </div>
			        -->
			        <!-- /.row -->
			
			        <!-- Features Section -->
			        <div class="row">
			            <div class="col-lg-12">
			                <h2 class="page-header">Holic의 추천 강의</h2>
			            </div>
			            <div class="col-md-6">
			                <p>Holic이 자신있게 추천하는 최고의 한국어 강의!</p>
			                <ul>
			                	<li>수강평점 : ${ videoLectureRecommendation.VScore / 10 } 점</li>
			                    <li>평점 수 : ${ videoLectureRecommendation.VScoreCount }</li>
			                    <li>제목 : <a href="/videoLecture/videoLectureDetail/${ videoLectureRecommendation.VIdx }">
			                    		<strong>${ videoLectureRecommendation.VTitle }</strong>
			                    </a></li>
			                    <li>분류 : <a href="/videoLecture/videoLectureList?type=vPackage&inputText=${ videoLectureRecommendation.VPackage }">
			                    	${ videoLectureRecommendation.VPackage }
			                    </a></li>
			                    <li>등록일시 : 
			                    	<fmt:parseDate var="data" value="${ fn:substring(videoLectureRecommendation.VDate,0,19) }" pattern="yyyy-MM-dd HH:mm:ss"/>
									<fmt:formatDate value="${ data }" pattern="yyyy년 MM월 dd일 hh시 mm분 ss초"/>
			                    </li>
			                </ul>
			                <br/>
			                <p>많은 사람들이 한국어를 배우기 위해 한국을 찾아 옵니다.<br/>
			                	하지만 그 중 한국어를 원어민 수준으로 사용할 수 있게 되는 사람은 많지 않습니다.<br/>
			                	그렇다면 도대체 어떻게 하면 한국어를 한국사람만큼 잘 쓸 수 있을까요?<br/>
			                	정답은 여기에 있습니다.<br/><br/>
			                	<strong>Holic.</strong><br/>한국어가 무섭지 않습니다.</p>
			            </div>
			            <div class="col-md-6">
			                <!-- <img class="img-responsive" src="http://placehold.it/700x450" alt=""> -->
			                <iframe class="img-responsive img-portfolio img-hover" src="${ videoLectureRecommendation.VContent }"
					                    	style="width: 700px; height: 315px; margin-bottom: 15px;"></iframe>
			            </div>
			        </div>
			        <!-- /.row -->
			
			        <hr>
			
			        <!-- Call to Action Section -->
			        <div class="well">
			            <div class="row">
			                <div class="col-md-8">
			                    <p>한국어를 배울 수 있는 가장 쉬운 방법!<br/>
			                    	Holic에서 한국어 강의 동영상을 보고 한국인을 만나며 배워 보세요!</p>
			                </div>
			                <div class="col-md-4">
			                    <a class="btn btn-lg btn-default btn-block" href="${pageContext.request.contextPath}/payment/paymentMain">결제하기</a>
			                </div>
			            </div>
			        </div>
				</div>
		        <hr>
		    </div>
			
		</section>
	<jsp:include page="default/footer.jsp" />
</body>
</html>