<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- 로그인 되지 않은 회원만 이용해야하는 경우 -->
<c:if test="${ sessionScope.LOGIN != null }">
	<script type="text/javascript">
		alert("이용하실 수 없습니다.");
		location.href='${pageContext.request.contextPath}/';
	</script>
</c:if>