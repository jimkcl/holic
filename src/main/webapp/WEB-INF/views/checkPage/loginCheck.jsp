<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- 로그인 회원만 이용해야 하는 경우 -->
<c:if test="${ sessionScope.LOGIN == null 
		|| sessionScope.LOGIN.userKey == null || sessionScope.LOGIN.userKey == '' 
		|| sessionScope.LOGIN.userNick == null || sessionScope.LOGIN.userNick == '' 
		|| sessionScope.LOGIN.userType == null || sessionScope.LOGIN.userType == '' }">
	<script type="text/javascript">
		alert("로그인 후 이용해 주시기 바랍니다.");
		location.href='${pageContext.request.contextPath}/user/login';
	</script>
</c:if>
<c:if test="${ sessionScope.LOGIN.userType < 1 }">
	<script type="text/javascript">
		alert("사용이 제한된 회원입니다.");
		location.href='${pageContext.request.contextPath}/';
	</script>
</c:if>