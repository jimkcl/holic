<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<body>
	<jsp:include page="loginCheck.jsp" />
	<c:if test="${ sessionScope.LOGIN.userType < 9 }">
		<script type="text/javascript">
			alert("이용하실 수 없습니다!");
			location.href='${pageContext.request.contextPath}/';
		</script>
	</c:if>
</body>
</html>