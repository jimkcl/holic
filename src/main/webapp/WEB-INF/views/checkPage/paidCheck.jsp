<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<body>
	<jsp:include page="loginCheck.jsp" />
	<c:if test="${ sessionScope.LOGIN.userType < 5 }">
		<script type="text/javascript">
			alert("유료회원만 이용가능합니다.");
			history.go(-1);
		</script>
	</c:if>
</body>
</html>