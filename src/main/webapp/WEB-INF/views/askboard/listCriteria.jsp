<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- el문법 쓸 때 꼭 써줘야함 -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<title>Holic - 문의게시판</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
.askHeaderNav h2{
	font-family: NANUMSQUAREER;
	font-weight: bold; 
	display: inline-block; 
}
.askHeaderNav p { 
	float: right;
	font-size: 12px;
	margin: 40px 10px 0 0; 
}

.container{
	margin-top: 20px;
}
#search_form{
	width: 50%;
	margin: auto;
}
.tt:hover{
	color:blue;
}

.table thead tr {
	background-color: black; 
	color: white;
}
.table th, 
.table td {
	text-align: center;
}
.table tbody tr:LAST-CHILD { 
	border-bottom: 1px solid black; 
}
.table tbody tr:NTH-CHILD(2n-1) {
	background-color: #F5F5F5;
}
</style>
</head>
<body>
​	<jsp:include page="../default/header.jsp" />
	<section id="main">
		<div class="askHeaderNav">
			<h2>공지/문의 게시판</h2>
			<p>※ 문의글을 남길 수 있습니다.</p>
		</div>
		
		<hr style="margin: 0 0 20px 0;"/>
		
		<div class="container">
			<div class="row">
				<div class="btn-group btn-group-justified" style="margin-bottom: 40px;">
					<a href="../noticeboard/listCriteria" class="btn btn-default">공지사항</a>
					<a href="../askboard/listCriteria" class="btn btn-default" style="background-color: rgb(180, 180, 180);">문의게시판</a>
				</div>
	
				<table class="table">
					<thead>
						<tr style="background-color: black; color: white; width: auto; text-align: center; padding: 20px 15px;">
							<!-- <th>NO</th> -->
							<th style="width: 55%;">제목</th>
							<th>작성자</th>
							<th>작성일자</th>
							<th>조회수</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${list}" var="AskBoardVO" varStatus="idx">
							<tr>
								<%-- <td>${idx.count}</td> --%>
								<%-- <td id="tt">${AskBoardVO.aTitle}</td> --%>
								<td style="text-align: left;">
									<input class="user_aVisibility" type="hidden" value="${AskBoardVO.AVisibility}">
									<input class="userIdx" type="hidden" value="${AskBoardVO.idx}">
									<input type="hidden" class="userKey" value="${AskBoardVO.userKey}">
									<a class="tt">${AskBoardVO.ATitle}</a>
								</td>
								<td>${AskBoardVO.userNick}</td>
								<td>
									<fmt:parseDate var="data" value="${ fn:substring(AskBoardVO.ADate,0,19) }" pattern="yyyy-MM-dd HH:mm:ss"/>
									<fmt:formatDate value="${ data }" pattern="yyyy년 MM월 dd일 HH시 mm분 ss초"/>
								</td>
								<td>${AskBoardVO.ACount}</td>
							</tr>
						</c:forEach>
						<c:if test="${ fn:length(list) == 0 }">
							<tr style="background-color: rgba(0,0,0,0);">
								<td colspan="5">
									<h1 style="margin: 60px 0;">문의사항이 없습니다.</h1>
								</td>
							</tr>
						</c:if>
					</tbody>
				</table>
				<div align="right" class="row">
					<a class="btn btn-default" href="/askboard/create${pageMaker.makeQuery(pageMaker.cri.page)}">글쓰기</a>
				</div>
	
				<div id="search_form" align="center" class="input-group">
					<span id="sspan" class="input-group-addon"> <!-- input-group을 통해 한번에 묶음 -->
						<select id="ssel" name="searchType">
							<option disabled>선택</option>
							<option ${scri.searchType=='userNick'?'selected':''} value="userNick">닉네임</option>
							<option ${scri.searchType=='ATitle'?'selected':''} value="aTitle">제목</option>
							<option ${scri.searchType=='AContent'?'selected':''} value="aContent">내용</option>
						</select>
					</span>
					
					<input class="form-control" value="${scri.keyword}" id="keyword" name="keyword">
					<span class="input-group-btn">
						<button class="btn btn-default searchBtn">검색</button>
					</span>
				</div>
				
			</div>
				
			<div class="row" align="center">
				<ul class="pagination">
					<c:if test="${pageMaker.cri.page>1}">
						<li><a
							href="listCriteria${pageMaker.makeQuery(pageMaker.cri.page-1)}">&laquo;</a></li>
					</c:if>
	
					<c:forEach var="idx" end="${pageMaker.endPageNum}"
						begin="${pageMaker.startPageNum}">
						<li
							<c:out value="${pageMaker.cri.page==idx?'class=active' : ''}" />><a
							href="listCriteria${pageMaker.makeQuery(idx)}">${idx}</a></li>
						<!-- 페이지번호 -->
					</c:forEach>
	
					<c:if test="${pageMaker.cri.page<pageMaker.totalPage}">
						<li><a
							href="listCriteria${pageMaker.makeQuery(pageMaker.cri.page+1)}">&raquo;</a></li>
					</c:if>
	
				</ul>
			</div>
	
		</div>
	</section>
	<jsp:include page="../default/footer.jsp" />
	
	<script type="text/javascript">
		$(document).ready(function(){
			var result1='${msg}';
			var result2='${param.msg}';
			
			if(result1=='SUCCESS'){
				alert("작업이 정상적으로 완료되었습니다.")
			} else if(result1=="DELETE_SUCCESS" || result2=="DELETE_SUCCESS"){
				alert("삭제되었습니다.")
			} 
			$(".searchBtn").click(function(){
				var uri="../saskboard/listCriteria${pageMaker.makeQuery(1)}"+"&searchType="+$("select#ssel option:selected").val()+"&keyword="+$("#keyword").val();
				uri=encodeURI(uri);
				self.location=uri;
			});
		});
		
		//접속한 유저의 닉네임이 글을 작성한 사람과 동일할 경우 글을 읽을 수 있다.
		$(".tt").on("click",function(event) {
			var userKey = $(this).prevAll(".userKey").val();
			var userIdx = $(this).prevAll(".userIdx").val();
			var user_aVisibility = $(this).prevAll(".user_aVisibility").val();
			if(user_aVisibility == 1){
				 if('${sessionScope.LOGIN.userKey}' == userKey) { //로그인한 유저키와 문의글 유저키와 같을 때
					self.location="/askboard/read${pageMaker.makeQuery(pageMaker.cri.page)}&idx="+userIdx;
				} else if('${sessionScope.LOGIN.userType}' >= 8) { //로그인한 유저키의 공지타입이 운영자 이상일 경우 
					self.location="/askboard/read${pageMaker.makeQuery(pageMaker.cri.page)}&idx="+userIdx;
				} else {
					alert("비밀글 입니다.");
				}
			}else{
				self.location="/askboard/read${pageMaker.makeQuery(pageMaker.cri.page)}&idx="+userIdx;
			}
		});
	</script>
</body>
</html>