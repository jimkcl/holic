<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Holic - 문의글 작성</title>
<style type="text/css">
.askHeaderNav h2{
	font-family: NANUMSQUAREER;
	font-weight: bold; 
	display: inline-block; 
}
.askHeaderNav p { 
	float: right;
	font-size: 12px;
	margin: 40px 10px 0 0; 
}
</style>
</head>
<body>
	<jsp:include page="../checkPage/loginCheck.jsp" />
	<jsp:include page="../default/header.jsp" />
	<section id="main">
		<div class="askHeaderNav">
			<h2>공지/문의 게시판</h2>
			<p>※ 문의글 작성합니다.</p>
		</div>
		
		<hr style="margin: 0 0 20px 0;"/>
		
		<div class="container">
			<div class="row">
				<h3 style="margin-top: 0; font-family: NANUMSQUAREER; font-weight: bold; ">문의 등록</h3>
				
				<form id="askform" method="post">
					<input value="${cri.page}" name="page" type="hidden">
					<input value="${cri.perPageNum}" name="perPageNum" type="hidden">
					
					<div class="form-group" style="width: 73%; display: inline-block;">
						<label for="aTitle">제목</label>
						<input name="aTitle" id="aTitle" class="form-control" placeholder="제목">
					</div>
					
					<div class="form-group" style="width: 25%; display: inline-block; float: right;">
						<label for="userNick">닉네임</label>
						<input value="${sessionScope.LOGIN.userNick}" readonly="readonly" name="userNick" id="userNick" class="form-control">
					</div>
					
					<div class="form-group">	
						<label for="aContent">내용</label>
						<textarea name="aContent" id="aContent" class="form-control" placeholder="문의내용을 작성해 주세요."
							style="height: 400px; resize: none;"></textarea>
					</div>
					
					<div class="form-group" style="text-align: center;"> 
						<label style="font-size: 15px;">
							<input type="checkbox" name="aVisibility" value="1" 
								style="width: 20px; height: 20px; margin-right: 10px;">비밀글
						</label>
					</div>	
				</form>
				
				<div class="form-group" style="text-align: center;">	
					<button class="btn btn1" type="submit" style="padding: 6px 40px;">등록</button>
					<button class="btn btn2" type="reset" style="margin-left: 10px; padding: 6px 40px;">취소</button>
				</div>
			</div>
		</div>
	</section>
	<jsp:include page="../default/footer.jsp" />
	
	<script type="text/javascript">
		$(document).ready(function(){
			
			$(".btn1").on("click", function(){
				
				var aTitle = $("#aTitle");
				var aContent = $("#aContent");
				var userNick = $("#userNick");
				
				if(userNick.val()=="") { 
					alert("로그인을 해야 이용이 가능합니다");
					return false;
				} else if($("#aTitle").val()==""){
					alert("제목을 입력하세요");
					$("#aTitle").focus();
					return false;
				} else if($("#aContent").val()==""){
					alert("내용를 입력하세요");
					$("#aContent").focus();
					return false;
				}else if(aTitle.val().length>30) {
					alert("30자 내로 작성하세요.");
					aTitle.focus();
					return false;
				} else if (aContent.val().length>255) {
					alert("255자 내로 작성하세요");
					aContent.focus();
					return false;
				} else {
					$("form").attr("action", "create");
					$("form").attr("method", "post");
					$("form").submit();
				}
			});
				
			$(".btn2").click(function(){
				self.location="/askboard/listCriteria";
			});
		});
	</script>
</body>
</html>