<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<title>Holic - ${vo.ATitle}</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<style type="text/css">
.askHeaderNav h2{
	font-family: NANUMSQUAREER;
	font-weight: bold; 
	display: inline-block; 
}
.askHeaderNav p { 
	float: right;
	font-size: 12px;
	margin: 40px 10px 0 0; 
}

.container {
	margin: 50px;
}

.board_view_aTitle {
	font-size: 18px;
	color: white;
	background-color: black;
	padding: 20px;
}

.board_view_userNick {
	background-color: #f5f5f5;
	border-bottom: 1px solid grey;
}
.board_view_userNick p { margin: 10px; }

.board_view_aContent {
	font-size: 14px;
	padding: 40px 20px;
	border-bottom: 1px solid grey;
}

.reply_cmt { 
	background-color: black;
	padding: 10px; 
}
.reply_one { margin: 10px; }

.readReply {
	font-family: NANUMSQUAREER;
	font-weight: bold;
	color: white; 
	display: inline-block;
	margin-top: 10px;
}
.replyDiv {
	color: white;
	border-bottom: 1px solid;
}


.board_view_btn {
	text-align: center;
}
.board_view_btn a { padding: 6px 40px; }

table tr td:FIRST-CHILD {
	width: 100%;
}

.textarea {
	width: 100%;
	height: 100px;
	margin: 0;
	resize: none;
}

.reply_btn {
	background-color: black;
	color: white;
	float: right;
	width: 100px;
	height: 100px;
	margin-top: -6px;
	padding: 0;
	border: 1px solid white;
	border-left: 0;
}

.reply_form {
	width: 98%;
	margin: auto;
	margin: 20px;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
	var $form=$("form[role='form']");
	var board_userKey = $("#board_userKey").val();
	var userKey = $("#userKey").val();
	$(".btn_delete").on("click", function(){
		
		if(board_userKey == userKey){
			if(confirm("삭제하시겠습니까?")){
				$form.attr("method", "post");
				$form.attr("action", "/askboard/delete");
				$form.submit();
			}
		}else{
			alert("본인만이 자신의계시글을 삭제할수 있습니다.")
		}
	});
	
	$(".btn_list").click(function(){
		$form.attr("method", "get");
		$form.attr("action", "/askboard/listCriteria");
		$form.submit();
	});
	
	$(".reply_btn").click(function(){
		/* if(${sessionScope.Login.userNick == null || sessionScope.Login.userNick == ""}) {
			alert("로그인을 해야 작성이 가능합니다.");
			return false;
		} else {
			return ;
		} */
		
		var rContent = $("#rContent").val();
		var bIdx = $("input[name=bIdx]").val();
		
		if($("#rContent").val()=="" || $("#rContent").val() == null) {
			alert("댓글을 입력해주세요.");
			$("#rContent").focus();
			return false;
		}
	
		if(${sessionScope.LOGIN.userKey != null}){
			$.ajax({
				type: "post",
				url: "/reply/create",
				dataType: "text",
				data: {
					"rContent": rContent,
					"bIdx": bIdx
					
				},
				success: function(result) {
					if(result == "INSERT_SUCCESS") {
						alert("댓글을 등록했습니다.");
						$("#rContent").val("");
						location.reload();
					}
				},
				error: function(data, result){
					alert("댓글등록을 실패했습니다.");
					console.log('result : ' + result);
				}
				
			});
		}else{
			alert("회원만이 댓글을 등록할수있습니다");
		}
		
		
	});
	
	$(".close").click(function() {
		var bIdx = ${vo.idx};
		var idx = $(this).prev().val();
		
		$.ajax({
			type: "post",
			url: "../reply/delete",
			dataType: "text",
			data:{
				"idx" : idx,
				"bIdx" : bIdx
			},
			success: function(result){
				if(result == "DELETE_SUCCESS"){
					location.reload();
					alert("댓글이 삭제되었습니다.");
				}
			},
			error: function(data){
				alert("댓글삭제를 실패했습니다.");
			}
		});
	});
	
	/* $("#rContent").click(function(){
		alert("로그인이 필요한 서비스입니다. 로그인 후 사용 가능합니다.");
	}); */
});
</script>
</head>
<body>
	<c:if test="${ (vo.AVisibility == 1 && vo.userKey != sessionScope.LOGIN.userKey) }">
		<c:if test="${ sessionScope.LOGIN.userType == null 
					|| sessionScope.LOGIN.userType == '' 
					|| sessionScope.LOGIN.userType < 8 }">
			<script type="text/javascript">
				alert('작성자만 볼 수 있습니다.');
			</script>
		</c:if>
		history.go(-1);
	</c:if>
	
	<jsp:include page="../default/header.jsp" />
	
	<%-- <!-- 로그인한 유저 닉네임과 글을 쓴 작성자와 같으면.. -->
	<c:if test="${sessionScope.LOGIN.userNick == vo.userNick}">
		<script type="text/javascript">
			location.href="/";
		</script>
	</c:if>  --%>
	
	<section id="main">
		<div class="askHeaderNav">
			<h2>문의 게시판</h2>
			<p>※ 문의글을 남길 수 있습니다.</p>
		</div>
		
		<input type="hidden" value="${sessionScope.LOGIN.userKey}" id="board_userKey">
		<input type="hidden" value="${vo.userKey}" id="userKey">
		
		<form role="form" method="post">
			<input value="${vo.idx}" name="idx" type="hidden">
			<input value="${cri.page}" name="page" type="hidden">
			<input value="${cri.perPageNum}" name="perPageNum" type="hidden">
		</form>
		
		<div class="container">
			<div class="row">
				<div class="form-group">
					<div class="board_view_aTitle">${vo.ATitle}</div>
						<div class="board_view_userNick">
							<p style="display: inline-block;">작성자 : ${vo.userNick}</p>
							<p style="float: right; font-size: 12px;">${vo.ADate}</p>
						</div>
					<div class="board_view_aContent">${vo.AContent}</div>
				</div>
			</div>
			
			<div class="form-group">
				<div class="board_view_btn">
					<c:if test="${ vo.userKey == sessionScope.LOGIN.userKey }">
						<a class="btn btn_delete" style="margin-right: 5px;">삭제</a>
					</c:if>
					<a class="btn btn_list">목록</a>
				</div>
			</div>
			
			<hr/>
			
			<div class="reply_cmt">
				<!-- 댓글 작성창 -->
				<div class="readReply">
					<h3>댓글 작성</h3>
				</div>
				
				<hr style="margin: 0;"/>
				
				<c:if test="${ sessionScope.LOGIN.userKey != null && sessionScope.LOGIN.userKey != '' }">
					<div class="reply_form">
						<div class="row">
							<table style="width: 98%;">
								<tr>
									<td>
										<input name="bIdx" value="${vo.idx}" type="hidden">
										<textarea id="rContent" class="textarea" placeholder="※ 욕설, 도배, 비방, 루머 등 운영정책에 어긋나는 게시물 등록 시에는 글쓰기 제한 등 불이익을 받으실 수 있습니다."></textarea>
									</td>
									<td><input type="button" class="reply_btn" value="댓글쓰기"></td>
								</tr>
							</table>
						</div>
					</div>
				</c:if>
				
				<!-- 댓글리스트 -->
				<div class="readReply">
					<h3>댓글</h3>
				</div>
				
				<hr style="margin: 0;"/>
				
				<c:forEach items="${ vo.list }" var="list">
					<div class="reply_one">
						<!--  collapse 기능안보이게.. -->
						<div class="replyDiv" id="closewrap">
							<div class="panel-heading" style="padding: 10px 15px 0 15px;">
								<span style="font-size: 20px;">${ list.userNick }</span>
								
								<c:if test="${ vo.userKey == sessionScope.LOGIN.userKey || sessionScope.LOGIN.userType >= 8 }">
									<input name="idx" value="${list.idx}" type="hidden">
									<button type="button" class="close" data-target="#closewrap" data-dismiss="alert" style="color: white; margin-left: 10px; border: 1px solid white; padding: 0 5px;">
										<span aria-hidden="true">&times;</span>
										<span class="sr-only">Close</span>
									</button>
								</c:if>
								
								<!-- ${vo.userNick}으로 받으면 안됨. userNick == 로그인한 userNick 같은지 동등검사 -->
								<span class="pull-right" style="font-size: 12px; margin-top: 5px;">
									<fmt:parseDate var="data" value="${ fn:substring(list.RDate,0,19) }" pattern="yyyy-MM-dd HH:mm:ss"/>
									<fmt:formatDate value="${ data }" pattern="yyyy년 MM월 dd일 HH시 mm분 ss초"/>
								</span>
							</div>
							<div class="panel-body">
								<p>${ list.RContent }</p>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>
			
			<div class="board_view_btn" style="margin-top: 20px;">
				<a class=" btn btn_list">목록</a>
			</div>
		</div>
	</section>
	<jsp:include page="../default/footer.jsp" />
</body>
</html>