<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<!-- HanJungIl - 작성 - 170610 ~ 170630 -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Holic - ${ videoLectureDetail.VTitle }</title>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bs/css/bootstrap.css">
<style type="text/css">
#videoLectureFrom { padding: 0; }
.videoLectureFormMessage {
	color: red;
	font-size: 11px;
	font-weight: bold;
	margin: 20px 0 !important;
}

.videoLectureNav h2 { 
	font-family: NANUMSQUAREER;
	font-weight: bold; 
	display: inline-block;
	margin-top: 0;
}
.videoLectureNav .videoLectureAdmin { float: right; }
.videoLectureContent { 
	width: 100%; 
	padding: 10px;
}
.videoLectureContent table { width: 100%; }
.videoLectureContent table tbody tr:FIRST-CHILD { 
	overflow: hidden; 
	height: 640px;
}
.videoLectureContent table tbody tr:FIRST-CHILD td { 
	height: 460px;
}
.videoLectureContent table tr:NTH-CHILD(2n-1) td {
	background-color: black; 
	color: white;
} 
.videoLectureContent table tr:FIRST-CHILD td:FIRST-CHILD { 
	width: 75%;
}
.videoLectureContent table tr:FIRST-CHILD td:LAST-CHILD { 
	width: 25%;
	border-left: 2px dashed white;
}
.videoLectureContent table .videoLectureTeacherInfo { 
	padding: 20px;
}
.videoLectureListDiv { 
	overflow: auto;
	height: 100%; 
}
iframe { 
	width: 100%;
	height: 100%; 
	border: 0 solid;
}
.videoLectureTeacherNav h3 { 
	font-family: NANUMSQUAREER;
	font-weight: bold; 
	display: inline-block;
	margin-top: 10px;
}
.videoLectureTeacherNav h6 { 
	float: right;
	margin: 25px 0 0 0; 
}
.videoLectureTeacherNav > p {
	font-size: 12px; 
	color: white;
	float: right; 
	margin: 25px 0 0 0;
}
.videoLectureTeacherNav input {
	float: right;
	margin-left: 10px;
	margin-top: 5px;
}
.videoLectureTeacherDiv {
	padding: 0 10px;
}
.videoLectureURLList { 
	width: 100%;
	padding: 5px 5px 0 5px;
}
.videoLectureURLList input {
	font-family: NANUMSQUAREER; 
	font-weight: bold;
	background-color: gray;
	color: white;
	width: 100%; 
	padding: 10px;
	border: 0 solid;
}
.videoLectureURLList input:HOVER { 
	background-color: rgb(230,230,230);
	color: black;
	cursor: pointer;
}
.videoLectureURLList > .videoLectureBtnDiv { 
	width: 100%;
}
.videoLectureURL, 
.videoLectureURLUpdate,
.videoLectureURLDelete {
	width: 100%;
	display: inline-block;
	padding: 10px;
	background-color: gray;
	color: white;
}
.videoLectureURL:HOVER, 
.videoLectureURLUpdate:HOVER,
.videoLectureURLDelete:HOVER { 
	color: black;
	background-color: rgb(230,230,230); 
}
.videoLectureURLUpdate, 
.videoLectureURLDelete {
	width: 10%;
	float: right;
}
.videoLectureAddDiv {
	padding: 5px;
	background-color: rgb(170,170,170);
}
.videoLectureAddDiv > input { background-color: black; }
.addURLDiv,
.addURLBtnDiv { 
	text-align: center; 
}
.videoLectureAddDiv .btn { 
	background-color: black;
	width: 49%; 
	margin-top: 5px;
}
.videoLectureAddDiv > .addURLDiv > input:NTH-CHILD(1), 
.videoLectureAddDiv > .addURLDiv > input:NTH-CHILD(2) {
	cursor: text;
	margin-bottom: 1px;
	color: black;
	background-color: white;
}
.videoLectureAddDiv > .addURLDiv > input:NTH-CHILD(3) { width: 13px; }
.addURLBtnDiv > p {
	font-size: 12px; 
	margin: 0 0 10px 0;
}
.videoLectureURLList form {
	background-color: gray;
	padding: 5px;
}
form .btn {
	background-color: black;
	width: 49%;
	margin-top: 5px;
}
.videoLectureURLList form .updateInput {
	cursor: auto;
	background-color: white; 
	color: black;
}
.videoLectureTeacherContent { float: left; }
.videoLectureTeacherScoreDiv { 
	float: right;
	text-align: center; 
}
.videoLectureTeacherScoreDiv h2 { 
	font-family: NANUMSQUAREER;
	font-size: 100px; 
	font-weight: bold;
	margin-top: -30px;
	margin-bottom: 0;
}
.videoLectureStar { 
	font-size: 20px;
	color: yellow; 
}
.videoLectureTeacherContentP { 
	width: 100%; 
	display: inline-block;
}
.teacherImg {
	text-align: center;
	width: 200px;
	display: inline-block;
}
.teacherInfo {
    text-align: left;
	width: 83%;
	display: inline-block;
}
.teacherInfo > hr { margin: 10px 0;}

.videoLectureRecommendDiv {
    overflow: hidden;
	width: 289px;
    margin-right: 10px;
    margin-bottom: 20px;
    border: 2px solid black;
}
.videoLectureRecommendDiv:LAST-CHILD { margin-right: 0; }
.videoLectureRecommendDiv > div { padding: 0 20px 20px 20px; }
.videoLectureRecommendDiv h2 {
	font-family: NANUMSQUAREER;
	font-weight: bold; 
	font-size: 20px;
}
.videoLectureRecommendDiv textarea {
	overflow: hidden;
	resize: none;
	border: 0;
}

/* 리플 */
.videoLectureTeacherReply { margin-bottom: 20px; }
.videoLectureTeacherAddReply textarea {
	color: black;
	width: 100%; 
	height: 150px; 
	padding: 10px; 
	resize: none;
}
.videoLectureTeacherAddReply .btn {
	color: black; 
	background-color: white;
	float: right; 
}
.videoLectureTeacherAddReplyScore { display: inline-block; }
.videoLectureTeacherAddReplyScore > span { cursor: pointer; }
.videoLectureReplyDiv { 
	padding: 15px;
	border-bottom: 1px solid white; 
}
.videoLectureReplyDiv h4 {
	font-family: NANUMSQUAREER;
	font-weight: bold;
	font-size: 20px;
	margin: 0;
}
.scoreStar {
	font-size: 20px; 
	color: #FFCB10;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
	/****** URL ******/
	// 영상 변경
	$(".videoLectureURL").click(function(){
		$("#videoLectureIFrame").attr("src", $(this).parent().children("input[type=hidden]").val());
		
		var vlURL = $(".videoLectureURL");
		
		for(i=0 ; vlURL.length > i ; ++i){
			vlURL.eq(i).css("background-color", "");
			vlURL.eq(i).css("color", "");
		}
		
		$(this).css("background-color", "rgb(200,200,200)");
		$(this).css("color", "black");
	});
	
	/****** URL 추가 ******/
	// 영상 URL 추가
	$("#videoLectureURLAddBtn").click(function(){
		var div = $(this).parent().children("#videoLectureFrom");
		
		$(this).attr("type", "hidden");
		
		div.prepend('<div class="videoLectureAddDiv">'
				+ '<div class="addURLDiv">'
					+ '<input type="text" name="listURL[' + $(".addURLDiv").length + '].vName" placeholder="영상제목" autocomplete="off">'
					+ '<input type="text" name="listURL[' + $(".addURLDiv").length + '].vURL" placeholder="URL" autocomplete="off">'
					+ '<input type="checkbox" class="checkeBox">'
					+ '<span style="font-size: 12px;">영상 ' + ($(".addURLDiv").length + 1) + ' 이(가) 잘 나옵니다.(재설정시 체크 해제)</span>'
					+ '<input type="button" value="확인" class="btn checkedVideo">'
					+ '<hr style="margin: 10px 0 5px 0;"/>'
				+ '</div>'
					+ '<div class="addURLBtnDiv">'
						+ '<input type="button" value="+" name="addURLBtn" class="btn addURLBtn"><br/>'
						+ '<p class="videoLectureFormMessage"></p>'
						+ '<p>※  하단의 등록을 누르면 완료됩니다. ※</p>'
					+ '</div>'
					+ '<hr style="margin: 0;"/>'
					+ '<input type="button" value="등록" id="videoLectureURLSubmit" style="margin-top: 10px;">'
					+ '<input type="button" value="취소" id="cancelBtn" style="margin-top: 5px;">'
				+ '</div>');
	});
	
	// URL 입력창 추가를 위한 설정
	var addURLBtnAdd = function(){
		var message = $(".addURLBtnDiv").children(".videoLectureFormMessage");
		var length = $(".videoLectureAddDiv").children(".addURLDiv").length;
		var maxLength = 10;
		
		if(length < maxLength){
			$(".addURLDiv").eq(length-1).after('<div class="addURLDiv">'
					+ '<input type="text" name="listURL[' + length + '].vName" placeholder="영상제목" autocomplete="off">'
					+ '<input type="text" name="listURL[' + length + '].vURL" placeholder="URL" autocomplete="off">'
					+ '<input type="checkbox" class="checkeBox">'
					+ '<span style="font-size: 12px;">영상 ' + (length + 1) + ' 이(가) 잘 나옵니다.(재설정시 체크 해제)</span>'
					+ '<input type="button" value="확인" class="btn checkedVideo">'
					+ '<input type="button" value="삭제" class="btn deleteVideoDiv" style="margin-left: 5px;">'
					+ '<hr style="margin: 10px 0 5px 0;"/>'
				+ '</div>');
			if(length + 1 == maxLength) {
				message.text("영상은 최대 10개까지 등록 가능합니다.");
				$(this).remove();
			}
			return;
		}
	}
	
	// URL 영상 확인용
	var checkedVideo = function(){
		var message = $(".addURLBtnDiv").children(".videoLectureFormMessage");
		var vURL = $(this).parent(".addURLDiv").children("input[placeholder=URL]");
		var iframe = $("#addIframe").children("iframe");
		
		if(vURL.val() == '') {
			message.text("동영상 주소를 입력해 주세요.");
			vURL.focus();
			return;
		}
		
		if(iframe != null){
			iframe.remove();
		}
		
		$("#videoLectureIFrame").attr("src", vURL.val());
		message.text("");
	}
	
	// 확인체크후
	var checkedBox = function(){
		var message = $(".addURLBtnDiv").children(".videoLectureFormMessage");
		var vName = $(this).parent(".addURLDiv").children("input[placeholder=영상제목]");
		var vURL = $(this).parent(".addURLDiv").children("input[placeholder=URL]");
		
		if(vName.val() == ''){
			message.text("동영상 제목을 입력해 주세요.");
			vName.focus();
			return;
		}else if(vURL.val() == ''){
			message.text("동영상 주소를 입력해 주세요.");
			vURL.focus();
			return;
		}else if($(this).is(":checked")){
			vURL.attr("readonly", true);
			vURL.css("backgroundColor", "rgb(235, 235, 228)");
			vName.attr("readonly", true);
			vName.css("backgroundColor", "rgb(235, 235, 228)");
			message.text("");
		}else{
			vURL.attr("readonly", false);
			vURL.css("backgroundColor", "");
			vName.attr("readonly", false);
			vName.css("backgroundColor", "");
			message.text("");
		}
	}
	
	// URL 등록창 제거
	$(".videoLectureBtnDiv").on("click", ".deleteVideoDiv", function(){
		$(this).parent(".addURLDiv").remove();
	});
	
	// URL 입력창 추가한 뒤 이벤트 설정
	$(".videoLectureBtnDiv").on("click", ".checkedVideo", checkedVideo);
	$(".videoLectureBtnDiv").on("click", ".checkeBox", checkedBox);
	$(".videoLectureBtnDiv").on("click", ".addURLBtn", addURLBtnAdd);
	
	// 등록 창 닫기
	$(".videoLectureURLList").on("click", "#cancelBtn", function(){
		$("#videoLectureURLAddBtn").attr("type", "button");
		$(".videoLectureAddDiv").remove();
	});
	
	// 등록 하기
	$(".videoLectureBtnDiv").on("click", "#videoLectureURLSubmit", function(){
		var message = $(".addURLBtnDiv").children(".videoLectureFormMessage");
		var addURLDiv = $(".addURLDiv");
		
		message.text(" ");
		
		for(i=0 ; i < addURLDiv.length ; ++i){
			var vName = addURLDiv.eq(i).children("input[placeholder=영상제목]");
			var vURL = addURLDiv.eq(i).children("input[placeholder=URL]");
			var checkeBox = addURLDiv.eq(i).children(".checkeBox");
			
			if(vName.val() == null || vName.val() == ''){
				message.text("동영상 제목을 입력해 주세요.");
				vName.focus();
				return;
			}else if(vURL.val() == null || vURL.val() == '' ){
				message.text("동영상 주소를 입력해 주세요.");
				vURL.focus();
				return;
			}else if(!checkeBox.is(":checked")){
				checkeBox.attr("readonly", true);
				message.text("동영상 확인을 체크해 주세요.");
				checkeBox.focus();
				return;
			}else{
				addURLDiv.append("<input type='hidden' name='listURL[" + i + "].vIdx' value='" + ${vIdx} + "'>");
			}
		}
		
		
		$.ajax({
			url		: "../../videoLecture/videoLectureAddURL",
			type	: "post",
			data	: $("#videoLectureFrom").serialize(),
			dataType: "text",
			success : function(data){
				alert(data);
				location.reload();
			},
			error	: function(xhr, status, error){
				console.log(xhr + " / " + status + " / " + error);
			}
		});
	});
	
	/****** URL 수정 ******/
	// URL 수정/삭제 버튼 초기화
	var aaaa = function () {
		var urlInputList = $(".videoLectureURL");
		
		alert(urlInputList.eq(i).css("width"));
		for(i=0 ; i < urlInputList.length ; ++i){
			if(urlInputList.eq(i).next(".videoLectureURLUpdate").text() != '◎'){
				urlInputList.eq(i).css("width", "90%");
				urlInputList.eq(i).after("<a class='videoLectureURLUpdate'>◎</a>");
			}else{
				urlInputList.eq(i).css("width", "100%");
				urlInputList.eq(i).next("a").remove();
			}
		}
	}
	
	// URL 수정 버튼 설정
	$("#videoLectureURLUpdateBtn").click(function(){
		var urlInputList = $(".videoLectureURL");
		
		for(i=0 ; i < urlInputList.length ; ++i){
			if(urlInputList.eq(i).next(".videoLectureURLDelete").text() == 'X'){
				urlInputList.eq(i).next("a").remove();
			}
			
			if(urlInputList.eq(i).next(".videoLectureURLUpdate").text() != '◎'){
				urlInputList.eq(i).css("width", "90%");
				urlInputList.eq(i).after("<a class='videoLectureURLUpdate'>◎</a>");
			}else{
				urlInputList.eq(i).css("width", "100%");
				urlInputList.eq(i).next("a").remove();
				$(".videoLectureURLList").children("form").remove();
			}
		}
	});
	
	// URL 수정용 DIV
	$(".videoLectureListDiv").on("click", ".videoLectureURLUpdate", function(){
		$(".videoLectureURLList").children("form").remove();
		
		var inputTitle = $(this).prev(".videoLectureURL");
		var vURLList = $(this).parent(".videoLectureURLList");
		
		vURLList.append("<form><input class='updateInput' type='text' name='vName' value='" + inputTitle.text() + "' style='margin-bottom: 1px;'>" 
				+ "<input class='updateInput' type='text' name='vURL' value='" + vURLList.children("input[name=vURL]").val() + "' style='margin-bottom: 10px;'>" 
				+ "<input type='hidden' name='idx' value='" + vURLList.children("input[name=idx]").val() + "' style='margin-bottom: 10px;'>"
				+ '<input type="button" value="확인" class="btn checkedUpdate" style="display: block; margin: auto;">' 
				+ '<hr style="margin: 10px 0;"/>'
				+ "<input type='button' class='btn videoLectureURLUpdateSubmit' value='수정' style='margin-right: 5px;'>" 
				+ "<input type='button' class='btn videoLectureURLUpdateCancel' value='취소'></form>");
	});
	
	// URL 수정시 영상 확인용 버튼
	$(".videoLectureListDiv").on("click", ".checkedUpdate", function(){
		$("#videoLectureIFrame").attr("src", $(this).parent("form").children("input[name=vURL]").val());
	});
	
	// URL 수정 DIV 닫기
	$(".videoLectureListDiv").on("click", ".videoLectureURLUpdateCancel", function(){
		$(this).parent("form").remove();
	});
	
	// URL 수정 전송
	$(".videoLectureListDiv").on("click", ".videoLectureURLUpdateSubmit", function(){
		if(!confirm($(this).parent("form").children("input[name=vName]").val() 
				+ " (을)를 수정하시겠습니까?")) {
			return;
		}
		
		$.ajax({
			url			: "../../videoLecture/videoLectureURLUpdate",
			data		: $(this).parent("form").serialize(),
			type		: "post",
			dataType	: "text",
			success : function(data){
				alert(data);
				location.reload();
			},
			error	: function(request, status, error){
				alert("실패하였습니다.\n다시 시도해 주시기 바랍니다.");
				console.log("code : " +  request.statusText  + "\r\nmessage : " + request.responseText);
			}
		});
	});
	
	/****** URL 삭제 ******/
	// URL 삭제 버튼 설정
	$("#videoLectureURLDeleteBtn").click(function(){
		var urlInputList = $(".videoLectureURL");
		
		for(i=0 ; i < urlInputList.length ; ++i){
			if(urlInputList.eq(i).next(".videoLectureURLUpdate").text() == '◎'){
				urlInputList.eq(i).next("a").remove();
			}
			
			if(urlInputList.eq(i).next(".videoLectureURLDelete").text() != 'X'){
				urlInputList.eq(i).css("width", "90%");
				urlInputList.eq(i).after("<a class='videoLectureURLDelete'>X</a>");
			}else{
				urlInputList.eq(i).css("width", "100%");
				urlInputList.eq(i).next("a").remove();
			}
		}
	});
	
	// URL 삭제
	$(".videoLectureURLList").on("click", ".videoLectureURLDelete", function(){
		if(confirm("이 영상을 삭제하시겠습니까?\n\n" + $(this).prev().text())){
			
			$.ajax({
				url			: "../../videoLecture/videoLectureURLDelete",
				data		: { "idx" : $(this).parent(".videoLectureURLList").children("input[name=idx]").val() },
				type		: "post",
				dataType	: "text",
				success : function(data, status){
					alert(data);
					location.reload();
				},
				error	: function(request, status, error){
					alert("실패하였습니다.\n다시 시도해 주시기 바랍니다.");
					console.log("code : " +  request.statusText  + "\r\nmessage : " + request.responseText);
				}
			});
		}
	});
	
	/****** 게시물 삭제  ******/
	// 게시물 삭제
	$(".videoLectureAdmin").children("input[value=삭제]").click(function(){
		if(confirm("정말 삭제하시겠습니까?")){
			location.href="../../videoLecture/videoLectureDelete/" + "${videoLectureDetail.VIdx}";
		}else{
			return;
		}
	});
	
	/****** 게시물 수정 ******/
	// 수정
	$(".videoLectureTeacherNav").children("input[value=수정]").click(function(){
		// 분류 가져오기
		$.ajax({
			url		: "../../videoLecture/videoLectureGetPackage",
			type	: "post",
			dataType: "json",
			async	: false,
			success : function(data){
				$("#vPackageSpanSelect").css("display", "inline-block");
				
				for(i=0 ; i < data.length ; ++i){
					$("#vPackageSpanSelect").append("<option>" + data[i] + "</option>");
				}
			},
			error	: function(request, status, error){
				 console.log("error : " + error + "\r\ncode : " +  request.statusText  + "\r\nmessage : " + request.responseText);
			}
		});
		
		var vTitleText = $(".vTitleSpan").text();
		var vPackageText = $(".vPackageSpan").text();
		
		$(".vTitleSpan").contents().unwrap().wrap('<input type="text" class="vTitleSpan" value="' + vTitleText + '">');
		
		$(".videoLectureTeacherNav").children("input[value=완료]").attr("type", "button");
		$(".videoLectureTeacherNav").children("input[value=수정]").attr("type", "hidden");
	});
	
	// 수정 완료
	$(".videoLectureTeacherNav").children("input[value=완료]").click(function(){
		$.ajax({
			url		: "../../videoLecture/videoLectureUpdate",
			type	: "post",
			data	: { "vIdx" : ${vIdx}, "vTitle" : $(".vTitleSpan").val(), "vPackage" : $("#vPackageSpanSelect").val() },
			dataType: "json",
			async: false,
			success : function(data, status){
				$("#vPackageSpanSelect").css("display", "none");
				
				alert(data);
				console.log("status:"+status+", "+"data:"+data);
			/*
			},
			error	: function(request, status, error){
				alert("실패하였습니다.\n다시 시도해 주시기 바랍니다.");
				console.log("error : " + error + "\r\ncode : " +  request.statusText  + "\r\nmessage : " + request.responseText);
			*/
			}
		});
		
		location.reload();
	});
	
	//****** 리플 ******//
	// 리플 별점
	$(".videoLectureTeacherAddReplyScore .scoreStar").click(function(){
		var scoreStarList = $(".videoLectureTeacherAddReplyScore").children(".scoreStar");
		
		// 별 초기화
		for(i=0; i < scoreStarList.length ;++i){
			scoreStarList.eq(i).text("☆");
			scoreStarList.eq(i).prev("input[name=vrScore]").prop("checked", false);
		}
		
		for(i=0; i < scoreStarList.length ;++i){
			scoreStarList.eq(i).text("★");
			
			if($(this).prev("input[name=vrScore]").val() == scoreStarList.eq(i).prev("input[name=vrScore]").val()){
				$(this).prev("input[name=vrScore]").prop("checked", true);
				return;
			}
		}
	});
	
	// 리플 전송
	$(".videoLectureTeacherAddReply input[type=button]").click(function(){
		if(${ sessionScope.LOGIN.userType == null || sessionScope.LOGIN.userKey == null }){
			if(confirm('로그인 후 이용가능합니다.\n\n로그인 하시겠습니까?')){
				location.href="../../user/login";
			}
			return;
		}else if(${ sessionScope.LOGIN.userType < 1 }){
			alert('이용하실 수 없습니다.');
			return;
		}
		
		var vrComment = $("textarea[name=vrComment]").val();
		var scoreStarList = $(".videoLectureTeacherAddReplyScore").children("input[name=vrScore]");
		var score;
		
		for(i=0; i < scoreStarList.length ;++i){
			if(scoreStarList.eq(i).prop("checked")) {
				score = scoreStarList.eq(i).val();
			}
		}
		
		if(score == ''){
			alert("점수를 입력해 주세요");
			return;
		}else if(vrComment.length < 6){
			alert("평가가 너무 짧습니다");
			return;
		}
		
		$.ajax({
			url		: "../../videoLecture/videoLectureAddReply",
			type	: "post",
			data	: { "vIdx" : $(".videoLectureTeacherAddReply input[name=vIdx]").val(), 
						"vrComment" : vrComment, 
						"vrScore" : score },
			dataType: "text",
			async: false,
			success : function(data, status){
				if(data == 0){
					alert("평가는 한 강의당 한 번만 가능합니다.");
				}else if(data == 1){
					alert("평가가 등록되었습니다.");
					location.reload();
				}
			},
			error	: function(request, status, error){
				alert("실패하였습니다.\n다시 시도해 주시기 바랍니다.");
				console.log("error : " + error + "\r\ncode : " +  request.statusText  + "\r\nmessage : " + request.responseText);
			}
		});
	});
	
	// 강의 평가 삭제
	$("input[name=videoLectureDeleteReplyBtn]").click(function(){
		if(confirm("강의평가를 삭제하시겠습니까?")){
			
			$.ajax({
				url		: "../../videoLecture/videoLectureDeleteReply",
				type	: "post",
				data	: { "vIdx" : $(".videoLectureTeacherAddReply input[name=vIdx]").val() },
				dataType: "text",
				async: false,
				success : function(data, status){
					if(data == 0){
						alert("실패하였습니다.\n다시 시도해 주시기 바랍니다.");
					}else if(data == 1){
						alert("삭제되었습니다.");
						location.reload();
					}
				},
				error	: function(request, status, error){
					alert("실패하였습니다.\n다시 시도해 주시기 바랍니다.");
					console.log("error : " + error + "\r\ncode : " +  request.statusText  + "\r\nmessage : " + request.responseText);
				}
			});
		}
	});
});
</script>
</head>
<body>
	<c:if test="${ videoLectureDetail.VType != 0 
			&& ( sessionScope.LOGIN == null 
			|| sessionScope.LOGIN.userKey == null || sessionScope.LOGIN.userKey == '' 
			|| sessionScope.LOGIN.userNick == null || sessionScope.LOGIN.userNick == '' 
			|| sessionScope.LOGIN.userType == null || sessionScope.LOGIN.userType == '' )}">
		<!-- 로그인 검증 -->
		<script type="text/javascript">
			alert('로그인 후 이용해 주시기 바랍니다.');
			location.href='${pageContext.request.contextPath}/user/login';
		</script>
	</c:if>
	<c:if test="${ videoLectureDetail.VType != 0 
			&& sessionScope.LOGIN.userType < videoLectureDetail.VType }">
		<!-- 회원 권한 확인 -->
		<script type="text/javascript">
			alert('결제 후 이용하실 수 있습니다.');
			history.go(-1);
		</script>
	</c:if>
	
	<jsp:include page="../default/header.jsp" />
	
	<section id="main">
		<div class="videoLectureNav">
			<h2>강의 동영상</h2>
			<c:if test="${ sessionScope.LOGIN.userType >= 8 }">
				<div class="videoLectureAdmin">
					<input type="button" class="btn" value="삭제">
				</div>
			</c:if>
		</div>
		
		<hr style="margin-top: 0;"/>
		
		<div class="videoLectureContent">
			<table>
				<tbody>
					<!-- 강의영상 -->
					<tr>
						<td>
							<iframe id="videoLectureIFrame" src="${ videoLectureDetail.listURL[0].VURL }"></iframe>
						</td>
						
						<td valign="top">
							<div class="videoLectureListDiv">
								<c:if test="${ sessionScope.LOGIN.userType >= 8 }">
									<div class="videoLectureURLList">
										<div class="videoLectureBtnDiv">
											<form id="videoLectureFrom"></form>
											<input type="button" value="등록" id="videoLectureURLAddBtn">
										</div>
									</div>
									<div class="videoLectureURLList">
										<input type="button" value="수정" id="videoLectureURLUpdateBtn">
									</div>
									<div class="videoLectureURLList" style="padding-bottom: 5px; border-bottom: 2px dashed white;">
										<input type="button" value="삭제" id="videoLectureURLDeleteBtn">
									</div>
								</c:if>
								
								<c:forEach items="${ videoLectureDetail.listURL }" var="urlBox">
									<div class="videoLectureURLList">
										<a type="text" class="videoLectureURL">${ urlBox.VName }</a>
										<input type="hidden" name="vURL" value="${ urlBox.VURL }">
										<input type="hidden" name="idx" value="${ urlBox.idx }">
									</div>
								</c:forEach>
								<c:if test="${ fn:length(videoLectureDetail.listURL) == 0 }">
									<h1 style="font-family: NANUMSQUAREER; font-weight: bold; text-align: center; margin: 60px 0;">영상이 없습니다</h1>
								</c:if>
							</div>
						</td>
					</tr>
					
					<!-- 강의소개 -->
					<tr>
						<td colspan="2" class="videoLectureTeacherInfo">
							<div class="videoLectureTeacherNav">
								<h3 style="display: inline-block;">강의 소개</h3>
								<c:if test="${ sessionScope.LOGIN.userType >= 8 }">
									<input type="button" class="btn" value="수정">
									<input type="hidden" class="btn" value="완료">
								</c:if>
								<h6>
									<fmt:parseDate var="data" value="${ fn:substring(videoLectureDetail.VDate,0,19) }" pattern="yyyy-MM-dd HH:mm:ss"/>
									<fmt:formatDate value="${ data }" pattern="yyyy년 MM월 dd일 HH시 mm분 ss초"/>
								</h6>
							</div>
							
							<hr style="margin-top: 0;"/>
							
							<div class="videoLectureTeacherDiv">
								<div class="videoLectureTeacherContent">
									<h4>강의 : <span class="vTitleSpan">${ videoLectureDetail.VTitle }</span></h4>
									<c:if test="${ videoLectureDetail.VPackage != null }">
										<h4 style="display: inline-block;">분류 : <a href="/videoLecture/videoLectureList?type=vPackage&inputText=${ videoLectureDetail.VPackage }"><span class="vPackageSpan">${ videoLectureDetail.VPackage }</span></a></h4>
									</c:if>
									<c:if test="${ videoLectureDetail.VPackage == null }">
										<h4 style="display: inline-block;">분류 : <a href="/videoLecture/videoLectureList?type=vPackage&inputText="><span class="vPackageSpan">기타</span></a></h4>
									</c:if>
									<select id="vPackageSpanSelect"  style="display: none;"></select>
									<c:if test="${ sessionScope.LOGIN.userType >= 8 }">
										<h4>조회수 : ${ videoLectureDetail.VCount }</h4>
									</c:if>
								</div>
								<div class="videoLectureTeacherScoreDiv">
									<h2>${ videoLectureDetail.VScore / 10 }</h2>
									<!-- <div class="videoLectureStar">★★★★★</div>
									<div class="videoLectureStar">☆☆☆☆☆</div> -->
									<p>${ videoLectureDetail.VScoreCount } 개의 수강평</p>
								</div>
								<hr style="width: 100%; display: inline-block;"/>
								<p class="videoLectureTeacherContentP">${ videoLectureDetail.VContent }</p>
							</div>
						</td>
					</tr>
					
					<!-- 강사정보 -->
					<tr>
						<td colspan="2" class="videoLectureTeacherInfo">
							<div class="videoLectureTeacherNav">
								<h3>강사 정보</h3>
								<p>강사님의 정보를 한눈에 알 수 있습니다.</p>
							</div>
							<hr style="margin-top: 0;"/>
							<div class="videoLectureTeacherDiv" style="text-align: center;">
								<div class="teacherImg"><img alt="" src="">강사이미지</div>
								<div class="teacherInfo">
									이&nbsp;&nbsp;&nbsp;&nbsp;름 : 
									<c:if test="${ videoLectureDetail.TUserName != null }">
										<a href="/videoLecture/videoLectureList?type=tUserName&inputText=${ videoLectureDetail.TUserName }">${ videoLectureDetail.TUserName }</a><br/>
									</c:if>
									<hr/>
									프로필 : ${ videoLectureDetail.teacherVO.TProfile }<br/>
									<hr/>
									<div style="display: inline-table; width: 50%">
										한국어 성적(듣기) : 
											<c:if test="${ videoLectureDetail.teacherVO.TListening != 0 }">${ videoLectureDetail.teacherVO.TListening }</c:if>
											<c:if test="${ videoLectureDetail.teacherVO.TListening == 0 }">미입력</c:if>
										<br/>
										
										한국어 성적(읽기) : 
											<c:if test="${ videoLectureDetail.teacherVO.TRead != 0 }">${ videoLectureDetail.teacherVO.TRead }</c:if>
											<c:if test="${ videoLectureDetail.teacherVO.TRead == 0 }">미입력</c:if>
										<br/>
										
										한국어 성적(쓰기) : 
											<c:if test="${ videoLectureDetail.teacherVO.TWriting != 0 }">${ videoLectureDetail.teacherVO.TWriting }</c:if>
											<c:if test="${ videoLectureDetail.teacherVO.TWriting == 0 }">미입력</c:if>
										<br/>
										<c:if test="${ videoLectureDetail.VPackage != null }">
											분류 : <a href="/videoLecture/videoLectureList?type=vPackage&inputText=${ videoLectureDetail.VPackage }">
												<span class="vPackageSpan">${ videoLectureDetail.VPackage }</span>
											</a>
										</c:if>
										<c:if test="${ videoLectureDetail.VPackage == null }">
											분류 : <a href="/videoLecture/videoLectureList?type=vPackage&inputText=">
												<span class="vPackageSpan">기타</span>
											</a>
										</c:if>
									</div>
									<div style="display: inline-table; width: 49%">
										한국어 등급 : 
											<c:if test="${ videoLectureDetail.teacherVO.TGrade != 0 }">${ videoLectureDetail.teacherVO.TGrade }</c:if>
											<c:if test="${ videoLectureDetail.teacherVO.TGrade == 0 }">미입력</c:if>
										<br/>
										<fmt:parseDate var="data" value="${ fn:substring(videoLectureDetail.teacherVO.TUpdateDate,0,19) }" pattern="yyyy-MM-dd HH:mm:ss"/>
										업데이트날짜 : <fmt:formatDate value="${ data }" pattern="yyyy년 MM월 dd일 HH시 mm분 ss초"/><br/>
										성적 보기
									</div>
								</div>
							</div>
						</td>
					</tr>
					
					<!-- 추천영상 -->
					<tr>
						<td colspan="2" class="videoLectureTeacherInfo">
							<div class="videoLectureTeacherNav">
								<h3>추천강의</h3>
								<p style="color: black;">${ videoLectureDetail.VPackage } 관련 영상을 추천해 드립니다.</p>
							</div>
							
							<hr style="margin-top: 0;"/>
							
							<div class="videoLectureTeacherDiv row">
								<c:forEach items="${ videoLectureDetail.listVideo }" var="list">
									<div class="videoLectureRecommendDiv col-md-3 col-sm-6">
										<div>
											<input type="hidden" name="vIdx" value="${ list.VIdx }">
											<input type="hidden" name="VType" value="${ list.VType }">
											<a href="/videoLecture/videoLectureDetail/${ list.VIdx }">
												<h2 style="display: inline-block;">${ list.VTitle }</h2>
											</a>
											
											<hr style="margin: 10px 0 10px 0;"/>
											
											<!-- 강사이름 -->
											<div style="display: inline-block;">
												<c:if test="${ list.TUserName != null }">
													<a href="/videoLecture/videoLectureList?type=tUserName&inputText=${ videoLectureDetail.TUserName }">
														<span class="tUserNameSpan">${ list.TUserName }</span>
													</a>
												</c:if>
												<c:if test="${ list.TUserName == null }">
													<a href="/videoLecture/videoLectureList?type=tUserName&inputText=${ videoLectureDetail.TUserName }">
														<span class="tUserNameSpan">기타</span>
													</a>
												</c:if>
											</div>
											
											<!-- 분류 -->
											<div style="float: right;">
												<c:if test="${ list.VPackage != null }">
													<a href="/videoLecture/videoLectureList?type=vPackage&inputText=${ list.VPackage }"
														style="display: inline-block;">
														<span class="vPackageSpan">${ list.VPackage }</span>
													</a>
												</c:if>
												<c:if test="${ list.VPackage == null }">
													<a href="/videoLecture/videoLectureList?type=vPackage&inputText=">
														<span class="vPackageSpan">없음</span>
													</a>
												</c:if>
											</div>
											
											<!-- 평점 -->
											<div>
												<c:forEach begin="1" end="${ list.VScore / 10 % 5 }">
													<span style="font-size: 20px; color: #FFCB10;">★</span>
												</c:forEach>
												<c:forEach begin="${ (list.VScore / 10 % 5 ) + 1 }" end="5">
													<span style="font-size: 20px; color: #FFCB10;">☆</span>
												</c:forEach>
											</div>
											
											<hr style="margin: 10px 0 10px 0;"/>
											
											<!-- 내용 -->
											<a href="/videoLecture/videoLectureDetail/${ list.VIdx }">
												<textarea readonly="readonly" rows="4">${ list.VContent }</textarea>
											</a>
											
											<hr style="margin: 10px 0 10px 0;"/>
											
											<div style="display: inline-block;">
												<span style="font-size: 12px;">평가 : ${ list.VScoreCount }</span><br/>
												<span style="font-size: 12px;">조회수 : ${ list.VCount }</span>
											</div>
											
											<!-- 등록일시 -->
											<div style="text-align: right; float: right; margin-top: 15px;">
												<span style="font-size: 10px; display: block;">
													<fmt:parseDate var="data1" value="${ fn:substring(list.VDate,0,10) }" pattern="yyyy-MM-dd"/>
													<fmt:formatDate value="${ data1 }" pattern="yyyy년 MM월 dd일"/><br/>
													<fmt:parseDate var="data2" value="${ fn:substring(list.VDate,11,19) }" pattern="HH:mm:ss"/>
													<fmt:formatDate value="${ data2 }" pattern="HH시 mm분 ss초"/>
												</span>
											</div>
										</div>
									</div>
								</c:forEach>
								<c:if test="${ fn:length(videoLectureDetail.listVideo) == 0 }">
									<h1 style="font-family: NANUMSQUAREER; font-weight: bold; text-align: center; margin: 60px 0;">추천강의가 없습니다</h1>
								</c:if>
							</div>
						</td>
					</tr>
					
					<!-- 강의 평가 -->
					<tr>
						<td colspan="2" class="videoLectureTeacherInfo">
							<div class="videoLectureTeacherNav">
								<h3>강의평가</h3>
								<p>해당 강의의 평가입니다.</p>
							</div>
							
							<hr style="margin: 0;"/>
							
							<div class="videoLectureTeacherDiv">
								<div class="videoLectureTeacherReply">
									<c:forEach items="${ videoLectureDetail.listReply }" var="list">
										<div class="videoLectureReplyDiv">
											<input type="hidden" name="userKey" value="${ list.userKey }">
											
											<span style="float: right;">
												<fmt:parseDate var="data" value="${ fn:substring(list.vrDate,0,19) }" pattern="yyyy-MM-dd HH:mm:ss"/>
												<fmt:formatDate value="${ data }" pattern="yyyy년 MM월 dd일 HH시 mm분 ss초"/>
											</span>
											
											<c:if test="${ sessionScope.LOGIN.userKey == list.userKey }">
												<input type="button" name="videoLectureDeleteReplyBtn" value="X" class="btn" 
														style="margin: -5px 5px 0 0; border: 1px solid white; display: inline-block;">
											</c:if>
											
											<h4 style="display: inline-block;">${ list.userNick }</h4><br/>
											
											<c:forEach begin="1" end="${ list.vrScore }">
												<span class="scoreStar">★</span>
											</c:forEach>
											<c:forEach begin="${ list.vrScore + 1 }" end="5">
												<span class="scoreStar">☆</span>
											</c:forEach>
											
											<p style="margin-top: 15px;">${ list.vrComment }</p>
										</div>
									</c:forEach>
								</div>
								
								<!-- 댓글작성 -->
								<div class="videoLectureTeacherAddReply">
									<input type="hidden" name="vIdx" value="${ videoLectureDetail.VIdx }">
									<h4 style="font-size: 20px; display: inline-block;">평가하기</h4>
									<div class="videoLectureTeacherAddReplyScore">
										<span>&nbsp;&nbsp;-&nbsp;&nbsp;강의 점수 : </span>
										<input type="radio" name="vrScore" value="1" style="display: none;">
										<span class="scoreStar">☆</span>
										
										<input type="radio" name="vrScore" value="2" style="display: none;">
										<span class="scoreStar">☆</span>
										
										<input type="radio" name="vrScore" value="3" style="display: none;">
										<span class="scoreStar">☆</span>
										
										<input type="radio" name="vrScore" value="4" style="display: none;">
										<span class="scoreStar">☆</span>
										
										<input type="radio" name="vrScore" value="5" style="display: none;">
										<span class="scoreStar">☆</span>
									</div>
									<textarea name="vrComment" maxlength="500" placeholder="최대 500자"></textarea>
									<input type="button" value="강의 평가 등록" class="btn">
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</section>
	<jsp:include page="../default/footer.jsp" />
</body>
</html>