<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<!-- HanJungIl - 작성 - 170610 ~ 170630 -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Holic - 강의 동영상</title>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bs/css/bootstrap.css">
<style type="text/css">
.videoLectureNav h2 { 
	font-family: NANUMSQUAREER;
	font-weight: bold; 
	display: inline-block; 
}
.videoLectureNav p { 
	float: right;
	font-size: 12px;
	margin: 40px 10px 0 0; 
}

#videoLectureForm .btn {
	color: white;
	font-weight: bold; 
	background-color: black; 
}
.videoLectureListMenu { 
	text-align: right;
	margin: 0 10px 20px 10px; 
}
.videoLectureContent {
    margin-top: 20px;
	padding: 15px;
}
.videoLectureContent table th,
.videoLectureContent table {
	text-align: center; 
	font-size: 15px;
	padding: 10px;
}
.videoLectureContent table { 
	width: 100%;
	margin-bottom: 20px; 
	border-bottom: 1px solid gray;
}
.videoLectureContent table th { 
	color: white;
	background-color: black;
	border-bottom: 1px solid black; 
}
.videoLectureContent table td { padding: 10px; }
.videoLectureContent tbody tr:NTH-CHILD(2n-1) { background-color: #F5F5F5; }
.videoLectureContent table .vTitle { text-align: left; }
.videoLectureContent table .vDate { font-size: 12px; }
.videoLecturePageNumberDiv { 
	display: inline-block;
	margin-left: 10px; 
}
.videoLectureSearchDiv { 
	float: right; 
	margin-right: 10px;
}
.videoLectureSearchDiv select { padding: 7px; }
.videoLectureSearchDiv input:NTH-CHILD(2) {
	box-shadow: 0 0 0 1000px white inset;
	width: 200px;
	padding: 6px;
	border: 1px solid rgb(169, 169, 169);
}
</style>
<script type="text/javascript">
$(document).ready(function(){
	// 강의 등록 버튼
	$(".addVideoLectureBtn").click(function(){
		location.href="../videoLecture/videoLecture";
	});
	
	// 페이지 버튼
	$(".pageBtn").click(function(){
		var pageNum = $(this).val();
		
		if(pageNum == '<<'){
			pageNum = 1;
		}else if(pageNum == '<'){
			pageNum = $(this).next().val() * 1 - 1;
		}else if(pageNum == '>'){
			pageNum = $(this).prev().val() * 1 + 1;
		}else if(pageNum == '>>' ){
			pageNum = -1;
		}
		
		location.href="/videoLecture/videoLectureList?pageNum=" + pageNum;
	});
	
	// 검색 버튼
	$(".videoLectureSearchDiv").children("form").children("input[type=button]").click(function(){
		var inputText = $(this).parent("form").children("input[name=inputText]");
		
		if(inputText.val() == null || inputText.val().replace(/(^\s*)|(\s*$)/gi, "") == ''){
			alert("검색어를 입력해 주세요");
			inputText.focus();
			return;
		}
		
		$(this).parent("form").submit();
	});
});
</script>
</head>
<body>
	<jsp:include page="../default/header.jsp" />
	<section id="main">
		<div class="videoLectureNav">
			<h2>강의 동영상</h2>
			<p>※ 한국어 강의 동영상을 볼 수 있습니다.</p>
		</div>
		
		<hr style="margin: 0 0 20px 0;"/>
		
		<div class="videoLectureContent">
			<c:if test="${ sessionScope.LOGIN.userType >= 8 }">
				<div class="videoLectureListMenu" style="margin-bottom: 20px;">
					<input type="button" value="강의 등록하기" class="btn addVideoLectureBtn">
				</div>
			</c:if>
			<table>
				<thead>
					<tr>
						<th>번호</th>
						<th>제&nbsp;&nbsp;&nbsp;&nbsp;목</th>
						<th>분류</th>
						<th>강사</th>
						<c:if test="${ sessionScope.LOGIN.userType >= 8 }"><th>작성자</th></c:if>
						<th>등록일시</th>
						<c:if test="${ sessionScope.LOGIN.userType >= 5 }"><th>조회수</th></c:if>
						<th>공개</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${ list }" var="videoLecture">
						<tr>
							<!-- 번호 -->
							<td>
								<a href="/videoLecture/videoLectureDetail/${ videoLecture.VIdx }">
									${ videoLecture.VIdx }
								</a>
							</td>
							
							<!-- 제목 -->
							<c:if test="${ sessionScope.LOGIN.userType >= 8 }">
								<td class="vTitle" style="width: 42%;">
									<a href="/videoLecture/videoLectureDetail/${ videoLecture.VIdx }">
										${ videoLecture.VTitle }
									</a>
								</td>
							</c:if>
							<c:if test="${ sessionScope.LOGIN.userType < 8 || sessionScope.LOGIN.userType == null }">
								<td class="vTitle" style="width: 55%;">
									<a href="/videoLecture/videoLectureDetail/${ videoLecture.VIdx }">
										${ videoLecture.VTitle }
									</a>
								</td>
							</c:if>
							
							<!-- 분류 -->
							<c:if test="${ videoLecture.VPackage == null }">
								<td><a href="/videoLecture/videoLectureList?type=vPackage&inputText=">없음</a></td>
							</c:if>
							<c:if test="${ videoLecture.VPackage != null }">
								<td><a href="/videoLecture/videoLectureList?type=vPackage&inputText=${ videoLecture.VPackage }">${ videoLecture.VPackage }</a></td>
							</c:if>
							
							<!-- 강사 -->
							<c:if test="${ videoLecture.TUserName != null }">
								<td><a href="/videoLecture/videoLectureList?type=tUserName&inputText=${ videoLecture.TUserName }">${ videoLecture.TUserName }</a></td>
							</c:if>
							<c:if test="${ videoLecture.TUserName == null }">
								<td><a href="/videoLecture/videoLectureList?type=tUserName&inputText=">기타</a></td>
							</c:if>
							
							<!-- 작성자 -->
							<c:if test="${ sessionScope.LOGIN.userType >= 8 }">
								<td><a href="/videoLecture/videoLectureList?type=userNick&inputText=${ videoLecture.userNick }">${ videoLecture.userNick }</a></td>
							</c:if>
							
							<!-- 등록일시 -->
							<td class="vDate">
								<fmt:parseDate var="data" value="${ fn:substring(videoLecture.VDate,0,19) }" pattern="yyyy-MM-dd HH:mm:ss"/>
								<fmt:formatDate value="${ data }" pattern="yyyy년 MM월 dd일 HH시 mm분 ss초"/>
							</td>
							
							<!-- 조회수 -->
							<c:if test="${ sessionScope.LOGIN.userType >= 5 }">
								<td>${ videoLecture.VCount }</td>
							</c:if>
							
							<!-- 공개 -->
								<c:if test="${ videoLecture.VType == 0 }"><td>전체공개</td></c:if>
								<c:if test="${ videoLecture.VType == 1 }"><td>일반회원</td></c:if>
								<c:if test="${ videoLecture.VType == 2 }"><td>유료회원</td></c:if>
							<c:if test="${ sessionScope.LOGIN.userType >= 8 }">
								<c:if test="${ videoLecture.VType == 5 }"><td>강사회원</td></c:if>
								<c:if test="${ videoLecture.VType == 8 }"><td>운 영 자</td></c:if>
							</c:if>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<c:if test="${ videoLecture.length == 0 }">
				<p style="text-align: center; font-family: NANUMSQUAREER; font-size: 20px; font-weight: bold; margin: 40px 0 20px 0;">동영상이 없습니다.</p>
			</c:if>
			<c:if test="${ sessionScope.LOGIN.userType >= 8 }">
				<div class="videoLectureListMenu">
					<input type="button" value="강의 등록하기" class="btn addVideoLectureBtn">
				</div>
			</c:if>
			<div class="videoLecturePageNumberDiv">
				<c:if test="${ page.firstPage != 1 }">
					<input type="button" value="&#60;&#60;" class="btn pageBtn" style="width: 50px;">&nbsp;&nbsp;
					<input type="button" value="&#60;" class="btn pageBtn" style="width: 50px;">&nbsp;&nbsp;
				</c:if>
				
				<c:forEach var="idx" end="${ page.endPage }" begin="${ page.firstPage }">
					<c:if test="${ page.pageNum != idx }">
						<input type="button" value="${ idx }" class="btn pageBtn" style="width: 50px;">&nbsp;&nbsp;
					</c:if>
					<c:if test="${ page.pageNum == idx }">
						<input type="button" value="${ idx }" class="btn pageBtn" style="width: 50px; background-color: rgb(200, 200, 200);">&nbsp;&nbsp;
					</c:if>
				</c:forEach>
				
				<c:if test="${ page.endBtnCheck }">
					<input type="button" value="&#62;" class="btn pageBtn" style="width: 50px;">&nbsp;&nbsp;
					<input type="button" value="&#62;&#62;" class="btn pageBtn" style="width: 50px;">
				</c:if>
			</div>
			<div class="videoLectureSearchDiv">
				<form>
					<select name="type">
						<option value="tUserName">이름</option>
						<option value="vPackage">분류</option>
						<option value="vTitle">제목</option>
						<option value="vContent">내용</option>
					</select>
					<input type="search" name="inputText">
					<input type="button" value="검색" class="btn">
				</form>
			</div>
		</div>
	</section>
	<jsp:include page="../default/footer.jsp" />
</body>
</html>