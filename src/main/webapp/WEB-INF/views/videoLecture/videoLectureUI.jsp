<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<!-- HanJungIl - 작성 - 170610 ~ 170630 -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Holic - 강의 동영상 등록하기</title>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bs/css/bootstrap.css">
<style type="text/css">
#videoLectureForm .btn, 
#addURL span { font-size: 12px; }
.videoLectureFormMessage {
	color: red;
	font-size: 11px;
	font-weight: bold;
	margin: 20px 0;
}
.messageSpan { 
	font-size: 12px;
	font-weight: bold; 
}
.videoLectureNav { border-bottom: 1px solid rgb(169, 169, 169); }
.videoLectureNav > h2 {
	font-family: NANUMSQUAREEB; 
	font-weight: bold; 
}
#videoLectureForm .vTitle { 
	width: 70%;
	float: left;
	border: 2px solid black;
}
#videoLectureForm .writer { 
	width: 30%;
	float: right;
	border: 2px solid black;
	border-left: 0 solid;
}
#videoLectureForm input { 
	margin-bottom: 10px;
	padding: 5px;
}
#videoLectureForm textarea { 
	width: 100%;
	height: 600px;
	padding: 5px;
	border: 2px solid black;
	resize: none;
}

#addURL .addURLDiv input {
	margin-left: 5px;
	margin-bottom: 0;
}
.addURLDiv input:FIRST-CHILD { 
	width: 30%;
	margin-left: 0; 
}
.addURLDiv input:NTH-CHILD(2) {
	width: 60%;
}
.addURLBtnDiv { text-align: center; }
#videoLectureForm > .addURLBtnDiv > .addURLBtn { 
	font-family: NANUMSQUAREEB;
	font-size: 15px;
	margin-bottom: 20px; 
}
#addIframe { text-align: center; }
#videoLectureForm select { 
	height: 32px; 
	width: 10%;
	margin-left: 5px;
	margin-bottom: 10px;
}
.videoLectureContent { 
	padding: 15px;
	padding-top: 0; 
}
.addURLBtn {
	width: 60px;
    font-weight: bold;
}
#videoLectureForm > div:LAST-CHILD {
	margin-top: 40px;
	margin-bottom: 20px;
} 
#videoLectureForm > div:LAST-CHILD > .btn { 
	font-family: NANUMSQUAREB;
	font-size: 15px;
	padding: 5px 40px; 
}
#videoLectureForm > div:LAST-CHILD > .btn:LAST-CHILD { 
	background-color: rgb(160,160,160);
	margin-left: 10px; 
}
#videoLectureForm > div:LAST-CHILD > .btn:LAST-CHILD:HOVER { background-color: rgb(80,80,80); }
</style>
<script type="text/javascript">
$(document).ready(function(){
	// form 설정
	$("#addURL").append('<div class="addURLDiv">' 
			+ '<input type="text" name="vName" placeholder="영상 제목" autocomplete="off">' 
			+ '<input type="text" name="vURL" placeholder="URL" autocomplete="off">'
			+ '<input type="button" value="확인" class="btn checkedVideo"><B></B><br/>'
			+ '<input type="checkbox" class="checkeBox"><span>영상이 잘 나옵니다.(재설정시 체크 해제)</span></div>');
	
	var message = $("#videoLectureForm").children(".videoLectureFormMessage");
	
	// URL 영상 확인용
	var checkedVideo = function(){
		var vURL = $(this).prev("input[name=vURL]");
		var iframe = $("#addIframe").children("iframe");
		
		if(vURL.val() == '') {
			message.text("동영상 주소를 입력해 주세요.");
			vURL.focus();
			return;
		}
		
		if(iframe != null){
			iframe.remove();
		}
		
		$("#addIframe").append('<iframe src=' + vURL.val() + ' width="640" height="480" ' 
				+ 'frameborder="0" allowfullscreen>이 브라우저는 iframe을 지원하지 않습니다.</iframe>');
	}
	
	// 확인체크후
	var checkedBox = function(){
		var vName = $(this).parent(".addURLDiv").children("input[name=vName]");
		var vURL = $(this).parent(".addURLDiv").children("input[name=vURL]");
		
		if(vName.val() == ''){
			message.text("동영상 제목을 입력해 주세요.");
			vName.focus();
			return;
		}else if(vName.val().length < 2){
			message.text("동영상 제목은 두 자 이상 입력해야 합니다.");
			vName.focus();
			return;
		}else if(vURL.val() == ''){
			message.text("동영상 주소를 입력해 주세요.");
			vURL.focus();
			return;
		}else if(vURL.val().length < 8){
			message.text("동영상 주소는 8자 이상 입력해야 합니다.");
			vURL.focus();
			return;
		}else if($(this).is(":checked")){
			vURL.attr("readonly", true);
			vURL.css("backgroundColor", "rgb(235, 235, 228)");
			vName.attr("readonly", true);
			vName.css("backgroundColor", "rgb(235, 235, 228)");
			message.text("");
		}else{
			vURL.attr("readonly", false);
			vURL.css("backgroundColor", "");
			vName.attr("readonly", false);
			vName.css("backgroundColor", "");
			message.text("");
		}
	}
	
	// URL 입력창 추가를 위한 설정
	var addURLBtnAdd = function(){
		var length = $("#addURL").children(".addURLDiv").children(".checkedVideo").length;
		var maxLength = 10;
		
		if(length < maxLength){
			$("#addURL").append('<div class="addURLDiv">' 
					+ '<input type="text" name="vName" placeholder="영상 제목" autocomplete="off">'
					+ '<input type="text" name="vURL" placeholder="URL" autocomplete="off">' 
					+ '<input type="button" value="확인" class="btn checkedVideo">' 
					+ '<input type="button" value="X" class="btn deleteVideoDiv"><br/>'
					+ '<input type="checkbox" class="checkeBox"><span style="font-size: 12px;">영상이 잘 나옵니다.(재설정시 체크 해제)</span></div>');
			if(length + 1 == maxLength) {
				message.text("영상은 최대 10개까지 등록 가능합니다.");
				$(this).remove();
			}
			return;
		}
	}
	
	// URL 입력창 추가한 뒤 이벤트 설정
	$("#addURL").on("click", ".checkedVideo", checkedVideo);
	$("#addURL").on("click", ".checkeBox", checkedBox);
	
	// URL 제거
	$("#addURL").on("click", ".deleteVideoDiv", function(){
		$(this).parents(".addURLDiv").remove();
		
		if($("input[name=addURLBtn]").length == 0){
			$("#addURL").after('<input type="button" value="+" name="addURLBtn" class="btn addURLBtn">');
			$("#videoLectureForm").on("click", ".addURLBtn", addURLBtnAdd);
		}
	});
	
	// URL 입력창 추가
	$("input[name=addURLBtn]").click(addURLBtnAdd);
	
	// URL 확인
	$("input[value=확인]").click(checkedVideo);
	
	// 패키지 분류 selelcter
	$("select[name=vPackage]").click(function(){
		var vPackSelect = $("select[name=vPackage]");
		var vPackIpt = $("input[name=vPackage]");
		
		vPackIpt.attr("readonly", true);
		vPackIpt.css("backgroundColor", "rgb(235, 235, 228)");
		
		// 없을 경우
		if(vPackSelect.val() == 'false'){
			vPackIpt.val("");
		}else if(vPackSelect.val() == '없음'){
			vPackIpt.val(vPackSelect.val());
		}else if(vPackSelect.val() == '새로 등록하기'){
			vPackIpt.attr("readonly", false);
			vPackIpt.css("backgroundColor", "");
			vPackIpt.val("");
		}else{
			vPackIpt.val(vPackSelect.val());
		}
	});
	
	// 전송
	$("#videoLectureForm").children("div").children("input[value=등록]").click(function(){
		var vTitle = $("#videoLectureForm").children("input[name=vTitle]");
		var vContent = $("#videoLectureForm").children("textarea[name=vContent]");
		var vName = $("input[name=vName]");
		var vURL = $("input[name=vURL]");
		var vType = $("#videoLectureForm").children("select[name=vType]");
		var vCheck = $("#addURL").children(".addURLDiv").children("input[type=checkbox]");
		var vPackage = $("#videoLectureForm").children("input[name=vPackage]");
		var tUserKey = $("#videoLectureForm").children("select[name=tUserKey]");
		
		message.text("");
		
		if(vTitle.val() == ''){
			message.text("제목을 입력해 주세요.");
			vTitle.focus();
			return;
		}else if(vTitle.val().length < 2){
			message.text("제목을 두 자 이상 입력해 주세요.");
			vTitle.focus();
			return;
		}else if(vContent.val() == ''){
			message.text("내용을 입력해 주세요.");
			vContent.focus();
			return;
		}else if(vType.val() == '00'){
			message.text("공개여부를 선택해 주세요.");
			vType.focus();
			return;
		}else if(vPackage.val() == 'false'){
			message.text("분류를 선택해 주세요.");
			vPackage.focus();
			return;
		}else if(tUserKey.val() == 'false'){
			message.text("강사를 선택해 주세요.");
			tUserKey.focus();
			return;
		}else{
			for(i = 0 ; i < vURL.length ; ++i){
				if(vName.eq(i).val() == ''){
					message.text("동영상 제목을 입력해 주세요.");
					vName.eq(i).focus();
					return;
				}else if(vName.eq(i).val().length < 2){
					message.text("동영상 제목은 두 자 이상 입력해야 합니다.");
					vName.eq(i).focus();
					return;
				}else if(vURL.eq(i).val() == ''){
					message.text("동영상 주소를 입력해 주세요.");
					vURL.eq(i).focus();
					return;
				}else if(vURL.eq(i).val().length < 8){
					message.text("동영상 주소는 8자 이상 입력해야 합니다.");
					vURL.eq(i).focus();
					return;
				}else if(!vURL.eq(i).parent(".addURLDiv").children(".checkeBox").is(":checked")){
					vURL.eq(i).attr("readonly", true);
					message.text("동영상 확인을 체크해 주세요.");
					vURL.eq(i).next().next(".checkeBox").focus();
					return;
				}
			}
			
			// 분류 전송 방지
			$("select[name=vPackage]").attr("disabled", true);
			
			$.ajax({
				url		: "../videoLecture/videoLecture",
				type	: "post",
				data	: $("#videoLectureForm").serialize(),
				dataType: "text",
				success : function(data){
					location.href=data;
				},
				error	: function(xhr, status, error){
					console.log(xhr + " / " + status + " / " + error);
				}
			});
		}
	});
	
	// 취소
	$("input[value=취소]").click(function(){
		location.href="/videoLecture/videoLectureList";
	});
});
</script>
</head>
<body>
	<jsp:include page="../checkPage/operatorCheck.jsp" />
	<jsp:include page="../default/header.jsp" />
	<section id="main">
		<div class="videoLectureNav">
			<h2>강의 동영상 등록</h2>
		</div>
		<div class="videoLectureContent">
			<form id="videoLectureForm" method="post">
				<p class="videoLectureFormMessage"></p>
				<input type="text" name="vTitle" class="vTitle" placeholder="제목" autocomplete="off">
				<input type="text" class="writer" value="${ sessionScope.LOGIN.userNick }" disabled="disabled"><br/>
				<textarea id="vContent" name="vContent" placeholder="내용"></textarea>
				<hr/>
				<div id="addURL"></div>
				
				<div class="addURLBtnDiv">
					<input type="button" value="+" name="addURLBtn" class="btn addURLBtn"><br/>
				</div>
				
				<div id=addIframe></div>
				<hr/>
				
				<input type="text" name="vPackage" placeholder="분류" readonly="readonly"
					style="background-color: rgb(235, 235, 228); width: 35%;"/>
				<select name="vPackage" style="width: 35%;">
					<option value="false">분류 선택</option>
					<option>없음</option>
					<c:forEach items="${ vData.packageList }" var="pkg">
						<option>${ pkg }</option>
					</c:forEach>
					<option>새로 등록하기</option>
				</select>
				<select name="tUserKey" style="width: 10%;">
					<option value="false">강사선택</option>
					<c:forEach items="${ vData.tList }" var="tList">
						<option value="${ tList.userKey }">${ tList.userName }(${ tList.userNick })</option>
					</c:forEach>
					<option>기타</option>
				</select>
				<select name="vType" style="width: 15%;">
					<option value="00">공개여부 선택</option>
					<option value="0">전체 공개</option>
					<option value="1">일반회원 공개</option>
					<option value="2">유료회원 공개</option>
				</select>
				<p class="videoLectureFormMessage"></p>
				<span class="messageSpan">※ 등록 전 아래에 영상이 나오는지 확인해 주세요</span><br/>
				<span class="messageSpan">※ 영상은 iframe 주소만 등록해 주세요.</span>
				<div style="text-align: center;">
					<input type="button" value="등록" class="btn">
					<input type="button" value="취소" class="btn">
				</div>
			</form>
		</div>
	</section>
	<jsp:include page="../default/footer.jsp" />
</body>
</html>