<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
	
	@media(min-width: 768px) {
		.group_form1{
			width: 20%;
		}
		.group_form2{
			width: 80%
		}
	}
	
	.profile_img{
		padding: 50px;
	}
	
	.comment_form{
		margin: auto;
		padding: 30px;
	}
	
	.area{
		width: 100%;
	}
	
	.title{
		text-indent: 10px;
	}
	
	.jumbotron{
		margin-top: 20px;
	}
	
	.enroll_btn{
		text-align: right;
	}
</style>
</head>
<body>
	<jsp:include page="../default/header.jsp" />
	
	<div class="container">
		<div class="jumbotron col-xs-12">
			<div class="row">
				<div class="enroll_btn">
					<button class="btn btn-default" type="submit">등록하기</button>
				</div>
				
				<div class="row">
					<h3 class="title" style="text-align: center;">초급부터 고급까지 한국어를 단계별로 학습할 수 있는 최적화된 외국인 한국어 공부!</h3> <!-- 주제 들어갈 자리 -->
				</div>
			</div>
			
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<div class="group_form1">
						<div class="form-group">
							<div class="profile_img">
								<img alt="강사 프로필사진 등록하는 곳 입니다." src="/resources/img/profile1.png">
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<div class="group_form2">
						
						<div class="form-group">
							<label for="name" style="margin-top: 5px;">이름: </label>
							<input type="text" class="form-control" id="name" style="border: none;">
						
							<label for="sex" style="margin-top: 5px;">성별: </label>
							<input type="text" class="form-control" id="sex" style="border: none;">
							
							<label for="birthday" style="margin-top: 5px;">생년월일: </label>
							<input type="text" class="form-control" id="birthday" style="border: none;">
							
							<label for="grade" style="margin-top: 5px;">성적: </label>
							<input type="text" class="form-control" id="grade" style="border: none;">
							
							<label for="date" style="margin-top: 5px;">날짜: </label>
							<input type="text" class="form-control" id="date" style="border: none;">
							
							<label for="person" style="margin-top: 5px;">신청인원: </label>
							<input type="text" class="form-control" id="person" style="border: none;">
						</div>	
						
						<div class="form-group">
							<div class="row">
								<button type="button" class="btn btn-default col-xs-12 col-sm-4 col-md-4 col-lg-4" style="background: white; color: red;">
									<span class="glyphicon glyphicon-heart" aria-hidden="true"></span>좋아요
								</button>
								
								<input readonly="readonly" value="2명" style="background: none; border: none; text-align: center;" class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<input readonly="readonly" value="진행여부" style="background: none; border: none; text-align: center;" class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							</div>	
						</div>	
					</div>
				</div>
			</div>

			<div class="row">
				<div class="comment_form">
					<textarea class="area" rows="7" style="resize: none;"></textarea>
				</div>
			</div>
				
			
		</div>
	</div>
	
</body>
</html>