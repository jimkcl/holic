<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
	
	.name_row{
		height: 60px;
	}
	
	.address_row{
		height: 60px;
	}
	
	.person_row{
		height: 60px;
	}
	
	.material_row{
		height: 60px;
	}
	
	.score_row{
		height: 60px;
	}
	
	.enrol_title{ 
		text-indent: 20%;
	}
	
	.btn-3d {
    position: relative;
    display: inline-block;
    font-size: 22px;
    padding: 20px 60px;
    color: white;
    margin-top: 2%;
    margin-bottom: 5%;
    margin-left: 30%;
    margin-right: 30%;
    border-radius: 6px;
    text-align: center;
    transition: top .01s linear;
    text-shadow: 0 1px 0 rgba(0,0,0,0.15);
    
	}
	
	.btn-3d.red:hover    {background-color: #e74c3c;}

	.btn-3d:active {
	    top: 9px;
	}

	.btn-3d.red {
	    background-color: #e74c3c;
	    box-shadow: 0 0 0 1px #c63702 inset,
	        0 0 0 2px rgba(255,255,255,0.15) inset,
	        0 8px 0 0 #C24032,
	        0 8px 0 1px rgba(0,0,0,0.4),
	                0 8px 8px 1px rgba(0,0,0,0.5);          
	}
	
	.btn-3d.red:active {
	    box-shadow: 0 0 0 1px #c63702 inset,
	                0 0 0 2px rgba(255,255,255,0.15) inset,
	                0 0 0 1px rgba(0,0,0,0.4);
	}
	
</style>
</head>
<body> 
	<jsp:include page="../default/header.jsp" />
	
	<div class="container" style=" margin:auto 10%;">
		
		<div class="row" style="margin-top: 5%; margin-left: 15%;">
			<div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
				<p style="margin-top: 5px; margin-left:18px;">
					<img alt="강사 프로필사진 등록하는 곳 입니다." src="/img/profile1.png">
				</p>
			</div>
			
			<div class="form-group col-xs-12 col-sm-8 col-md-8 col-lg-8">
				 	<div class="name_row" style="margin-top: 15px;">
						<label for="name" class="col-sm-3">이름: </label>
						<div class="col-sm-9">
							<p>&nbsp;</p>
						</div>
					<div class="address_row">
						<label for="address" class="col-sm-3" style="margin-top: 5px;">장소: </label>
						<div class="col-sm-9" style="margin-top: 10px;">
							<p>&nbsp;</p>
						</div>
					</div>
					
					<div class="person_row">
						<label for="person" class="col-sm-3" style="margin-top: 5px;">수업인원: </label>
						<div class="col-sm-9" style="margin-top: 10px;">
							<p>&nbsp;</p>
						</div>
					</div>
					
					<div class="material_row">
						<label for="material" class="col-sm-3" style="margin-top: 5px;">준비물: </label>
						<div class="col-sm-9" style="margin-top: 10px;">
							<p>&nbsp;</p>
						</div>
					</div>
					
					<div class="score_row">
						<button style="margin-left:3%; margin-top:2%;" type="button" class="btn btn-default" data-toggle="modal" data-target="#specModal">스펙보기</button>
						<button style="margin-left:3%; margin-top:2%;" type="button" class="btn btn-default">신청버튼</button>
					</div>
					
					<div class="modal fade" id="specModal" role="dialog" style="position:fixed; top:20%;">
    					<div class="modal-dialog">
    					
    						<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">OOO 강사님 성적 정보</h4>
								</div>

								<div class="modal-body" style="height: 300px; text-align: center;" >
									<img alt="강사님 성적을 올리는 곳 입니다." src="">
								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-default"
										data-dismiss="modal">Close</button>
								</div>
							</div>
							
    					</div>
    				</div>

				</div>
			</div>
		</div>
		
		<div class="row col-xs-12">
			<section class="form-group" style="margin-top:50px;">
				<div class="enrol_title">
					<h3>주제</h3>
					<p>주제넣을곳임</p>
				</div>
			</section>
			
			<div class="form-group" style="margin-top: 5%; margin-left: 10%;">
				<p style=" margin-left:18px; border: 1px solid black; width:80%; height: 50%; text-align: center; margin-bottom: 4%;">수업을 신청하려면 가져야 할 기초능력수업을 신청하려면 가져야 할 기초능력수업을 신청하려면 가져야 할 기초능력수업을 신청하려면 가져야 할 기초능력수업을 신청하려면 가져야 할 기초능력수업을 신청하려면 가져야 할 기초능력수업을 신청하려면 가져야 할 기초능력수업을 신청하려면 가져야 할 기초능력수업을 신청하려면 가져야 할 기초능력수업을 신청하려면 가져야 할 기초능력</p>
			</div>
		</div>
		
		<div class="row col-xs-12">
		
			<div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4" style="float:right;">
				<textarea style="border: 3px solid black; width:100%; resize: none;" rows="20" readonly="readonly"></textarea>
			</div>
			
			<div class="form-group col-xs-12 col-sm-8 col-md-8 col-lg-8" style="float:right;" >
				<textarea style="border: 3px solid black; resize: none; width:100%; " rows="18" readonly="readonly"></textarea>
				<input class="col-xs-10 col-sm-10 col-md-10 col-lg-10" type="text" id="profile_comment" style="width: 89%; height: 33px;">
				<button type="button" class="btn btn-default">댓글등록</button>
			</div>
			
		</div>
		
		<div class="btn-container">
			<a class="btn-3d red col-xs-4" style="text-decoration:none;">수 강 신 청</a>
		</div> 
			
	</div>
	 <jsp:include page="../default/footer.jsp" />
</body>
</html>