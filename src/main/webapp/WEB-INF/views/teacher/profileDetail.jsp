<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<title>Holic - 스터디 상세정보</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<style type="text/css">
#main .row { margin: 0; }
.profileDetailNav > h2 { 
	font-family: NANUMSQUAREER;
	font-weight: bold; 
	display: inline-block;
	margin-top: 0;
}
.profileDetailNav > p {
	font-size: 12px; 
	float: right; 
	margin: 20px 0 0 0;
}

.lessonDetail{ padding: 0 30px !important; }
.row h4 { 
	font-family: NANUMSQUAREER;
	font-weight: bold; 
}
.lessonDetail p { padding-left: 15px; }
.profileTeacher {
	background-color: #Fafafa; 
	padding: 10px; 
	border: 1px solid rgba(0,0,0,.08);
}

.teacherEvaluationAll {
	padding: 15px;
}

.teacherEvaluationAdd { text-align: right; }
.teacherEvaluationAdd textarea {
	resize: none;
	width: 100%;
	height: 150px;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
	// 스터디 평가 열기 버튼
	$("#teacherEvaluationBtn").click(function(){
		if($(this).val() == "열기"){
			$(this).val("닫기")
			$(".teacherEvaluationAll").css("display", "block");
		}else if($(this).val() == "닫기"){
			$(this).val("열기")
			$(".teacherEvaluationAll").css("display", "none");
		}
	});
	
	// 스터디 평가 작성 후 ajax
	$("#teacherEvaluationAddBtn").click(function(){
		if(${ sessionScope.LOGIN.userKey == null || sessionScope.LOGIN.userKey == '' }){
			alert("로그인 후 진행해 주시기 바랍니다.");
			return;
		}
		
		var $div = $(this).parent(".teacherEvaluationAdd");
		var $lIdx = $div.children("input[name=lIdx]");
		var $lContent = $div.children("textarea[name=lContent]");
		
		if($lContent.val().length < 6){
			alert("길게 입력해 주시기 바랍니다.");
			$lContent.focus();
			return;
		}else if($lIdx.val() == '' || $lIdx.val() == null){
			alert("잘못된 입력입니다.");
			location.reload();
			return;
		}
		
		alert("데이터 작업 후 진행할 것.");
		return;
		
		$.ajax({
			url		: "",
			type	: "post",
			data	: { "lIdx" : $lIdx, "lContent" : $lContent },
			dataType: "text",
			success : function(data){
				alert(data);
				location.reload();
			},
			error	: function(xhr, status, error){
				console.log(xhr + " / " + status + " / " + error);
			}
		});
	});
});
</script>
</head>
<body>
	<jsp:include page="../default/header.jsp" />
	
	<section id="main">
		<div class="profileDetailNav">
			<h2>스터디 주제</h2>
			<p>스터디 관련 정보를 보다 자세히 알 수 있습니다.</p>
		</div>
		
		<hr style="margin-top: 0;"/>
		
		<!-- 스터디 정보 -->
		<div class="row">
			<div class="col-md-9 lessonDetail">
				<h4><strong>1.</strong> 장소</h4>
				<p>장소</p>
				<br/>
				
				<h4>2. 스터디 날짜 및 시간</h4>
				<p>날짜</p>
				<br/>
				
				<h4>3. 이런분께 이 스터디를 권장합니다.</h4>
				<p>기초능력</p>
				<br/>
				
				<h4>4. 스터디 참여를 위한 필요한 것</h4>
				<p>준비물</p>
				<br/>
				
				<h4 style="text-align: right;">신청인원 / 수업 최대 인원</h4>
				<br/>
				
				<div style="width: 100%; text-align: center;">
					<input type="button" value="신청하기" class="btn" style="margin-bottom: 20px; padding: 6px 60px;">
				</div>
			</div>
			
			<!-- 강사 프로필 -->
			<div class="col-md-3 col-sm-6 profileTeacher">
				<p style="text-align: right;">강사 좋아요 숫자</p>
				<div style="height: 200px; text-align: center;">
					<img alt="프로필 사진" src="http://www.dlto.co.kr/wp-content/uploads/2015/11/bonobono_emot.gif" style="height: 100%;">
				</div>
				<p style="text-align: center;">보노보노</p>
				
				<hr style="margin: 10px 0;"/>
				
				<p>한국어 등급 : </p>
				<p>한국어 성적(듣기) : </p>
				<p>한국어 성적(읽기) : </p>
				<p>한국어 성적(쓰기) : </p>
				
				<hr style="margin: 10px 0;"/>
				
				<pre>프로필 내용</pre>
			</div>
			
			<!-- 스터디 평가 -->
			<div class="col-md-8" style="margin-top: 20px; padding-top: 20px; border-top: 1px solid rgba(0,0,0,.08);">
				<h4 style="display: inline-block;">스터디 평가</h4>
				<input id="teacherEvaluationBtn" type="button" value="열기" class="btn" style="font-size: 12px; margin: 0 0 5px 10px; padding: 4px 6px;">
				
				<hr style="margin: 0 0 10px 0;"/>
				
				<div class="teacherEvaluationAll" style="display: none;">
					<div class="teacherEvaluation">
						<p style="display: inline-block;">닉네임</p>
						<p style="font-size: 12px; float: right; margin: 5px;">평가일시</p>
						<p style="margin: 10px 0 20px 0;">평가내용</p>
						<hr style="margin: 5px 0;"/>
					</div>
					
					<%-- <c:if test="${ sessionScope.LOGIN.userNick != null && sessionScope.LOGIN.userNick != '' }"> --%>
					<c:if test="${ true }">
						<div class="teacherEvaluationAdd">
							<input type="hidden" name="lIdx" value="강의 인덱스 값을 넣을 것!">
							<p>${ sessionScope.LOGIN.userNick }</p>
							<textarea name="lContent" placeholder="최대 500자" style="resize: none;"></textarea>
							<input id="teacherEvaluationAddBtn" type="button" value="확인" class="btn" style="padding: 6px 40px;">
						</div>
					</c:if>
				</div>
			</div>
			
			<!-- 신청인 리스트 -->
			<div class="col-md-4" style="margin-top: 20px; padding-top: 20px; border-top: 1px solid rgba(0,0,0,.08);">
				<h4>신청인 리스트</h4>
				
				<hr style="margin: 10px 0;"/>
				
				<div class="lessonUser">
					<pre><strong style="display: inline-block;">No</strong><p style="font-size: 12px; float: right; margin-top: 5px;">신청일시</p><p style="margin: 10px 0 0 0;">신청인 닉네임</p></pre>
					<pre><strong style="display: inline-block;">No</strong><p style="font-size: 12px; float: right; margin-top: 5px;">신청일시</p><p style="margin: 10px 0 0 0;">신청인 닉네임</p></pre>
				</div>
			</div>
		</div>
	</section>
	<jsp:include page="../default/footer.jsp" />
</body>
</html>