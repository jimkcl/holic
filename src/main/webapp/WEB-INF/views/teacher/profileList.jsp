<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<title>Holic - 강사/스터디</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<style type="text/css">
@media ( min-width : 768px) {
	.group_form1 {
		width: 20%;
	}
	.group_form2 {
		width: 80%
	}
}

.profileNav h2 { 
	font-family: NANUMSQUAREER;
	font-weight: bold; 
	display: inline-block; 
}
.profileNav p { 
	float: right;
	font-size: 12px;
	margin: 40px 10px 0 0; 
}

.profileContent p { margin: 0 0 5px; }
</style>
<script type="text/javascript">
$(document).ready(function(){
	
	// 진행중인 스터디
	$("#proceedingStudy").click(function(){
		alert('진행중인 스터디 설정 필요');
		location.href="../teacher/profileList";
	});
	
	// 종료된 스터디
	$("#endStudy").click(function(){
		alert('종료된 스터디 설정 필요');
		location.href="../teacher/profileList";
	});
	
	// 강사님 리스트
	$("#teacherList").click(function(){
		location.href="../teacher/teacherProfileList";
	});
	
	// 상세보기
	$("input[value=상세보기]").click(function(){
		location.href="../teacher/profileDetail";
	});
});
</script>
</head>
<body>
	<jsp:include page="../default/header.jsp" />
	<section id="main">
		<div class="profileNav">
			<h2>스터디 리스트</h2>
			<input id="proceedingStudy" type="button" value="진행중인 스터디" class="btn" style="display: inline-block; margin: 0 0 10px 10px;">
			<input id="endStudy" type="button" value="종료된 스터디" class="btn" style="display: inline-block; margin: 0 0 10px 10px;">
			<input id="teacherList" type="button" value="강사님" class="btn" style="display: inline-block; margin: 0 0 10px 10px;">
			<c:if test="${ session.LOGIN.userType == 5 }">
				<input type="button" value="스터디 등록하기" class="btn" style="display: inline-block; margin: 0 0 10px 30px;">
			</c:if>
			<p>※ 원하는 스터디를 신청하여 수강할 수 있습니다.</p>
		</div>
		
		<hr style="margin: 0 0 20px 0;"/>
		
		<div class="row">
			<%-- <c:forEach items="${ videoLectureList }" var="list"> --%>
            	<div class="col-md-4 col-sm-6" style="margin-bottom: 20px;">
            		<div style="background-color: #f5f5f5; border: 1px solid black;">
						<h4 style="text-align: center; background-color: black; margin: auto; padding: 10px 0;">
							<a href="/videoLecture/videoLectureDetail/${ list.VIdx }" style="color: white; display: inline-block;">스터디 주제</a>
						</h4>
		                
		                <div class="profileContent" style="padding: 15px;">
							<div style="display: inline-block; vertical-align: top; width: 160px; height: 200px; overflow: hidden;">
								<img alt="프로필 사진" src="http://www.dlto.co.kr/wp-content/uploads/2015/11/bonobono_emot.gif" style="height: 100%;">
							</div>
							
							<div style="display: inline-block; padding: 0 0 0 10px;">
								<p>이름 : 보노보노</p>
								<p>스터디 장소</p>
								<p>스터디 날짜</p>
								<p>스터디 진행여부</p>
			                </div>
			                
			                <hr style="margin: 10px 0;"/>
			                
							<div style="padding: 0 0 0 10px;">
								<p style="font-size: 12px; float: right; margin-top: 3px;">업데이트 날짜</p>
								<p>강사 프로필</p>
								<pre>강사 프로필 내용</pre>
								<p style="display: inline-block;">한국어 등급 : </p>
			                </div>
			                
			                <hr style="margin: 10px 0;"/>
								<p style="display: inline-block;">강사님 좋아요</p>
								<p style="float: right;">신청인원 / 신청가능인원</p>
			                <hr style="margin: 10px 0;"/>
			                
			                <div>
			                	<input type="button" value="강사님 추천하기" class="btn" style="margin-left: 10px;">
			                	<input type="button" value="상세보기" class="btn" style="float: right; margin-right: 10px;">
			                </div>
	                    </div>
            		</div>
	            </div>
            <%-- </c:forEach> --%>
            <!--
            <c:if test="${ true }">
            	<div style="display: inline-block;">
            		<h1 style="margin: 60px 0;">등록된 스터디가 없습니다.</h1>
            	</div>
            </c:if>
            -->
        </div>
	</section>
	<jsp:include page="../default/footer.jsp" />
</body>
</html>