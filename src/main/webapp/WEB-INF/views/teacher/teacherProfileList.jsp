<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<title>Holic - 강사님 입니다.</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<style type="text/css">
@media ( min-width : 768px) {
	.group_form1 {
		width: 20%;
	}
	.group_form2 {
		width: 80%
	}
}

.profileNav h2 { 
	font-family: NANUMSQUAREER;
	font-weight: bold; 
	display: inline-block; 
}
.profileNav p { 
	float: right;
	font-size: 12px;
	margin: 40px 10px 0 0; 
}

.profileContent p { margin: 0 0 5px; }

.profileTeacher {
	background-color: #Fafafa; 
	padding: 10px; 
	border: 1px solid /* rgba(0,0,0,.08) */ black;
}

</style>
<script type="text/javascript">
$(document).ready(function(){
	
	// 진행중인 스터디
	$("#proceedingStudy").click(function(){
		alert('진행중인 스터디 설정 필요');
		location.href="../teacher/profileList";
	});
	
	// 종료된 스터디
	$("#endStudy").click(function(){
		alert('종료된 스터디 설정 필요');
		location.href="../teacher/profileList";
	});
	
	// 강사 이미지 클릭
	$(".profileTeacher img").click(function(){
		alert('해당 강사의 스터디 리스트를 출력할 수 있도록!');
		alert($(this).next("input[type=hidden]").val());
		
		location.href="../teacher/profileList";
	});
});
</script>
</head>
<body>
	<jsp:include page="../default/header.jsp" />
	<section id="main">
		<div class="profileNav">
			<h2>강사님</h2>
			<input id="proceedingStudy" type="button" value="진행중인 스터디" class="btn" style="display: inline-block; margin: 0 0 10px 10px;">
			<input id="endStudy" type="button" value="종료된 스터디" class="btn" style="display: inline-block; margin: 0 0 10px 10px;">
			<p>※ 원하는 강사님을 선택하여 스터디를 신청할 수 있습니다.</p>
		</div>
		
		<hr style="margin: 0 0 20px 0;"/>
		
		<div class="row">
			<!-- 강사 프로필 -->
			<%-- <c:forEach items="${ videoLectureList }" var="list"> --%>
				<div class="col-md-3 col-sm-6 profileTeacher">
					<div style="height: 200px; text-align: center;">
						<img alt="프로필 사진" src="http://www.dlto.co.kr/wp-content/uploads/2015/11/bonobono_emot.gif" style="height: 100%; cursor: pointer;">
						<input type="hidden" value="강사키값을넣으시오">
					</div>
					<h2 style="text-align: center;">보노보노</h2>
					
					<hr style="margin: 10px 0;"/>
					
					<p>한국어 등급 : </p>
					<p>한국어 성적(듣기) : </p>
					<p>한국어 성적(읽기) : </p>
					<p>한국어 성적(쓰기) : </p>
					
					<hr style="margin: 10px 0;"/>
					
					<p>프로필</p>
					<pre>프로필 내용</pre>
					
					<hr style="margin: 10px 0;"/>
					
					<p style="display: inline-block;">이 강사님을 추천합니다 : 추천수</p>
					<input type="button" value="추천" class="btn" style="font-size: 12px; float: right; margin-left: 10px; padding: 4px 6px;">
				</div>
            <%-- </c:forEach> --%>
            <!--
            <c:if test="${ true }">
            	<div style="display: inline-block;">
            		<h1 style="margin: 60px 0;">등록된 강사가 없습니다.</h1>
            	</div>
            </c:if>
            -->
        </div>
	</section>
	<jsp:include page="../default/footer.jsp" />
</body>
</html>