<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!-- HanJungIl - 작성 - 170610 ~ 170630 -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Holic - 운영자 페이지</title>
<style type="text/css">
.videoLectureNav > h2 { 
	font-family: NANUMSQUAREER;
	font-weight: bold; 
	display: inline-block;
	margin-top: 0;
}
.videoLectureNav > p {
	font-size: 12px; 
	float: right; 
	margin: 20px 0 0 0;
}
table {
	margin: auto; 
	margin-top: 20px; 
}
table tr, table th { text-align: center !important; }
#main > table > thead > tr > th { 
	background-color: black;
	color: white;
	padding: 10px 20px;
}
#main > table > thead > tr > th { border-left: 1px solid white; }
#main > table > thead > tr > th:FIRST-CHILD { border-left: none; }
#main > table > tbody > tr > td { color: black; }
#main > table > tbody > tr > td > a { 
	color: black;
	display: block;
	width: 100%; 
	height: 100%;
	padding: 10px 40px;
}
#main > table > tbody > tr > td > a:HOVER {
	color: white; 
	background-color: gray; 
}
#main table th { font-size: 20px; }
table tr > td:NTH-CHILD(2n-1) {
	background-color: #F5F5F5;
}
</style>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){});
</script>
</head>
<body>
	<jsp:include page="../default/header.jsp" />
	<section id="main">
		<div class="videoLectureNav">
			<h2>운영자 페이지</h2>
			<p>사이트를 운영하는 사람 나야나</p>
		</div>
		<hr style="margin-top: 0;"/>
		<table>
			<thead>
				<tr>
					<th>강의동영상</th>
					<th>영화/드라마</th>
					<th>강사/스터디</th>
					<th>공지/문의 게시판</th>
					<th>회원</th>
					<th>결제</th>
					<th>통계</th>
					<th>기타</th>
				</tr>
			</thead>
			<tbody>
				<tr><!-- 1 -->
					<td><a href="${pageContext.request.contextPath}/videoLecture/videoLectureList">강의글 리스트</a></td>
					<td><a href="javascript:alert('준비중');">리스트</a></td>
					<td><a href="javascript:alert('준비중');">강사 리스트</a></td>
					<td><a href="javascript:alert('준비중');">공지게시판</a></td>
					<td><a href="${pageContext.request.contextPath}/operator/operatorUserList">전체회원 리스트</a></td>
					<td><a href="javascript:alert('준비중');">결제관리</a></td>
					<td></td>
					<td><a href="javascript:alert('준비중');">회사소개</a></td>
				</tr>
				<tr><!-- 2 -->
					<td><a href="javascript:alert('준비중');">동영상 리스트</a></td>
					<td><a href="javascript:alert('준비중');">등록</a></td>
					<td><a href="javascript:alert('준비중');">스터디 리스트</a></td>
					<td><a href="javascript:alert('준비중');">문의게시판</a></td>
					<td><a href="${pageContext.request.contextPath}/operator/operatorUserList?userType=2">유료회원 리스트</a></td>
					<td><a href="javascript:alert('준비중');">쿠폰관리</a></td>
					<td></td>
					<td><a href="${pageContext.request.contextPath}/payment/paymentMain">안내</a></td>
				</tr>
				<tr><!-- 3 -->
					<td><a href="${pageContext.request.contextPath}/videoLecture/videoLecture">등록</a></td>
					<td><a href="javascript:alert('준비중');"></a></td>
					<td><a href="javascript:alert('준비중');">강사승인</a></td>
					<td><a href="javascript:alert('준비중');">문의댓글</a></td>
					<td><a href="${pageContext.request.contextPath}/operator/operatorUserList?userType=0">블럭회원 리스트</a></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr><!-- 4 -->
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td><a href="${pageContext.request.contextPath}/operator/operatorUserList?userType=5">강사회원 리스트</a></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</section>
	<jsp:include page="../default/footer.jsp" />
</body>
</html>