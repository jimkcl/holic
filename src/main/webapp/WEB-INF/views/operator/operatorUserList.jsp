<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:useBean id="now" class="java.util.Date" />
<!DOCTYPE html>
<!-- HanJungIl - 작성 - 170610 ~ 170630 -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Holic - 운영자 페이지 - 회원리스트</title>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<!-- jQuery UI CSS파일 --> 
<link rel="stylesheet" href="http://code.jquery.com/ui/1.8.18/themes/base/jquery-ui.css" type="text/css" />  
<!-- jQuery 기본 js파일 -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>  
<style type="text/css">
.operatorUserListNav h2 { 
	font-family: NANUMSQUAREER;
	font-weight: bold; 
	display: inline-block;
	margin-top: 0;
}
.operatorUserListNav > p {
	font-size: 12px; 
	color: red;
	float: right; 
	margin: 20px 0 0 0;
}
.userListCheckDiv { 
	text-align: center; 
	font-size: 12px;
	display: inline-block;
	margin-left: 10px;
}
#main ul { 
	margin: 0; 
	padding: 0; 
}
.userListCheckDiv P { 
	display: inline-block;
	margin: 0; 
}
.userTypeList { 
	float: right;
	margin-top: 5px;
	margin-right: 10px; 
}
.userTypeList > ul > li,
.userListCheckDiv > ul > li {
	font-family: NANUMSQUAREER;
	font-weight: bold; 
	display: inline-block;
	padding: 5px;
}
.userListCheckDiv input {
	width: 20px;
	height: 20px;
}
.userTypeList a {
	font-family: NANUMSQUAREER;
	font-weight: bold;
	color: black; 
}
.userTypeList a:HOVER {
	font-family: NANUMSQUAREER;
	font-weight: bold; 
	color: white;
}

/* 관리버튼 */
.userListManagementDiv { text-align: right; }
.userListManagementDiv ul { display: inline-block; }
.userListManagementDiv ul li { display: inline-block; }
.userListManagementDiv ul li:HOVER { color: white; }
.userListManagementDiv ul li input {
	font-family: NANUMSQUAREER;
	color: black; 
}

.operatorUserListContent { padding: 15px; }
.operatorUserListContent table th,
.operatorUserListContent table {
	text-align: center; 
	font-size: 13px;
	padding: 10px;
}
.operatorUserListContent table { 
	width: 100%;
	margin-bottom: 20px; 
	border-bottom: 1px solid gray;
}
.operatorUserListContent table tr { border-bottom: 1px solid rgba(0,0,0,0.1); }
.operatorUserListContent table th { 
	color: white;
	background-color: black;
}
.operatorUserListContent table td { padding: 10px; }
.operatorUserListContent tbody tr:HOVER { background-color: #F5F5F5 !important; }
tr > td:FIRST-CHILD > input {
	width: 20px;
	height: 20px;
}
.operatorUserListContent table .vTitle { text-align: left; }
.operatorUserListContent table .vDate { font-size: 12px; }
tbody .infoNick { 
	font-family: NANUMSQUAREER;
	font-weight: bold; 
}

.infoPayEnd, .infoJoinDay { font-size: 11px; }

.videoLecturePageNumberDiv { 
	display: inline-block;
	margin-left: 10px; 
}
.videoLectureSearchDiv { 
	float: right; 
	margin-right: 10px;
}
.videoLectureSearchDiv select { padding: 7px; }
.videoLectureSearchDiv input:NTH-CHILD(2) {
	box-shadow: 0 0 0 1000px white inset;
	width: 200px;
	padding: 6px;
	border: 1px solid rgb(169, 169, 169);
}
</style>
<script type="text/javascript">
$(document).ready(function(){
	// 리스트 컬럼 보기 설정
	$(".userListCheckDiv input[type=checkbox]").click(function(){
		var className = "." + $(this).attr("class");
		
		if($(this).prop("checked")){
			$(className).css("display", "table-cell");
		}else{
			$(className).css("display", "none");
		}
		
		$(this).css("display", "table-cell");
	});
	
	// 전체 선택 버튼
	$("thead .infoCheck input").click(function(){
		var $checkList = $(".infoCheck input[type=checkbox]");
		var checked = $(this).prop("checked");
		
		for(i=0 ; i < $checkList.length ; ++i){
			$checkList.eq(i).prop("checked", checked);
		}
	});
	
	// 관리 열기 버튼
	$("input[name=manageBtn]").click(function(){
		var $manageDiv = $(".userListManagementDiv");
		var $infoCheck = $(".infoCheck");
		
		if($manageDiv.css("display") == "block"){
			$manageDiv.css("display", "none");
			$infoCheck.css("display", "none");
		}else{
			$manageDiv.css("display", "block");
			$infoCheck.css("display", "block");
		}
	});
	
	/****** 관리버튼 ******/
	// 날짜창 생성 및 설정
	var dayCheckFunction = function(element){
		var $checkList = $(".infoCheck input[type=checkbox]");
		var check = 0;
		
		for(i=0 ; i < $checkList.length ; ++i){
			if($checkList.eq(i).prop("checked")) {
				check += 1;
			}
		}
		
		if(check  == 0) {
			alert('선택된 회원이 없습니다.');
			return;
		}
		var date = new Date();
		var month = date.getMonth() + 1;
		var day = date.getDate() + 1;
		var daysMax = 31;
		
		if(month == 4 || month == 6 || month == 9 || month == 11) {
			daysMax = 30;
		}else if(month == 2) {
			daysMax = 29;
		}
		
		var textHtml = '<div style="position: absolute; width: 100%; height: 110%; background: rgba(0,0,0,0.5); top: 0; left: 0; z-index: 1;">'
			+ '<div style="width: 400px; background-color: white; margin: auto; margin-top: 25%; border-radius: 5px; padding: 25px 35px">'
				+ '<table style="width: 100%;">'
					+'<tr><td style="width: 50%;">'
						+ '<select name="yyyy" style="width: 100%; padding: 5px;">';
		
		for(i=0; i < 10 ;++i){
			textHtml += ('<option value="' + (date.getFullYear() + i) + '">' 
							+ (date.getFullYear() + i) + '년</option>');
		}
		
		textHtml += ('</select></td>'
					+ '<td style="width: 25%;">'
						+ '<select name="MM" style="width: 100%; padding: 5px;">'
							+ '<option value="' + month + '">' + month + '월</option>');
		
		for(i=month; i < 12 ;++i){
			textHtml += ('<option value="' + (1 + i) + '">' + (1 + i) + '월</option>');
		}
		
		textHtml += '</select></td>'
					+ '<td style="width: 25%;">'
						+ '<select name="dd" style="width: 100%; padding: 5px;">';
		
		for(i=day; i <= daysMax ;++i){
			textHtml += ('<option value="' + i + '">' + i + '일</option>');
		}
		
		textHtml += '</select></td></tr>'
				+ '<tr style="height: 40px; text-align: center;">'
					+ '<td colspan="3">진행중인 메뉴에 대한 설명</td></tr>'
				+ '<tr style="text-align: center;">'
					+ '<td colspan="3">'
						+ '<input type="hidden" value="' + element.attr("name") + '">'
						+ '<input type="button" value="확인" id="userDataSubmit" class="btn" style="width: inherit; height: inherit;">'
						+ '<input type="button" value="취소" id="userDataCancel" class="btn" style="width: inherit; height: inherit; margin-left: 5px;">'
				+ '</td></tr></table></div></div>';
		
		$("#dateSelectDiv").append(textHtml);
	}
	
	// 날짜 DIV 제거
	$("#dateSelectDiv").on("click", "#userDataCancel", function(){
		$(this).parent("td").parent("tr").parent("tbody").parent("table").parent("div").parent("div").remove();
	});
	
	// 년도 변경시 월 설정
	$("#dateSelectDiv").on("change", "select[name=yyyy]", function(){
		var $selectMM = $("#dateSelectDiv select[name=MM]");
		var date = new Date();
		var textHtml = '';
		var month = 1;
		
		if($(this).val() == date.getFullYear()) month = date.getMonth() + 1;
		
		for(i=month; i < 12 ;++i){
			textHtml += ('<option value="' + i + '">' + i + '월</option>');
		}
		
		$selectMM.empty();
		$($selectMM).append(textHtml);
	});	
	
	// 월 변경시 일 설정
	$("#dateSelectDiv").on("change", "select[name=MM]", function(){
		var $selectdd = $("#dateSelectDiv select[name=dd]");
		var date = new Date();
		var textHtml = '';
		var month = $(this).val();
		var days = 1;
		var daysMax = 31;
		
		if(month == (date.getMonth() + 1)) days = date.getDate();
		
		if(month == 4 || month == 6 || month == 9 || month == 11) {
			daysMax = 30;
		}else if(month == 2) {
			daysMax = 29;
		}
		
		for(i=days; i <= daysMax ;++i){
			textHtml += ('<option value="' + i + '">' + i + '일</option>');
		}
		
		$selectdd.empty();
		$($selectdd).append(textHtml);
	});
	
	// 확인시 날짜검증 후 전송
	$("#dateSelectDiv").on("click", "#userDataSubmit", function(){
		var name = $(this).prev("input[type=hidden]").val();
		var yyyyMMdd = $("#dateSelectDiv select[name=yyyy]").val() + "-" 
						+ $("#dateSelectDiv select[name=MM]").val() + "-" 
						+ $("#dateSelectDiv select[name=dd]").val();
		var $checkList = $(".infoCheck input[type=checkbox]");
		var userList = new Array();
		var check = 0;
		
		for(i=0 ; i < $checkList.length ; ++i){
			if($checkList.eq(i).prop("checked")) {
				userList.push($checkList.eq(i).val());
				check += 1;
			}
		}
		
		if(check  == 0) {
			alert('선택된 회원이 없습니다.');
			return;
		}
		
		if(!confirm("회원정보를 변경하시겠습니까?\n\n변경 : " + name)) return;
		
		$.ajaxSettings.traditional = true;
		
		$.ajax({
			url		: "../operator/operatorUserChange",
			data	: { "buttonType": name,
						"yyyyMMdd"	: yyyyMMdd,
						"userKeyList": userList },
			type	: "post",
			dataType: "text",
			success	: function(data){
				alert("회원 정보가 변경되었습니다.");
				location.reload();
			},
			error	: function(data){
				alert("정보 변경에 실패하였습니다.\n확인 후 다시 시도해 주십시오.");
				console.log(data);
			}
		});
	});
	
	
	// 관리버튼 - 블럭
	$("input[name=userBlock]").click(function(){ dayCheckFunction($(this)); });
	
	// 관리버튼 - 유료
	$("input[name=userPaid]").click(function(){ dayCheckFunction($(this)); });
	
	// 관리 버튼 - 그 외
	$(".userListManagementDiv input[type=button]").click(function(){
		var name = $(this).attr("name");
		
		if(name == "userBlock" || name == "userPaid") return;
		
		var $checkList = $(".infoCheck input[type=checkbox]");
		var userList = new Array();
		var check = 0;
		
		for(i=0 ; i < $checkList.length ; ++i){
			if($checkList.eq(i).prop("checked")) {
				userList.push($checkList.eq(i).val());
				check += 1;
			}
		}
		
		if(check  == 0) {
			alert('선택된 회원이 없습니다.');
			return;
		}
		
		if(name != "userNormal" 
				&& !confirm("회원정보를 변경하시겠습니까?\n\n변경 : " + $(this).attr("value"))) {
			return;
		}else if(name == "userRealDelete" 
				&& !confirm("회원 정보를 '삭제' 하려합니다.\n계속하시겠습니까?\n\n삭제하면 되돌릴 수 없습니다.")) {
			return;
		}
		
		
		$.ajaxSettings.traditional = true;
		
		$.ajax({
			url		: "../operator/operatorUserChange",
			data	: { "buttonType" : $(this).attr("name") ,
						"userKeyList" : userList },
			type	: "post",
			dataType: "text",
			success	: function(data){
				alert("회원 정보가 변경되었습니다.");
				location.reload();
			},
			error	: function(data){
				console.log(data);
			}
		});
	});
	
	// 검색
	$(".videoLectureSearchDiv input[type=button]").click(function(){
		var inputText = $(this).parent("form").children("input[name=inputText]");
		
		if(inputText.val().replace(/(^\s*)|(\s*$)/gi, "") == '' ) {
			inputText.focus();
		}else{
			$(this).parent("form").submit();
		}
	});
});
</script>
</head>
<body>
	<jsp:include page="../checkPage/operatorCheck.jsp" />
	<jsp:include page="../default/header.jsp" />
	<section id="main">
		<div class="operatorUserListNav">
			<h2>회원 리스트</h2>
			<input type="button" name="manageBtn" class="btn" value="관리" style="padding: 3px 6px; margin: 0 0 8px 10px;">
			<p>※ 경고 : 사측의 동의 없이 정보를 이용 및 유출할 경우 책임을 물을 수 있습니다.</p>
		</div>
		
		<hr style="margin-top: 0; margin-bottom: 15px;"/>
		
		<!-- 보기선택 -->
		<div class="userListCheckDiv">
			<ul>
				<li><div>
					<p>위챗</p><br/>
					<input type="checkbox" class="infoWechat" checked="checked">
				</div></li>
				<li><div>
					<p>닉네임</p><br/>
					<input type="checkbox" class="infoNick" checked="checked">
				</div></li>
				<li><div>
					<p>메일</p><br/>
					<input type="checkbox" class="infoMail" checked="checked">
				</div></li>
				<li><div>
					<p>지역</p><br/>
					<input type="checkbox" class="infoArea" checked="checked">
				</div></li>
				<li><div>
					<p>학교</p><br/>
					<input type="checkbox" class="infoSchool" checked="checked">
				</div></li>
				<li><div>
					<p>학과</p><br/>
					<input type="checkbox" class="infoDepartment" checked="checked">
				</div></li>
				<li><div>
					<p>우편번호</p><br/>
					<input type="checkbox" class="infoPost" checked="checked">
				</div></li>
				<li><div>
					<p>주소</p><br/>
					<input type="checkbox" class="infoAddress1" checked="checked">
				</div></li>
				<li><div>
					<p>상세주소</p><br/>
					<input type="checkbox" class="infoAddress2" checked="checked">
				</div></li>
				<li><div>
					<p>생년월일</p><br/>
					<input type="checkbox" class="infoBirth" checked="checked">
				</div></li>
				<li><div>
					<p>성별</p><br/>
					<input type="checkbox" class="infoGender" checked="checked">
				</div></li>
				<li><div>
					<p>결제종료일</p><br/>
					<input type="checkbox" class="infoPayEnd" checked="checked">
				</div></li>
				<li><div>
					<p>가입일시</p><br/>
					<input type="checkbox" class="infoJoinDay" checked="checked">
				</div></li>
			</ul>
		</div>
		
		<!-- 회원별 보기 -->
		<div class="userTypeList">
			<ul>
				<c:if test="${ sessionScope.LOGIN.userType == 9 }">
					<li style="background-color: #FFB0CF;">
						<a href="${pageContext.request.contextPath}/operator/operatorUserList?userType=-1">삭제회원</a>
					</li>
				</c:if>
				<li style="background-color: #FFC6A5;">
					<a href="${pageContext.request.contextPath}/operator/operatorUserList?userType=0">블럭회원</a>
				</li>
				<li style="background-color: rgba(0,0,0,0.1);">
					<a href="${pageContext.request.contextPath}/operator/operatorUserList?userType=1">일반회원</a>
				</li>
				<li style="background-color: #AFFFEE;">
					<a href="${pageContext.request.contextPath}/operator/operatorUserList?userType=2">유료회원</a>
				</li>
				<li style="background-color: rgba(0,0,255,0.3);">
					<a href="${pageContext.request.contextPath}/operator/operatorUserList?userType=5">강사회원</a>
				</li>
				<li style="background-color: #CBFF75;">
					<a href="${pageContext.request.contextPath}/operator/operatorUserList?userType=8">운 영 자</a>
				</li>
				<li style="background-color: black;">
					<a href="${pageContext.request.contextPath}/operator/operatorUserList" style="color: white;">전&nbsp;&nbsp;&nbsp;&nbsp;체</a>
				</li>
			</ul>
		</div>
		
		<hr style="margin: 5px 0 10px 0;"/>
		
		<!-- 관리버튼 -->
		<div class="userListManagementDiv" style="margin-left: 0 20px; display: none;">
			<p style="float: left; margin: 15px 0 0 10px;">※ 선택 후 우측 버튼을 눌려주세요.</p>
			<ul>
				<c:if test="${ sessionScope.LOGIN.userType == 9 }">
					<li>
						<input type="button" name="userRealDelete" class="btn" 
								value="회원완전삭제" style="background-color: rgb(255, 100, 100); margin-right: 10px;"">
					</li>
				</c:if>
				<li>
					<input type="button" name="userDelete" class="btn" 
							value="회원삭제" style="background-color: rgb(255, 176, 207);">
				</li>
				<li>
					<input type="button" name="userBlock" class="btn" 
							value="회원블럭" style="background-color: rgb(255, 198, 165);">
				</li>
				<li>
					<input type="button" name="userNormal" class="btn" 
							value="일반회원전환" style="background-color: rgba(0,0,0,0.1); margin-left: 10px;">
				</li>
				<li>
					<input type="button" name="userPaid" class="btn" 
							value="유료회원전환" style="background-color: #AFFFEE;">
				</li>
				<li>
					<input type="button" name="userTeacher" class="btn" 
							value="강사회원전환" style="background-color: rgba(0,0,255,0.3); margin-left: 10px;">
				</li>
				<c:if test="${ sessionScope.LOGIN.userType == 9 }">
					<li>
						<input type="button" name="userOperator" class="btn" 
								value="운영자전환" style="background-color: rgb(203, 255, 117); margin-left: 10px;">
					</li>
				</c:if>
			</ul>
			<hr style="margin: 10px 0 0 0;"/>
		</div>
		
		<div class="operatorUserListContent">
			<table>
				<!-- 회원 리스트 상단 -->
				<thead>
					<tr>
						<th class="infoCheck" style="display: none;">
							<input type="checkbox" style="width: 15px; height: 15px;"> 선택
						</th>
						<th>아이디</th>
						<th class="infoWechat">위챗</th>
						<th class="infoNick">닉네임</th>
						<th class="infoMail">메일</th>
						<th class="infoArea">지역</th>
						<th class="infoSchool">학교</th>
						<th class="infoDepartment">학과</th>
						<th class="infoPost">우편번호</th>
						<th class="infoAddress1">주소</th>
						<th class="infoAddress2">상세주소</th>
						<th class="infoBirth">생년월일</th>
						<th class="infoGender">성별</th>
						<th class="infoPayEnd">종료일</th>
						<th class="infoJoinDay">가입일시</th>
					</tr>
				</thead>
				
				<!-- 회원리스트 -->
				<tbody>
					<c:forEach items="${ list }" var="user">
						
						<!-- 삭제 회원 -->
						<c:if test="${ user.userType == -1 && sessionScope.LOGIN.userType == 9 }">
							<tr style="background-color: #FFB0CF;">
						</c:if>
						
						<!-- 블럭 회원 -->
						<c:if test="${ user.userType == 0 }">
							<tr style="background-color: #FFC6A5;">
						</c:if>
						
						<!-- 일반 회원 -->
						<c:if test="${ user.userType == 1 }">
							<tr style="background-color: rgba(0,0,0,0.1);">
						</c:if>
						
						<!-- 유료 회원 -->
						<c:if test="${ user.userType == 2 }">
							<tr style="background-color: #AFFFEE;">
						</c:if>
						
						<!-- 강사 회원 -->
						<c:if test="${ user.userType == 5 }">
							<tr style="background-color: rgba(0,0,255,0.3);">
						</c:if>
						
						<!-- 운영자 -->
						<c:if test="${ user.userType == 8 }">
							<tr style="background-color: #CBFF75;">
						</c:if>
						
							<!-- 선택 -->
							<c:if test="${ user.userType != -1 || (user.userType == -1 && sessionScope.LOGIN.userType == 9) }">
								<td class="infoCheck" style="display: none;">
									<input type="checkbox" name="userCheck" value="${ user.userKey }">
								</td>
								
								<!-- 아이디 -->
								<td>
									<input type="hidden" value="userId">
									${ user.userLoginVO.userId }
								</td>
								
								<!-- 위챗 -->
								<td class="infoWechat">
									<input type="hidden" value="userWechat">
									${ user.userWechat }
								</td>
								
								<!-- 닉네임 -->
								<td class="infoNick">
									<input type="hidden" value="userNick">
									${ user.userNick }
								</td>
								
								<!-- 메일 -->
								<td class="infoMail">
									<input type="hidden" value="userMail">
									${ user.userMail }
								</td>
								
								<c:set var="userSchool" value="${ fn:split(user.userSchool, '+') }" />
								
								<!-- 지역 -->
								<td class="infoArea">
									<input type="hidden" value="userArea">
									${ userSchool[0] }
								</td>
								
								<!-- 학교 -->
								<td class="infoSchool">
									<input type="hidden" value="userSchool">
									${ userSchool[1] }
								</td>
								
								<!-- 학과 -->
								<td class="infoDepartment">
									<input type="hidden" value="userDepartment">
									${ userSchool[2] }
								</td>
								
								<!-- 우편번호 -->
								<td class="infoPost">
									<input type="hidden" value="userPost">
									<c:if test="${ user.userPost != 0 }">${ user.userPost }</c:if>
								</td>
								
								<!-- 주소 -->
								<td class="infoAddress1">
									<input type="hidden" value="userAddress1">
									${ user.userAddress1 }
								</td>
								
								<!-- 상세주소 -->
								<td class="infoAddress2">
									<input type="hidden" value="userAddress2">
									${ user.userAddress2 }
								</td>
								
								<!-- 생년월일 -->
								<td class="infoBirth">
									<input type="hidden" value="userBirth">
									${ user.userBirth }
								</td>
								
								<!-- 성별 -->
								<td class="infoGender">
									<input type="hidden" value="userGender">
									<c:if test="${ user.userGender != 0 }">${ user.userGender }</c:if>
								</td>
								
								<!-- 결제종료일 -->
								<td class="infoPayEnd">
									<input type="hidden" value="pDate">
									<fmt:parseDate var="data" value="${ fn:substring(user.userPaymentVO.PDate,0,19) }" pattern="yyyy-MM-dd HH:mm:ss"/>
									<fmt:formatDate value="${ data }" pattern="yyyy년 MM월 dd일 hh시 mm분 ss초"/>
								</td>
								
								<!-- 가입일시 -->
								<td class="infoJoinDay">
									<input type="hidden" value="userDate">
									<fmt:parseDate var="data" value="${ fn:substring(user.userDate,0,19) }" pattern="yyyy-MM-dd HH:mm:ss"/>
									<fmt:formatDate value="${ data }" pattern="yyyy년 MM월 dd일 hh시 mm분 ss초"/>
								</td>
							</c:if>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<c:if test="${ fn:length(list) == 0 }">
				<p style="text-align: center; font-family: NANUMSQUAREER; font-size: 20px; font-weight: bold; margin: 40px 0 20px 0;">해당 회원이 없습니다.</p>
			</c:if>
		</div>
		
		<hr style="margin: 10px 0;"/>
		
		<!-- 페이지 -->
		<div class="videoLecturePageNumberDiv">
			<c:if test="${ page.firstPage != 1 }">
				<input type="button" value="&#60;&#60;" class="btn pageBtn" style="width: 50px;">&nbsp;&nbsp;
				<input type="button" value="&#60;" class="btn pageBtn" style="width: 50px;">&nbsp;&nbsp;
			</c:if>
			
			<c:forEach var="idx" end="${ page.endPage }" begin="${ page.firstPage }">
				<c:if test="${ page.pageNum != idx }">
					<input type="button" value="${ idx }" class="btn pageBtn" style="width: 50px;">&nbsp;&nbsp;
				</c:if>
				<c:if test="${ page.pageNum == idx }">
					<input type="button" value="${ idx }" class="btn pageBtn" style="width: 50px; background-color: rgb(200, 200, 200);">&nbsp;&nbsp;
				</c:if>
			</c:forEach>
			
			<c:if test="${ page.endBtnCheck }">
				<input type="button" value="&#62;" class="btn pageBtn" style="width: 50px;">&nbsp;&nbsp;
				<input type="button" value="&#62;&#62;" class="btn pageBtn" style="width: 50px;">
			</c:if>
		</div>
		
		<!-- 검색 -->
		<div class="videoLectureSearchDiv">
			<form>
				<select name="type">
					<option value="userId">아이디</option>
					<option value="userWechat">위챗</option>
					<option value="userNick">닉네임</option>
					<option value="userMail">메일</option>
					<option value="userArea">지역</option>
					<option value="userBirth">생년월일</option>
				</select>
				<input type="search" name="inputText">
				<input type="button" value="검색" class="btn">
			</form>
		</div>
		<div id="dateSelectDiv"></div>
	</section>
	<jsp:include page="../default/footer.jsp" />
</body>
</html>