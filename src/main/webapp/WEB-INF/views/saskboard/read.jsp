<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Insert title here</title>
<style type="text/css">

</style>
</head>
<body>
	<jsp:include page="../default/header.jsp" />
	<%-- <!-- 로그인한 유저 닉네임과 글을 쓴 작성자와 같으면.. -->
	<c:if test="${sessionScope.LOGIN.userNick == vo.userNick}">
		<script type="text/javascript">
			location.href="/";
		</script>
	</c:if>  --%>
	
	<form role="form" method="post">
		<input value="${vo.idx}" name="idx" type="hidden">
		<input value="${cri.page}" name="page" type="hidden">
		<input value="${cri.perPageNum}" name="perPageNum" type="hidden">
		<input value="${cri.searchType}" name="searchType" type="hidden">	
		<input value="${cri.keyword}" name="keyword" type="hidden">	
	</form>
	
	<div class="container">
	
		<div class="row">
			
			<div class="form-group">
				<label class="control-label" for="userNick">작성자</label>
				<div>
					<input readonly="readonly" value="${vo.userNick}" name="userNick" class="form-control"> <!-- 수정금지 -->
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label" for="aTitle">제목</label>
				<div>
					<input readonly="readonly" value="${vo.aTitle}" name="aTitle" class="form-control"> <!-- 수정금지 -->
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label" for="aContent">내용</label>
				<div>
					<textarea readonly="readonly" name="aContent" id="aContent" rows="10" class="form-control">${vo.aContent}</textarea><br>
				</div>
			</div>
			
		</div>
		
		<div class="form-group">
			<div align="center" class="col-sm-offset-2 col-sm-10">
				<button class="btn btn-default" type="reset">삭제</button>
				<button class="btn btn-primary" type="submit">목록</button>
			</div>	
		</div>	
			
	</div>

	<script type="text/javascript">
		$(document).ready(function(){
			var $form=$("form[role='form']");
			
			$(".btn-reset").on("click", function(){
				$form.attr("method", "post");
				$form.attr("action", "/delete");
				$form.submit();
			});
			
			$(".btn-primary").click(function(){
				$form.attr("method", "get");
				$form.attr("action", "/askListCriteria");
				$form.submit();
			});
			
		});
		
	</script>

	<jsp:include page="../default/footer.jsp" />
</body>
</html>