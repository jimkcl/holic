<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!-- HanJungIl - 작성 - 170610 ~ 170630 -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Holic - 결제하기</title>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bs/css/bootstrap.css">
<style type="text/css">
.paymentNav h2 { 
	font-family: NANUMSQUAREER;
	font-weight: bold; 
	display: inline-block; 
}
.paymentNav p { 
	float: right;
	font-size: 12px;
	margin: 40px 10px 0 0; 
}

.paymentContent { text-align: center; }
.paymentContent > div > p {
	text-align: left; 
	font-size: 12px; 
	margin: 0 0 5px 10px;
}

.paymentSideMenu {
	text-align: left;
}
.paymentSideMenu ul {
	list-style: none;
	padding: 0;
}
.paymentSideMenu ul li {
	cursor: pointer;
	font-size: 20px;
	color: white;
	background-color: black;
	padding: 10px 20px;
	border-bottom: 1px solid gray;
}
.paymentSideMenu ul li:HOVER { background-color: gray; }
.paymentSideMenu ul li:LAST-CHILD { border-bottom: 0;}

#paymentDiv {
	display: inline-table; 
	padding: 0 20px 20px 20px; 
	border-left: 2px dashed black;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
	
	// 페이지 초기화 메서드
	
	var cleanDiv = function(){
		// 결제 div 삭제
		$("#paymentDiv").empty();
	}
	
	
	// 안내
	// 안내메뉴
	$("#guidanceBtn").click(function(){
		var $paymentDiv = $("#paymentDiv");
		cleanDiv();
		$paymentDiv.load("../payment/paymentInformation");
	});
	
	//****** 결제 ******
	// 결제메뉴
	$("#paymentBtn").click(function(){
		var $paymentDiv = $("#paymentDiv");
		cleanDiv();
		$paymentDiv.load("../payment/paymentSet");
	});
	
	// 쿠폰메뉴
	$("#couponBtn").click(function(){
		var $paymentDiv = $("#paymentDiv");
		cleanDiv();
		$paymentDiv.load("../payment/coupon");
	});
});
</script>
</head>
<body>
	<jsp:include page="../default/header.jsp" />
	<section id="main">
		<div class="paymentNav">
			<h2>결제/안내</h2>
			<p>※ 결제를 통해 보다 다양한 서비스를 이용하실 수 있습니다.</p>
		</div>
		
		<hr style="margin: 0 0 20px 0;"/>
		
		<!-- 월 선택 -->
		<div class="paymentContent row">
			<div class="col-md-3 paymentSideMenu">
				<ul>
					<li id="guidanceBtn">안내</li>
					<li id="paymentBtn">결제</li>
					<li id="couponBtn">쿠폰</li>
				</ul>
			</div>
			<div id="paymentDiv" class="col-md-9">
				<jsp:include page="./paymentInformation.jsp"/>
			</div>
		</div>
	</section>
	<jsp:include page="../default/footer.jsp" />
</body>
</html>