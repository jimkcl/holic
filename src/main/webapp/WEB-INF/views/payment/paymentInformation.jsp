<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- HanJungIl - 작성 - 170610 ~ 170630 -->
<style type="text/css">
.paymentDivHeader, .paymentTypeHeader {
	text-align: right;
	margin: auto;
	margin-left: 20px;
}
.paymentDivHeader h2 {
	font-family: NANUMSQUAREER;
	font-weight: bold;
	display: inline-block;
}
.paymentDivHeader p {
	float: right;
    font-size: 12px;
    margin: 40px 10px 0 0;
}
.paymentBody img { width: 100%; }
</style>
<div class="paymentDivHeader">
	<h2>안내</h2>
</div>

<hr style="margin: 0 0 20px 5px;"/>

<div class="paymentInfoDiv">
	<!-- 
	<div class="paymentHeader">
		<h2 align="left">이벤트</h2>
	</div>
	-->
	<div class="paymentBody">
		<img src="${pageContext.request.contextPath}/img/payment/con2.jpg">
	</div>
</div>