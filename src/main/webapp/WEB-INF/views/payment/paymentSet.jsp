<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- HanJungIl - 작성 - 170610 ~ 170630 -->
<style type="text/css">
.paymentContent > div > p {
	text-align: left; 
	font-size: 12px; 
	margin: 0 0 5px 10px;
}

.paymentDivHeader, .paymentTypeHeader {
	text-align: right;
	margin: auto;
	margin-left: 20px;
}
.paymentDivHeader h2 {
	font-family: NANUMSQUAREER;
    font-weight: bold;
    display: inline-block;
}
.paymentDivHeader p {
	float: right;
    font-size: 12px;
    margin: 40px 10px 0 0;
}

.paymentMonthMenu { margin-bottom: 40px; }
.paymentMonthMenu > .paymentBlind {
	display: table-column-group;
	background-color: rgba(0,0,0,0.3);
}
.paymentMonthMenu {
	font-family: NANUMSQUAREER;
	font-weight: bold;
	text-align: center;
	color: black;
	background-color: white;
	display: inline-table;
	width: 230px;
	margin-right: 10px;
	border: 2px solid black;
}
.paymentMonthMenu:HOVER { background-color: rgb(200,200,200); }
.paymentMonthMenu:LAST-CHILD { margin-right: 0; }
.paymentHeader {
	color: white;
	background-color: black;
	width: 150px;
	height: 150px;
	margin: auto;
	margin-top: 20px;
	margin-bottom: 40px;
	border-radius: 100px;
}
.paymentMonthMenu > .paymentHeader > h2 {
	font-family: NANUMSQUAREER;
	font-weight: bold;
	font-size: 90px;
	margin: 0;
	padding: 20px 20px 0 20px;
}
.paymentMonthMenu > .paymentHeader > h4 {
	font-weight: bold;
	font-size: 20px;
	text-align: right;
	margin: -5px 30px 0 0; 
	padding: 0 0 20px 0;
}

.paymentMonthMenu > p { text-align: left; }

.paymentLeftDiv {
	width: 10px;
	height: 200px;
	background-color: black;
	float: left;
	margin: 0 0 0 -20px;
}

.paymentMonthMenu > .paymentBody {
	padding: 0 20px 20px 20px;
}
.paymentBody > .paymentCalc {
	width: 100%;
	margin: auto;
	margin-bottom: 15px;
	border-top: 2px solid black;
	border-bottom: 1px solid black;
}
.paymentBody > .paymentCalc > p {
	font-size: 20px;
	display: inline-block;
	margin: 0;
}
.paymentBody > .paymentCalc > p > b { font-size: 40px; }

/**** 결제방식 선택 ****/
.paymentType {
	font-family: NANUMSQUAREER;
	font-weight: bold; 
	text-align: left;
	float: right;
	display: inline-block;
	width: 400px;
	padding: 20px;
	margin-top: 190px;
	border: 6px solid;
}
.paymentTypeHeader h2 {
	font-family: NANUMSQUAREER;
	font-weight: bold; 
	display: inline-block; 
}
.paymentTypeHeader p { 
	font-size: 12px;
	float: right;
	margin: 40px 10px 0 0; 
}

.paymentTypeBody > p {
	cursor: pointer;
	display: inline-block;
	width: 100%;
	margin-left: 10px;
}
.paymentTypeBody > p:HOVER { background-color: rgb(180,180,180); }
.paymentText > p {
	display: block;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
	$(".paymentMonthMenu .paymentBody input[type=button]").click(function(){
		// 월 선택
		var $monthMenu = $(this).parent(".paymentBody").parent(".paymentMonthMenu");
		var radio = $(this).next("input[type=radio]");
		
		if(!radio.is(":checked")){
			$monthMenu.css("backgroundColor", "rgb(255,180,180)");
			$(this).attr("value", "취소");
			radio.prop("checked", true);
			$("#checkedMonth").text("기간 : " + $(this).next().val() + "개월");
			$("#checkedPayment").text("금액 : " + $(this).parent(".paymentBody").children(".paymentCalc").children("p").children("b").text() + "원");
		}else{
			$monthMenu.css("backgroundColor", "");
			$(this).attr("value", "선택");
			radio.prop("checked", false);
		}
	});
	
	// 결제방식 선택
	$(".paymentTypeBody").children("p").click(function(){
		var $paymenyTypeRadio = $(this).prev("input[name=paymenyTypeRadio]");
		var pList = $(".paymentTypeBody").children("p");
		
		if($paymenyTypeRadio.attr("disabled") != "disabled"){
			for(i=0; i < pList.length ;++i){
				pList.eq(i).css("backgroundColor", "");
				pList.eq(i).css("color", "");
			}
			
			$paymenyTypeRadio.prop("checked", true);
			$("#checkedPaymentType").text("결제방식 : " + $(this).text());
			$(this).css("backgroundColor", "black");
			$(this).css("color", "white");
		}
	});
	
	// 결제
	$("#paymenyBtn").click(function(){
		if(${sessionScope.LOGIN == null || sessionScope.LOGIN.userKey == null}){
			if(confirm("로그인후 이용가능합니다.\n\n로그인 하시겠습니까?")){
				location.href="../user/login"
			}
			return;
		}else if(${sessionScope.LOGIN.userType != 1}){
			alert("현재 일반회원만 이용가능합니다.");
			return;
		}
		
		var $selectMonthList = $("input[name=selectMonth]");
		var $paymenyTypeRadio = $("input[name=paymenyTypeRadio]");
		
		var monthCheck = 0;
		var payTypeCheck = 0;
		
		var month = '';
		var payType = '';
		
		for(i=0 ; i < $selectMonthList.length ; ++i){
			if($selectMonthList.eq(i).is(":checked")) {
				monthCheck += 1;
				month = $selectMonthList.eq(i).val();
			}
		}
		
		for(i=0 ; i < $paymenyTypeRadio.length ; ++i){
			if($paymenyTypeRadio.eq(i).is(":checked")) {
				payTypeCheck += 1;
				payType = $paymenyTypeRadio.eq(i).next().text();
			}
		}
		
		if(monthCheck != 1){
			alert("기간이 선택되지 않았습니다.");
			return;
		}else if(payTypeCheck != 1){
			alert("결제방식이 선택되지 않았습니다.");
			return;
		}else if(!confirm("해당사항에 대한 결제를 진행합니다.\n\n기간 : " + month + " 개월" 
					+ "\n결제방식 : " + payType
					+ "\n\n※ 결제 및 환불에 관한 사항은 약관에 기재된 정책에 따라 진행됩니다.")){
			return;
		}
		
		$.ajax({
			url		: "/payment/paymentChangeUser",
			type	: "post",
			data	: { "pDate" : month, "payType" : payType },
			dataType: "text",
			success : function(data){
				if(data == 1) {
					alert("결제가 완료되었습니다.");
				}else{
					alert("결제가 되지 않았습니다.");
				}
			},
			error	: function(xhr, status, error){
				console.log(xhr + " / " + status + " / " + error);
			}
		});
	});
});
</script>
<div class="paymentDivHeader">
	<h1 style="color: red; float: left; margin-top: 10px;">결제를 이용하실 수 없습니다.</h1>
	<h2>이용기간</h2>
</div>

<hr style="margin: 0 0 20px 5px;"/>

<!-- 1개월 -->
<div class="paymentMonthMenu">
	<div class="paymentBlind"></div>
	<div class="paymentHeader">
		<h2>1</h2>
		<h4>개월</h4>
	</div>
	<div class="paymentBody">
		<div class="paymentCalc" style="width: 100%;">
			<p style="margin: 20px 0 20px 20px;"><b>10,000</b>원</p><br/>
		</div>
		<div class="paymentLeftDiv"></div>
		<p style="font-size: 18px;">
			1개월 동안<br/>
			원하는 강의를<br/>
			<br/>
			<strong style="font-size: 30px;">
				자유롭게<br/>
			</strong>
			<br/>
			이용하실 수<br/>
			있습니다.
		</p>
		<br/>
		<!-- <input type="button" name="select12BtnFree" class="btn" value="선택"> -->
		<p style="color: rgb(230,0,0); margin: 5px 0 10px 0px;">선택할 수 없습니다</p>
		<input type="radio" name="selectMonth" value="12" style="display: none;">
	</div>
</div>

<!-- 3개월 -->
<div class="paymentMonthMenu">
	<div class="paymentBlind"></div>
	<div class="paymentHeader">
		<h2>3</h2>
		<h4>개월</h4>
	</div>
	<div class="paymentBody">
		<div class="paymentCalc">
			<p style="margin: 10px 0 5px 20px;"><b>29,700</b>원</p><br/>
			<p style="text-decoration: line-through; margin-top: -10px; padding-bottom: 5px;">￦30,000</p>
		</div>
		<div class="paymentLeftDiv"></div>
		<p style="font-size: 18px;">
			3개월 동안<br/>
			원하는 강의를<br/>
			<br/>
			<strong style="font-size: 30px;">
				자유롭게<br/>
			</strong>
			<br/>
			이용하실 수<br/>
			있습니다.
		</p>
		<br/>
		<!-- <input type="button" name="select12BtnFree" class="btn" value="선택"> -->
		<p style="color: rgb(230,0,0); margin: 5px 0 10px 0px;">선택할 수 없습니다</p>
		<input type="radio" name="selectMonth" value="12" style="display: none;">
	</div>
</div>

<!-- 6개월 -->
<div class="paymentMonthMenu">
	<div class="paymentBlind"></div>
	<div class="paymentHeader">
		<h2>6</h2>
		<h4>개월</h4>
	</div>
	<div class="paymentBody">
		<div class="paymentCalc">
			<p style="margin: 10px 0 5px 20px;"><b>57,000</b>원</p><br/>
			<p style="text-decoration: line-through; margin-top: -10px; padding-bottom: 5px;">￦120,000</p>
		</div>
		<div class="paymentLeftDiv"></div>
		<p style="font-size: 18px;">
			6개월 동안<br/>
			원하는 강의를<br/>
			<br/>
			<strong style="font-size: 30px;">
				자유롭게<br/>
			</strong>
			<br/>
			이용하실 수<br/>
			있습니다.
		</p>
		<br/>
		<!-- <input type="button" name="select12BtnFree" class="btn" value="선택"> -->
		<p style="color: rgb(230,0,0); margin: 5px 0 10px 0px;">선택할 수 없습니다</p>
		<input type="radio" name="selectMonth" value="12" style="display: none;">
	</div>
</div>

<!-- 12개월 -->
<div class="paymentMonthMenu">
	<div class="paymentBlind"></div>
	<div class="paymentHeader">
		<h2>12</h2>
		<h4>개월</h4>
	</div>
	<div class="paymentBody">
		<div class="paymentCalc">
			<p style="margin: 10px 0 5px 0;"><b>108,000</b>원</p><br/>
			<p style="text-decoration: line-through; margin-top: -10px; padding-bottom: 5px;">￦120,000</p>
		</div>
		<div class="paymentLeftDiv"></div>
		<p style="font-size: 18px;">
			12개월 동안<br/>
			원하는 강의를<br/>
			<br/>
			<strong style="font-size: 30px;">
				자유롭게<br/>
			</strong>
			<br/>
			이용하실 수<br/>
			있습니다.
		</p>
		<br/>
		<!-- <input type="button" name="select12BtnFree" class="btn" value="선택"> -->
		<p style="color: rgb(230,0,0); margin: 5px 0 10px 0px;">선택할 수 없습니다</p>
		<input type="radio" name="selectMonth" value="12" style="display: none;">
	</div>
</div>

<!-- 행사상품 -->
<!-- 
<hr style="margin: 20px 0 0 0;"/>

<div class="paymentMonthMenu" style="margin-top: 60px;">
	<div class="paymentHeader">
		<h2>12</h2>
		<h4>개월</h4>
	</div>
	<div class="paymentBody">
		<div class="paymentCalc" style="width: 100px;">
			<p style="margin: -10px 0 -5px 20px;"><b style="color: red; font-size: 60px;">0</b>원</p><br/>
			<p style="text-decoration: line-through; margin-top: -10px; padding-bottom: 5px;">￦120,000</p>
		</div>
		<div class="paymentLeftDiv"></div>
		<p style="font-size: 18px;">오픈 기념<br/>
			감사 이벤트!<br/>
			<br/>
			지금 결제하시면<br/>
			12개월간<br/>
			<br/>
			<strong style="font-size: 30px; color: red;">
				무료!<br/>
			</strong>
		</p>
		<br/>
		<input type="button" name="select12BtnFree" class="btn" value="선택">
		<input type="radio" name="selectMonth" value="12" style="display: none;">
	</div>
</div>
 -->
 
<!-- 결제 선택 -->
<!--
<div class="paymentType">
	<div class="paymentTypeHeader">
		<h2>결제방식</h2>
	</div>
	
	<hr style="margin: 0 0 20px 5px;"/>
	
	<div class="paymentTypeBody">
		<input type="radio" name="paymenyTypeRadio" value="WeChatPay" disabled="disabled" style="display: none;">
			<p style="text-decoration: line-through;">WeChat Pay(준비중)</p><br/>
		
		<input type="radio" name="paymenyTypeRadio" value="kakaoPay" disabled="disabled" style="display: none;">
			<p style="text-decoration: line-through;">kakao Pay(준비중)</p><br/>
		
		<input type="radio" name="paymenyTypeRadio" value="DepositPay" style="display: none;"><p>무통장입금</p><br/>
		
		<hr/>
		
		<div class="paymentText">
			<p id="checkedMonth">기간 : </p>
			<p id="checkedPaymentType">결제방식 : </p>
			<p id="checkedPayment" style="color: red; font-size: 30px;">금액 : </p>
		</div>
		<hr/>
		<input type="button" value="결제하기" id="paymenyBtn" class="btn" style="font-size: 30px; width: 100%; padding: 10px;">
	</div>
</div>
-->

<hr style="margin: 60px 0 20px 0;"/>
<p>※ 결제 및 환불 사항은 Holic 가입시 결제약관에 따라 이용하실 수 있습니다.</p>
<p>※ 결제후 재로그인을 해야 유료회원용 컨텐츠를 이용할 수 있습니다.</p>
<p>※ 환불관련 문의는 고객센터를 이용해 주시기 바랍니다.</p>