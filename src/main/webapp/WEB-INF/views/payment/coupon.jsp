<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- HanJungIl - 작성 - 170610 ~ 170630 -->
<style type="text/css">
.paymentDivHeader {
	text-align: right;
	margin: auto;
	margin-left: 20px;
}
.paymentDivHeader h2 {
	font-family: NANUMSQUAREER;
    font-weight: bold;
    display: inline-block;
}
.paymentDivHeader p {
	float: right;
    font-size: 12px;
    margin: 40px 10px 0 0;
}

.couponInputDiv {
	padding: 20px 0;
}
.couponInputDiv .col-md-2 {
	margin: 0 5px 5px 0;
	padding: 0;
}
.couponInputDiv input {
	width: 100%;
	padding: 5px;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
	// input
	$(".couponInputDiv input").click(function(){
		// 로그인체크
		if(${ sessionScope.LOGIN == null || sessionScope.LOGIN == '' }){
			alert("로그인 후 이용 가능합니다.")
			location.href="../user/login";
			return;
		}
	});
	
	// 확인버튼
	$("#couponSubmitBtn").click(function(){
		// 유효성 검사
		var $inputList = $(".couponInputDiv input");
		var serial = '';
		
		for(i=0 ; i < $inputList.length ; ++i){
			if($inputList.eq(i).val().length < 4){
				alert("모두 입력해 주시기 바랍니다.");
				$inputList.eq(i).focus();
				serial = '';
				return;
			}
			
			serial += $inputList.eq(i).val();
			
			if(i + 1 < $inputList.length) serial += "-";
		}
		
		$.ajax({
			url		: "../payment/couponUsing",
			data	: { "serial" : serial },
			type	: "post",
			dataType: "text",
			success : function(data){
				alert("쿠폰이 사용되었습니다.");
				location.href="../";
			},
			error	: function(data, status){
				alert("잘못된 입력입니다.\n\n확인후 다시 이용해 주시기 바랍니다.");
			}
		});
	});
});
</script>
<div class="paymentDivHeader">
	<h2>쿠폰</h2>
</div>

<hr style="margin: 0 0 20px 5px;"/>

<div class="row" style="margin: auto;">
	<div class="col-md-4"></div>
	<div class="col-md-4" style="padding: 15px; border: 3px solid black;">
		<p style="font-size: 12px; margin: 10px;">쿠폰을 사용하여 Holic을 이용하실 수 있습니다.</p>
		
		<hr style="margin: 0"/>
		
		<div class="couponInputDiv row">
			<span class="col-md-3" style="padding: 7px;">쿠폰 : </span>
			<div class="col-md-2">
				<input type="text" name="input1" maxlength="4" placeholder="4자리">
			</div>
			<div class="col-md-2">
				<input type="text" name="input2" maxlength="4" placeholder="4자리">
			</div>
			<div class="col-md-2">
				<input type="password" name="input3" maxlength="4" placeholder="4자리" style="font: icon; font-size: 14px; height: 34px;">
			</div>
			<div class="col-md-2">
				<input type="password" name="input4" maxlength="4" placeholder="4자리" style="font: icon; font-size: 14px; height: 34px;">
			</div>
		</div>
		
		<hr style="margin: 0"/>
		
		<input id="couponSubmitBtn" type="button" value="확인" class="btn" style="margin: 10px 0 0 0;">
	</div>
	<div class="col-md-4"></div>
</div>

<hr style="margin: 60px 0 20px 0;"/>
<p>※ 쿠폰은 환불이 불가능 합니다.</p>
<p>※ 16자리 쿠폰 번호를 입력하여 사용하실 수 있습니다.</p>
<p>※ 불법적인 방법으로 충전, 도용 시 법적 제재를 받습니다.</p>