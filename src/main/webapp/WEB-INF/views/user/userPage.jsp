<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<title>Holic - 내 정보</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<jsp:include page="../default/source.jsp"></jsp:include>
<!-- <script type="text/javascript" src="/js/upload.js"></script> -->
<style type="text/css">
.userPageNav h2 { 
	font-family: NANUMSQUAREER;
	font-weight: bold; 
	display: inline-block; 
}
.userPageNav p { 
	float: right;
	font-size: 12px;
	margin: 40px 10px 0 0; 
}

.col-md-3 .btn {
	text-align: left;
	font-size: 20px;
	color: white;
	background-color: black;
	width: 100%;
	padding: 10px 20px;
	border-bottom: 1px solid gray;
	border-radius: 0;
}

.col-md-9 {
	text-align: right;
	margin: auto;
}
.col-md-9 h2{
	font-family: NANUMSQUAREER;
	font-weight: bold;
	display: inline-block;
}
article .panel-body { padding: 0; }

li {
	list-style: none;
}
.page_no {
	border: none;
	background: rgba(0, 0, 0, 0);
}
.button_click_on {
	display: none;
}
.button_click_no p span {
	color: red;
}
.user_Data {
	text-align: center;
}
.user_title { display: inline-block; }
.user_right {
	text-align: left;
	height: 60px;
	margin: 0;
	padding: 15px;
	border-bottom: 1px solid #ddd;
}
.user_right input { padding: 3px; }
.user_right button {
	margin-left: 30px;
}
.user_left {
	color: white;
	background-color: black;
	height: 60px;
	margin: 0;
	padding: 15px;
	border-bottom: 1px solid white;
}
#birth_year, #birth_month, #birth_day {
	width: 60px;
	height: 25px;
}
#birth_year, #birth_month {
	margin-right: 20px;
}
input type:button {
	font-size: 12px;
}
.inpu_design {
	border: none;
	background: rgba(0, 0, 0, 0);
}
.inpu_design1 {
	border: 1px solid black;
	background: white;
	color: black;
	width: 210px;
}
.filebox label {
	display: inline-block;
	padding: .5em .75em;
	color: white;
	font-size: inherit;
	line-height: normal;
	vertical-align: middle;
	background-color: black;
	cursor: pointer;
	border: 1px solid #ebebeb;
	border-bottom-color: #e2e2e2;
	border-radius: .25em;
	margin-left: 10%;
}
.filebox input[type="file"] {
	/* 파일 필드 숨기기 */
	position: absolute;
	width: 1px;
	height: 1px;
	padding: 0;
	margin: -1px;
	overflow: hidden;
	clip: rect(0, 0, 0, 0);
	border: 0;
}

/* 강사정보 */
.form-group img { width: auto !important; }
.form-group li:FIRST-CHILD {
	border-top: 1px solid rgb(221, 221, 221);
}
.form-group li {
	border-bottom: 1px solid rgb(221, 221, 221);
}
.form-group li:NTH-LAST-CHILD(2) ,
.form-group li:LAST-CHILD {
	border-bottom: none;
}
.form-group li p:FIRST-CHILD {
	color: white;
	background-color: black;
	margin: 0;
	padding: 15px;
	border-bottom: 1px solid white;
}
.form-group li p:NTH-CHILD(2),
.form-group li div:NTH-CHILD(2) {
	text-align: left;
	padding: 12px 15px;
}

.form-group textarea {
	width: 100%; 
	height: 150px;
	resize: none;
}
</style>
<!-- <script src="../../../js/userPage.js" type="text/javascript"></script> -->
<script type="text/javascript">
$(document).ready(function(){
	// 메뉴 버튼
	$(".col-md-3 button").click(function(){
		if($(this).text() == "회원정보설정"){
			location.href="../user/userPage";
		}else if($(this).text() == "주문/결제관련"){
			alert("준비중");
		}
	});
	
	// 탈퇴 버튼
	$("#userDelete").click(function(){
		alert("암호 입력 받은 후 일치하면 탈퇴하는 방식으로 변경");
		if(confirm("Holic을 탈퇴합니다.\n\n※ 탈퇴하면 모든 정보가 삭제되며, 복구할 수 없습니다.\n\n정말 탈퇴하시겠습니까?")){
			$.ajax({
				url		: "/user/userDelete",
				type	: "post",
				data	: {},
				dataType: "text",
				success	: function(data){
					if(data == 0){
						alert("다시 시도해 주시기 바랍니다.");
					}else if(data == 1){
						alert("Holic을 이용해 주셔서 감사합니다.\n\n안녕히 가십시오. (_ _)");
						location.href="../";
					}
				},
				error	: function(data, status){
					console.log(data);
					console.log(status);
				}
			});
		}
	});
	
	// 정보수정버튼
	$("#submit_button").click(function(){
		if($(this).text() != "정보수정") return;
		
		var liList = $(this).parent().parent("ul").children("li");
		var infoP = liList.eq(liList.length - 2).children("p");
		
		infoP.eq(1).css("height", "auto");
		$("#userDelete").css("display", "none");
	});
	
	// 상세주소창
	$("#sample6_address2").click(function(){
		if($("#sample6_postcode").val() != '' && $("#sample6_address").val() != '') return;
		
		$(".user_right input[type=button]").trigger("click");
	});
});
</script>
</head>
<body>
	<jsp:include page="../checkPage/loginCheck.jsp" />
	<jsp:include page="../default/header.jsp" />
	<div id="main">
		<div class="userPageNav">
			<h2>회원정보</h2>
			<p>※ 회원님의 등록 정보를 확인할 수 있습니다.</p>
		</div>
		
		<hr style="margin-top: 0;"/>
		
		<div class="row">
			<div class="col-md-3">
				<button type="button" class="btn">회원정보설정</button>
				<button type="button" class="btn">주문/결제관련</button>
			</div>
			
			<div class="col-md-9" style="border-left: 2px dashed black;">
				<h2 class="user_title col-xs-12 col-sm-12 col-md-12 col-lg-12">회원정보</h2>
				
				<hr style="display: inline-block; width: 100%; margin: 0 0 20px 5px;"/>
				
				<section class="user_Detaile col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<article>
						<ul class="panel-body">
							<li class="form-group">
								<p class="user_left col-xs-12 col-sm-2 col-md-2 col-lg-2">
									<strong>닉네임</strong>
								</p>
								<p class="user_right col-xs-12 col-sm-10 col-md-10 col-lg-10" style="border-top: 1px solid #ddd">
									<span class="userNick">${user.userDetail.userNick}</span>
									<c:if test="${ sessionScope.LOGIN.userType < 8 }">
										<input type="button" id="userDelete" value="탈퇴" class="btn" style="float: right;">
									</c:if>
								</p>
							</li>

							<c:if test="${user.userDetail.userGender==0}">
								<li class="form-group button_click_no">
									<p class="user_left col-xs-12 col-sm-2 col-md-2 col-lg-2">
										<strong>성별</strong>
									</p>
									<p class="user_right col-xs-12 col-sm-10 col-md-10 col-lg-10">
										<span>회원님정보를 입력하세요</span>
									</p>
								</li>
								<li class="form-group button_click_on">
									<p class="user_left col-xs-12 col-sm-2 col-md-2 col-lg-2">
										<strong>성별</strong>
									</p>
									<p class="user_right col-xs-12 col-sm-10 col-md-10 col-lg-10">
										<input type="radio" name="1" value="man"> 남자 
										<input type="radio" name="1" value="women"> 여자 
										<input type="radio" name="1" value="no" style="display: none;" checked="checked">
									</p>
								</li>
							</c:if>

							<c:if
								test="${user.userDetail.userGender!=0 && user.userDetail.userGender!=null}">
								<li class="form-group">
									<p class="user_left col-xs-12 col-sm-2 col-md-2 col-lg-2">
										<strong>성별</strong>
									</p>
									<p class="user_right col-xs-12 col-sm-10 col-md-10 col-lg-10">
										<input type="radio" name="1" style="display: none;"
											value="${user.userDetail.userGender}" checked="checked">
										<c:if test="${user.userDetail.userGender==1}">
											<span>남성회원</span>
										</c:if>
										<c:if test="${user.userDetail.userGender==2}">
											<span>여성회원</span>
										</c:if>
									</p>
								</li>
							</c:if>

							<c:if test="${empty user.userDetail.userBirth}">
								<input type="hidden" value="no" class="birth_check">
								<li class="form-group button_click_no">
									<p class="user_left col-xs-12 col-sm-2 col-md-2 col-lg-2">
										<strong>생년월일</strong>
									</p>
									<p class="user_right col-xs-12 col-sm-10 col-md-10 col-lg-10">
										<span>회원님정보를 입력하세요</span>
									</p>
								</li>
								<li class="form-group button_click_on" style="display: none;">
									<p class="user_left col-xs-12 col-sm-2 col-md-2 col-lg-2">
										<strong>생년월일</strong>
									</p>
									<p class="user_right col-xs-12 col-sm-10 col-md-10 col-lg-10">
										<select name="birth_year" id="birth_year"
											class="selectpicker ">
										</select><span style="padding-right: 13px;">-</span>
											<select name="birth_month" id="birth_month" class="selectpicker">
										</select>
										<span style="padding-right: 13px;">-</span>
										<select name="birth_day" id="birth_day" class="selectpicker">
										</select>
									</p>
								</li>
								<script type="text/javascript">
									var arr = [ 1, 0, 9, 8, 7 ]
								
									for (var i in arr) {
								
										if (arr[i] == 0) {
											for (k = 9; k > 0; k--) {
												var option = document.createElement('option');
												option.value = "200+k";
												var textOption = document.createTextNode("200" + k);
												option.appendChild(textOption);
												document.getElementById("birth_year").appendChild(option);
											}
										} else if (arr[i] == 1) {
											for (k = 7; k > 0; k--) {
												var option = document.createElement('option');
												option.value = "201" + k;
												var textOption = document.createTextNode("201" + k);
												option.appendChild(textOption);
												if (k == 7) {
													option.value = "201" + k;
												}
												document.getElementById("birth_year").appendChild(option);
											}
										} else {
											for (k = 9; k > 0; k--) {
												var option = document.createElement('option');
												option.value = "19" + arr[i] + k;
												var textOption = document.createTextNode("19" + arr[i] + k);
												option.appendChild(textOption);
												document.getElementById("birth_year").appendChild(option);
											}
										}
									}
								
									for (i = 1; i < 13; i++) {
										$("#birth_month").append("<option value=" + i + ">" + i + "</option>");
									}
									for (i = 1; i < 32; i++) {
										$("#birth_day").append("<option value=" + i + ">" + i + "</option>");
									}
								</script>
							</c:if>

							<c:if test="${not empty user.userDetail.userBirth}">
								<input type="hidden" value="ok" class="birth_check">
								<li class="form-group">
									<p class="user_left col-xs-12 col-sm-2 col-md-2 col-lg-2">
										<strong>생년월일</strong>
									</p>
									<p class="user_right col-xs-12 col-sm-10 col-md-10 col-lg-10">
										<fmt:parseDate var="data" value="${ user.userDetail.userBirth }" pattern="yyyy-MM-dd"/>
										<input disabled="disabled" value='<fmt:formatDate value="${ data }" pattern="yyyy년 MM월 dd일"/>' class="ok_birth page_no">
									</p>
								</li>
							</c:if>

							<c:if test="${empty user.userDetail.userSchool}">
								<li class="form-group button_click_no">
									<p class="user_left col-xs-12 col-sm-2 col-md-2 col-lg-2">
										<strong>지역/학교/학과</strong>
									</p>
									<p class="user_right col-xs-12 col-sm-10 col-md-10 col-lg-10">
										<span>회원님 정보를 입력하세요</span>
									</p>
								</li>
								<li class="form-group button_click_on">
									<p class="user_left col-xs-12 col-sm-2 col-md-2 col-lg-2">
										<strong>지역/학교/학과</strong>
									</p>
									<p class="user_right col-xs-12 col-sm-10 col-md-10 col-lg-10" style="height: auto;">
										<select name="area" style="margin-right: 10px; padding: 5px;">
											<option value="서울">서울</option>
											<option value="부산">부산</option>
											<option value="인천">인천</option>
											<option value="대구">대구</option>
											<option value="대전">대전</option>
											<option value="광주">광주</option>
											<option value="울산">울산</option>
											<option value="경기도">경기도</option>
											<option value="충청도">충청도</option>
											<option value="전라도">전라도</option>
											<option value="경상도">경상도</option>
											<option value="강원도">강원도</option>
										</select>
										<input style="display: inline-block; text-align: center;  
											placeholder="학교를 입력하세요" class="school1">
										<input style="text-align: center;" placeholder="학과를 입력하세요" class="school2">
									</p>
								</li>
							</c:if>
							
							<c:if test="${not empty user.userDetail.userSchool}">
								<li class="form-group form-group button_click_no">
									<p class="user_left col-xs-12 col-sm-2 col-md-2 col-lg-2">
										<strong>지역/학교/학과</strong>
									</p>
									<p class="user_right col-xs-12 col-sm-10 col-md-10 col-lg-10">
										<span style="color: black;">${ fn:replace(user.userDetail.userSchool, '+', ' ') }</span>
									</p>
								</li>

								<li class="form-group button_click_on">
									<p class="user_left col-xs-12 col-sm-2 col-md-2 col-lg-2">
										<strong>지역/학교/학과</strong>
									</p>
									<p class="user_right col-xs-12 col-sm-10 col-md-10 col-lg-10" style="height: auto;">
										<select name="area" style="margin-right: 10px; padding: 5px;">
											<option value="서울">서울</option>
											<option value="부산">부산</option>
											<option value="인천">인천</option>
											<option value="대구">대구</option>
											<option value="대전">대전</option>
											<option value="광주">광주</option>
											<option value="울산">울산</option>
											<option value="경기도">경기도</option>
											<option value="충청도">충청도</option>
											<option value="전라도">전라도</option>
											<option value="경상도">경상도</option>
											<option value="강원도">강원도</option>
										</select>
										<input style="display: inline-block; text-align: center;"
											placeholder="학교를 입력하세요" class="school1">
										<input style="text-align: center;" placeholder="학과를 입력하세요" class="school2">
									</p>
								</li>
							</c:if>
							
							<li class="form-group">
								<p class="user_left col-xs-12 col-sm-2 col-md-2 col-lg-2">
									<strong>WeChat</strong>
								</p>
								<p class="user_right col-xs-12 col-sm-10 col-md-10 col-lg-10">
									<input disabled="disabled" class="page_no" type="text"
										name="Wechat" value="${user.userDetail.userWechat}">
									<!--
									<input type="button" value="수정" class="btn  weChat_check"
										style="display: none; margin-left: 30px;">
									-->
								</p>
							</li>

							<li class="form-group">
								<p class="user_left col-xs-12 col-sm-2 col-md-2 col-lg-2">
									<strong>Email</strong>
								</p>
								<p class="user_right col-xs-12 col-sm-10 col-md-10 col-lg-10">
									<span>${user.userDetail.userMail}</span>
								</p>
							</li>

							<c:if test="${empty user.userDetail.userAddress1}">
								<li class="form-group button_click_no">
									<p class="user_left col-xs-12 col-sm-2 col-md-2 col-lg-2">
										<strong>주소</strong>
									</p>
									<p class="user_right col-xs-12 col-sm-10 col-md-10 col-lg-10">
										<span>회원님정보를 입력하세요</span>
									</p>
								</li>
								<li class="form-group button_click_on" style="display: none;">
									<p class="user_left col-xs-12 col-sm-2 col-md-2 col-lg-2">
										<strong>주소</strong>
									</p>
									<p class="user_right col-xs-12 col-sm-10 col-md-10 col-lg-10">
										<input type="button" onclick="sample6_execDaumPostcode()"
											style="display: block;" class="btn " value="우편번호찾기">
										
										<input class="col-xs-10 col-sm-2 col-md-2 col-lg-2"
											disabled="disabled" type="text" id="sample6_postcode" placeholder="우편번호"
											style="border: 1px solid #ddd; display: block; margin-top: 10px; margin-right: 10px;">
										
										<input class="col-xs-10 col-sm-5 col-md-5 col-lg-5"
											disabled="disabled" type="text" id="sample6_address" placeholder="주소"
											style="border: 1px solid #ddd; display: block; margin-top: 10px;">
										
										<input class="col-xs-10 col-sm-5=10 col-md-10 col-lg-10"
											type="text" id="sample6_address2" placeholder="상세주소"
											style="border: 1px solid #ddd; display: block; margin-top: 10px;">

										<script src="https://ssl.daumcdn.net/dmaps/map_js_init/postcode.v2.js"></script>
										<script>
											function sample6_execDaumPostcode() {
												new daum.Postcode({
													oncomplete : function(data) {
														// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.
										
														// 각 주소의 노출 규칙에 따라 주소를 조합한다.
														// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
														var fullAddr = ''; // 최종 주소 변수
														var extraAddr = ''; // 조합형 주소 변수
										
														// 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
														if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
															fullAddr = data.roadAddress;
										
														} else { // 사용자가 지번 주소를 선택했을 경우(J)
															fullAddr = data.jibunAddress;
														}
										
														// 사용자가 선택한 주소가 도로명 타입일때 조합한다.
														if (data.userSelectedType === 'R') {
															//법정동명이 있을 경우 추가한다.
															if (data.bname !== '') {
																extraAddr += data.bname;
															}
															// 건물명이 있을 경우 추가한다.
															if (data.buildingName !== '') {
																extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
															}
															// 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
															fullAddr += (extraAddr !== '' ? ' (' + extraAddr + ')' : '');
														}
										
														// 우편번호와 주소 정보를 해당 필드에 넣는다.
														document.getElementById('sample6_postcode').value = data.zonecode; //5자리 새우편번호 사용
														document.getElementById('sample6_address').value = fullAddr;
										
														// 커서를 상세주소 필드로 이동한다.
														document.getElementById('sample6_address2').focus();
													}
												}).open();
											}
										</script>
									</p>
								</li>
							</c:if>
							
							<c:if test="${not empty user.userDetail.userAddress1}">
								<li class="form-group button_click_no">
									<p class="user_left col-xs-12 col-sm-2 col-md-2 col-lg-2">
										<strong>주소</strong>
									</p>
									<p class="user_right col-xs-12 col-sm-10 col-md-10 col-lg-10">
										<span style="color: black;">${user.userDetail.userAddress1}
											${user.userDetail.userAddress2}</span>
									</p>
								</li>

								<li class="form-group button_click_on" style="display: none;">
									<p class="user_left col-xs-12 col-sm-2 col-md-2 col-lg-2">
										<strong>주소</strong>
									</p>
									<p class="user_right col-xs-12 col-sm-10 col-md-10 col-lg-10">
										<input type="button" onclick="sample6_execDaumPostcode()"
											style="display: block;" class="btn " value="우편번호찾기">
										<input class="user_right  col-xs-10 col-sm-2 col-md-2 col-lg-2"
											disabled="disabled" type="text" id="sample6_postcode" placeholder="우편번호"
											style="border: 1px solid #ddd; display: block; margin-top: 2%;">
										
										<input class="user_right  col-xs-10 col-sm-5 col-md-5 col-lg-5"
											value="${user.userDetail.userAddress1}" disabled="disabled"
											type="text" id="sample6_address" placeholder="주소"
											style="border: 1px solid #ddd; display: block; margin-top: 2%; margin-left: 2%;">
										
										<input class="user_right col-xs-10 col-sm-5=10 col-md-10 col-lg-10"
											value="${user.userDetail.userAddress2}" type="text"
											id="sample6_address2" placeholder="상세주소"
											style="border: 1px solid #ddd; display: block; margin-top: 2%;">

										<script src="https://ssl.daumcdn.net/dmaps/map_js_init/postcode.v2.js"></script>
										<script>
											function sample6_execDaumPostcode() {
												new daum.Postcode({
													oncomplete : function(data) {
														// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.
										
														// 각 주소의 노출 규칙에 따라 주소를 조합한다.
														// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
														var fullAddr = ''; // 최종 주소 변수
														var extraAddr = ''; // 조합형 주소 변수
										
														// 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
														if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
															fullAddr = data.roadAddress;
										
														} else { // 사용자가 지번 주소를 선택했을 경우(J)
															fullAddr = data.jibunAddress;
														}
										
														// 사용자가 선택한 주소가 도로명 타입일때 조합한다.
														if (data.userSelectedType === 'R') {
															//법정동명이 있을 경우 추가한다.
															if (data.bname !== '') {
																extraAddr += data.bname;
															}
															// 건물명이 있을 경우 추가한다.
															if (data.buildingName !== '') {
																extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
															}
															// 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
															fullAddr += (extraAddr !== '' ? ' (' + extraAddr + ')' : '');
														}
										
														// 우편번호와 주소 정보를 해당 필드에 넣는다.
														//내가수정한곳
														if (data.zonecode != null) {
															document.getElementById('sample6_postcode').value = data.zonecode; //5자리 새우편번호 사용
															document.getElementById('sample6_address').value = fullAddr;
															// 커서를 상세주소 필드로 이동한다.
															document.getElementById('sample6_address2').focus();
														}
										
													}
												}).open();
											}
										</script>
									</p>
								</li>
							</c:if>

							<li class="form-group" style="text-align: center;">
								<button id="submit_button" class="btn"
									style="padding: 6px 30px; margin-top: 3%;">정보수정</button>
							</li>
						</ul>
					</article>
				</section>
				
				<!-- 강사정보 입력 -->
				<c:if test="${ sessionScope.LOGIN.userType == 5 }">
					<hr style="width: 100%;"/>
					
					<h2 style="text-align: center; margin-top: 50px;">강사정보</h2>
					
					<hr style="display: inline-block; width: 100%; margin: 0 0 20px 5px;"/>
					
					<ul class="form-group" style="padding: 0;">
						<li class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<p class="col-xs-12 col-sm-3 col-md-3 col-lg-3">사진</p>
							<c:if test="${empty user.timg.provePath}">
								<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 filebox" style="text-align: left; color: red;">
									<span class="click_no">사진 정보 입력하세요</span>
									<form id="profile" enctype="multipart/form-data" style="display: none; margin-top: -5px;" class="file">
										<label for="provePath_file" style="margin: 0;">업로드</label>
										<input type="file" id="provePath_file">
									</form>
									<span style="display: block;">
										<img id="prove_img" style="display: none;">
									</span>
								</div>
							</c:if>
							
							<c:if test="${not empty user.timg.provePath }">
								<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 filebox">
									<img id="teacher_profile" class="click_no" alt="강사님 사진" src="${user.timg.provePath}">
									<form id="profile" enctype="multipart/form-data" style="display: none;" class="file">
										<label for="provePath_file" style="margin: 0;">업로드</label>
										<input type="file" id="provePath_file">
									</form>
									<span style="display: block;">
										<img id="prove_img" style="display: none; width: auto;">
									</span>
								</div>
							</c:if>
						</li>
						
						<li class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<p class="col-xs-12 col-sm-3 col-md-3 col-lg-3">이름</p>
							<c:if test="${empty user.teacherDetail.userName}">
								<p class="col-xs-12 col-sm-9 col-md-9 col-lg-9" style="color: red;">
									<input id="userName" value="이름을 입력하세요" disabled="disabled" class="inpu_design">
								</p>
							</c:if>
							<c:if test="${not empty user.teacherDetail.userName }">
								<p class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
									<input id="userName" type="text" value="${user.teacherDetail.userName}" 
										disabled="disabled" class="inpu_design">
								</p>
							</c:if>
						</li>
						
						<li class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<p class="col-xs-12 col-sm-3 col-md-3 col-lg-3">한국어 성적등급</p>
							<c:if test="${empty user.teacherDetail.TGrade}">
								<p class="col-xs-12 col-sm-9 col-md-9 col-lg-9" style="color: red;">
									<input id="tGrade" value="한국어 성적등급을 입력하세요" 
										disabled="disabled" class="inpu_design">
								</p>
							</c:if>
							<c:if test="${not empty user.teacherDetail.TGrade }">
								<p class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
									<input id="tGrade" type="number" value="${user.teacherDetail.TGrade}" 
										disabled="disabled" class="inpu_design" maxlength="1">
								</p>
							</c:if>
						</li>
	
						<li class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<p class="col-xs-12 col-sm-3 col-md-3 col-lg-3">한국어 성적(듣기)</p>
							<c:if test="${empty user.teacherDetail.TListening}">
								<p class="col-xs-12 col-sm-9 col-md-9 col-lg-9" style="color: red;">
									<input id="tListening" value="한국어 듣기 성적을 입력하세요"
										disabled="disabled" class="inpu_design">
								</p>
							</c:if>
							<c:if test="${not empty user.teacherDetail.TListening }">
								<p class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
									<input id="tListening" type="number" value="${user.teacherDetail.TListening}" 
										disabled="disabled" class="inpu_design" maxlength="1">
								</p>
							</c:if>
						</li>
	
						<li class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<p class="col-xs-12 col-sm-3 col-md-3 col-lg-3">한국어 성적(읽기)</p>
							<c:if test="${empty user.teacherDetail.TRead}">
								<p class="col-xs-12 col-sm-9 col-md-9 col-lg-9" style="color: red;">
									<input id="tRead" value="한국어 읽기 성적을 입력하세요" 
										disabled="disabled" class="inpu_design">
								</p>
							</c:if>
							<c:if test="${not empty user.teacherDetail.TRead }">
								<p class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
									<input id="tRead" type="number" value="${user.teacherDetail.TRead}" 
										disabled="disabled" class="inpu_design" maxlength="1">
								</p>
							</c:if>
						</li>
						
						<li class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<p class="col-xs-12 col-sm-3 col-md-3 col-lg-3">한국어 성적(쓰기)</p>
							<c:if test="${empty user.teacherDetail.TWriting}">
								<p class="col-xs-12 col-sm-9 col-md-9 col-lg-9" style="color: red;">
									<input id="tWriting" value="한국어 쓰기 성적을 입력하세요"
										disabled="disabled" class="inpu_design">
								</p>
							</c:if>
							<c:if test="${not empty user.teacherDetail.TWriting }">
								<p class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
									<input id="tWriting" type="number" value="${user.teacherDetail.TWriting}" 
										disabled="disabled" class="inpu_design" maxlength="1">
								</p>
							</c:if>
						</li>
	
						<li class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<p class="col-xs-12 col-sm-3 col-md-3 col-lg-3">한국어 성적(사진)</p>
							<c:if test="${empty user.timg.gradePath}">
								<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 filebox" style="color: red;">
									<span class="click_no">사진 정보 입력하세요</span>
									<form id="" enctype="multipart/form-data" style="display: none; margin-top: -5px;" class="file">
										<label for="gradePath_file" style="margin: 0;">업로드</label>
										<input type="file" id="gradePath_file">
									</form>
									<span style="display: block;">
										<img id="grade_img" style="display: none;">
									</span>
								</div>
							</c:if>
							
							<c:if test="${not empty user.timg.gradePath }">
								<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 filebox">
									<img class="click_no" id="teacher_grade" alt="성적 사진" src="${user.timg.gradePath}">
									<form id="" enctype="mutipart/form-data" style="display: none;" class="file">
										<label for="gradePath_file">업로드</label>
										<input type="file" id="gradePath_file">
									</form>
									<span style="display: block;">
										<img id="grade_img" style="display: none;">
									</span>
								</div>
							</c:if>
						</li>
						
						<li class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<p class="col-xs-12 col-sm-3 col-md-3 col-lg-3">프로필</p>
							<c:if test="${empty user.teacherDetail.TProfile}">
								<p class="col-xs-12 col-sm-9 col-md-9 col-lg-9" style="color: red; border-bottom: 1px solid rgb(221, 221, 221);">
									<textarea id="tProfile" class="inpu_design profile" 
										readonly="readonly">강사님의 프로필을 입력하세요</textarea>
								</p>
							</c:if>
							<c:if test="${not empty user.teacherDetail.TProfile }">
								<textarea id="tProfile" class="inpu_design profile" rows="5" readonly="readonly"
									style="width: 60%; resize: none; border: none; text-align: left;">
										${user.teacherDetail.TProfile}
								</textarea>
							</c:if>
						</li>
						
						<li class="form-group" style="text-align: center;">
							<input type="button" id="teacher_button" value="수정"
								class="btn" style="margin-top: 10px; padding: 6px 30px;">
						</li>
					</ul>
				</c:if>
				
			</div>
		</div>
	</div>
	<jsp:include page="../default/footer.jsp" />
	
	<script src="/resources/js/userPage.js" type="text/javascript"></script>
	<script type="text/javascript">
		var clickIsOk = true;
		var gradePath = $("#teacher_grade").attr("href");
		var provePath = $("#teacher_profile").attr("href");
		
		if(typeof gradePath == "undefined"){
			gradePath = null;
		}
		
		if(typeof provePath == "undefined"){
			provePath = null;
		}
		
		var userNick = $(".userNick").html();
		var ajax1 = true;
		var ajax2 = true;
		
		$(document).ready(function(){
			$("#teacher_button").click(function(event){
				
				if(clickIsOk){
					$(".file").css("display","inline-block");
					$(".inpu_design").removeAttr("disabled").removeClass("inpu_design").addClass("inpu_design1");
					$(".inpu_design1").focus(function(){
						$(this).val("");
					});
					$(".profile").removeAttr("readonly");
					$(".click_no").css("display","none");
					$(".filebox label").css("margin-left","0");
					$("#teacher_button").val("확인");
					
					clickIsOk = false;
					
				}else{
					var userName = $("#userName").val();
					var tProfile = $("#tProfile").val();
					var tListening = $("#tListening").val();
					var tRead = $("#tRead").val();
					var tWriting = $("#tWriting").val();
					var tGrade = $("#tGrade").val();
					var profileFile = $("#provePath_file")[0].files[0];
					var gradeFile = $("#gradePath_file")[0].files[0];
					var teacherProfile = new FormData();
					var teacherGrade = new FormData();
					teacherProfile.append("file",profileFile);
					teacherGrade.append("file",gradeFile);
					
					//파일 ajax
					if(typeof profileFile != "undefined" ){
						$.ajax({
							
							type:'post',
							url:'/file/teacherProfile',
							data:teacherProfile,
							dataType:'text',
							async:false,
							contentType:false,
							processData:false,
							success:function(result){
								console.log(result);
								if(result == "Profile_NO"){
							 		alert("프로필 파일을 이미지로 업로드 해주세요");
							 		ajax1 = false;
							 	}else{
							 		var key = "profile";
							 		provePath = getDbFileName(result,key);
							 		console.log(provePath);
									ajax1= true;
							 	}
							}
						});
					}
						
					if(typeof gradeFile != "undefined"){
						$.ajax({
							type:'post',
							url:'/file/teacherGrade',
							data:teacherGrade,
							async:false,
							dataType:'text',
							contentType:false,
							processData:false,
							success:function(result){
								console.log(result);
							 	if(result == "Grade_NO"){
							 		alert("성적파일을 이미지로 업로드 해주세요");
							 		ajax2 = false;
							 	}else{
							 		var key = "grade";
							 		gradePath = getDbFileName(result,key);
							 		console.log(gradePath);
									ajax2 = true;
							 	}
							}
						});
					}
					
					if(ajax1 && ajax2){
						
						$.ajax({
							type:'post',
							url:'/user/teacherDetail',
							data:{
								gradePath:gradePath,
								provePath:provePath,
								userName:userName,
								tProfile:tProfile,
								tListening:tListening,
								tRead:tRead,
								tWriting:tWriting,
								tGrade:tGrade
							},
							async: false,
							dataType:'text',
							success:function(result){
								alert(result);
								location.reload();
							}
						});
						$("#teacher_button").val("수정");
						clickIsOk = true;
					}
				}
			});
		});
		
		function checkImageType(fileName){
			var pattern=/jpg|gif|png|jpeg/i;
			return fileName.match(pattern);
		}
		
		function getDbFileName(fullName, key){
			var num = fullName.indexOf("img");
			if(key == "profile"){
				return fullName.substr(num-1);
			}else if(key == "grade"){
				var id = fullName.indexOf(userNick);
				folderName = fullName.substring(num-1, id+userNick.length+1);
				fileName = fullName.substr(id+userNick.length+1);
				return folderName+fileName;
			}
		}
	</script>
</body>
</html>