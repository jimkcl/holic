<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Header</title>
<jsp:include page="../default/source.jsp"/>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<style>
form { margin: 0px 10px; }

h2 {
  margin-top: 2px;
  margin-bottom: 2px;
}

.container { max-width: 480px; }

.divider {
  text-align: center;
  margin-top: 20px;
  margin-bottom: 5px;
}

.divider hr {
  margin: 7px 0px;
  width: 35%;
}

.left { float: left; }

.right { float: right; }
.useOk {
	cursor: pointer;
	color:blue; 
	font-weight:bold;
}
.useOk:hover{color:red;}
.error_text{
	font-weight:bold;
	font-size:15px;
}
.Check_error{
	color:red;
	font-size:20px;
	font-weight:bold;
	text-align:center;
}
.error_Email{
	text-align:center;
	height: 105px;
}
</style>
<script src="/resources/js/userJoin.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("img").click(function(){
		location.href="/";
	});
});
</script>
</head>
<body>
	<div style="background-image: url(/img/books-1845614_1920.jpg); z-index: -1; width: 100%; height: 100%; position: absolute; opacity: 0.3;"></div>
    <div class="container">
    	<div class="row">
    		<div style="text-align: center;">
    			<p style="">
    				<img style="cursor: pointer; display: inline-block ; width:50%;" src="/img/holic1.png" alt="logo">
    			</p>
    		</div>
    	</div>
    	
		<div class="row">
			<div class="panel panel-primary" style="border: 4px solid black;" >
				<div class="panel-body">
					<form method="POST" action="/user/join" id="isOk">
						<div class="form-group">
							<label class="control-label" for="userId">아이디</label>
							<input required id="userId" name="userId" type="text" placeholder="아이디를 입력하세요" maxlength="12" class="form-control" onkeyup="uIdCheck()" autocomplete="off">
							<div class="error_id"></div>
						</div>
						<c:if test="">
							<div class="form-group">
								<label class="control-label" for="userPassword1">비밀번호</label>
								<input required id="userPassword1" type="password" maxlength="17" class="form-control" placeholder="비밀번호입력하세요" maxlength="17" onkeyup="passCheck1()" autocomplete="off">
								<div class="error_pass1"></div>
							</div>
						</c:if>
						
						<div class="form-group">
							<label class="control-label" for="userPassword">비밀번호 확인</label>
							<input required id="userPassword" name="userPassword" type="password" maxlength="17" placeholder="비밀번호를 재 입력하세요" maxlength="17" class="form-control" onkeyUp="passCheck()" autocomplete="off">
							<div class="error_pass"></div>
						</div>
						
						<div class="form-group">
							<label  class="control-label" for="userNick">닉네임</label>
							<input required id="userNick" name="userNick"  maxlength="5" placeholder="닉네임을 입력하세요" class="form-control" onkeyup="NickCheck()" autocomplete="off">
							<div class="error_Nike"></div>
						</div>
						
						<div class="form-group">
							<label class="control-label" for="userWechat">WeChat</label>
							<input required id="userWechat" name="userWechat" maxlength="20" placeholder="WeChat를 입력하세요" class="form-control" onkeyup="WeChatCheck()" autocomplete="off">
							<div class="error_Wechat"></div>
						</div>
						<div style="padding-left:0;" class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<div class="form-group">
								<label class="control-label" for="userMail1">E-mail</label>
								<input required id="userMail1" class="form-control" placeholder="E-mail입력" onkeyup="Email1Check()" autocomplete="off">
							</div>
						</div>
						<div style="padding-top:6%; padding-left:0; padding-right:0; font-size:20px;" class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
							<span>@</span>
						</div>
						<div class="col-xs-11 col-sm-7 col-md-7 col-lg-7" style="padding-left:0; padding-right:0;">
							<div class="form-group">
								<label class="control-label" for="userMail2">E-mail 타입</label>
								<input required id="userMail2" class="form-control" placeholder="나머지 E-mail을 입력하세요" onkeyup="Email2Check()" autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<div class="error_Email"></div>
							<button id="loginSubmit" type="button" class="btn btn-info btn-block"><strong>동의하고 가입</strong></button>
						</div>
						
						<div class="form-group">
							<p class="Check_error"></p>
							<p>본인은 만 14세 이상이며, <span class="useOk">Holic 이용약관</span>, 개인정보 제공 내용을 확인 하였으면 동의합니다.</p>
						</div>
						
						<div class="form-group">
							<textarea readonly="readonly" class="useOk_text" rows="5" cols="60" style=" resize:none; margin:auto; display:none; width:100%;">Holic 이용약관
회원은 아래에 명시된 약관의 내용에 따라 사이트를 이용함에 동의한다.

1. Holic 사이트 가입자는 이하 회원이라 총칭한다.

2. 회원은 사이트를 이용함에 있어 사이트 운영자가 명시하는 사이트 이용 약관에 입각하여 사이트를 이용하며, 해당 약관에 명시되어 있거나 혹은 명시되어 있지 않으나 운영진 측이 문제가 있다고 판단되는 행동을 할 경우 그에 상응하는 제재를 받을 수 있음에 동의한다.

3. 이하 어쩌고저쩌고..
							</textarea>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>