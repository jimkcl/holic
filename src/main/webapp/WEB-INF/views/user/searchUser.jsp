<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Holic - 회원정보 찾기</title>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bs/css/bootstrap.css">
<style type="text/css">
.searchIDDiv h2, 
.searchPWDiv h2 {
	font-family: NANUMSQUAREER;
	font-weight: bold; 
	display: inline-block;
	margin-top: 0;
}
.searchIDDiv > p ,
.searchPWDiv > p {
	font-size: 12px;
	float: right;
	margin: 23px 0 0 0;
}

.searchIDForm, 
.searchPWForm {
	margin: auto;
	margin-bottom: 60px;
	padding: 15px;
	border: 4px solid black;
}
.searchIDForm > input ,
.searchPWForm > input {
	width: 100%;
	margin-bottom: 10px;
	padding: 6px 12px;
}

.message {
	text-align: center;
	font-size: 12px;
	color: red;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
	// 엔터키 설정 - id
	$(".searchIDForm input[name=userMail]").keydown(function(key){
		if(key.keyCode == 13) $(".searchIDForm input[type=button]").trigger("click");
	});
	
	// 엔터키 설정 - pw
	$(".searchPWForm input[name=userId]").keydown(function(key){
		if(key.keyCode == 13) $(".searchPWForm input[name=userMail]").focus();
	});
	$(".searchPWForm input[name=userMail]").keydown(function(key){
		if(key.keyCode == 13) $(".searchPWForm input[type=button]").trigger("click");
	});
	
	
	// 아이디 찾기
	$(".searchIDForm input[type=button]").click(function(){
		var checkMail = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i	// 메일
		
		var userMail = $(".searchIDForm").children("input[name=userMail]");
		var message = $(".searchIDForm").children(".message");
		
		// 유효성 검사
		if(userMail.val().length < 5){
			message.text("메일은 5자이상 입력해야 합니다");
			userMail.focus();
			return;
		}else if(!checkMail.test(userMail.val())){
			message.text("올바른 메일 형식이 아닙니다.");
			userMail.focus();
			return;
		}
		
		message.text("");
		
		// ajax
		$.ajax({
			url		: "/user/searchId",
			type	: "post",
			async	: false,
			data	: { "userMail" : userMail.val() },
			dataType: "text",
			success	: function(data){
				if(data == '') alert("일치하는 메일이 없습니다.\n\n확인후 다시 시도해 주시기 바랍니다.");
				else message.text("아이디 : " + data);
			},
			error	: function(data, status){
				console.log(data);
				console.log(status);
			}
		});
	});
	
	// 비밀번호 찾기
	$(".searchPWDiv input[type=button]").click(function(){
		var checkMail = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i;
		var checkId = /[a-zA-Z0-9]/;
		
		var userId = $(".searchPWForm").children("input[name=userId]");
		var userMail = $(".searchPWForm").children("input[name=userMail]");
		var message = $(".searchPWForm").children(".message");
		
		// 유효성 검사
		if(userId.val().length < 4){
			message.text("아이디는 4자이상 입력해야 합니다.");
			userId.focus();
			return;
		}else if(userMail.val().length < 5){
			message.text("메일은 5자이상 입력해야 합니다.");
			userMail.focus();
			return;
		}else if(!checkId.test(userId.val())){
			message.text("아이디는 영문 대소문자 숫자만 가능합니다.");
			userId.focus();
			return;
		}else if(!checkMail.test(userMail.val())){
			message.text("올바른 메일형식이 아닙니다.");
			userMail.focus();
			return;
		}
		
		message.text("");
		
		// ajax
		$.ajax({
			url		: "/user/searchPassword",
			type	: "post",
			async	: false,
			data	: { "userId" : userId.val(), 
						"userMail" : userMail.val() },
			dataType: "text",
			success	: function(data){
				if(data == -1) alert("잘못된 입력입니다.\n\n확인후 다시 시도해 주시기 바랍니다.");
				else if(data == 0) alert("일치하는 정보가 없습니다.\n\n확인후 다시 시도해 주시기 바랍니다.");
				else message.text("등록된 메일로 새로운 비밀번호를 발송하였습니다.");
			},
			error	: function(data, status){
				console.log(data);
				console.log(status);
			}
		});
	});
});
</script>
</head>
<body>
	<jsp:include page="../checkPage/notLoginCheck.jsp" />
	<jsp:include page="../default/header.jsp" />
	<section id="main">
		<div class="row">
			<div class="col-md-3"></div>
			
			<!-- 아이디 찾기 -->
			<div class="searchIDDiv col-md-6">
				<h2>아이디 찾기</h2>
				<p>등록정보를 통해 아이디를 찾습니다.</p>
				
				<hr style="margin-top: 0;"/>
				
				<div class="searchIDForm">
					<p>※ 등록되어 있는 정보를 입력해 주시면 ID의 일부를 찾을 수 있습니다.</p>
					
					<hr style="margin-top: 0;"/>
					
					<input type="email" name="userMail" placeholder="메일">
					
					<hr style="margin: 0 0 10px 0;"/>
					<p class="message"></p>
					<hr style="margin-top: 0;"/>
					
					<input type="button" value="확인" class="btn">
				</div>
			</div>
			
			<div class="col-md-3"></div>
			
			<hr style="width: 100%;"/>
			
			<div class="col-md-3"></div>
			
			<!-- 비밀번호 찾기 -->
			<div class="searchPWDiv col-md-6">
				<h2>비밀번호 찾기</h2>
				<p>등록정보를 통해 새로운 비밀번호를 발급받습니다.</p>
				
				<hr style="margin-top: 0;"/>
				
				<div class="searchPWForm">
					<p>※ 등록되어 있는 메일로 새로운 비밀번호를 발송해 드립니다.</p>
					
					<hr style="margin-top: 0;"/>
					
					<input type="text" name="userId" placeholder="ID">
					<input type="email" name="userMail" placeholder="메일"><br/>
					
					<hr style="margin: 0 0 10px 0;"/>
					<p class="message"></p>
					<hr style="margin-top: 0;"/>
					
					<input type="button" value="확인" class="btn">
				</div>
			</div>
			
			<div class="col-md-3"></div>
		</div>
	</section>
	<jsp:include page="../default/footer.jsp" />
</body>
</html>