<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Header</title>
<jsp:include page="../default/source.jsp"/>
<style>
form {
    border: 3px solid #f1f1f1;
}

input[type=text], input[type=password] {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
}

button {
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
}

button:hover {
    opacity: 0.8;
}

.cancelbtn {
    width: auto;
    padding: 10px 18px;
    background-color: #f44336;
}

.imgcontainer {
    text-align: center;
    margin: 24px 0 12px 0;
}

.imgcontainer > img { cursor: pointer; }
/* 
img.avatar {
    width: 40%;
    border-radius: 50%;
}
 */
.container {
    padding: 16px;
}

span.psw {
    float: right;
    padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
    span.psw {
       display: block;
       float: none;
    }
    .cancelbtn {
       width: 100%;
    }
}
</style>
<script type="text/javascript">
	$(document).ready(function(){
		$(".imgcontainer img").click(function(){
			location.href="${pageContext.request.contextPath}/";
		});
		
		$("input[type=text]").keydown(function(key){
			if(key.keyCode == 13) $("input[type=password]").focus();
		});
		
		$("input[type=password]").keydown(function(key){
			if(key.keyCode == 13) $(".login_submit").trigger("click");
		});
	});
</script>
</head>
<body>
	<div style="background-image: url(/img/books-1845614_1920.jpg); z-index: -1; width: 100%; height: 100%; position: absolute; opacity: 0.3;"></div>
	<form action="/user/loginPost" method="post">
		<div class="imgcontainer">
		  <img style="width: 25%;" src="${pageContext.request.contextPath}/img/holic1.png" alt="Avatar" class="avatar">
		</div>
		
		<div class="container">
		  <label><b>아이디</b></label>
		  <input type="text" placeholder="아이디입력" name="userId" id="userId" autocomplete="off">
		
		  <label><b>비밀번호입력</b></label>
		  <input type="password" placeholder="비밀번호입력" name="userPassword" id="userPassword" >
		      
		  <button type="button" class="btn login_submit" style="background-color: black; color: white; font-weight: bold;">Login</button>
		  <input type="checkbox" checked="checked"> 자동로그인
		</div>
		
		<div class="container" style="background-color:#f1f1f1">
			<button id="join" type="button" class="btn cancelbtn">회원가입</button>
		  <span class="psw"><a href="../user/searchUser">아이디/비밀번호찾기</a></span>
		</div>
	</form>
	<script type="text/javascript">
		var userId = "";
		var userPassword = ""; 
		
		$(document).ready(function(){
			$(".imgcontainer > img").click(function(){
				location.href="${pageContext.request.contextPath}/";
			});
	 
			$(".login_submit").click(function(){
				userId = $("#userId").val();
			    userPassword = $("#userPassword").val();
				getLoginMessage(userId, userPassword);	
			});
			$("#join").click(function(){
				self.location="/user/join";
			});
		});
		
		function getLoginMessage(userId, userPassword){
			
			var userName ="";
			var loginOk = "";
			$.ajax({
				type:'post',
				url:'/user/loginPost',
				dataType:"text",
				data:{	
						userId:userId,
						userPassword:userPassword 
				},
				success:function(result){
					var number = result.indexOf("&");
					if(number != -1){
						loginOk = result.substr(0,number);
						userName = result.substr(number+1);
					}
					if(result == "NO_ID"){
						alert("동일한 아이디가 없습니다");
					}else if(result == "NO_PASS"){
						alert("비밀번호가 틀렸습니다.")
					}else if(loginOk == "YES_LOGIN"){
						alert(userName+"님 저의 사이트에 방문하여주셔서 감사합니다.");
						self.location="/";
					}
				}
			});
		}
	</script>
</body>
</html>