<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Header</title>
<jsp:include page="source.jsp" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/defaultCSS.css">
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/earlyaccess/hanna.css">
<style>
.nav.navbar-nav li a{
	font-family: NANUMSQUAREEB; 
	font-size:25px;
	padding: 15px 40px;
}
.navbar-brand{
	color:white; 
	font-size:42px;
}
.logo img{
	cursor: pointer;
	display:block;
	width:300px;
	height:160px;
	margin:auto;
}
header{
	background-image:url('/resources/img/notebook-1587526_1920.jpg');
	/* background-image:url('/resources/img/books-1845614_1920.jpg'); */
	/* background-image:url('/resources/img/person-690157_1920.jpg'); */
	/* background-image:url('/resources/img/writing-1149962_1920.jpg'); */
	background-size:cover;
	margin-top:-25px;
	background-position:30% 62%;
}
.container1{
	width:100%;
	height:230px;
}

.container1{
	padding-top:30px;
}

.container1 ul {
	list-style-type: none;
	margin: 0;
	padding: 0;
	overflow: hidden;
}

.container1 li {
    float:right;
}

.container1 li a {
    text-decoration: none;
    text-shadow: 1px 1px 5px black;
    text-align: center;
    color: white;
    display: block;
    padding: 14px 16px;
}

.container1 li a:hover:not(.active) {
    color:black;
    font-weight:bold;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
	$(".logo").click(function(){
		location.href="${pageContext.request.contextPath}/";
	});
});
</script>
</head>
<body> 
	<header>
		<div class="container1">
			<ul>
				<li class="logConnect"></li>
				<c:if test="${ sessionScope.LOGIN.userType != null }">
					<li><a href="${pageContext.request.contextPath}/user/logout">로그아웃</a></li>
				</c:if>
				<c:if test="${ sessionScope.LOGIN.userType == null }">
					<li><a href="${pageContext.request.contextPath}/user/login">로그인</a></li>
					<li><a href="${pageContext.request.contextPath}/user/join">회원가입</a></li>
				</c:if>
				<c:if test="${ sessionScope.LOGIN.userType != null }">
					<li><a href="javascript:alert('준비중');">알람</a></li>
					<li><a href="${pageContext.request.contextPath}/user/userPage">Mypage</a></li>
				</c:if>
				<c:if test="${ sessionScope.LOGIN.userType >= 8 }">
					<li><a href="${pageContext.request.contextPath}/operator/operatorMain">운영자 페이지</a></li>
				</c:if>
				<li>
					<a style="display: inline-block;">
						<c:if test="${ sessionScope.LOGIN.userType == 1 }">일반회원</c:if>
						<c:if test="${ sessionScope.LOGIN.userType == 2 }">유료회원</c:if>
						<c:if test="${ sessionScope.LOGIN.userType == 5 }">강사회원</c:if>
						<c:if test="${ sessionScope.LOGIN.userType == 8 }">운영자</c:if>
						<c:if test="${ sessionScope.LOGIN.userType == 9 }">관리자</c:if>
						&nbsp;&nbsp;&nbsp;${sessionScope.LOGIN.userNick}
					</a>
				</li>
			</ul>
			<div style="text-align: center;">
				<h1 class="logo" style="margin-top:-30px;display: inline-block;">
					<img src="${pageContext.request.contextPath}/img/holic1.png" alt="">
				</h1>
			</div>
		</div>
		
		<nav class="navbar navbar-inverse">
		  <div class="container-fluid" style="text-align: center;/*  height: 50px; */">
		   <%--  
		   <div class="navbar-header" style="margin-right:200px;">
		      <a class="navbar-brand " href="${pageContext.request.contextPath}/">Holic - Study</a>
		    </div>
		     --%>
		    <ul class="nav navbar-nav" style="float: none; display: inline-block; padding-top: 10px;">
		      <!-- <li class="active"><a href="#">Home</a></li> -->
		      <li><a href="${pageContext.request.contextPath}/videoLecture/videoLectureList" style="margin-left: 0;">강의동영상</a></li>
		      <!-- 
		      <li><a href="javascript:alert('준비중');">영화/드라마</a></li>
		       -->
		      <li><a href="${pageContext.request.contextPath}/teacher/teacherProfileList">강사/스터디</a></li>
		      <li><a href="${pageContext.request.contextPath}/payment/paymentMain">결제/안내</a></li>
		      <!-- 
		      <li><a href="javascript:alert('준비중');">회사소개</a></li>
		      -->
		      <li><a href="${pageContext.request.contextPath}/noticeboard/listCriteria">공지/문의 게시판</a></li>
		    </ul>
		  </div>
		</nav>
	</header>
</body>
</html>