<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- HanJungIl - 작성 - 170610 ~ 170630 -->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/default/footer.css">
<style type="text/css">
body > footer,
body > footer .col-lg-12 {
	background-color: #222;
	color: white; 
	margin: 0; 
}
.footerNav { 
	border-top: 1px solid rgb(180,180,180); 
	border-bottom: 1px solid rgb(180,180,180); 
}
.footerNav > ul { 
	text-align: center;
	background-color: rgb(230,230,230); 
	width: 100%;
	margin: 0;
	padding: 0;
}
.footerNav > ul > li { 
	list-style: none;
	display: inline-block;
	line-height: 40px;
	padding: 0 20px;
}
.footerNav > ul > li > a { color: black; }
footer > .row { 
	margin: auto; 
	margin-top: 20px;
	padding: 20px 0;
}
footer > .row > .col-lg-12 {
	font-size: 12px;
	margin: 10px 0;
}
.address {
	text-align: left;
	font-size: 11px;
	float: left;
	padding: 0 0 0 20px;
	border-left: 1px dotted gray;
}
.address:FIRST-CHILD { border-left: none; }
.address > p:FIRST-CHILD {
	font-weight: bold;
	font-size: 15px;
	margin-bottom: 5px;
} 
.address > a { 
	cursor: pointer; 
	font-weight: bold;
	font-size: 15px;
	color: white;
	display: inline-block;
	padding-bottom: 5px;
}
.address > p > a { 
	cursor: pointer;
	color: white; 
}
footer > .row > p > span {
	font-weight: bold;
	color : #6e85a0;
}
footer > .col-lg-12 {
	text-align: center;
	font-size: 12px;
	border-top: 1px solid rgb(200,200,200);
	padding-top: 10px;
}
</style>

<footer>
	<div class="footerNav">
		<ul>
			<li onclick="alert('준비중');"><a>회사소개</a></li>
			<li onclick="alert('준비중');"><a>이용약관</a></li>
			<!-- <li onclick="alert('준비중');"><a>전자금융거래약관</a></li> -->
			<li onclick="alert('준비중');"><a>개인정보처리방침</a></li>
			<li onclick="alert('준비중');"><a>문의</a></li>
		</ul>
	</div>
	<div class="row">
		<div class="address col-md-4">
			<a onclick="alert('준비중');" style="font-family: NANUMSQUAREEB">고객센터</a>
			<p>
				<a onclick="alert('준비중');">상담가능시간 : 오전 9시~오후 6시 (토요일 및 공휴일은 휴무)<br/>
					서울 어딘가 여기저기 건물안 어딘가<br/>
					Tel : 0000-0000	Fax : 02-000-0000<br/>
					Mail : mail@mail.co.kr<br/>
				</a>
			</p>
		</div>
		<div class="address col-md-4">
			<p style="font-family: NANUMSQUAREEB">Holic</p>
			<p>서울시 어딘가 여기저기 이런건물 저런건물<br/>
				대표이사 : H-CL<br/>
				사업자등록번호 :	000-00-00000<br/>
				통신판매업신고 : 저기	00000호	Fax : 02-000-0000
			</p>
		</div>
		<div class="address col-md-4">
			<p style="font-family: NANUMSQUAREEB">전자금융거래분쟁담당</p>
			<p>Tel : 0000-0000<br/>
				Fax : 02-000-0000<br/>
				E-mail : Agick@meWhan.sung
			</p>
		</div>
	</div>
	<div class="col-lg-12">
		<p>(주) Holic의 사전 서면 동의 없이 Holic 사이트의 일체의 정보, 콘텐츠 및 UI등을 상업적 목적으로 전재, 전송, 스크래핑 등 무단 사용할 수 없습니다<br/>
		<p>Copyright &copy; 2017 by JungIl & MinKi & ChoonLim. All pictures cannot be copied without permission.</p>
		<p style="font-size: 11px;">이 사이트는 네이버 나눔글꼴을 사용하여 제작되었습니다.</p>
	</div>
</footer>