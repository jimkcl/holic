<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- el문법 쓸 때 꼭 써줘야함 -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<title>Holic - 공지사항</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
.noticeHeaderNav h2{
	font-family: NANUMSQUAREER;
	font-weight: bold; 
	display: inline-block; 
}
.noticeHeaderNav p { 
	float: right;
	font-size: 12px;
	margin: 40px 10px 0 0; 
}

.container{
	margin-top: 20px;
}
   
#search_form{
	width: 50%;
	margin: auto;
}

.table thead tr {
	background-color: black; 
	color: white;
}
.table th, 
.table td {
	text-align: center;
}
.table tbody tr:LAST-CHILD { 
	border-bottom: 1px solid black; 
}
.table tbody tr:NTH-CHILD(2n-1) {
	background-color: #F5F5F5;
}
</style>
</head>
<body>
	<jsp:include page="../default/header.jsp" />
	<section id="main">
		<div class="noticeHeaderNav">
			<h2>공지/문의 게시판</h2>
			<p>※ 공지사항을 볼 수 있습니다.</p>
		</div>
		
		<hr style="margin: 0 0 20px 0;"/>
		
		<div class="container">
			<div class="row">
				<div class="btn-group btn-group-justified" style="margin-bottom: 40px;">
	  				<a href="../noticeboard/listCriteria" class="btn btn-default" style="background-color: rgb(180, 180, 180);">공지사항</a>
	  				<a href="../askboard/listCriteria" class="btn btn-default">문의게시판</a>
				</div>
				<table class="table table-hover">
					<thead>
						<tr>
							<!-- <th>글번호</th> -->
							<th style="width: 55%;">제목</th>
							<th>작성자</th>
							<th>작성일자</th>
							<th>조회수</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${list}" var="NoticeBoardVO" varStatus="index">
							<tr>
								<%-- <td>${index.count}</td> --%>
								<td style="display:none;">${NoticeBoardVO.idx}</td>
								<td style="text-align: left;">
									<input type="hidden" value="${pageMaker.makeQuery(pageMaker.cri.page)}"><a class="titleClick">${NoticeBoardVO.aTitle}</a>
								</td>
								<td>${NoticeBoardVO.userNick}</td>
								<td>
									<fmt:parseDate var="data" value="${ fn:substring(NoticeBoardVO.aDate,0,19) }" pattern="yyyy-MM-dd HH:mm:ss"/>
									<fmt:formatDate value="${ data }" pattern="yyyy년 MM월 dd일 HH시 mm분 ss초"/>
								</td>
								<td>${NoticeBoardVO.aCount}</td>
							</tr>
						</c:forEach>
						<c:if test="${ fn:length(list) == 0 }">
							<tr style="background-color: rgba(0,0,0,0);">
								<td colspan="5">
									<h1 style="margin: 60px 0;">공지사항이 없습니다.</h1>
								</td>
							</tr>
						</c:if>
					</tbody>
				</table>
					
				<c:if test="${sessionScope.LOGIN.userType>=8}">
					<div align="right" class="row">
						<a class="btn btn-default" href="/noticeboard/create${pageMaker.makeQuery(pageMaker.cri.page)}">글쓰기</a>
					</div>
				</c:if>
				
				<div id="search_form" align="center" class="input-group">
					<span id="sspan" class="input-group-addon"> <!-- input-group을 통해 한번에 묶음 -->
						<select id="ssel" name="searchType">
							<option disabled>검색기준</option>
							<option ${cri.searchType=='userNick'?'selected':''} value="userNick">닉네임</option>
							<option ${cri.searchType=='aTitle'?'selected':''} value="aTitle">제목</option>
							<option ${cri.searchType=='aContent'?'selected':''} value="aContent">내용</option>
						</select>
					</span>
					
					<input class="form-control" value="${cri.keyword}" id="keyword" name="keyword">
					<span class="input-group-btn">
						<button class="btn btn-success searchBtn">검색</button>
					</span>
				</div>
			</div>
					
			<div class="row" align="center">
				<ul class="pagination">
					<c:if test="${pageMaker.cri.page>1}">
						<li><a href="listCriteria${pageMaker.makeQuery(pageMaker.cri.page-1)}">&laquo;</a></li>
					</c:if>
					
					<c:forEach var="idx" end="${pageMaker.endPageNum}" begin="${pageMaker.startPageNum}">
						<li <c:out value="${pageMaker.cri.page==idx?'class=active' : ''}" />><a href="listCriteria${pageMaker.makeQuery(idx)}">${idx}</a></li> <!-- 페이지번호 -->
					</c:forEach>
							
					<c:if test="${pageMaker.cri.page<pageMaker.totalPage}">
						<li><a href="listCriteria${pageMaker.makeQuery(pageMaker.cri.page+1)}">&raquo;</a></li>
					</c:if>
				</ul>
			</div>
		</div>
	</section>
	<jsp:include page="../default/footer.jsp" />
	
	<script type="text/javascript">
		$(document).ready(function(){
			var result1='${msg}';
			var result2='${param.msg}';
				
			if(result1=='SUCCESS'){
				alert("작업이 정상적으로 완료되었습니다.")
			} else if(result1=="DELETE_SUCCESS" || result2=="DELETE_SUCCESS"){
				alert("삭제되었습니다.")
			} 
			
			/* $(document).ready(function() {
				$("#notice1 strong").click(function(){
					
				});
			} */
			/* $(document).ready(function() {
				var date = ${NoticeBoardVO.aDate};
				date.substr(0, 9);
				
			});  */
			
			$(".searchBtn").click(function(){
				var uri="listCriteria${pageMaker.makeQuery(1)}"+"&searchType="+$("select#ssel option:selected").val()+"&keyword="+$("#keyword").val();
				uri=encodeURI(uri);
				self.location=uri;
			});
			
			$(".titleClick").click(function(event){
	            //href="/noticeboard/read${pageMaker.makeQuery(pageMaker.cri.page)}&idx=${NoticeBoardVO.idx}"
	            var pageMaker = $(this).prev().val();
	            var idx = $(this).parents().prev().html();
	            var uri= "/noticeboard/read"+pageMaker+ "&idx=" + idx;
	            location.href=uri;
	         });
		});
	</script>
</body>
</html>