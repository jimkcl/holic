<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Holic - 공지글 작성</title>
<style type="text/css">
.noticeHeaderNav h2{
	font-family: NANUMSQUAREER;
	font-weight: bold; 
	display: inline-block; 
}
.noticeHeaderNav p { 
	float: right;
	font-size: 12px;
	margin: 40px 10px 0 0; 
}
</style>
</head>
<body>
	<jsp:include page="../checkPage/operatorCheck.jsp" />
	<jsp:include page="../default/header.jsp" />
	<section id="main">
		<div class="noticeHeaderNav">
			<h2>공지/문의 게시판</h2>
			<p>※ 문의글 작성합니다.</p>
		</div>
		
		<hr style="margin: 0 0 20px 0;"/>
		
		<div class="container">
			<div class="row">
			
				<h3 style="margin-top: 0; font-family: NANUMSQUAREER; font-weight: bold; ">공지 등록</h3>
				
				<form id="noticeform" method="post">
					<input value="${cri.page}" name="page" type="hidden">
					<input value="${cri.perPageNum}" name="perPageNum" type="hidden">
					
					<div class="form-group" style="width: 73%; display: inline-block;">
						<label for="aTitle">제목</label>
						<input name="aTitle" id="aTitle" class="form-control">
					</div>
					
					<div class="form-group" style="width: 25%; display: inline-block; float: right;">
						<label for="userNick">닉네임</label>
						<input value="${sessionScope.LOGIN.userNick}" readonly="readonly" name="userNick" id="userNick" class="form-control">
					</div>
					
					<div class="form-group">
						<label for="aContent">내용</label>
						<textarea name="aContent" id="aContent" class="form-control" placeholder="내용을 작성해 주세요."
							style="height: 400px; resize: none;"></textarea>
					</div>
					
					<div class="form-group"  style="text-align: center;">
						<label for="aType" style="margin-right: 10px;">공개</label>
						<select id="aType" name="aType" style="padding: 5px 10px;">
							<option value="selected">선택</option>
							<option value="0">전체공개</option>
							<option value="1">일반회원 공개</option>
							<option value="2">유료회원 공개</option>
							<option value="5">강사회원 공개</option>
						</select>
					</div>
				</form>
				
				<div class="form-group" style="text-align: center;">
					<button class="btn btn1" type="submit" style="padding: 6px 40px;">등록</button>
					<button class="btn btn2" type="reset" style="margin-left: 10px; padding: 6px 40px;">취소</button>
				</div>
			</div>
			
		</div>
	</section>
	<jsp:include page="../default/footer.jsp" />
	
	<script type="text/javascript">
		$(document).ready(function(){
			$(".btn1").click(function(){
				var noticeform = $("#noticeform");
				var aTitle = $("#aTitle");
				var aContent = $("#aContent");
				var userNick = $("#userNick");
			
				if(aTitle.val().length>30) {
					alert("30자 내로 작성하세요.");
					aTitle.focus();
					return false;
				} else if (aContent.val().length>255) {
					alert("255자 내로 작성하세요");
					aContent.focus();
					return false;
				} else if($("#aTitle").val()==""){
					alert("제목을 입력하세요");
					$("#aTitle").focus();
					return false;
				} else if($("#aContent").val()==""){
					alert("내용를 입력하세요");
					$("#aContent").focus();
					return false;
				} else if($("#aType").val()=="selected"){
					alert("타입을 선택하세요");
					$("#aType").focus();
					return false;
				} else if(userNick.val()=="") { 
					alert("로그인을 해야 이용이 가능합니다");
					return false;
				} else {
					var aType = $("#aType").val();
					
					if(aType=="user"){
						aType = 1;
					}else if(aType=="decluser"){
						aType = 2;
					}else if(aType=="teacher"){
						aType = 5;
					}
					noticeform.attr("action", "../noticeboard/create");
					noticeform.append("<input type='hidden' name='aType' value='" + aType + "'>");
					noticeform.submit();
				}
			});
				
			$(".btn2").click(function(){
				self.location="/noticeboard/listCriteria";
			});
		});
	</script>
</body>
</html>