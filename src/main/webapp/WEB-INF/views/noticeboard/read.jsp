<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<title>Holic - ${ vo.aTitle }</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<style type="text/css">
.noticeHeaderNav h2{
	font-family: NANUMSQUAREER;
	font-weight: bold; 
	display: inline-block; 
}
.noticeHeaderNav p { 
	float: right;
	font-size: 12px;
	margin: 40px 10px 0 0; 
}

.reply_one { margin: 10px; }

.board_view_aTitle {
	font-size: 18px;
	color: white;
	background-color: black;
	padding: 20px;
}

.board_view_userNick {
	background-color: #f5f5f5;
	border-bottom: 1px solid grey;
}
.board_view_userNick p { margin: 10px; }

.board_view_aContent {
	font-size: 14px;
	padding: 40px 20px;
	border-bottom: 1px solid grey;
}


#content {
	width: 50%;
	margin: auto;
	margin-top: 10px;
	height: 500px;
	border: 1px solid black;
	overflow: auto;
}

#two_btn {
	margin: auto;
	margin-top: 10px;
}
#two_btn button {
	padding: 6px 40px;
}
</style>
</head>
<body>
	<jsp:include page="../default/header.jsp" />
	
	<form role="form" method="post">
		<input value="${vo.idx}" name="idx" type="hidden">
		<input value="${cri.page}" name="page" type="hidden">
		<input value="${cri.perPageNum}" name="perPageNum" type="hidden">
	</form>
	
	<section id="main">
		<div class="noticeHeaderNav">
			<h2>공지 게시판</h2>
			<p>※ 공지사항을 확인할 수 있습니다.</p>
		</div>
		
		<hr style="margin: 0 0 20px 0;"/>
		
		<div class="container">
			<div class="row">
				<div class="form-group">
					<div class="board_view_aTitle">${vo.aTitle}</div>
						<div class="board_view_userNick">
							<p style="display: inline-block;">작성자 : ${vo.userNick}</p>
							<p style="float: right; font-size: 12px;">
								<fmt:parseDate var="data" value="${ fn:substring(vo.aDate,0,19) }" pattern="yyyy-MM-dd HH:mm:ss"/>
								<fmt:formatDate value="${ data }" pattern="yyyy년 MM월 dd일 HH시 mm분 ss초"/>
							</p>
						</div>
					<div class="board_view_aContent">${vo.aContent}</div>
				</div>
			</div>
	
			<div class="row">
				<!-- 업로드한 이미지 파일 들어갈 곳 -->
			</div>
	
			<div id="two_btn" class="form-group">
				<div align="center">
					<c:if test="${ vo.userKey == sessionScope.LOGIN.userKey }">
						<button class="btn btn-reset" type="submit" style="margin-right: 5px;">삭제</button>
					</c:if>
					<button class="btn btn-primary" type="submit">목록</button>
				</div>
			</div>
		</div>
		
		<script type="text/javascript">
			$(document).ready(function(){
				var $form=$("form[role='form']");
				
				$(".btn-reset").click(function(){
					$form.attr("method", "post");
					$form.attr("action", "/noticeboard/delete");
					$form.submit();
				});
				
				$(".btn-primary").click(function(){
					$form.attr("method", "get");
					$form.attr("action", "/noticeboard/listCriteria");
					$form.submit();
				});
			});
		</script>
	</section>
	<jsp:include page="../default/footer.jsp" />
</body>
</html>