<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
	
	.table{
		width: 50%;
		margin: auto;
		margin-top: 20px; 
		padding: 20px;
		border-top: 2px solid black;
    	background-color: white;
    	
	}
	
	#content{
	
		width: 50%;
		margin: auto;
		margin-top: 10px;
		height: 500px;
		border: 1px solid black;
		overflow: auto;
	}
	
	#two_btn{
		margin: auto;
		margin-top: 10px;
		
	}
</style>
</head>
<body>
<jsp:include page="../default/header.jsp" />
	<c:if test="${sessionScope.LOGIN.userType >= vo.aType}">
		<script type="text/javascript">
		    /* userType 숫자체크하여 검사하기*/
			location.href="/";
		</script>
	</c:if>
	
	<form role="form" method="post">
		<input value="${vo.idx}" name="idx" type="hidden">
		<input value="${cri.page}" name="page" type="hidden">
		<input value="${cri.perPageNum}" name="perPageNum" type="hidden">
		<input value="${cri.searchType}" name="searchType" type="hidden">	
		<input value="${cri.keyword}" name="keyword" type="hidden">		
	</form>
	
	<div class="container">
	
		<div class="row">
			<table class="table">
				<thead>
					<tr>
						<th>${vo.aTitle}</th>
						<th style="text-align: right;">${vo.aDate}</th>
					</tr>
				</thead>
			</table>
		</div>
		
		<div class="row">
			<p id="content">${vo.aContent}</p>
		</div>
		
		<div class="row">
			<!-- 업로드한 이미지 파일 들어갈 곳 -->
		</div>
		
		<div id="two_btn" class="form-group">
			<div align="center" class="col-sm-offset-2 col-sm-10">
				<%-- 
				<c:if test="${not empty login}"> <!-- 로그인 객체를 가지고 있을때만.. 댓글버튼 나오게하기 -->
					<button id="reply_form" class="btn btn-primary" type="submit">댓글</button>
				</c:if> 
				--%>
				<button class="btn btn-reset" type="submit">삭제</button>
				<button class="btn btn-primary" type="submit">목록</button>
			</div>	
		</div>	
			
	</div>

	<script type="text/javascript">
		$(document).ready(function(){
			var $form=$("form[role='form']");
			
			$(".btn-reset").click(function(){
				$form.attr("method", "post");
				$form.attr("action", "/delete");
				$form.submit();
			});
			
			$(".btn-primary").click(function(){
				$form.attr("method", "get");
				$form.attr("action", "/noticeListCriteria");
				$form.submit();
			});
			
		});
		
	</script>

	<jsp:include page="../default/footer.jsp" />
</body>
</html>