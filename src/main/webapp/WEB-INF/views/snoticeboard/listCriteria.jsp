<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
    <!-- el문법 쓸 때 꼭 써줘야함 -->
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
    
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<style type="text/css">
	.container{
		margin-top: 20px;
	}
	
	/* .notice {
    padding: 15px;
    background-color: #fafafa;
    border-left: 6px solid #7f7f84;
    margin-bottom: 10px;
    -webkit-box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
       -moz-box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
            box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
	}
	
	.notice-info {
    border-color: #45ABCD;
	}
	
	.notice-info>strong {
    color: #45ABCD;
    }
    
    .wrapper{
    	float : left;
    	
    }
    
    #notice1{
    	display: block;
    }
    
    #ask{
    	display: block;
    } */
</style>
<title>공지사항</title>
</head>
<body>
<jsp:include page="../default/header.jsp" />
	<div class="container">
		<div class="row">
			<!-- <div class="wrapper">
				<div id="notice1" class="notice notice-info">
			       <a href="../noticeboard/listCriteria"><strong>공지사항</strong></a>
			    </div>
			   	
				<div id="ask" class="notice notice-info">
			        <a href="../askboard/listCriteria"><strong>문의게시판</strong></a>
			    </div>
		    </div> -->
			<div class="btn-group btn-group-justified">
  				<a href="../noticeboard/listCriteria" class="btn btn-default">공지사항</a>
  				<a href="../askboard/listCriteria" class="btn btn-default">문의게시판</a>
			</div>
				
			<h3>고객센터</h3>
				<table class="table table-hover">
					<thead>
						<tr>
							<th>글번호</th>
							<th>제목</th>
							<th>작성자</th>
							<th>작성일자</th>
							<th>조회수</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${list}" var="NoticeBoardVO">
							<tr>
								<td>${NoticeBoardVO.idx}</td>
								<td><a class="titleClick" href="/searchboard/read${pageMaker.makeSearch(pageMaker.cri.page)}&idx=${NoticeBoardVO.idx}">${NoticeBoardVO.aTitle}</a></td>
								<td>${NoticeBoardVO.userNick}</td>
								<td>${NoticeBoardVO.aDate}</td>
								<td>${NoticeBoardVO.aCount}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
						
					<div align="right" class="row">
						<a class="btn btn-default" href="/noticeboard/create${pageMaker.makeQuery(pageMaker.cri.page)}">글쓰기</a>
					</div>
						
					<!-- </div>
				</div> -->
			
			<div class="input-group col-sm-10 col-sm-8">
				<span id="sspan" class="input-group-addon"> <!-- input-group을 통해 한번에 묶음 -->
					<select id="ssel" name="searchType">
						<option disabled>검색기준</option>
						<option ${cri.searchType=='userNick'?'selected':''} value="userNick">닉네임</option>
						<option ${cri.searchType=='aTitle'?'selected':''} value="aTitle">제목</option>
						<option ${cri.searchType=='aContent'?'selected':''} value="aContent">내용</option>
					</select>
				</span>
				
				<input class="form-control" value="${cri.keyword}" id="keyword" name="keyword">
				<span class="input-group-btn">
					<button class="btn btn-success searchBtn">검색</button>
				</span>
			</div>
				
	</div>
			
	<div class="row" align="center">
		<ul class="pagination">
			<c:if test="${pageMaker.cri.page>1}"> <!-- 작거나 같으면 이동x -->
				<li><a class="titleClick" href="listCriteria${pageMaker.makeSearch(pageMaker.cri.page-1)}">&laquo;</a></li>
			</c:if>
			
			<c:forEach var="idx" end="${pageMaker.endPageNum}" begin="${pageMaker.startPageNum}">
				<li <c:out value="${pageMaker.cri.page==idx?'class=active' : ''}" />><a class="titleClick" href="listCriteria${pageMaker.makeSearch(idx)}">${idx}</a></li> <!-- 페이지번호 -->
			</c:forEach>
			
			<c:if test="${pageMaker.cri.page<pageMaker.totalPage}">
				<li><a class="titleClick" href="listCriteria${pageMaker.makeSearch(pageMaker.cri.page+1)}">&raquo;</a></li>
			</c:if>
			
		</ul>
	</div>
		
</div>
		
	<script type="text/javascript">
	
		var result1='${msg}';
		var result2='${param.msg}';
			
		if(result1=='SUCCESS'){
			alert("작업이 정상적으로 완료되었습니다.")
		} else if(result1=="DELETE_SUCCESS" || result2=="DELETE_SUCCESS"){
			alert("삭제되었습니다.")
		} 
		
		/* $(document).ready(function() {
			$("#notice1 strong").click(function(){
				
			});
		} */
		/* $(document).ready(function() {
			var date = ${NoticeBoardVO.aDate};
			date.substr(0, 9);
			
		});  */
		
		$(document).ready(function(){
			$(".searchBtn").click(function(){
				var uri="listCriteria${pageMaker.makeQuery(1)}"+"&searchType="+$("select#ssel option:selected").val()+"&keyword="+$("#keyword").val();
				uri=encodeURI(uri);
				self.location=uri;
			});
			
			$(".titleClick").click(function(event){
				event.preventDefault();
				var uri=$(this).attr("href");
				uri=encodeURI(uri);
				self.location=uri;
			});
		
		});
	</script>
	<jsp:include page="../default/footer.jsp" />
</body>
</html>