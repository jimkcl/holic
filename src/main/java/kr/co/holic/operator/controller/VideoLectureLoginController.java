package kr.co.holic.operator.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.co.holic.operator.VO.VideoLectureURLVO;
import kr.co.holic.operator.VO.VideoLectureVO;
import kr.co.holic.operator.service.VideoLectureService;
import lombok.extern.java.Log;

@Log
@Controller
@RequestMapping("videoLecture")
public class VideoLectureLoginController {
	
	@Inject
	private VideoLectureService service;
	
	/* - HanJungIl - 170610 ~ 170630
	 * 강의 동영상 관련 controller - 운영자, 관리자 만 접근 가능
	 */
	
	@RequestMapping(value="videoLecture", method=RequestMethod.GET)
	public String videoLectureUI(HttpSession session, Model model) throws Exception{
		// TODO 강의 동영상 링크 등록 UI
		
		model.addAttribute("vData", service.videoLectureUI());
		
		return "videoLecture/videoLectureUI";
	}
	
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value="videoLecture", method=RequestMethod.POST)
	public ResponseEntity<String> videoLectureCreate(HttpSession session, 
			@ModelAttribute @Valid VideoLectureVO vo,
			@ModelAttribute @Valid VideoLectureURLVO urlVo) throws Exception{
		// TODO 강의 동영상 등록
		
		vo.setUserKey((String) ((Map<String, Object>) session.getAttribute("LOGIN")).get("userKey"));
		vo.setListVURL(urlVo);
		
		service.videoLectureCreate(vo);
		
		return new ResponseEntity<String>("../videoLecture/videoLectureList", HttpStatus.OK);
	}
	
	@RequestMapping(value="videoLectureDelete/{vIdx}")
	public String videoLectureDelete(HttpSession session,
			@PathVariable("vIdx") @Valid @NotNull @Size(min=1) int vIdx) throws Exception {
		// TODO 강의 동영상 삭제
		service.videoLectureDelete(vIdx);
		
		return "redirect:../../videoLecture/videoLectureList";
	}
	
	@ResponseBody
	@RequestMapping(value="videoLectureGetPackage", method=RequestMethod.POST)
	public List<String> videoLectureGetPackage(HttpSession session, Model model) throws Exception {
		// TODO 강의 동영상 패키지(분류) 리스트
		HashMap<String, String> map = new HashMap<String, String>();
		
		List<String> list = service.videoLectureGetPackage();
		
		return list;
	}
	
	@ResponseBody
	@RequestMapping(value="videoLectureUpdate", method=RequestMethod.POST)
	public ResponseEntity<String> videoLectureUpdate(HttpSession session,
			@RequestParam("vIdx") @Valid @NotNull @Size(min=1) int vIdx, 
			@RequestParam("vTitle") @Valid @NotNull String vTitle, 
			@RequestParam("vPackage") @Valid @NotNull String vPackage) throws Exception {
		// TODO 강의 동영상 수정
		VideoLectureVO vo = new VideoLectureVO();
		
		vo.setVIdx(vIdx);
		vo.setVTitle(vTitle);
		vo.setVPackage(vPackage);
		
		service.videoLectureUpdate(vo);
		
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	@ResponseBody
	@RequestMapping(value="videoLectureAddURL" , method=RequestMethod.POST)
	public ResponseEntity<String> videoLectureAddURL(HttpSession session, @ModelAttribute("VideoLectureVO") VideoLectureVO vo) throws Exception{
		// TODO 강의 동영상 URL 추가
//		log.info(" :: URL List :: " + vo);
		
		service.videoLectureAddURL(vo);
		
		return new ResponseEntity<String>("Added URL", HttpStatus.OK);
	}
	
	@ResponseBody
	@RequestMapping(value="videoLectureURLDelete", method=RequestMethod.POST)
	public ResponseEntity<String> videoLectureURLDelete(HttpSession session, @RequestParam("idx") int idx) throws Exception {
		// TODO 강의 동영상 URL 삭제
//		log.info(" :: :: " + idx);
		
		service.videoLectureURLDelete(idx);
		
		return new ResponseEntity<String>("URL is Removed.", HttpStatus.OK);
	}
	
	@ResponseBody
	@RequestMapping(value="videoLectureURLUpdate", method=RequestMethod.POST)
	public ResponseEntity<String> videoLectureURLUpdate(HttpSession session,
			@ModelAttribute("VideoLectureURLVO") @Valid VideoLectureURLVO vo) throws Exception {
		// TODO 강의 동영상 URL 수정
//		log.info(" :: vo :: " + vo);
		
		service.videoLectureURLUpdate(vo);
		
		return new ResponseEntity<String>("URL is update", HttpStatus.OK);
	}
}