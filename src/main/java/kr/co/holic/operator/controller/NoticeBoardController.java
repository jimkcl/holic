package kr.co.holic.operator.controller;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import kr.co.holic.operator.VO.Criteria;
import kr.co.holic.operator.VO.NoticeBoardVO;
import kr.co.holic.operator.VO.PageMaker;
import kr.co.holic.operator.VO.SearchCriteria;
import kr.co.holic.operator.service.NoticeBoardService;
import lombok.extern.java.Log;

@Log
@Controller
@RequestMapping("/noticeboard")
public class NoticeBoardController {
	
	@Inject
	private NoticeBoardService service;
	
	//글등록 폼
	@RequestMapping(value="create", method=RequestMethod.GET)
	public void createGet() throws Exception {
		
	}
	
	//글 등록
	@RequestMapping(value="create", method=RequestMethod.POST)
	public String createPost(NoticeBoardVO vo, RedirectAttributes rttr, HttpServletRequest request, Model model) throws Exception {
		
		HttpSession session = request.getSession();
		Map map = (Map) session.getAttribute("LOGIN");
		vo.setUserKey((String) map.get("userKey"));
		System.out.println((String) map.get("userKey"));
		log.info(" - vo : " + vo);
		service.create(vo);
		rttr.addFlashAttribute("msg", "SUCCESS");
		
		return "redirect:../noticeboard/listCriteria";
	}
	
	//조회수 중복방지
	@RequestMapping("bread")
	public String newRead(HttpServletRequest request, HttpServletResponse response, int idx, Criteria cri, RedirectAttributes rttr) throws Exception {  
		
		Cookie[] cookies = request.getCookies();
		Cookie cookie=null;
		
		if(cookies!=null && cookies.length>0) { 
			System.out.println("배열쿠키가 있음");
			for(int i=0; i<cookies.length; i++) {
				if(cookies[i].getName().equals("ck")) { 
					cookie=cookies[i];
				}
			}
		}
		
		if(cookie == null) { 
			cookie=new Cookie("ck", "|"+idx+"|");
			
			response.addCookie(cookie);
			service.viewCount(idx);
			
		} else {
			String value=cookie.getValue(); 
			if(value.indexOf("|"+idx+"|")<0) { 
				
				value=value+"|"+idx+"|";
				cookie.setValue(value); 
				response.addCookie(cookie); 
				service.viewCount(idx); 
			}
		}
		
		rttr.addAttribute("page", cri.getPage());
		rttr.addAttribute("perPageNum", cri.getPerPageNum());
		rttr.addAttribute("idx", idx);
		
		return "redirect:/noticeboard/read";
	} 
	
	//글 보기
	@RequestMapping(value="read", method=RequestMethod.GET)
	public void read(@RequestParam("idx") int idx, @ModelAttribute("cri") Criteria cri, Model model) throws Exception {
		/*service.viewCount(idx);*/
		NoticeBoardVO vo = service.read(idx);
		model.addAttribute("vo", vo);
	}
	
	//페이징처리된 글목록
	@RequestMapping(value="listCriteria", method=RequestMethod.GET)
	public void listCriteria(@ModelAttribute("cri") SearchCriteria cri, Model model) throws Exception {
		
		List<NoticeBoardVO> list = service.listCriteria(cri);
		model.addAttribute("list", list);
		
		log.info(" - list : " + list + " / " + cri);
		
		int totalCount=service.totalCount();
		PageMaker pageMaker = new PageMaker();
		pageMaker.setCri(cri);
		pageMaker.setTotalCount(totalCount);
		
		model.addAttribute("pageMaker", pageMaker);
	}
	
	//글 삭제
	@RequestMapping(value="delete", method={RequestMethod.POST, RequestMethod.GET})
	public String delete(@RequestParam("idx") int idx, Criteria cri, RedirectAttributes rttr) throws Exception { //조회 화면에서 글 삭제
		
		service.delete(idx);
		
		rttr.addFlashAttribute("msg", "DELETE_SUCCESS");
		rttr.addAttribute("page", cri.getPage());
		rttr.addAttribute("perPageNum", cri.getPerPageNum());
		
		return "redirect:/noticeboard/listCriteria";
	}
	
}
