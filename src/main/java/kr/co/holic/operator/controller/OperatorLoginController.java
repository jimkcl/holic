package kr.co.holic.operator.controller;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.co.holic.operator.VO.UserChangeVO;
import kr.co.holic.operator.service.OperatorService;
import kr.co.holic.util.PageCriteriaVO;
import lombok.extern.java.Log;

@Log
@Controller
@RequestMapping("operator")
public class OperatorLoginController {
	
	@Inject
	private OperatorService servise;
	
	/* - HanJungIl - 170610 ~ 170630
	 * 운영자 관련 controller - 운영자만 접근 가능
	 */
	
	@RequestMapping("operatorMain")
	public String operaterMain(HttpSession session){
		// TODO 운영자 메인 페이지
		return "operator/operatorMain";
	}
	
	@RequestMapping(value="operatorUserList", method=RequestMethod.GET)
	public String operatorUserList(HttpSession session, Model model, 
			@ModelAttribute("page") PageCriteriaVO page) throws Exception{
		// TODO 회원리스트 (검색)
		
		model.addAttribute("list", servise.operatorUserList(page));
		
		return "operator/operatorUserList";
	}
	
	@ResponseBody
	@RequestMapping(value="operatorUserChange", method=RequestMethod.POST)
	public ResponseEntity<String> operatorUserChange(HttpSession session,
			@ModelAttribute UserChangeVO vo) throws Exception{
		// TODO 회원 타입 변경
		
		if(!servise.operatorUserChange(vo)){
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}else{
			return new ResponseEntity<String>(HttpStatus.OK);
		}
	}
}