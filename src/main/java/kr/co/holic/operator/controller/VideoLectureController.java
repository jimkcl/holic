package kr.co.holic.operator.controller;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.co.holic.operator.VO.VideoLectureReplyVO;
import kr.co.holic.operator.VO.VideoLectureVO;
import kr.co.holic.operator.service.VideoLectureService;
import kr.co.holic.util.PageCriteriaVO;
import lombok.extern.java.Log;

@Log
@Controller
@RequestMapping("videoLecture")
public class VideoLectureController {
	
	@Inject
	private VideoLectureService service;
	
	/* - HanJungIl - 170610 ~ 170630
	 * 강의 동영상 관련 controller
	 */
	
//	@RequestMapping("test") public void test() throws Exception { service.test(); }
	
	@RequestMapping(value="videoLectureList", method=RequestMethod.GET)
	public void videoLectureList(HttpSession session, Model model, 
			@ModelAttribute("page") PageCriteriaVO pVO) throws Exception {
		// TODO 강의 동영상 리스트 (검색)
		
		List<VideoLectureVO> list = service.videoLectureList(pVO);
		
		model.addAttribute("list", list);
		model.addAttribute("page", pVO);
	}
	
	@RequestMapping(value="videoLectureDetail/{vIdx}")
	public String videoLectureDetail(HttpSession session, Model model, 
			@PathVariable("vIdx") @Valid @NotNull @Size(min=1) int vIdx) throws Exception {
		// TODO 강의 상세 페이지
		
		Map map = (Map) session.getAttribute("LOGIN");
		Integer userType = 0;
		
		if(map != null) userType = (Integer) map.get("userType");
		
		VideoLectureVO vo = new VideoLectureVO();
		
		vo.setVIdx(vIdx);
		vo.setVType(userType);
		
		vo = service.videoLectureDetail(vo);
		
		if(vo == null) return "redirect:../videoLectureList";
		
		model.addAttribute("videoLectureDetail", vo);
		
		return "videoLecture/videoLectureDetail";
	}
	
	@ResponseBody
	@RequestMapping(value="videoLectureAddReply", method=RequestMethod.POST)
	public ResponseEntity<String> videoLectureAddReply(
			HttpSession session, 
			@Valid VideoLectureReplyVO replyVO) throws Exception{
		// TODO 강의 동영상 평가리플 등록
//		log.info("\n\n :::::: replyVo : " + replyVO);
		
		Map<String, String> map = (Map<String, String>) session.getAttribute("LOGIN");
		
		String key = map.get("userKey");
		
		replyVO.setUserKey(key);
		
		if(!service.videoLectureAddReply(replyVO)){
			return new ResponseEntity<String>("0", HttpStatus.OK);
		}
		
		return new ResponseEntity<String>("1", HttpStatus.OK);
	}
	
	@ResponseBody
	@RequestMapping(value="videoLectureDeleteReply", method=RequestMethod.POST)
	public ResponseEntity<String> videoLectureDeleteReply(
			HttpSession session, 
			@RequestParam("vIdx") int vIdx) throws Exception {
		// TODO 강의 동영상 평가리플 삭제
		
		Map<String, String> map = (Map<String, String>) session.getAttribute("LOGIN");
		
		String key = map.get("userKey");
		
		if(service.videoLectureDeleteReply(vIdx, key) != 1){
			return new ResponseEntity<String>("0", HttpStatus.OK);
		}
		
		return new ResponseEntity<String>("1", HttpStatus.OK);
	}
}