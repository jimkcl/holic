package kr.co.holic.operator.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.co.holic.operator.DAO.OperatorDAO;
import kr.co.holic.operator.VO.UserChangeVO;
import kr.co.holic.user.VO.UserDetailVO;
import kr.co.holic.util.AES256;
import kr.co.holic.util.PageCriteriaVO;
import lombok.extern.java.Log;

@Log
@Service
public class OperatorServiceImpl implements OperatorService {
	
	/* - HanJungIl - 170610 ~ 170630
	 * 운영자 관련 service
	 */
	
	@Inject
	private OperatorDAO dao;
	
	@Override
	public List<UserDetailVO> operatorUserList(PageCriteriaVO pVO) throws Exception {
		// TODO 회원리트스
		if(pVO.getPageNum() == 0) pVO.setPageNum(1);
		
		pVO.setTypeNInputText();
		
		int totalCount = dao.operatorUserCount(pVO);
		
		String type = pVO.getType();
		
		// 비교를 위한 암호화
		if("userId".equals(type) || "userWechat".equals(type) || "userMail".equals(type)){
			pVO.setInputText(new AES256().aesEncode(type));
		}
		
		pVO.setInputText(pVO.getInputText());
		
		pVO.setPageCriteriaVO(pVO.getPageNum(), totalCount);
		
		// 유저 검색
		List<UserDetailVO> list = dao.operatorUserList(pVO);
		
		// 값 설정
		for(UserDetailVO user : list){
			// 복호화
			user.restoreCode(user);
			AES256 aes = new AES256();
			
			for(int j = 0 ; j < 3 ; ++j){
				StringBuffer strBuffer = null;
				
				if(j == 0){ // 모자이크처리 - id
					strBuffer = new StringBuffer( aes.aesDecode( user.getUserLoginVO().getUserId() ));
				}else if(j == 1){ // 모자이크처리 - 위챗
					strBuffer = new StringBuffer( user.getUserWechat() );
				}else if(j == 2){ // 모자이크처리 - 메일
					strBuffer = new StringBuffer( user.getUserMail().split("@")[0] );
				}
				
				int i = 3;	// 보여줄 부분 길이 설정
				
				if(strBuffer.length() < 4) i = 2;
				
				for(; i < strBuffer.length() ; ++i){
					strBuffer.setCharAt(i, '*');
				}
				
				if(j == 0){ // 모자이크처리 - id
					user.setUserId(strBuffer.toString());
				}else if(j == 1){ // 모자이크처리 - 위챗
					user.setUserWechat(strBuffer.toString());
				}else if(j == 2){ // 모자이크처리 - 메일
					user.setUserMail(strBuffer.toString() + "@" + user.getUserMail().split("@")[1]);
				}
			}
		}
		
		return list;
	}
	
	@Transactional
	@Override
	public boolean operatorUserChange(UserChangeVO vo) throws Exception {
		// TODO 회원 타입 변경
		
		List<UserDetailVO> list = new ArrayList<UserDetailVO>();
		boolean success = false;
		
		String typeString = vo.getButtonType();
		int type = -99;
		
		if("userNormal".equals(typeString)){ 
			type = 1; // 일반회원전환
		}else if("userPaid".equals(typeString)){ 
			type = 2; // 유료회원전환
		}else if("userTeacher".equals(typeString)){ 
			type = 5; // 강사회원전환
		}else if("userBlock".equals(typeString)){ 
			type = 0; // 회원 블럭
		}else if("userDelete".equals(typeString)){ 
			type = -1; // 회원삭제
		}else if("userOperator".equals(typeString)){ 
			type = 8; // 운영자회원전환
		}
		
		for(int i = 0 ; i < vo.getUserKeyList().size() ; ++i){
			list.add(new UserDetailVO(vo.getUserKeyList().get(i), 
					null, null, null, null, null, 
					null, null, 0, null, type, null));
		}
		
		// 관리자 - 회원 삭제
		if("userRealDelete".equals(typeString)){ 
			dao.operatorUserDelete(list);
			success = true;
			return success;
		}
		
		if(type == -99) return success;
		
		// 등록 정보 삭제
		dao.operatorUserDataDelete(vo);
		
		// 관리자 - 회원 유료전환, 블럭
		if("userPaid".equals(typeString)){
			if(vo.getYyyyMMdd() == null) return success;
			vo.setDate();
			dao.operatorUserPayment(vo);
		}else if("userBlock".equals(typeString)){ 
			if(vo.getYyyyMMdd() == null) return success;
			vo.setDate();
			dao.operatorUserBlock(vo);
		}
		
		// 관리자 - 회원 강사 전환
		if("userTeacher".equals(typeString)){
			dao.operatorTeacherInsert(vo);
		}
		
		// 그 외 기타
		dao.operatorUserChange(list);
		
		success = true;
		
		return success;
	}
}