package kr.co.holic.operator.service;

import java.util.List;

import kr.co.holic.operator.VO.UserChangeVO;
import kr.co.holic.user.VO.UserDetailVO;
import kr.co.holic.util.PageCriteriaVO;

public interface OperatorService {
	
	/* - HanJungIl - 170610 ~ 170630
	 * 운영자 관련 service
	 */
	
	// TODO 회원리트스
	public List<UserDetailVO> operatorUserList(PageCriteriaVO vo) throws Exception;
	
	// TODO 회원 타입 변경
	public boolean operatorUserChange(UserChangeVO vo) throws Exception;
}