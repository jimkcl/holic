package kr.co.holic.operator.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.holic.operator.DAO.NoticeBoardDAO;
import kr.co.holic.operator.VO.Criteria;
import kr.co.holic.operator.VO.NoticeBoardVO;
import kr.co.holic.operator.VO.SearchCriteria;

@Service
public class NoticeBoardServiceImpl implements NoticeBoardService {
	
	@Autowired
	NoticeBoardDAO nbdao;
	
	
	@Override
	public void create(NoticeBoardVO vo) throws Exception {
		// TODO Auto-generated method stub
		nbdao.create(vo);
	}


	@Override
	public NoticeBoardVO read(int idx) throws Exception {
		// TODO Auto-generated method stub
		return nbdao.read(idx);
	}


	@Override
	public List<NoticeBoardVO> listAll() throws Exception {
		// TODO Auto-generated method stub
		return nbdao.listAll();
	}


	@Override
	public void delete(int idx) throws Exception {
		// TODO Auto-generated method stub
		nbdao.delete(idx);
	}


	@Override
	public void viewCount(int idx) throws Exception {
		// TODO Auto-generated method stub
		nbdao.viewCount(idx);
	}

	@Override
	public List<NoticeBoardVO> listCriteria(Criteria cri) throws Exception {
		// TODO Auto-generated method stub
		return nbdao.listCriteria(cri);
	}


	@Override
	public int totalCount() throws Exception {
		// TODO Auto-generated method stub
		return nbdao.totalCount();
	}


	@Override
	public List<NoticeBoardVO> listSearch(SearchCriteria cri) throws Exception {
		// TODO Auto-generated method stub
		return nbdao.listSearch(cri);
	}


	@Override
	public int listSearchCount(SearchCriteria cri) throws Exception {
		// TODO Auto-generated method stub
		return nbdao.listSearchCount(cri);
	}

}
