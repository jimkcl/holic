package kr.co.holic.operator.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.holic.operator.DAO.AskBoardDAO;
import kr.co.holic.operator.VO.AskBoardVO;
import kr.co.holic.operator.VO.Criteria;
import kr.co.holic.operator.VO.SearchCriteria;
import kr.co.holic.user.VO.AskReplyVO;
import lombok.extern.java.Log;

@Log
@Service
public class AskBoardServiceImpl  implements AskBoardService {
	
	@Autowired
	AskBoardDAO abdao;
	
	@Override
	public void create(AskBoardVO vo) throws Exception {
		// TODO Auto-generated method stub
		abdao.create(vo);
	}

	@Override
	public AskBoardVO read(int idx) throws Exception {
		// TODO Auto-generated method stub
		
		AskBoardVO vo = abdao.read(idx);
		List<AskReplyVO> list = abdao.list(idx);
		
		vo.setList(list);
		log.info("list=========" + list);
		log.info("Cvo=========" + vo);
		
		return vo;
		
	}

	@Override
	public List<AskBoardVO> listAll() throws Exception {
		// TODO Auto-generated method stub
		return abdao.listAll();
	}

	@Override
	public void delete(int idx) throws Exception {
		// TODO Auto-generated method stub
		abdao.delete(idx);
	}

	@Override
	public void viewCount(int idx) throws Exception {
		// TODO Auto-generated method stub
		abdao.viewCount(idx);
	}

	@Override
	public List<AskBoardVO> listCriteria(Criteria cri) throws Exception {
		// TODO Auto-generated method stub
		return abdao.listCriteria(cri);
	}

	@Override
	public int totalCount() throws Exception {
		// TODO Auto-generated method stub
		return abdao.totalCount();
	}

	@Override
	public List<AskBoardVO> listSearch(SearchCriteria cri) throws Exception {
		// TODO Auto-generated method stub
		return abdao.listSearch(cri);
	}

	@Override
	public int listSearchCount(SearchCriteria cri) throws Exception {
		// TODO Auto-generated method stub
		return abdao.listSearchCount(cri);
	}
	
	
}
