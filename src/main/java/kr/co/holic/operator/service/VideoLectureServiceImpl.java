package kr.co.holic.operator.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.co.holic.operator.DAO.VideoLectureDAO;
import kr.co.holic.operator.VO.VideoLectureReplyVO;
import kr.co.holic.operator.VO.VideoLectureURLVO;
import kr.co.holic.operator.VO.VideoLectureVO;
import kr.co.holic.teacher.VO.TeacherDetailVO;
import kr.co.holic.util.PageCriteriaVO;
import lombok.extern.java.Log;

@Log
@Service
public class VideoLectureServiceImpl implements VideoLectureService{
	
	/* - HanJungIl - 170610 ~ 170630
	 * 강의 동영상 관련 service
	 */
	
	@Inject
	VideoLectureDAO dao;
	
//	@Override public void test() throws Exception{ dao.test(); }
	
	
	/* Home */
	@Override
	public List<VideoLectureVO> homeVideoLectureList(int param1) throws Exception{
		// TODO 메인 페이지용 최신 동영상 강의 6개
		return dao.homeVideoLectureList(param1);
	}
	
	@Override
	public VideoLectureVO homeVideoLectureRecommendation(int param1) throws Exception {
		// TODO 메인 페이지용 평가수 가장 많은 강의
		return dao.homeVideoLectureRecommendation(param1);
	}
	
	
	@Override
	public List<VideoLectureVO> videoLectureList(PageCriteriaVO pVO) throws Exception {
		// TODO 강의 동영상 리스트
		
		if(pVO.getPageNum() == 0) pVO.setPageNum(1);
		
		pVO.setTypeNInputText();
		
		int totalCount = dao.videoLectureTotalCount(pVO);
		
		pVO.setPageCriteriaVO(pVO.getPageNum(), totalCount);
		
		return dao.videoLectureList(pVO);
	}
	
	@Override
	public Map<String, ArrayList> videoLectureUI() throws Exception {
		// TODO 강의 동영상 등록 UI 페이지용 - 패키지, 강사 정보 전송
		
		ArrayList<String> packageList = (ArrayList<String>) dao.videoLectureGetPackage();
		ArrayList<TeacherDetailVO> tList = (ArrayList<TeacherDetailVO>) dao.tUserList();
		
		Map<String, ArrayList> map = new HashMap<String, ArrayList>();
		
		map.put("packageList", packageList);
		map.put("tList", tList);
		
		return map;
	}
	
	@Transactional
	@Override
	public void videoLectureCreate(VideoLectureVO vo) throws Exception {
		// TODO 강의 동영상 등록 서비스
		
		ArrayList<VideoLectureURLVO> listURLVO = vo.getListURL();
		
		String vPackage = vo.getVPackage();
		
		if(vPackage != null && !"".equals(vPackage)){
			dao.videoLecturePackageSearchNCreate(vPackage);
		}
		
		// 값 검사 및 설정
		vo.setTUserKey();
		vo.setVPackage();
		vo.setContentN();
		
//		log.info(" :: vo :: " + vo);
		
		VideoLectureVO vIdxKey = dao.videoLectureCreate(vo);
		
		if(vIdxKey == null) return;
		
		for(VideoLectureURLVO urlVO : vIdxKey.getListURL()){
			urlVO.setVIdx(vIdxKey.getVIdx());
		}
		
		dao.videoLectureURLCreate(listURLVO);
	}
	
	@Transactional
	@Override
	public VideoLectureVO videoLectureDetail(VideoLectureVO vo) throws Exception {
		// TODO 강의 동영상 세부페이지
		// 조회수
		dao.videoLectureCount(vo.getVIdx());
		
		// 강의동영상 세부페이지
		vo = dao.videoLectureDetail(vo);
		
		if(vo == null) return vo;
		
		// 강의동영상 강사정보
		if(vo.getTUserKey() != null) {
			vo.setTeacherVO( dao.videoLectureTeacherDetail( vo.getTUserKey() ));
		}
		
		List<VideoLectureVO> videoLectureVO = dao.videoLectureRecommend(vo);
		
		if(videoLectureVO != null || videoLectureVO.size() != 0){
			vo.setListVideo(videoLectureVO);
		}
		
		// 강의동영상 세부페이지 URL 리스트
		ArrayList<VideoLectureURLVO> urlList = (ArrayList<VideoLectureURLVO>) dao.videoLectureURLList(vo.getVIdx());
		
		if(urlList.size() > 0) vo.setListURL(urlList);
		if(vo != null) vo.setContentBR();
		
		// 강의동영상 평가 리스트
		List<VideoLectureReplyVO> replyList = dao.videoLectureRecommendList(vo.getVIdx());
		
		vo.setListReply(replyList);
		
		return vo;
	}

	@Override
	public void videoLectureDelete(int vIdx) throws Exception {
		// TODO 강의 동영상 삭제
		dao.videoLectureDelete(vIdx);
	}

	@Override
	public List<String> videoLectureGetPackage() throws Exception {
		// TODO 강의 동영상 수정시 패키지(분류) 리스트
		return dao.videoLectureGetPackage();
	}

	@Override
	public void videoLectureUpdate(VideoLectureVO vo) throws Exception {
		// TODO 강의 동영상 수정
		dao.videoLectureUpdate(vo);
	}

	@Override
	public void videoLectureAddURL(VideoLectureVO vo) throws Exception {
		// TODO 강의 동영상 URL 추가
		dao.videoLectureAddURL(vo);
	}

	@Override
	public void videoLectureURLDelete(int idx) throws Exception {
		// TODO 강의 동영상 URL 삭제
		dao.videoLectureURLDelete(idx);
	}

	@Override
	public void videoLectureURLUpdate(VideoLectureURLVO vo) throws Exception {
		// TODO 강의 동영상 URL 수정
		dao.videoLectureURLUpdate(vo);
	}
	
	@Transactional
	@Override
	public boolean videoLectureAddReply(VideoLectureReplyVO replyVO) throws Exception {
		// TODO 강의 동영상 평가리플 등록
		boolean check = false;
		
		int replyCheck = dao.videoLectureAddReplyCheck(replyVO);
		
		if(replyCheck != 0) return check;
		
		// 강의 동영상 평점 및 개수
		VideoLectureVO vo = dao.videoLectureReplyScoreCount(replyVO.getVIdx());
		
		// (기존평균 * 리플수) + 리플점수 / (리플개수 + 1)
		vo.setVScore( (vo.getVScore() * vo.getVScoreCount() + replyVO.getVrScore() * 10) / (vo.getVScoreCount() + 1) );
		vo.setVScoreCount(vo.getVScoreCount() + 1);
		vo.setVIdx(replyVO.getVIdx());
		
		// 업데이트
		dao.videoLectureScoreCountUpdate(vo);
		
		// 리플등록
		dao.videoLectureAddReply(replyVO);
		
		check = true;
		
		return check;
	}

	@Override
	public int videoLectureDeleteReply(int vIdx, String userKey) throws Exception {
		// TODO 강의 동영상 평가리플 삭제
		
		// 값 설정
		VideoLectureReplyVO vo = new VideoLectureReplyVO(vIdx, userKey, null, 0, null, null);
		
		// 내 리플 정보
		VideoLectureReplyVO myVO = dao.videoLectureReplyData(vo);
		
		// 강의 동영상 평점 및 개수
		VideoLectureVO vVO = dao.videoLectureReplyScoreCount(vIdx);
		
		vVO.setVIdx(vIdx);
		
		// 강의동영상 (평균 점수(X10 되어있음) * 평가 수 - 내점수 * 10) / (평가 수 - 1)
		vVO.setVScore( ( vVO.getVScore() * vVO.getVScoreCount() - myVO.getVrScore() * 10 ) / (vVO.getVScoreCount() - 1) );
		vVO.setVScoreCount(vVO.getVScoreCount() - 1);
		
		// 업데이트
		dao.videoLectureScoreCountUpdate(vVO);
		
		return dao.videoLectureDeleteReply(vo);
	}
}