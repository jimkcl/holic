package kr.co.holic.operator.service;

import java.util.List;

import kr.co.holic.operator.VO.SearchCriteria;
import kr.co.holic.operator.VO.Criteria;
import kr.co.holic.operator.VO.NoticeBoardVO;

public interface NoticeBoardService {
	
	public abstract void create(NoticeBoardVO vo) throws Exception; //공지사항 글등록
	public abstract NoticeBoardVO read(int idx) throws Exception; //글 읽기
	public abstract List<NoticeBoardVO> listAll() throws Exception; //글 전체보기
	public abstract void delete(int idx) throws Exception; //글 삭제
	public abstract void viewCount(int idx) throws Exception; //조회수
	
	public abstract List<NoticeBoardVO> listCriteria(Criteria cri) throws Exception; //페이징처리된 목록
	public abstract int totalCount() throws Exception; //문의게시판 글 개수
	
	public abstract List<NoticeBoardVO> listSearch(SearchCriteria cri) throws Exception; //검색
	public abstract int listSearchCount(SearchCriteria cri) throws Exception; //검색수
	
}
