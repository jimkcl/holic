package kr.co.holic.operator.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import kr.co.holic.operator.VO.VideoLectureReplyVO;
import kr.co.holic.operator.VO.VideoLectureURLVO;
import kr.co.holic.operator.VO.VideoLectureVO;
import kr.co.holic.util.PageCriteriaVO;

public interface VideoLectureService {
	
	/* - HanJungIl - 170610 ~ 170630
	 * 강의 동영상 관련 service
	 */
	
//	public void test() throws Exception;
	
	/* Home */
	public List<VideoLectureVO> homeVideoLectureList(int param1) throws Exception;
	public VideoLectureVO homeVideoLectureRecommendation(int param1) throws Exception;
	
	public List<VideoLectureVO> videoLectureList(PageCriteriaVO pVO) throws Exception;
	public Map<String, ArrayList> videoLectureUI() throws Exception;
	public void videoLectureCreate(VideoLectureVO vo) throws Exception;
	public VideoLectureVO videoLectureDetail(VideoLectureVO vo) throws Exception;
	public void videoLectureDelete(int vIdx) throws Exception;
	public List<String> videoLectureGetPackage() throws Exception;
	public void videoLectureUpdate(VideoLectureVO vo) throws Exception;
	public void videoLectureAddURL(VideoLectureVO vo) throws Exception;
	public void videoLectureURLDelete(int idx) throws Exception;
	public void videoLectureURLUpdate(VideoLectureURLVO vo) throws Exception;
	public boolean videoLectureAddReply(VideoLectureReplyVO replyVO) throws Exception;
	public int videoLectureDeleteReply(int vIdx, String userKey) throws Exception;
}