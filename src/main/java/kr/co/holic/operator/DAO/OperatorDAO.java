package kr.co.holic.operator.DAO;

import java.util.List;

import kr.co.holic.operator.VO.UserChangeVO;
import kr.co.holic.user.VO.UserDetailVO;
import kr.co.holic.util.PageCriteriaVO;

public interface OperatorDAO {
	
	/* - HanJungIl - 170610 ~ 170630
	 * 운영자 관련 DAO
	 */
	
	// TODO 회원 수
	int operatorUserCount(PageCriteriaVO pVO) throws Exception;
	
	// TODO 회원리트스
	List<UserDetailVO> operatorUserList(PageCriteriaVO vo) throws Exception;
	
	// TODO 회원 강사등록
	void operatorTeacherInsert(UserChangeVO vo) throws Exception;
	
	// TODO 회원 타입 변경
	void operatorUserChange(List<UserDetailVO> vo) throws Exception;
	
	// TODO 회원 완전 삭제
	void operatorUserDelete(List<UserDetailVO> vo) throws Exception;
	
	// TODO 회원 결제/블럭 정보 삭제
	void operatorUserDataDelete(UserChangeVO vo) throws Exception;
	
	// TODO 회원 유료 기간 설정
	void operatorUserPayment(UserChangeVO vo) throws Exception;
	
	// TODO 회원 블럭 기간 설정
	void operatorUserBlock(UserChangeVO vo) throws Exception;
}