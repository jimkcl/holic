package kr.co.holic.operator.DAO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import kr.co.holic.operator.VO.Criteria;
import kr.co.holic.operator.VO.NoticeBoardVO;
import kr.co.holic.operator.VO.SearchCriteria;

@Repository
public class NoticeBoardDAOImpl implements NoticeBoardDAO {
	
	@Inject
	private SqlSession session;
	private final String NAMESPACE="kr.co.holic.operatorNBMapper";
	
	@Override
	public void create(NoticeBoardVO vo) throws Exception {
		// TODO Auto-generated method stub
		session.insert(NAMESPACE+".create", vo);
	}

	@Override
	public NoticeBoardVO read(int idx) throws Exception {
		// TODO Auto-generated method stub
		return session.selectOne(NAMESPACE+".read", idx);
	}

	@Override
	public List<NoticeBoardVO> listAll() throws Exception {
		// TODO Auto-generated method stub
		return session.selectList(NAMESPACE+".listAll");
	}

	@Override
	public void delete(int idx) throws Exception {
		// TODO Auto-generated method stub
		session.delete(NAMESPACE+".delete", idx);
	}

	@Override
	public void viewCount(int idx) throws Exception {
		// TODO Auto-generated method stub
		session.update(NAMESPACE+".viewCount", idx);
	}

	@Override
	public List<NoticeBoardVO> listPage(int page) throws Exception {
		// TODO Auto-generated method stub
		int displayPerPageCount=10; //한 페이지에 출력할 개수
		int startrow=(page-1)*displayPerPageCount+1; //(1-1)*10+1 = 1 -> 1페이지의 처음
		int endrow=startrow+(displayPerPageCount-1); //1+(10-1) = 10 -> 1페이지의 끝
		
		Map<String, Integer> map = new HashMap<String, Integer>(); //두 개 이상의 값을 넣을 때 map을 사용
		map.put("startrow", startrow);
		map.put("endrow", endrow);
		return session.selectList(NAMESPACE+".listPage", map);
	}

	@Override
	public List<NoticeBoardVO> listCriteria(Criteria cri) throws Exception {
		// TODO Auto-generated method stub
		return session.selectList(NAMESPACE+".listCriteria", cri);
	}

	@Override
	public int totalCount() throws Exception {
		// TODO Auto-generated method stub
		Integer i = session.selectOne(NAMESPACE+".totalCount");
		if(i==null){
			return 0;
		} 
		return i;
	}

	@Override
	public int listSearchCount(SearchCriteria cri) throws Exception {
		// TODO Auto-generated method stub
		Integer cnt=session.selectOne(NAMESPACE+".listSearchCount", cri);
		if(cnt==null) {
			return 0;
		}
		
		return cnt;
	}

	@Override
	public List<NoticeBoardVO> listSearch(SearchCriteria cri) throws Exception {
		// TODO Auto-generated method stub
		return session.selectList(NAMESPACE+".listSearch", cri);
	}
	
}
