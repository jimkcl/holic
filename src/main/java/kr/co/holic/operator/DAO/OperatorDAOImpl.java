package kr.co.holic.operator.DAO;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import kr.co.holic.operator.VO.UserChangeVO;
import kr.co.holic.user.VO.UserDetailVO;
import kr.co.holic.util.PageCriteriaVO;
import lombok.extern.java.Log;

@Log
@Repository
public class OperatorDAOImpl implements OperatorDAO {

	/* - HanJungIl - 170610 ~ 170630
	 * 운영자 관련 DAO
	 */
	
	@Inject
	private SqlSession session;
	private final String NAMESPACE = "kr.co.mapper.operaterMapper";
	
	@Override
	public int operatorUserCount(PageCriteriaVO pVO) throws Exception{
		// TODO 회원 수
		return session.selectOne(NAMESPACE + ".operatorUserCount", pVO);
	}
	
	@Override
	public List<UserDetailVO> operatorUserList(PageCriteriaVO pVO) throws Exception {
		// TODO 회원리스트
		return session.selectList(NAMESPACE + ".operatorUserList", pVO);
	}
	
	@Override
	public void operatorTeacherInsert(UserChangeVO vo) throws Exception {
		// TODO 회원 강사 등록
		session.insert(NAMESPACE + ".operatorTeacherInsert", vo);
	}

	@Override
	public void operatorUserChange(List<UserDetailVO> vo) throws Exception {
		// TODO 회원 타입 변경
		session.update(NAMESPACE + ".operatorUserChange", vo);
	}

	@Override
	public void operatorUserDelete(List<UserDetailVO> vo) throws Exception {
		// TODO 회원 완전 삭제
		session.delete(NAMESPACE + ".operatorUserDelete", vo);
	}
	
	@Override
	public void operatorUserDataDelete(UserChangeVO vo) throws Exception {
		// TODO 회원 유료/블럭 정보 삭제
		session.delete(NAMESPACE + ".operatorUserDataDelete", vo);
	}

	@Override
	public void operatorUserPayment(UserChangeVO vo) throws Exception {
		// TODO 회원 유료 기간 설정
		session.insert(NAMESPACE + ".operatorUserPayment", vo);
	}
	
	@Override
	public void operatorUserBlock(UserChangeVO vo) throws Exception {
		// TODO 회원 블럭 기간 설정
		session.insert(NAMESPACE + ".operatorUserBlock", vo);
	}
}