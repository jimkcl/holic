package kr.co.holic.operator.DAO;

import java.util.ArrayList;
import java.util.List;

import kr.co.holic.operator.VO.VideoLectureReplyVO;
import kr.co.holic.operator.VO.VideoLectureURLVO;
import kr.co.holic.operator.VO.VideoLectureVO;
import kr.co.holic.teacher.VO.TeacherDetailVO;
import kr.co.holic.util.PageCriteriaVO;

public interface VideoLectureDAO {
	
	/* - HanJungIl - 170610 ~ 170630
	 * 강의 동영상 관련 DAO
	 */
	
//	void test() throws Exception;
	
	
	// TODO 메인 페이지용 최신 동영상 강의 n개
	public List<VideoLectureVO> homeVideoLectureList(int param1) throws Exception;
	
	// TODO 메인 페이지용 평가수 가장 많은 강의
	public VideoLectureVO homeVideoLectureRecommendation(int param1) throws Exception;
	
	
	// TODO 강의 동영상 전체 수
	int videoLectureTotalCount(PageCriteriaVO vo) throws Exception;
	
	// TODO 강의 동영상 페이지
	List<VideoLectureVO> videoLectureList(PageCriteriaVO pcVO) throws Exception;
	List<TeacherDetailVO> tUserList() throws Exception;
	
	// TODO 강의 동영상 링크 등록 UI
	List<String> videoLectureGetPackage() throws Exception;
	
	// TODO 강의 동영상 등록 서비스
	void videoLecturePackageSearchNCreate(String vPackage) throws Exception;
	VideoLectureVO videoLectureCreate(VideoLectureVO vo) throws Exception;
	void videoLectureURLCreate(ArrayList<VideoLectureURLVO> urlVO) throws Exception;
	
	// TODO 강의 동영상 세부페이지
	void videoLectureCount(int vIdx) throws Exception;
	VideoLectureVO videoLectureDetail(VideoLectureVO vo) throws Exception;
	List<VideoLectureURLVO> videoLectureURLList(int vIdx) throws Exception;
	TeacherDetailVO videoLectureTeacherDetail(String tUserKey) throws Exception;
	List<VideoLectureVO> videoLectureRecommend(VideoLectureVO VideoLectureVO) throws Exception;
	List<VideoLectureReplyVO> videoLectureRecommendList(int vIdx) throws Exception;
	
	// TODO 강의 동영상 삭제
	void videoLectureDelete(int vIdx) throws Exception;
	
	// TODO 강의 동영상 업데이트
	void videoLectureUpdate(VideoLectureVO vo) throws Exception;
	
	// TODO 강의 동영상 URL 추가
	void videoLectureAddURL(VideoLectureVO vo) throws Exception;
	
	// TODO 강의 동영상 URL 삭제
	void videoLectureURLDelete(int idx) throws Exception;
	
	// TODO 강의 동영상 URL 업데이트
	void videoLectureURLUpdate(VideoLectureURLVO vo) throws Exception;
	
	// TODO 강의 동영상 평가리플 등록여부 확인
	int videoLectureAddReplyCheck(VideoLectureReplyVO replyVO) throws Exception;
	
	// TODO 강의 동영상 평가리플 등록
	void videoLectureAddReply(VideoLectureReplyVO replyVO) throws Exception;
	
	// TODO 강의 동영상 평가리플 평점 및 개수
	VideoLectureVO videoLectureReplyScoreCount(int vIdx) throws Exception;
	
	// TODO 강의 동영상 평가리플 평점 및 개수 업데이트
	void videoLectureScoreCountUpdate(VideoLectureVO videoLectureVO) throws Exception;
	
	// TODO 삭제를 위한 정보 얻기용
	VideoLectureReplyVO videoLectureReplyData(VideoLectureReplyVO replyVO) throws Exception;
	
	// TODO 강의 동영상 평가리플 삭제
	int videoLectureDeleteReply(VideoLectureReplyVO replyVO) throws Exception;
}