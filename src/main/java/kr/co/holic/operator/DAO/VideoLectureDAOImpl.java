package kr.co.holic.operator.DAO;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import kr.co.holic.operator.VO.VideoLectureReplyVO;
import kr.co.holic.operator.VO.VideoLectureURLVO;
import kr.co.holic.operator.VO.VideoLectureVO;
import kr.co.holic.teacher.VO.TeacherDetailVO;
import kr.co.holic.util.PageCriteriaVO;
import lombok.extern.java.Log;

@Log
@Repository
public class VideoLectureDAOImpl implements VideoLectureDAO {
	
	/* - HanJungIl - 170610 ~ 170630
	 * 강의 동영상 관련 DAOImpl
	 */
	
	@Inject
	private SqlSession session;
	private final String NAMESPACE = "kr.co.mapper.VideoLectureMapper";
	
//	@Override public void test() throws Exception {
//		for(int i=0 ; i < 300 ; ++i) session.insert(NAMESPACE + ".test");
//	}
	
	@Override
	public List<VideoLectureVO> homeVideoLectureList(int param1) throws Exception {
		// TODO 메인 페이지용 최신 동영상 강의 6개
		return session.selectList(NAMESPACE + ".homeVideoLectureList", param1);
	}
	
	@Override
	public VideoLectureVO homeVideoLectureRecommendation(int param1) throws Exception {
		// TODO 메인 페이지용 평가수 가장 많은 강의
		return session.selectOne(NAMESPACE + ".homeVideoLectureRecommendation", param1);
	}
	
	
	@Override
	public int videoLectureTotalCount(PageCriteriaVO vo) throws Exception {
		// TODO 강의 동영상 전체 수
		return session.selectOne(NAMESPACE + ".videoLectureTotalCount", vo);
	}
	
	@Override
	public List<VideoLectureVO> videoLectureList(PageCriteriaVO pcVO) throws Exception {
		// TODO 강의 동영상 페이지 리스트
		return session.selectList(NAMESPACE + ".videoLectureList", pcVO);
	}
	
	@Override
	public List<String> videoLectureGetPackage() throws Exception {
		// TODO 강의 동영상 패키지(분류) 리스트
		return session.selectList(NAMESPACE + ".videoLectureGetPackage");
	}
	
	@Override
	public List<TeacherDetailVO> tUserList() throws Exception {
		// TODO 강사 리스트 - key, name
		return session.selectList(NAMESPACE + ".tUserList");
	}
	
	@Override
	public void videoLecturePackageSearchNCreate(String vPackage) throws Exception {
		// TODO 패키지 등록 - 있을경우 등록X
		session.insert(NAMESPACE + ".videoLecturePackageSearchNCreate", vPackage);
	}
	
	@Override
	public VideoLectureVO videoLectureCreate(@Valid VideoLectureVO vo) throws Exception {
		// TODO 강의동영상 게시글 등록
		session.insert(NAMESPACE + ".videoLectureCreate", vo);
		return vo;
	}

	@Override
	public void videoLectureURLCreate(ArrayList<VideoLectureURLVO> urlVO) throws Exception {
		// TODO 강의동영상 URL 등록
		session.insert(NAMESPACE + ".videoLectureURLCreate", urlVO);
	}

	@Override
	public void videoLectureCount(int vIdx) throws Exception {
		// TODO 조회수 증가
		session.update(NAMESPACE + ".videoLectureCount", vIdx);
	}
	
	@Override
	public VideoLectureVO videoLectureDetail(VideoLectureVO vo) throws Exception {
		// TODO 강의동영상 세부페이지
		return session.selectOne(NAMESPACE + ".videoLectureDetail", vo.getVIdx());
	}

	@Override
	public List<VideoLectureURLVO> videoLectureURLList(int vIdx) throws Exception {
		// TODO 강의동영상 세부페이지 URL 리스트
		return session.selectList(NAMESPACE + ".videoLectureURLList", vIdx);
	}
	
	@Override
	public TeacherDetailVO videoLectureTeacherDetail(String tUserKey) throws Exception {
		// TODO 강의동영상 세부페이지 강사정보
		return session.selectOne(NAMESPACE + ".videoLectureTeacherDetail", tUserKey);
	}
	
	@Override
	public List<VideoLectureVO> videoLectureRecommend(VideoLectureVO VideoLectureVO) throws Exception {
		// TODO 강의동영상 추천 영상
		return session.selectList(NAMESPACE + ".videoLectureRecommend", VideoLectureVO) ;
	}
	
	@Override
	public void videoLectureDelete(int vIdx) throws Exception {
		// TODO 강의동영상 삭제
		session.delete(NAMESPACE + ".videoLectureDelete", vIdx);
	}

	@Override
	public void videoLectureUpdate(VideoLectureVO vo) throws Exception {
		// TODO 강의동영상 수정
		session.update(NAMESPACE + ".videoLectureUpdate", vo);
	}

	@Override
	public void videoLectureAddURL(VideoLectureVO vo) throws Exception {
		// TODO 강의동영상 URL 추가
		session.insert(NAMESPACE + ".videoLectureAddURL", vo);
	}
	
	@Override
	public List<VideoLectureReplyVO> videoLectureRecommendList(int vIdx) throws Exception {
		// TODO 강의동영상 평가 리트스
		return session.selectList(NAMESPACE + ".videoLectureRecommendList", vIdx);
	}

	@Override
	public void videoLectureURLDelete(int idx) throws Exception {
		// TODO 강의동영상 URL 삭제
		session.delete(NAMESPACE + ".videoLectureURLDelete", idx);
	}

	@Override
	public void videoLectureURLUpdate(VideoLectureURLVO vo) throws Exception {
		// TODO 강의동영상 URL 수정
		session.update(NAMESPACE + ".videoLectureURLUpdate", vo);
	}
	
	@Override
	public int videoLectureAddReplyCheck(VideoLectureReplyVO replyVO) throws Exception {
		// TODO 강의동영상 평가리플 등록여부 확인
		return session.selectOne(NAMESPACE + ".videoLectureAddReplyCheck" , replyVO);
	}
	
	@Override
	public void videoLectureAddReply(VideoLectureReplyVO replyVO) throws Exception {
		// TODO 강의동영상 평가리플 등록
		session.insert(NAMESPACE + ".videoLectureAddReply", replyVO);
	}

	@Override
	public VideoLectureVO videoLectureReplyScoreCount(int vIdx) throws Exception {
		// TODO 강의 동영상 평가리플 평점 및 개수
		return session.selectOne(NAMESPACE + ".videoLectureReplyScoreCount", vIdx);
	}

	@Override
	public void videoLectureScoreCountUpdate(VideoLectureVO videoLectureVO) throws Exception {
		// TODO 강의 동영상 평가리플 평점 및 개수 업데이트
		session.update(NAMESPACE + ".videoLectureScoreCountUpdate", videoLectureVO);
	}
	
	@Override
	public VideoLectureReplyVO videoLectureReplyData(VideoLectureReplyVO replyVO) throws Exception {
		// TODO 삭제를 위한 정보 얻기용
		return session.selectOne(NAMESPACE + ".videoLectureReplyData", replyVO);
	}

	@Override
	public int videoLectureDeleteReply(VideoLectureReplyVO replyVO) throws Exception {
		// TODO 강의 동영상 평가리플 삭제
		return session.delete(NAMESPACE + ".videoLectureDeleteReply", replyVO);
	}
}