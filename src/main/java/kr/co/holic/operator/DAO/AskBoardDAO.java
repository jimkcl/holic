package kr.co.holic.operator.DAO;

import java.util.List;

import kr.co.holic.operator.VO.AskBoardVO;
import kr.co.holic.operator.VO.Criteria;
import kr.co.holic.operator.VO.SearchCriteria;
import kr.co.holic.user.VO.AskReplyVO;

public interface AskBoardDAO {
	
	public abstract void create(AskBoardVO vo) throws Exception; //글 등록
	public abstract AskBoardVO read(int idx) throws Exception; //글 읽기
	public abstract List<AskBoardVO> listAll() throws Exception; //글 전체보기
	public abstract void delete(int idx) throws Exception; //글 삭제
	public abstract void viewCount(int idx) throws Exception; //조회수
	
	public abstract List<AskBoardVO> listPage(int page) throws Exception; //페이징처리(이전, 다음)
	public abstract List<AskBoardVO> listCriteria(Criteria cri) throws Exception;//페이징처리된 목록
	public abstract int totalCount() throws Exception; //게시물의 총 개수
	
	public int listSearchCount(SearchCriteria cri) throws Exception; //search의 개수
	public List<AskBoardVO> listSearch(SearchCriteria cri) throws Exception; //검색
	
	public List<AskReplyVO> list(int bidx) throws Exception; //댓글 보기
	
}

