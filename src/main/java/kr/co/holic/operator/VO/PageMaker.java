package kr.co.holic.operator.VO;

import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import lombok.extern.java.Log;

@Log
public class PageMaker {
	
	/**
	 * 페이지 처리(이전, 다음) VO
	 * 
	 * */
	
	private int totalCount;
	private int startPageNum;
	private int endPageNum;
	private Criteria cri;
	private int totalPage;
	
	public PageMaker() {
		// TODO Auto-generated constructor stub
	}

	public PageMaker(int totalCount, int startPageNum, int endPageNum, Criteria cri) {
		super();
		this.totalCount = totalCount;
		this.startPageNum = startPageNum;
		this.endPageNum = endPageNum;
		this.cri = cri;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
		
		totalPage=totalCount/cri.getPerPageNum(); //총페이지수=총게시글수/한페이지에출력되는수
		if(totalCount%cri.getPerPageNum()!=0){
			++totalPage;
		}
		
		int page=cri.getPage();
		startPageNum=((page-1)/10)*10+1;
		endPageNum=startPageNum+9;
		if(totalPage<endPageNum){ //예를 들어, 전체 페이지 수가 5페이지인데 endPage가 10인 경우
			endPageNum=totalPage; //endPage를 totalPage로 바꿈
		}
		
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public int getStartPageNum() {
		return startPageNum;
	}

	public void setStartPageNum(int startPageNum) {
		this.startPageNum = startPageNum;
	}

	public int getEndPageNum() {
		return endPageNum;
	}

	public void setEndPageNum(int endPageNum) {
		this.endPageNum = endPageNum;
	}

	public Criteria getCri() {
		return cri;
	}

	public void setCri(Criteria cri) {
		this.cri = cri;
	}
	
	public String makeSearch(int page) {
		log.info(" :: :: " + cri);
		UriComponents uc = UriComponentsBuilder.newInstance()
				.queryParam("page", page)
				.queryParam("perPageNum", cri.getPerPageNum())
				.queryParam("searchType", ((SearchCriteria)cri).getSearchType()) //cri를 SearchCriteria로 형변환 시킴
				.queryParam("keyword", ((SearchCriteria)cri).getKeyword())
				.build();
		
		return uc.toUriString();
	}
	
	public String makeQuery(int page) { // 현재페이지와 현페이지개수를 uri객체를 만들어 리턴으로 반환하여 쉽게 사용할 수 있게 됨
		UriComponents uc=UriComponentsBuilder.newInstance().queryParam("page", page).queryParam("perPageNum", cri.getPerPageNum()).build();
		return uc.toUriString();
	}

	@Override
	public String toString() {
		return "PageMaker [totalCount=" + totalCount + ", startPageNum=" + startPageNum + ", endPageNum=" + endPageNum
				+ ", cri=" + cri + ", totalPage=" + totalPage + "]";
	}
	
}
