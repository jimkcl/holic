package kr.co.holic.operator.VO;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VideoLectureReplyVO {
	@NotNull @Range(min=1) 
	private int vIdx;
	
	private String userKey;
	
	@NotNull @Length(min=1, max=500) 
	private String vrComment;
	
	@NotNull @Range(min=1, max=5) 
	private int vrScore;
	
	private String vrDate;
	
	private String userNick;
}