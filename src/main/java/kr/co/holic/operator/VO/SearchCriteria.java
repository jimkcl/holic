package kr.co.holic.operator.VO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SearchCriteria extends Criteria {
	
	/**
	 * 검색 VO
	 * 
	 * */
	
	private String searchType; //검색종류
	private String keyword; //검색키워드
}
