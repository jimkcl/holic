package kr.co.holic.operator.VO;

public class NoticeBoardVO {
	
	/**
	 * 공지사항 VO
	 * 
	 * */
	
	private int idx;
	private String userKey;
	private String userNick;
	private String aTitle;
	private String aContent;
	private String aDate;
	private int aCount;
	private int aType;
	
	public NoticeBoardVO() {
		// TODO Auto-generated constructor stub
	}

	public NoticeBoardVO(int idx, String userKey, String userNick, String aTitle, String aContent, String aDate,
			int aCount, int aType) {
		super();
		this.idx = idx;
		this.userKey = userKey;
		this.userNick = userNick;
		this.aTitle = aTitle;
		this.aContent = aContent;
		this.aDate = aDate;
		this.aCount = aCount;
		this.aType = aType;
	}

	public int getIdx() {
		return idx;
	}

	public void setIdx(int idx) {
		this.idx = idx;
	}

	public String getUserKey() {
		return userKey;
	}

	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}

	public String getUserNick() {
		return userNick;
	}

	public void setUserNick(String userNick) {
		this.userNick = userNick;
	}

	public String getaTitle() {
		return aTitle;
	}

	public void setaTitle(String aTitle) {
		this.aTitle = aTitle;
	}

	public String getaContent() {
		return aContent;
	}

	public void setaContent(String aContent) {
		this.aContent = aContent;
	}

	public String getaDate() {
		return aDate;
	}

	public void setaDate(String aDate) {
		this.aDate = aDate;
	}

	public int getaCount() {
		return aCount;
	}

	public void setaCount(int aCount) {
		this.aCount = aCount;
	}

	public int getaType() {
		return aType;
	}

	public void setaType(int aType) {
		this.aType = aType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((userKey == null) ? 0 : userKey.hashCode());
		result = prime * result + ((userNick == null) ? 0 : userNick.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NoticeBoardVO other = (NoticeBoardVO) obj;
		if (userKey == null) {
			if (other.userKey != null)
				return false;
		} else if (!userKey.equals(other.userKey))
			return false;
		if (userNick == null) {
			if (other.userNick != null)
				return false;
		} else if (!userNick.equals(other.userNick))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "NoticeBoardVO [idx=" + idx + ", userKey=" + userKey + ", userNick=" + userNick + ", aTitle=" + aTitle
				+ ", aContent=" + aContent + ", aDate=" + aDate + ", aCount=" + aCount + ", aType=" + aType + "]";
	}
}
