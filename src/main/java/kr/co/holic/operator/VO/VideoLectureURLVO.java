package kr.co.holic.operator.VO;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VideoLectureURLVO {
	
	/* - HanJungIl - 170610 ~ 170630
	 * 강의 동영상 관련 VO
	 */
	
	private int idx;
	private int vIdx;
	@NotNull @Size(min=1, max=250) private String vURL;
	@NotNull @Size(min=2, max=50) private String vName;
	
	public VideoLectureURLVO(VideoLectureURLVO vo){
		this.idx = vo.getIdx();
		this.vIdx = vo.getVIdx();
		this.vURL = vo.getVURL();
		this.vName = vo.getVName();
	}
}