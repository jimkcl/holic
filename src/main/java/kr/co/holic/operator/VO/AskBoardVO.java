package kr.co.holic.operator.VO;

import java.util.List;

import kr.co.holic.user.VO.AskReplyVO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AskBoardVO {
	
	/**
	 * 문의게시판 VO
	 * 
	 * */
	private int idx;
	private String userKey;
	private String userNick;
	private String aTitle;
	private String aContent;
	private String aDate;
	private int aCount;
	private int aVisibility;
	private List<AskReplyVO> list;
	
}
