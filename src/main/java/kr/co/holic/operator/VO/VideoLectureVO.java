package kr.co.holic.operator.VO;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import kr.co.holic.teacher.VO.TeacherDetailVO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VideoLectureVO {
	
	/* - HanJungIl - 170610 ~ 170630
	 * 강의 동영상 관련 VO
	 */
	
	private int vIdx;
	private String userKey;
	private String tUserKey;
	private String vPackage;
	@NotNull @Length(min=2) private String vTitle;
	@NotNull @Length(min=1) private String vContent;
	private int vScore;
	private int vScoreCount;
	private String vDate;
	@NotNull private int vType;
	private int vCount;
	
	private String userNick;
	private String tUserName;
	
	private TeacherDetailVO teacherVO;
	private List<VideoLectureVO> listVideo;
	private ArrayList<VideoLectureURLVO> listURL;
	private List<VideoLectureReplyVO> listReply;
	
	// home 페이지용
	public void setVURL(String vURL){
		this.vContent = vURL;
	}
	
	// listURL setting
	public void setListVURL(VideoLectureURLVO vo){
		String urlList = vo.getVURL();
		String vNameList = vo.getVName();
		
		if(urlList == null || "".equals(urlList) 
				|| vNameList == null || "".equals(vNameList)){
			return;
		}
		
		listURL = new ArrayList<VideoLectureURLVO>();
		
		String[] urlListSplit = urlList.split(",");
		String[] vNameListSplit = vNameList.split(",");
		
		for(int i = 0 ; i < urlListSplit.length ; ++i){
			vo.setVURL(urlListSplit[i]);
			vo.setVName(vNameListSplit[i]);
			
			listURL.add(new VideoLectureURLVO(vo));
		}
	}
	
	public void setContentBR(){
		if(vContent == null || "".equals(vContent)) return;
		
		this.vContent = vContent.replaceAll("\n", "<br/>");
	}
	
	public void setContentN(){
		if(vContent == null || "".equals(vContent)) return;
		
		this.vContent = vContent.replaceAll("<br/>", "\n");
	}
	
	public void setVPackage(){
		if("없음".equals(vPackage) || "새로 등록하기".equals(vPackage))
			this.vPackage = null;
	}
	public void setTUserKey(){
		if("기타".equals(tUserKey))
			this.tUserKey = null;
	}
}