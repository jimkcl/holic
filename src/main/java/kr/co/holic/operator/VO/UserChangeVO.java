package kr.co.holic.operator.VO;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserChangeVO {
	
	/* - HanJungIl - 170610 ~ 170630
	 * 회원 정보 변경용 VO
	 */
	
	private String buttonType;
	private String yyyyMMdd;
	private Date pDate;
	private List<String> userKeyList;
	
	public void setDate() throws Exception {
		this.pDate = new SimpleDateFormat("yyyy-MM-dd").parse(yyyyMMdd);
	}
}