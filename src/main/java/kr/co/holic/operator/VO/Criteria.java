package kr.co.holic.operator.VO;

import lombok.Data;

@Data
public class Criteria {
	
	/**
	 * 페이지 처리 VO
	 * 
	 * */
	
	private int page;
	private int perPageNum;
	
	public Criteria() {
		page=1;
		perPageNum=10;
	}

	//getStartrow와 getEndrow는 boardMapper쪽에서 반환받기 위해 사용
	public int getStartrow(){
		int startrow=(page-1)*perPageNum+1;
		return startrow;
	}
	
	public int getEndrow(){
		int endrow=page*perPageNum;
		//((page-1)*perPageNum+1)+(perPageNum-1)=page*perPageNum-perPageNum+1+perPageNum-1=page*perPageNum
		return endrow;
	}
}