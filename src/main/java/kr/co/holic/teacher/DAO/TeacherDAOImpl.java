package kr.co.holic.teacher.DAO;

import javax.inject.Inject;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import kr.co.holic.teacher.VO.LessonVO;
import kr.co.holic.teacher.VO.TeacherDetailVO;
import lombok.extern.java.Log;

@Log
@Repository
public class TeacherDAOImpl implements TeacherDAO {

	@Inject
	private SqlSession session;
	private final String NAMESPACE="ko.co.holic.teacherMapper";
	
	
	@Override
	public void profileCreate(TeacherDetailVO detailVO) throws Exception {
		// TODO Auto-generated method stub
		session.insert(NAMESPACE+".profileCreate", detailVO);
	}


	@Override
	public void insertTeacherLesson(LessonVO lessonVO) throws Exception {
		// TODO Auto-generated method stub
		
		
	}

}
