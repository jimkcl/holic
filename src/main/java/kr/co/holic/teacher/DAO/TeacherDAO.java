package kr.co.holic.teacher.DAO;

import kr.co.holic.teacher.VO.LessonVO;
import kr.co.holic.teacher.VO.TeacherDetailVO;

public interface TeacherDAO {

	public abstract void profileCreate(TeacherDetailVO detailVO) throws Exception;
	public abstract void insertTeacherLesson(LessonVO lessonVO)throws Exception;
	
}
