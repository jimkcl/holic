package kr.co.holic.teacher.service;

import kr.co.holic.teacher.VO.LessonVO;
import kr.co.holic.teacher.VO.TeacherDetailVO;

public interface TeacherService {

	public abstract void profileCreate(TeacherDetailVO detailVO) throws Exception;
	public abstract void insertTeacherLesson(LessonVO lessonVO)throws Exception;
}
