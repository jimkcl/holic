package kr.co.holic.teacher.service;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import kr.co.holic.teacher.DAO.TeacherDAO;
import kr.co.holic.teacher.VO.LessonVO;
import kr.co.holic.teacher.VO.TeacherDetailVO;
import lombok.extern.java.Log;

@Log
@Service
public class TeacherServiceImpl implements TeacherService {
	
	@Inject
	private TeacherDAO dao;
	
	@Override
	public void profileCreate(TeacherDetailVO detailVO) throws Exception {
		// TODO Auto-generated method stub
		dao.profileCreate(detailVO);
	}

	@Override
	public void insertTeacherLesson(LessonVO lessonVO) throws Exception {
		// TODO Auto-generated method stub
		
		dao.insertTeacherLesson(lessonVO);
	}

}
