package kr.co.holic.teacher.VO;


public class TimgVO {
		private String userKey;
		private String gradePath;
		private String provePath;
		
		public TimgVO() {
			// TODO Auto-generated constructor stub
		}

		public TimgVO(String gradePath, String provePath, String userKey) {
			super();
			this.gradePath = gradePath;
			this.provePath = provePath;
			this.userKey = userKey;
		}
		
		
		public String getUserKey() {
			return userKey;
		}

		public void setUserKey(String userKey) {
			this.userKey = userKey;
		}

		public String getGradePath() {
			return gradePath;
		}

		public void setGradePath(String gradePath) {
			this.gradePath = gradePath;
		}

		public String getProvePath() {
			return provePath;
		}

		public void setProvePath(String provePath) {
			this.provePath = provePath;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((gradePath == null) ? 0 : gradePath.hashCode());
			result = prime * result + ((provePath == null) ? 0 : provePath.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			TimgVO other = (TimgVO) obj;
			if (gradePath == null) {
				if (other.gradePath != null)
					return false;
			} else if (!gradePath.equals(other.gradePath))
				return false;
			if (provePath == null) {
				if (other.provePath != null)
					return false;
			} else if (!provePath.equals(other.provePath))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "TimgVO [userKey=" + userKey + ", gradePath=" + gradePath + ", provePath=" + provePath + "]";
		}
		
}
