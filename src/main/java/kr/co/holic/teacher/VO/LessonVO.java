package kr.co.holic.teacher.VO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LessonVO {
	
	/**
	 * 수강신청 VO
	 * 
	 * */
	
	private int lIdx;
	private String userKey;
	private String lTitle;
	private String lAddress;
	private String lDate;
	private int lPersonnel;
	private String lAbility;
	private String lMaterials;
	private int lType;
}
