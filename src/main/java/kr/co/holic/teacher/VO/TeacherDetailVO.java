package kr.co.holic.teacher.VO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TeacherDetailVO {
	private String userKey;
	private String userName;
	private String tProfile;
	private int tListening;
	private int tRead;
	private int tWriting;
	private int tGrade;
	private String tUpdateDate;
	
	private String userNick;
}