package kr.co.holic.teacher.controller;

import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import kr.co.holic.teacher.VO.LessonVO;
import kr.co.holic.teacher.VO.TeacherDetailVO;
import kr.co.holic.teacher.service.TeacherService;
import lombok.extern.java.Log;

@Log
@Controller
@RequestMapping("/teacher")
public class TeacherProFileController {

	@Inject
	private TeacherService service;
	
	@RequestMapping(value="profile_create", method=RequestMethod.GET)
	public void profile_createGet() throws Exception{}
	
	@RequestMapping(value="profile_create", method=RequestMethod.POST)
	public void profile_create(TeacherDetailVO detailVO) throws Exception{
		service.profileCreate(detailVO);
	}
	
	@RequestMapping(value="profile_detail_page", method=RequestMethod.GET)
	public void profile_detail_pageGet() throws Exception {}
	
	@RequestMapping(value="teacherProfileList", method=RequestMethod.GET)
	public void teacherProfileList() throws Exception {
		// TODO 강사/스터디 강사 리스트 페이지
	}
	
	@RequestMapping(value="profileList", method=RequestMethod.GET)
	public void profileList() throws Exception {
		// TODO 강사/스터디 스터디 리스트 페이지
	}
	
	@RequestMapping(value="profileDetail", method=RequestMethod.GET)
	public void profileDetail() throws Exception {
		// TODO 강사/스터디 스터디 상세정보 페이지
	}
	
	@RequestMapping(value="teacherLesson", method=RequestMethod.GET)
	public void teacherLessonGet()throws Exception{
		
		
	}
	
	@RequestMapping(value="teacherLesson", method=RequestMethod.POST)
	public void teacherLessonPost(LessonVO lessonVO, HttpSession session)throws Exception{
		
		@SuppressWarnings("unchecked")
		Map<String, Object> map = (Map<String, Object>) session.getAttribute("LOGIN");
		String userKey = (String) map.get("userKey");
		
		
	}
}