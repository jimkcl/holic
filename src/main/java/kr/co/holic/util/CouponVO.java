package kr.co.holic.util;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CouponVO {
	@NotNull @Length(min=19, max=19)
	private String serial;
	private String validity;
	private int term;
}