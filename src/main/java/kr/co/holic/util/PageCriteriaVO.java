package kr.co.holic.util;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.java.Log;

@Log
@ToString
@NoArgsConstructor
public class PageCriteriaVO {
	
	/* - HanJungIl - 170610 ~ 170630
	 * 페이징 관련 VO - 강의동영상, 회원리스트
	 */
	
	private final int PAGE_COLUMN_COUNT = 10;
	@Getter private final int PAGE_UNDER_COUNT = 10;
	@Getter @Setter private int pageNum;
	@Getter @Setter private int totalCount;
	@Getter @Setter private int firstPage;
	@Getter @Setter private int endPage;
	@Getter @Setter private int firstColumn;
	@Getter @Setter private int endColumn;
	@Getter private boolean endBtnCheck = true;
	
	@Getter @Setter private String type;
	@Getter @Setter private String inputText;
	
	@Getter @Setter private String userType;
	
	
	public void setTypeNInputText(){
		if("vPackage".equals(type) && "없음".equals(inputText)) inputText = null;
		if("tUserName".equals(type) && "기타".equals(inputText)) inputText = null;
		
		if(inputText != null && inputText.length() != 0) {
			inputText = inputText.replaceAll("%", "");
			inputText = "%" + inputText + "%";
		}else{
			inputText = null;
		}
	}
	
	public void setPageCriteriaVO(final int pageNum, final int totalCount){
		int totalEndPage = totalCount / PAGE_COLUMN_COUNT;
		
		if(totalCount % PAGE_COLUMN_COUNT != 0) totalEndPage += 1;
		
		if(pageNum == -1) this.pageNum = totalEndPage;
		else this.pageNum = pageNum;
		
		this.firstPage = ((this.pageNum - 1) / PAGE_COLUMN_COUNT) * PAGE_UNDER_COUNT + 1;
		this.endPage = ((this.pageNum - 1) / PAGE_COLUMN_COUNT) * PAGE_UNDER_COUNT + PAGE_COLUMN_COUNT;
		
		if(totalEndPage < endPage) endPage = totalEndPage;
		
		this.firstColumn = (this.pageNum * PAGE_COLUMN_COUNT) - PAGE_COLUMN_COUNT + 1;
		this.endColumn = this.pageNum * PAGE_COLUMN_COUNT;
		this.totalCount = totalCount;
		
		if(totalEndPage == endPage) this.endBtnCheck = false;
		
		setTypeNInputText();
		
		log.info(" :: pageVO :: " + this.toString());
	}
}