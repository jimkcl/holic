package kr.co.holic.util;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.stereotype.Component;

@Component
public class EmailSender {
	private final String username = "theholickorean@gmail.com";
	private final String password = "rksrhemddj";
	
	public void sendEmail(String recipient, String title, String content) {
		Properties props = new Properties();
		
		props.put("mail.smtp.auth", "true");			// gmail은 무조건 true 고정
		props.put("mail.smtp.starttls.enable", "true");	// gmail은 무조건 true 고정
		props.put("mail.smtp.host", "smtp.gmail.com");	// smtp 서버 주소
		props.put("mail.smtp.port", "587");				// gmail 포트
		
		//*
		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});
		//*/
		try {
			/*
			MimeMessage mimeMessage = mailSender.createMimeMessage();

            mimeMessage.setFrom(new InternetAddress(mailSender.getUsername()));

            mimeMessage.addRecipient(RecipientType.TO, new InternetAddress(recipient));

            mimeMessage.setSubject(title);

            mimeMessage.setText(content, "UTF-8", "html");
			//*/
			//*
			Message message = new MimeMessage(session);
			
			// 편지보낸시간
			message.setSentDate(new Date());
			
			// 이메일 발신자
			message.setFrom(new InternetAddress());
			
			// 이메일 수신자
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(recipient));
			
			// 이메일 제목
			message.setSubject(title);
			
			// 이메일 내용
			message.setText(content);
			
			// 이메일 헤더
			message.setHeader("content-Type", "text/html");
			
			Transport.send(message);
			//*/
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
}