package kr.co.holic.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.imgscalr.Scalr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.FileCopyUtils;


public class UploadFile {
	
	private static final Logger logger=LoggerFactory.getLogger(UploadFile.class);
	
	public static String uploadFile(String uploadPath, String originalName, byte[] fileData,String userNick,String folderType) throws Exception {
			
			UUID uid=UUID.randomUUID(); 
			String savedName=uid.toString()+"_"+originalName; 
			String savedPath=calcPath(uploadPath, userNick).replace(File.separatorChar, '/');
			File target=new File(uploadPath+savedPath, savedName);
			FileCopyUtils.copy(fileData, target);  
			String uploadedFileName=savedPath+"/"+savedName;
			
			if(folderType.equals("Profile")){
				uploadedFileName=makeThumbnail(uploadPath, savedPath, savedName);
			}
			
			return uploadedFileName;
		}
	

	//업로드 경로를 계산하는 부분
	private static String calcPath(String uploadPath,String userNick) {
		
		String nickPath = File.separatorChar+userNick;
		
		makeDir(uploadPath, nickPath);
		
		return nickPath;
	}
	
	//폴더 생성
	private static void makeDir(String uploadPath, String nickPath) { 
		// TODO Auto-generated method stub
		File f=new File(nickPath);
		
		if(f.exists()){ //있으면 빠져나옴
			return;
		}else{
			File folder = new File(uploadPath+nickPath);
			folder.mkdirs();
		}
	}
	
	private static String makeThumbnail(String uploadPath, String path, String fileName) throws Exception {
		
		File sF=new File(uploadPath+path, fileName); 
		BufferedImage sourceImg=ImageIO.read(sF); 
		
		BufferedImage destImg=Scalr.resize(sourceImg, Scalr.Method.AUTOMATIC, Scalr.Mode.FIT_TO_HEIGHT, 250);
		
		String thumbnailName=uploadPath+path+File.separator+"s_"+fileName; 
		
		File newFile=new File(thumbnailName);
		String formatName=fileName.substring(fileName.lastIndexOf(".")+1); 
		
		ImageIO.write(destImg, formatName.toUpperCase(), newFile); 
		
		String re=thumbnailName.substring(uploadPath.length()).replace(File.separatorChar, '/'); 
		
		return re;
	}	
	
	
}
