package kr.co.holic.scheduler;

import java.util.List;

public interface SchedulerDAO {
	public void finishUserChange(List<String> payUserList);
	
	public List<String> paymentFinishUserList();
	public void paymentFinishUserDelete();
	
	public List<String> blockFinishUserList();
	public void blockFinishUserDelete();
}