package kr.co.holic.scheduler;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import lombok.extern.java.Log;

@Log
@Repository
public class SchedulerDAOImpl implements SchedulerDAO {
	
	/* - HanJungIl - 170610 ~ 170630
	 * 스케줄러 DAO
	 */
	
	@Inject
	private SqlSession session;
	private static String NAMESPACE = "kr.co.mapper.SchedulerMapper";
	
	@Override
	public void finishUserChange(List<String> payUserList) {
		// TODO 회원 타입 변경
		session.update(NAMESPACE + ".finishUserChange", payUserList);
	}
	
	
	@Override
	public List<String> paymentFinishUserList() {
		// TODO 유료 종료 회원 리스트
		return session.selectList(NAMESPACE + ".paymentFinishUserList");
	}
	
	@Override
	public void paymentFinishUserDelete() {
		// TODO 유료 회원 리스트 삭제
		session.delete(NAMESPACE + ".paymentFinishUserDelete");
	}
	
	@Override
	public List<String> blockFinishUserList() {
		// TODO 블럭 종료 회원 리스트
		return session.selectList(NAMESPACE + ".blockFinishUserList");
	}
	
	@Override
	public void blockFinishUserDelete() {
		// TODO 블럭 회원 리스트 삭제
		session.delete(NAMESPACE + ".blockFinishUserDelete");
	}
}