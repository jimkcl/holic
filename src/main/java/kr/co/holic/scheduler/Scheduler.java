package kr.co.holic.scheduler;

import javax.inject.Inject;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.extern.java.Log;

@Log
@Component
public class Scheduler {
	
	/* - HanJungIl - 170610 ~ 170630
	 * 스케줄러
	 */
	
	@Inject
	private SchedulerService service;
	
	@Scheduled(cron="00 00 04 * * *")
	public void changeUser(){
		// TODO 유료회원 / 블럭회원 해제용
		service.changeUser();
	}
}