package kr.co.holic.scheduler;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.java.Log;

@Log
@Service
public class SchedulerService {
	
	/* - HanJungIl - 170610 ~ 170630
	 * 스케줄러 Service
	 */
	
	@Inject
	private SchedulerDAO dao;
	
	@Transactional
	public void changeUser(){
		List<String> list = dao.paymentFinishUserList();
		
		if(list.size() != 0){
			dao.finishUserChange(list);
			dao.paymentFinishUserDelete();
		}
		
		list = dao.blockFinishUserList();
		
		if(list.size() != 0){
			dao.finishUserChange(list);
			dao.blockFinishUserDelete();
		}
	}
}