package kr.co.holic.interceptor;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import lombok.extern.java.Log;

@Log
public class OperaterInterceptor extends HandlerInterceptorAdapter {
	
	/* - HanJungIl - 170610 ~ 170630
	 * operator check 용 interceptor
	 */
	
	// TODO 회원 타입
	public final int USER = 1;
	public final int DECL_USER = 2;
	public final int TEACHER = 5;
	public final int DELETE_USER = 6;
	public final int OPERATOR = 8;
	public final int ADMINISTRATOR = 9;
	
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		super.postHandle(request, response, handler, modelAndView);
	}
	
	// TODO 로그인 체크
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		log.info(" :: operater interceptor :: ");
		HttpSession session = request.getSession();
		
		Map map = (Map) session.getAttribute("LOGIN");
		
		String returnPage = "redirect:../";
		
		if(map == null || map.get("userKey") == null 
				|| "".equals( ((String) map.get("userKey")).replaceAll(" ", "") )
				|| map.get("userNick") == null || "".equals( ((String) map.get("userNick")).replaceAll(" ", "") )
				|| map.get("userType") == null || "".equals( ((Integer) map.get("userType")) )){
			log.info(" - not login");
			
			saveDest(request);
			response.sendRedirect(returnPage);
			return false;
		}
		
		// TODO 로그인 유지
		session.setAttribute("LOGIN", map);
		
		if( (Integer) map.get("userType") != OPERATOR 
				&& (Integer)map.get("userType") != ADMINISTRATOR ) {
			log.info(" - not admin");
			
			response.sendRedirect(returnPage);
			return false;
		}
		return true;
	}
	
	// TODO GET 일 경우 경로설정을 위한 용도.. 인데 사용여부 확인 후 처리 요망
	private void saveDest(HttpServletRequest request) {
		String uri=request.getRequestURI();
		String query=request.getQueryString();
		
		if(query==null||query.equals("null")) {
			query="";
		} else{
			query="?"+query;
		}
		
		if(request.getMethod().equals("GET")) {
			request.getSession().setAttribute("dest", uri+query);
		}
	}
}