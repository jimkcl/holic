package kr.co.holic.aop;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

import lombok.extern.java.Log;

@Log
@Aspect
public class PaymentAspect {
	
	/* - HanJungIl - 170610 ~ 170630
	 * payment check용 AOP
	 */

	// TODO 회원 타입
	public final int USER = 1;
	public final int DECL_USER = 2;
	public final int TEACHER = 5;
	public final int DELETE_USER = 6;
	public final int OPERATOR = 8;
	public final int ADMINISTRATOR = 9;
	
	@Pointcut("execution(* kr.co.holic.user.controller.UserPaymentController.paymentChangeUser(..))")
	public void loginCheck() throws Throwable{}
	
	@Around("loginCheck()")
	public Object loginCheck(ProceedingJoinPoint joinPoint) throws Throwable {
		log.info(" - loginCheck - ");
		
		String returnPage = "redirect:../";
		HttpSession session = null;
		
		for(Object o : joinPoint.getArgs()){
			if(o instanceof HttpSession){
				session = (HttpSession) o;
			}
		}
		
		if(session == null) {
			log.info(" - not login");
			return returnPage;
		}
		
		Map<String, Object> login = (Map<String, Object>) session.getAttribute("LOGIN");
		
		if(login == null){
			log.info(" :: not login :: ");
			return returnPage;
		}
		
		String userKey = (String) login.get("userKey");
		String userNick = (String) login.get("userNick");
		Integer userType = (Integer) login.get("userType");
		
		if( userType == null || userKey == null 
				|| userNick == null || userType < USER 
				|| "".equals(userKey.replaceAll(" ", "")) 
				|| "".equals(userNick.replaceAll(" ", ""))) {
			log.info(" - not Login : sequence : " + userKey + "\tnick : " + userNick + "\tetc : " + userType);
			return returnPage;
		}
		
		if(userType < USER || TEACHER < userType){
			log.info(" - not userType");
			return returnPage;
		}
		
		return joinPoint.proceed();
	}
}