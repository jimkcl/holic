package kr.co.holic.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import lombok.extern.java.Log;

@Log
@Aspect
public class LoggerAspect {
	
	/* - HanJungIl - 170610 ~ 170630
	 * Log 출력용 AOP
	 */
	
	@Before("execution(* kr.co.holic..*Controller.*(..))")
	public void logPrint(JoinPoint joinPoint) throws Throwable{
		log.info(" :::: method : " + joinPoint.getSignature());
	}
}