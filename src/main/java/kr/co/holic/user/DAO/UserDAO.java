package kr.co.holic.user.DAO;

import kr.co.holic.teacher.VO.TeacherDetailVO;
import kr.co.holic.teacher.VO.TimgVO;
import kr.co.holic.user.VO.UserDetailVO;
import kr.co.holic.user.VO.UserLoginVO;
import kr.co.holic.util.CouponVO;

public interface UserDAO {
	
	// TODO 아이디 찾기
	public String searchID(String userMail) throws Exception;
	
	// TODO 비밀번호 찾기
	public int searchPassword(UserLoginVO userLoginVO) throws Exception;
	
	// TODO 회원탈퇴
	public int userDelete(String userKey) throws Exception;
	
	// TODO 가입시 쿠폰 발행용
	public int joinCoupon(CouponVO couponVO) throws Exception;
	
	public abstract UserDetailVO getDetail(String userKey)throws Exception;
	public abstract UserLoginVO getUserLoginVO(String userId)throws Exception;
	public abstract void UserkeyInsert()throws Exception;
	public abstract int getUserKey()throws Exception;
	public abstract void DeletUserKey()throws Exception;
	public abstract void LoginVOInsert(UserLoginVO loginVO)throws Exception;
	public abstract void DetailInsert(UserDetailVO detailVO)throws Exception;
	public abstract String userJoinCheck(String userKind, String KindName)throws Exception;
	public abstract void upDateUserDetaiVO(UserDetailVO userDetailVO)throws Exception;
	public abstract TeacherDetailVO getTeacherDetailVO(String userKey)throws Exception;
	public abstract TimgVO getTimgVO(String userKey)throws Exception;
	public abstract void updateTeacherDetailVO(TeacherDetailVO teacherDetailVO)throws Exception;
	public abstract void updateTimg(TimgVO timgVO)throws Exception;

}
