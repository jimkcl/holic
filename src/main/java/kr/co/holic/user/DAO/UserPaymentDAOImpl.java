package kr.co.holic.user.DAO;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import kr.co.holic.user.VO.UserDetailVO;
import kr.co.holic.user.VO.UserPaymentVO;
import kr.co.holic.util.CouponVO;

@Repository
public class UserPaymentDAOImpl implements UserPaymentDAO {
	
	/* - HanJungIl - 170610 ~ 170630
	 * 결제/안내 페이 관련 DAOInfo
	 */
	
	@Inject
	private SqlSession session;
	private final String NAMESPACE = "kr.co.mapper.PaymentMapper";
	
	
	@Override
	public int couponUsing(CouponVO coupon) throws Exception {
		// TODO 쿠폰 사용
		return session.selectOne(NAMESPACE + ".couponUsing", coupon);
	}
	
	@Override
	public void couponExpiration(CouponVO coupon) throws Exception {
		// TODO 쿠폰 만료 처리
		session.delete(NAMESPACE + ".couponExpiration", coupon);
	}
	
	
	@Override
	public int paymentChangeUser(UserPaymentVO userPaymentVO) throws Exception {
		// TODO 유료 회원 결제정보 등록(기간등록)
		return session.insert(NAMESPACE + ".paymentChangeUser", userPaymentVO);
	}

	@Override
	public void paymentChangeUserType(UserPaymentVO userPaymentVO) throws Exception {
		// TODO 유료 회원 결제정보 등록시 회원타입 변경
		session.update(NAMESPACE + ".paymentChangeUserType", userPaymentVO);
	}

	@Override
	public UserDetailVO paymentChangeUserCheck(UserPaymentVO userPaymentVO) throws Exception {
		// TODO 에러시 회원에 있는 기간을 확인
		return session.selectOne(NAMESPACE + ".paymentChangeUserCheck", userPaymentVO);
	}

	@Override
	public void paymentChangeUserDataDelete(UserPaymentVO userPaymentVO) throws Exception {
		// TODO 결제내역은 있으나 회원정보가 없는 경우 - 결제내역 삭제
		session.delete(NAMESPACE + ".paymentChangeUserDataDelete", userPaymentVO);
	}

	@Override
	public void paymentChangeUserDate(UserPaymentVO userPaymentVO) throws Exception {
		// TODO 결제기간 연장
		session.update(NAMESPACE + ".paymentChangeUserDate", userPaymentVO);
	}
}