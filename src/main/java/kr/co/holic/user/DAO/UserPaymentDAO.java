package kr.co.holic.user.DAO;

import kr.co.holic.user.VO.UserDetailVO;
import kr.co.holic.user.VO.UserPaymentVO;
import kr.co.holic.util.CouponVO;

public interface UserPaymentDAO {
	
	/* - HanJungIl - 170610 ~ 170630
	 * 결제/안내 페이 관련 DAO
	 */
	
	// TODO 쿠폰 사용
	int couponUsing(CouponVO coupon) throws Exception;
	
	// TODO 쿠폰 만료 처리
	void couponExpiration(CouponVO coupon) throws Exception;
	
	
	// TODO 유료 회원 결제정보 등록(기간등록)
	int paymentChangeUser(UserPaymentVO userPaymentVO) throws Exception;
	
	// TODO 유료 회원 결제정보 등록시 회원타입 변경
	void paymentChangeUserType(UserPaymentVO userPaymentVO) throws Exception;
	
	// TODO 에러시 회원에 있는 기간을 확인
	UserDetailVO paymentChangeUserCheck(UserPaymentVO userPaymentVO) throws Exception;
	
	// TODO 결제내역은 있으나 회원정보가 없는 경우 - 결제내역 삭제
	void paymentChangeUserDataDelete(UserPaymentVO userPaymentVO) throws Exception;
	
	// TODO 결제기간 연장
	void paymentChangeUserDate(UserPaymentVO userPaymentVO) throws Exception;
}