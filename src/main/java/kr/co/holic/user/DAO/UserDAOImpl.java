package kr.co.holic.user.DAO;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import kr.co.holic.teacher.VO.TeacherDetailVO;
import kr.co.holic.teacher.VO.TimgVO;
import kr.co.holic.user.VO.UserDetailVO;
import kr.co.holic.user.VO.UserLoginVO;
import kr.co.holic.util.CouponVO;
import lombok.extern.java.Log;

@Log
@Repository 
public class UserDAOImpl implements UserDAO {
	
	/* - HanJungIl - 170610 ~ 170709 - 추가 및 수정
	 * 회원 관련 DAO
	 */
	
	@Inject
	private SqlSession session;
	private final String NAMESPACE = "ko.co.holic.userMapper";
	
	
	@Override
	public String searchID(String userMail) throws Exception {
		// TODO 아이디 찾기
		return session.selectOne(NAMESPACE + ".searchID", userMail);
	}

	@Override
	public int searchPassword(UserLoginVO userLoginVO) throws Exception {
		// TODO 비밀번호 찾기
		return session.update(NAMESPACE + ".searchPassword", userLoginVO);
	}
	
	@Override
	public int userDelete(String userKey) throws Exception {
		// TODO 회원탈퇴
		return session.delete(NAMESPACE + ".userDelete", userKey);
	}
	
	@Override
	public int joinCoupon(CouponVO couponVO) throws Exception {
		// TODO 가입시 쿠폰 발행용
		return session.insert(NAMESPACE + ".joinCoupon", couponVO);
	}

	@Override
	public UserDetailVO getDetail(String userKey) throws Exception {
		// TODO Auto-generated method stub
		
		return session.selectOne(NAMESPACE+".getDetail",userKey);
	}

	@Override
	public UserLoginVO getUserLoginVO(String userId) throws Exception {
		// TODO Auto-generated method stub
		return session.selectOne(NAMESPACE+".getUserLoginVO",userId);
	}

	@Override
	public String userJoinCheck(String userKind, String KindName) throws Exception {
		// TODO Auto-generated method stub
		
		return session.selectOne(NAMESPACE+"."+userKind, KindName);
	}

	@Override
	public int getUserKey() throws Exception {
		// TODO Auto-generated method stub
		return session.selectOne(NAMESPACE+".getUserKey");
	}

	@Override
	public void UserkeyInsert() throws Exception {
		// TODO Auto-generated method stub
		int userkey = 0;
		session.insert(NAMESPACE+".UserkeyInsert",userkey);
	}
	
//delet
	@Override
	public void DeletUserKey() throws Exception {
		// TODO Auto-generated method stub
		session.delete(NAMESPACE+".DeletUserKey");
	}
	
//insert VO 부분
	@Override
	public void LoginVOInsert(UserLoginVO loginVO) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> map =new HashMap<String, Object>();
		map.put("userKey", loginVO.getUserKey());
		map.put("userId", loginVO.getUserId());
		map.put("userPassword", loginVO.getUserPassword());
		
		session.insert(NAMESPACE+".userLoginJoin",map);
	}

	@Override
	public void DetailInsert(UserDetailVO detailVO) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("userKey", detailVO.getUserKey());
		map.put("userWechat", detailVO.getUserWechat());
		map.put("userNick", detailVO.getUserNick());
		map.put("userMail", detailVO.getUserMail());
		
		session.insert(NAMESPACE+".userDetailJoin",map);
	}

	@Override
	public void upDateUserDetaiVO(UserDetailVO userDetailVO) throws Exception {
		// TODO Auto-generated method stub
		
		session.update(NAMESPACE+".upDateUserDetaiVO",userDetailVO);
		
	}

	@Override
	public TeacherDetailVO getTeacherDetailVO(String userKey) throws Exception {
		// TODO Auto-generated method stub
		
		
		return session.selectOne(NAMESPACE+".getTeacherDetailVO",userKey);
	}

	@Override
	public TimgVO getTimgVO(String userKey) throws Exception {
		// TODO Auto-generated method stub
		return session.selectOne(NAMESPACE+".getTimg",userKey);
	}

	@Override
	public void updateTeacherDetailVO(TeacherDetailVO teacherDetailVO) throws Exception {
		// TODO Auto-generated method stub
		
		session.update(NAMESPACE+".updateTeacherDetailVO",teacherDetailVO);
	}

	@Override
	public void updateTimg(TimgVO timgVO) throws Exception {
		// TODO Auto-generated method stub
		
		session.update(NAMESPACE+".updateTimg",timgVO);
	}
}