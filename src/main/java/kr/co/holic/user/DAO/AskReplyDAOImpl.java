package kr.co.holic.user.DAO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import kr.co.holic.operator.VO.Criteria;
import kr.co.holic.user.VO.AskReplyVO;
import lombok.extern.java.Log;

@Log
@Repository
public class AskReplyDAOImpl implements AskReplyDAO {
	
	@Inject
	private SqlSession session;
	private final String NAMESPACE = "kr.co.holic.AskReplyMapper";
	
	@Override
	public void create(AskReplyVO vo) throws Exception {
		// TODO Auto-generated method stub
		log.info(" :::: AskReplyDAOImpl - aaaa :::: " + vo);
		session.insert(NAMESPACE+".create", vo);
	}

	@Override
	public List<AskReplyVO> list(Integer idx) throws Exception {
		// TODO Auto-generated method stub
		return session.selectList(NAMESPACE+".list", idx);
	}

	@Override
	public void delete(Integer bIdx, Integer idx) throws Exception {
		// TODO Auto-generated method stub
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("bIdx", bIdx);
		map.put("idx", idx);
		
		session.delete(NAMESPACE+".delete", map);
	}

	@Override
	public List<AskReplyVO> listPage(Integer idx, Criteria cri) throws Exception {
		// TODO Auto-generated method stub
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("idx", idx);
		map.put("startrow", cri.getStartrow());
		map.put("endrow", cri.getEndrow());
		
		return session.selectList(NAMESPACE, map);
	}



}
