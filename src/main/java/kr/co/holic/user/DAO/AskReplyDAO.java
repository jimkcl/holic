package kr.co.holic.user.DAO;

import java.util.List;

import kr.co.holic.operator.VO.Criteria;
import kr.co.holic.user.VO.AskReplyVO;

public interface AskReplyDAO {

	public abstract void create(AskReplyVO vo) throws Exception; //댓글쓰기
	public abstract List<AskReplyVO> list(Integer idx) throws Exception; //댓글 리스트
	public abstract void delete(Integer bIdx, Integer idx) throws Exception; //댓글 삭제
	public abstract List<AskReplyVO> listPage(Integer idx, Criteria cri) throws Exception; //페이지목록
	

	
}
