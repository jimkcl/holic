package kr.co.holic.user.VO;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import kr.co.holic.util.AES256;
import kr.co.holic.util.SHA256;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserLoginVO {
	
	private String userKey;
	@NotNull @Size(min=1) private String userId;
	@NotNull @Size(min=1) private String userPassword;

	//암호화 부분
	public void secretCode(UserLoginVO loginVO) throws Exception{
		AES256 aes256 = new AES256();
		SHA256 sha256 = new SHA256();
		
		this.userKey = sha256.encrypt(loginVO.getUserKey());
		this.userPassword = sha256.encrypt(loginVO.getUserPassword());
		this.userId = aes256.aesEncode(loginVO.getUserId());
	}
			
	// 복호화
	public UserLoginVO restoreCode(UserLoginVO loginVO) throws Exception{
		AES256 aes256 = new AES256();
		this.userId = aes256.aesDecode(loginVO.getUserId());
		
		return loginVO;
	}
}