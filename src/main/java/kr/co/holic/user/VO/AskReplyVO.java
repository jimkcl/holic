package kr.co.holic.user.VO;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AskReplyVO {
	
	/**
	 * 문의게시판 댓글 VO
	 * 
	 * */
	private int idx;
	@NotNull private int bIdx;
	private String userKey;
	private String userNick;
	@NotNull private String rContent;
	private String rDate;
}