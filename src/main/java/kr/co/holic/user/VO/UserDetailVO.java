package kr.co.holic.user.VO;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import kr.co.holic.util.AES256;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserDetailVO {
	
	private String userKey;
	@NotNull @Size(min=1) private String userWechat;
	@NotNull @Size(min=1) private String userNick;
	@NotNull @Size(min=1) private String userMail;
	private String userSchool;
	private String userPost;
	private String userAddress1;
	private String userAddress2;
	private int userGender;
	private String userBirth;
	private int userType;
	private String userDate;
	
	private UserLoginVO userLoginVO;
	private UserPaymentVO userPaymentVO;
	

	public UserDetailVO(String userKey, String userWechat, 
			String userNick, String userMail, String userSchool,
			String userPost, String userAddress1, String userAddress2, 
			int userGender, String userBirth, int userType,
			String userDate) {
		super();
		this.userKey = userKey;
		this.userWechat = userWechat;
		this.userNick = userNick;
		this.userMail = userMail;
		this.userSchool = userSchool;
		this.userPost = userPost;
		this.userAddress1 = userAddress1;
		this.userAddress2 = userAddress2;
		this.userGender = userGender;
		this.userBirth = userBirth;
		this.userType = userType;
		this.userDate = userDate;
	}

	//암호화 부분
	public void secretCode(UserDetailVO detailVO) throws Exception{
		AES256 aes256 = new AES256();
		
		this.userWechat = aes256.aesEncode(detailVO.getUserWechat());
		this.userMail = aes256.aesEncode(detailVO.getUserMail());
	}
	
	// 복호화
	public UserDetailVO restoreCode(UserDetailVO detailVO) throws Exception{
		AES256 aes256 = new AES256();
		
		this.userWechat = aes256.aesDecode(detailVO.getUserWechat());
		this.userMail = aes256.aesDecode(detailVO.getUserMail());
		
		return detailVO;
	}
	
	// mapper용
	public void setUserId(String userId){
		if(userLoginVO == null) userLoginVO = new UserLoginVO();
		this.userLoginVO.setUserId(userId);
	}
	
	// mapper용 PaymentDate
	public void setPDate(String pDate){
		if(userPaymentVO == null) userPaymentVO = new UserPaymentVO();
		this.userPaymentVO.setPDate(pDate);
	}
	
	// mapper용 blockUser Date
	public void setBDate(String bDate){
		if(userPaymentVO == null) userPaymentVO = new UserPaymentVO();
		this.userPaymentVO.setPDate(bDate);
	}
}