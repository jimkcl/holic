
package kr.co.holic.user.controller;

import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.co.holic.user.VO.UserPaymentVO;
import kr.co.holic.user.service.UserPaymentService;
import kr.co.holic.util.CouponVO;
import lombok.extern.java.Log;

@Log
@Controller
@RequestMapping("payment")
public class UserPaymentLoginController {
	
	/* - HanJungIl - 170610 ~ 170630
	 * 결제/안내 페이 관련 controller
	 */
	
	@Inject
	private UserPaymentService service;
	
	
	@ResponseBody
	@RequestMapping(value="couponUsing")
	public ResponseEntity<String> couponUsing(HttpSession session, @Valid CouponVO coupon) throws Exception{
		// TODO 쿠폰 사용
//		log.info("\n\n :: coupon :: " + coupon + "\n\n");
		
		service.couponUsing(session, coupon);
		
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	@ResponseBody
	@RequestMapping(value="paymentChangeUser")
	public ResponseEntity<String> paymentChangeUser(HttpSession session,
			@ModelAttribute("vo") UserPaymentVO vo,
			@RequestParam("payType") @Valid @NotNull String payType) throws Exception {
		// TODO 결제
		service.paymentChangeUser(session, vo, payType);
		
		Map<String, String> map = (Map<String, String>) session.getAttribute("LOGIN");
		
		map.put("userType", "2");
		
		session.setAttribute("LOGIN", map);
		
		return new ResponseEntity<String>("1", HttpStatus.OK);
	}
}