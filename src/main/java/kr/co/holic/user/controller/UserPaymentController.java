
package kr.co.holic.user.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import lombok.extern.java.Log;

@Log
@Controller
@RequestMapping("payment")
public class UserPaymentController {
	
	/* - HanJungIl - 170610 ~ 170630
	 * 결제/안내 페이 관련 controller
	 */
	
	@RequestMapping(value="paymentMain")
	public String paymentMain(){
		// TODO 메인 - 기본 안내 페이지
		return "payment/paymentMain";
	}
	
	@RequestMapping(value="paymentInformation")
	public String paymentInformation(){
		// TODO 안내 페이지
		return "payment/paymentInformation";
	}
	
	@RequestMapping(value="paymentSet")
	public String paymentSet(){
		// TODO 결제 페이지
		return "payment/paymentSet";
	}
	
	@RequestMapping(value="coupon")
	public String coupon(){
		// TODO 쿠폰 페이지
		return "payment/coupon";
	}
}