package kr.co.holic.user.controller;

import java.io.File;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import kr.co.holic.util.MediaUtils;
import kr.co.holic.util.UploadFile;

@Controller
@RequestMapping("file")
public class ImgFileController {

	@Resource(name="teacherProfile")
	private String teacherProfile;
	
	@Resource(name="teacherGrade")
	private String teacherGrade;
	
	
	@ResponseBody
	@RequestMapping(value="teacherProfile", method=RequestMethod.POST)
	public ResponseEntity<String> teacherProfile(MultipartFile file, HttpSession session)throws Exception{
		String folderType="Profile";
		String originalName = file.getOriginalFilename();
		String originalNameType = originalName.substring(originalName.lastIndexOf(".")+1);
		if(MediaUtils.getMediaType(originalNameType) != null){
			@SuppressWarnings("unchecked")
			Map<String, Object> map = (Map<String, Object>) session.getAttribute("LOGIN");
			String userNick = (String) map.get("userNick");
			String teacherProfileName = UploadFile.uploadFile(teacherProfile, originalName, file.getBytes(), userNick,folderType);
			String deleteFileName2 = teacherProfileName.substring(userNick.length()+4);
			String deleteFileName1 =teacherProfileName.substring(0,userNick.length()+2);
			String deleteFileName =deleteFileName1+deleteFileName2;
			File deleteFile = new File(teacherProfile+deleteFileName.replace('/', File.separatorChar));
			deleteFile.delete();
			String teacherGradePath = teacherProfile.replace(File.separatorChar, '/');
			 return new ResponseEntity<String>(teacherGradePath+teacherProfileName, HttpStatus.CREATED);
		}
		
		return new ResponseEntity<String>("Profile_NO",HttpStatus.OK);
	}
	
	@ResponseBody
	@RequestMapping(value="deleteProfile", method=RequestMethod.POST)
	public ResponseEntity<String> deleteProfile(String fileName)throws Exception{
		
		String deleteFileName = teacherProfile+fileName.replace('/', File.separatorChar);
		File deleteFile = new File(deleteFileName);
		deleteFile.delete();
		return new ResponseEntity<String>("DELETE_OK",HttpStatus.OK);
		
	}
	
	@ResponseBody
	@RequestMapping(value="teacherGrade", method=RequestMethod.POST)
	public ResponseEntity<String> teacherGrade(MultipartFile file, HttpSession session)throws Exception{
		
		String folderType="Grade";
		String originalName = file.getOriginalFilename();
		String originalNameType = originalName.substring(originalName.lastIndexOf(".")+1);
		if(MediaUtils.getMediaType(originalNameType) != null){
			@SuppressWarnings("unchecked")
			Map<String, Object> map = (Map<String, Object>) session.getAttribute("LOGIN");
			String userNick = (String) map.get("userNick");
			String teacherProfileName = UploadFile.uploadFile(teacherGrade, originalName, file.getBytes(), userNick,folderType);
			String teacherGradePath = teacherGrade.replace(File.separatorChar, '/');
			 return new ResponseEntity<String>(teacherGradePath+teacherProfileName, HttpStatus.CREATED);
		}
		
		return new ResponseEntity<String>("Grade_NO",HttpStatus.OK);
	}
	
	@ResponseBody
	@RequestMapping(value="deleteGrade", method=RequestMethod.POST)
	public ResponseEntity<String> deleteGrade(String fileName)throws Exception{
		
		String deleteFileName = teacherGrade+fileName.replace('/', File.separatorChar);
		File deleteFile = new File(deleteFileName);
		deleteFile.delete();
		
		return new ResponseEntity<String>("DELETE_OK",HttpStatus.OK);
		
	}
	
}
