package kr.co.holic.user.controller;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.co.holic.user.VO.UserDetailVO;
import kr.co.holic.user.VO.UserLoginVO;
import kr.co.holic.user.service.UserService;
import kr.co.holic.util.AES256;
import kr.co.holic.util.SHA256;
import lombok.extern.java.Log;

@Log
@Controller
@Validated
@RequestMapping("/user")
public class UserController {
	
	/* - HanJungIl - 170610 ~ 170709 - 수정
	 * 회원 관련 controller
	 */
	
	@Inject
	private UserService service;
	
	
	@ResponseBody
	@RequestMapping(value="searchId", method=RequestMethod.POST)
	public String searchID(@RequestParam("userMail") @Valid @NotNull @Length(min=4) String userMail) throws Exception {
		// TODO 아이디 찾기
//		log.info("\n\n :: userMail :: " + userMail + "\n\n");
		return service.searchID(userMail);
	}
	
	@ResponseBody
	@RequestMapping(value="searchPassword", method=RequestMethod.POST)
	public int searchPassword(HttpServletRequest request,
			@RequestParam("userId") String userId, 
			@RequestParam("userMail") String userMail) throws Exception {
		// TODO 비밀번호 찾기
//		log.info("\n\n :: userId / userMail : " + userId + " / " + userMail + "\n\n");
		
		if( "".equals(userId.replaceAll(" ", "")) 
				|| "".equals(userMail.replaceAll(" ", ""))) {
			return -1;
		}
		
		return service.searchPassword(request, userMail, userId);
	}
	
	
	@RequestMapping(value="searchUser", method=RequestMethod.GET)
	public void searchUser() throws Exception{}
	
	//로그인부분
	@RequestMapping(value="/login", method=RequestMethod.GET)
	public void logGet(@ModelAttribute("userlog") UserLoginVO userlog) throws Exception{}
	
	//로그인부분
	@ResponseBody
	@RequestMapping(value="/loginPost", method=RequestMethod.POST)
	public ResponseEntity<String> logPost(@ModelAttribute("UserLoginVO") UserLoginVO dto, HttpServletRequest request)throws Exception{
		
		String loginChek = "YES_LOGIN";
		AES256 aes256 = new AES256();
		SHA256 sha256 = new SHA256();
		HttpSession session = request.getSession();
		
		if(session != null){
			session.removeAttribute("Login");//session 지우는것
		}
		
		String userId = aes256.aesEncode(dto.getUserId());
		String userPassword = sha256.encrypt(dto.getUserPassword());
		UserLoginVO loginVo = service.getUserLoginVO(userId);
		
		if(loginVo == null){
			loginChek = "NO_ID";
			return new ResponseEntity<String>(loginChek,HttpStatus.CREATED);
		}
		
		if(loginVo != null && !loginVo.getUserPassword().equals(userPassword)){
			loginChek = "NO_PASS";
			return new ResponseEntity<String>(loginChek,HttpStatus.CREATED);
		}
		
		if(loginVo != null && loginVo.getUserPassword().equals(userPassword)){
			
			UserDetailVO detail = service.getDetail(loginVo.getUserKey());
			
			if(detail.getUserType() < 1) {
				return new ResponseEntity<String>(loginChek, HttpStatus.CREATED);
			}
			
			Map<String, Object> map =new HashMap<String, Object>();
			map.put("userKey", detail.getUserKey());
			map.put("userType", detail.getUserType());
			map.put("userNick", detail.getUserNick());
			loginChek =loginChek +"&"+detail.getUserNick();
			session.setAttribute("LOGIN", map);
		}
		
		return new ResponseEntity<String>(loginChek,HttpStatus.OK);
	}
	

	
	//join 부분
	@RequestMapping(value="/join", method=RequestMethod.GET)
	public void joinGet()throws Exception{}
	
	@ResponseBody
	@RequestMapping(value="/join", method=RequestMethod.POST)
	public ResponseEntity<String> joinPost(HttpServletRequest request,
			@ModelAttribute("UserDetailVO") @Valid UserDetailVO detailVO, 
			@ModelAttribute("UserLoginVO") @Valid UserLoginVO loginVO)throws Exception{
		String OuserId = loginVO.getUserId();
		
		String coupon = service.userJoin(request, detailVO, loginVO);
		
		// 메일발송
		service.sendCouponEmail(request, detailVO, coupon);
		
		UserLoginVO dbLoginVO = service.getUserLoginVO(loginVO.getUserId());
		
//		log.info("\n\n" + dbLoginVO + "\n\n");
		
		if(dbLoginVO != null){
			String dbUserId = dbLoginVO.restoreCode(dbLoginVO).getUserId();
			
//			log.info("\n\n" + dbUserId + "\n\n");
			
			if(OuserId.equals(dbUserId)){
				return new ResponseEntity<String>("Join_Ok",HttpStatus.OK);
			}
		}
		
		return new ResponseEntity<String>("Join_No", HttpStatus.OK);
	}
	
	//아이디 체크
	@ResponseBody
	@RequestMapping(value="/joinIdCheck", method = RequestMethod.POST)
	public ResponseEntity<String> joinIdCheck(String userId)throws Exception{
		String check = "Id_null"; 
		AES256 aes256 = new AES256();
		
		if(userId != null){
			userId =aes256.aesEncode(userId);
			String dbUserId = service.userJoinCheck("CheckUserId",userId);
			if(dbUserId == null){
				return new ResponseEntity<String>("Id_No",HttpStatus.OK);
			}else if(dbUserId.equals(userId)){
				return new ResponseEntity<String>("Id_Yes",HttpStatus.OK);
			}
		}
		return new ResponseEntity<String>(check,HttpStatus.OK);
	}
	
	
	//닉넴 체크
	@ResponseBody
	@RequestMapping(value="/joinNicCheck", method = RequestMethod.POST)
	public ResponseEntity<String> joinCheck(String userNick)throws Exception{
		String check = "Nike_null"; 
		
		if(userNick != null){
			String dbUserNick = service.userJoinCheck("CheCkUserNick",userNick);
			if(dbUserNick == null){
				return new ResponseEntity<String>("Nick_No",HttpStatus.OK);
			}else if(dbUserNick.equals(userNick)){
				return new ResponseEntity<String>("Nick_Yes",HttpStatus.OK);
			}
		}
		return new ResponseEntity<String>(check,HttpStatus.OK);
	}
	
	//위쳇 체크
	@ResponseBody
	@RequestMapping(value="/joinWechatCheck", method = RequestMethod.POST)
	public ResponseEntity<String> joinWechatCheck(String userWechat)throws Exception{
		String check = "Wecha_tNull"; 
		AES256 aes256 = new AES256();
		
		if(userWechat != null){
			userWechat = aes256.aesEncode(userWechat);
			String dbUserWechat = service.userJoinCheck("checkUserWeChat", userWechat);
			if(dbUserWechat == null){
				return new ResponseEntity<String>("Wechat_No",HttpStatus.OK);
			}else if(dbUserWechat.equals(userWechat)){
				return new ResponseEntity<String>("Wechat_Yes",HttpStatus.OK);
			}
			
		}
		return new ResponseEntity<String>(check,HttpStatus.OK);
	}
	
	//메일체크
	@ResponseBody
	@RequestMapping(value="/joinMailCheck", method = RequestMethod.POST)
	public ResponseEntity<String> joinMailCheck(String userMail)throws Exception{
		String check = "Mail_null"; 
		AES256 aes256 = new AES256();
		
		if(userMail != null){
			userMail = aes256.aesEncode(userMail);
			String dbUserMail = service.userJoinCheck("CheckUserMail", userMail);
			if(dbUserMail == null){
				return new ResponseEntity<String>("Mail_No",HttpStatus.OK);
			}else if(dbUserMail.equals(userMail)){
				return new ResponseEntity<String>("Mail_Yes",HttpStatus.OK);
			}
		}
		return new ResponseEntity<String>(check,HttpStatus.OK);
	}
}