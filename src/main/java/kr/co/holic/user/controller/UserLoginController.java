package kr.co.holic.user.controller;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.co.holic.teacher.VO.TeacherDetailVO;
import kr.co.holic.teacher.VO.TimgVO;
import kr.co.holic.user.VO.UserDetailVO;
import kr.co.holic.user.service.UserService;
import lombok.extern.java.Log;

@Log
@Controller
@RequestMapping("user")
public class UserLoginController {
	
	@Inject
	private UserService service;
	
	
	//로그아웃부분
	@RequestMapping("/logout")
	public String logOut(HttpSession session)throws Exception{
		session.removeAttribute("LOGIN");
		session.invalidate();
		return "redirect:/";
	}
	
	@ResponseBody
	@RequestMapping(value="userDelete", method=RequestMethod.POST)
	public int userDelete(HttpSession session) throws Exception {
		// TODO 회원탈퇴
		// 비밀번호 입력받는 방식으로 변경 필요
		return service.userDelete(session);
	}
	
	//마이페지
	@SuppressWarnings("unchecked")
	@RequestMapping(value="userPage", method=RequestMethod.GET)
	public String userPageGet(HttpSession session, Model model) throws Exception{
		
		Map<String, Object> map = (Map<String, Object>) session.getAttribute("LOGIN");
		String dbuserKey = (String) map.get("userKey");
		UserDetailVO userDetail = service.getDetail(dbuserKey);
		userDetail =userDetail.restoreCode(userDetail);
		
		////teacher 부분
		TeacherDetailVO teacherDetail = service.getTeacherDetailVO(dbuserKey);
		TimgVO timg = service.getTimgVO(dbuserKey);
		
		log.info("===========================");
		log.info("\n\n :: teacherDetail :: " + teacherDetail);
		log.info("\n :: timg :: " + timg + "\n\n");
		log.info("===========================");
		
		Map<String, Object> map1 = new HashMap<String, Object>();
		map1.put("teacherDetail", teacherDetail);
		map1.put("timg", timg);
		map1.put("userDetail",userDetail);
		model.addAttribute("user", map1);

		return "/user/userPage";
	}
	
	@ResponseBody
	@RequestMapping(value="userPage", method=RequestMethod.POST)
	public HttpEntity<String> userPagePost(HttpSession session, 
			@ModelAttribute("UserDetailVO") UserDetailVO UserDetailVO ) throws Exception {
		/*String birth = UserDetailVO.getUserBirth();
		Date date = new SimpleDateFormat("yyyy-MM-dd").parse(birth);*/
		
		@SuppressWarnings("unchecked")
		Map<String, Object> map = (Map<String, Object>) session.getAttribute("LOGIN");
		
		String userKey = (String) map.get("userKey");
		UserDetailVO.setUserKey(userKey);
		
		service.upDateUserDetaiVO(UserDetailVO);
		
		HttpEntity<String> entt =new HttpEntity<String>("ok");
		
		return entt;
	}
	
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value="teacherDetail",method=RequestMethod.POST)
	public String teacherPost(HttpSession session, 
			@ModelAttribute TeacherDetailVO teacherDetailVO, 
			@ModelAttribute TimgVO timgVO) throws Exception{
		Map<String, Object> user =(Map<String, Object>) session.getAttribute("LOGIN");
		String userKey = (String) user.get("userKey");
		
		log.info("=================================");
		log.info("\n\n :: timgVO :: " + timgVO);
		log.info("=================================");
		
		service.updateTeaCher(userKey,teacherDetailVO,timgVO);
		
		return "a";
	}
}