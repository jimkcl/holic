package kr.co.holic.user.controller;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import kr.co.holic.user.VO.AskReplyVO;
import kr.co.holic.user.service.AskReplyService;
import lombok.extern.java.Log;

@Log
@Controller
@RequestMapping("/reply")
public class AskReplyController {

	@Inject
	private AskReplyService service;
	
	//댓글 생성
	@RequestMapping(value="create", method=RequestMethod.POST)
	public ResponseEntity<String> create(@Valid AskReplyVO vo, HttpSession session) {
		log.info(" :::: AskReplyController - aaaa :::: " + vo);
		
		ResponseEntity<String> entity=null;
		try {
			service.create(vo, session);
			entity=new ResponseEntity<String>("INSERT_SUCCESS", HttpStatus.OK);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			entity=new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST); 
		}
		
		return entity;
	}
	
	//댓글 삭제
	@RequestMapping(value="delete", method=RequestMethod.POST)
	public ResponseEntity<String> delete(@RequestParam("bIdx") Integer bIdx, @RequestParam("idx") Integer idx) throws Exception { 
		
		/*service.MaxbIdx();*/
		
		ResponseEntity<String> entity=null;
		log.info("bIdx============" + bIdx);
		log.info("idx=============" + idx);
		service.delete(bIdx, idx);
		entity=new ResponseEntity<String>("DELETE_SUCCESS", HttpStatus.OK);
		
		return entity;
	}
	
}
