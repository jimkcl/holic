package kr.co.holic.user.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

import kr.co.holic.operator.VO.Criteria;
import kr.co.holic.user.DAO.AskReplyDAO;
import kr.co.holic.user.VO.AskReplyVO;
import lombok.extern.java.Log;

@Log
@Service
public class AskReplyServiceImpl implements AskReplyService{
	
	@Inject
	private AskReplyDAO dao;
	
	@SuppressWarnings("unchecked")
	@Override
	public void create(AskReplyVO vo, HttpSession session) throws Exception {
		// TODO 댓글 생성
		log.info(" :::: AskReplyServiceImpl - aaaa :::: " + vo);
		
		Map<String, String> map = (Map<String, String>) session.getAttribute("LOGIN");
		String userKey = map.get("userKey");
		String userNick = map.get("userNick");
		
		vo.setUserKey(userKey);
		vo.setUserNick(userNick);
		
		dao.create(vo);
	}
    
	@Override
	public List<AskReplyVO> list(Integer idx) throws Exception {
		// TODO Auto-generated method stub
		return dao.list(idx);
	}

	@Override
	public void delete(Integer bIdx, Integer idx) throws Exception {
		// TODO 댓글 삭제
		
		if(bIdx == 0) {
			return;
		} else if(bIdx>=1) {
			dao.delete(bIdx, idx);
		}
		
	}

	@Override
	public List<AskReplyVO> listPage(Integer idx, Criteria cri) throws Exception {
		// TODO Auto-generated method stub
		return dao.listPage(idx, cri);
	}
	
}
