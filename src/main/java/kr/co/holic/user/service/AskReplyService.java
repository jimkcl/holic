package kr.co.holic.user.service;

import java.util.List;

import javax.servlet.http.HttpSession;

import kr.co.holic.operator.VO.Criteria;
import kr.co.holic.user.VO.AskReplyVO;

public interface AskReplyService {
	public abstract void create(AskReplyVO vo, HttpSession session) throws Exception; //댓글쓰기
	public abstract List<AskReplyVO> list(Integer idx) throws Exception; //댓글 리스트
	public abstract void delete(Integer bIdx, Integer idx) throws Exception; //댓글 삭제
	public abstract List<AskReplyVO> listPage(Integer idx, Criteria cri) throws Exception; //페이지목록
	
	
}
