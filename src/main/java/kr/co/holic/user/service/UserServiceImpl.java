package kr.co.holic.user.service;

import java.util.Map;
import java.util.Random;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.co.holic.teacher.VO.TeacherDetailVO;
import kr.co.holic.teacher.VO.TimgVO;
import kr.co.holic.user.DAO.UserDAO;
import kr.co.holic.user.VO.UserDetailVO;
import kr.co.holic.user.VO.UserLoginVO;
import kr.co.holic.util.AES256;
import kr.co.holic.util.CouponVO;
import kr.co.holic.util.EmailSender;
import kr.co.holic.util.SHA256;
import lombok.extern.java.Log;

@Log
@Service
public class UserServiceImpl implements UserService {
	
	/* - HanJungIl - 170610 ~ 170709 - 수정
	 * 회원 관련 controller
	 */
	
	@Inject
	private UserDAO dao;
	
	
	@Override
	public int userDelete(HttpSession session) throws Exception {
		// TODO 회원탈퇴
		
		Map<String, Object> map = (Map<String, Object>) session.getAttribute("LOGIN");
		
		int check = dao.userDelete((String) map.get("userKey"));
		
		if(check > 0){
			session.removeAttribute("LOGIN");
			session.invalidate();
		}
		
		return check;
	}
	
	@Override
	public String searchID(String userMail) throws Exception {
		// TODO 아이디 찾기
		
		String userId = dao.searchID(new AES256().aesEncode(userMail));
		
		if(userId == null || "".equals(userId.replaceAll(" ", ""))) return null;
		
		StringBuffer strBuffer = new StringBuffer( new AES256().aesDecode(userId) );
		
		int i = 3;	// 보여줄 부분 길이 설정
		
		if(strBuffer.length() < 4) i = 2;
		
		for(; i < strBuffer.length() ; ++i){
			strBuffer.setCharAt(i, '*');
		}
		
		return strBuffer.toString();
	}

	@Override
	public int searchPassword(HttpServletRequest request, String userMail, String userId) throws Exception {
		// TODO 비밀번호 찾기
		
		AES256 aes = new AES256();
		
		UserLoginVO userLoginVO = new UserLoginVO(aes.aesEncode(userMail), aes.aesEncode(userId), null);
		
		// 새로운 비번 설정
		final int newPWLength = 16;
		
		StringBuffer pwBuf = new StringBuffer();
		Random random = new Random();
		
		for(int i = 0 ; i < newPWLength ; ++i){
			int rIdx = random.nextInt(3);
			
			switch (rIdx) {
			case 0:		// a-z
				pwBuf.append( (char) ( (int) ( random.nextInt(26) ) + 97 ) );
				break;
			case 1:		// A-Z
				pwBuf.append( (char) ( (int) ( random.nextInt(26) ) + 97 ) );
				break;
			case 2:		// 0-9
				pwBuf.append(random.nextInt(10));
				break;
			default:
				break;
			}
		}
		
		// pw = newPW - key = mail
		userLoginVO.setUserPassword(new SHA256().encrypt( pwBuf.toString() ));
		
		log.info("\n\n :: userLoginVO : " + userLoginVO);
		
		int check = dao.searchPassword(userLoginVO);
		
		log.info("\n\n :: check : " + check + "\n\n");
		
		// 변경될 경우 메일 발송
		if(check != 0){
			new EmailSender().sendEmail(aes.aesDecode(userLoginVO.getUserKey()), "Holic 새로운 비밀번호 입니다.", 
					"<div style='display: table;width: 100%;margin: 0;'>"
					+ "<div style='display: table;float: left;width: 25%;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;'></div>"
					+ "<div style='display: table;float: left;width: 50%;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;'>"
						+ "<h2 style='font-size: 40px;font-weight: bold;display: inline-block;margin: 0;'>Holic</h2>"
						+ "<p style='font-size: 12px;float: right;margin: 23px 0 0 0;'>한국에서 배우는 한국어 공부의 선구자 Holic</p>"
						+ "<hr style='margin-top: 0;'/>"
						+ "<div style='margin-bottom: 60px;padding: 15px;border: 4px solid black;'>"
							+ "<p>※ 새로운 비밀번호를 발송해 드립니다.</p>"
							+ "<p style='padding-left: 10px;'>Holic 사이트 로그인을 한 뒤 비밀번호를 변경해 주시기 바랍니다.</p>"
							+ "<hr style='margin-top: 0;'/>"
							+ "<p style='padding-left: 20px;'>☆ 비밀번호 : " + pwBuf.toString() + "</p>"
							+ "<p style='padding-left: 20px;'>☆ 사이트 : " + request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + "</p>"
							+ "<hr style='margin: 0 0 10px 0;'/>"
							+ "<p style='font-size: 12px;text-align: right;margin: 0;'>회원님들의 한국어 능력발전에 Holic이 함께하겠습니다.</p>"
							+ "</div>"
						+ "</div>"
					+ "<div style='display: table;float: left;width: 25%;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;'></div>"
					+ "</div>");
		}
		
		return check;
	}
	
	@Override
	public UserDetailVO getDetail(String userKey) throws Exception {
		// TODO Auto-generated method stub
		
		return dao.getDetail(userKey);
	}

	@Override
	public UserLoginVO getUserLoginVO(String userId) throws Exception {
		// TODO Auto-generated method stub
		
		return dao.getUserLoginVO(userId);
	}
	
	@Override
	public String userJoinCheck(String userKind, String KindName) throws Exception {
		// TODO Auto-generated method stub
		
		return dao.userJoinCheck(userKind, KindName);
	}
	
	@Transactional
	@Override
	public String userJoin(HttpServletRequest request, UserDetailVO detailVO, UserLoginVO loginVO) throws Exception {
		// TODO 회원가입
		dao.DeletUserKey();		
		dao.UserkeyInsert();
		
		Integer userKey = dao.getUserKey();
		loginVO.setUserKey(Integer.toString(userKey));
		loginVO.secretCode(loginVO);
		detailVO.setUserKey(loginVO.getUserKey());
		detailVO.secretCode(detailVO);
		
		dao.LoginVOInsert(loginVO);
		dao.DetailInsert(detailVO);
		dao.DeletUserKey();
		
		// 쿠폰 DB
		final int couponLength = 16;
		
		StringBuffer couponBuf = new StringBuffer();
		Random random = new Random();
		
		for(int i = 0 ; i < couponLength ; ++i){
			int rIdx = random.nextInt(3);
			
			switch (rIdx) {
			case 0:		// a-z
				couponBuf.append( (char) ( (int) ( random.nextInt(26) ) + 97 ) );
				break;
			case 1:		// A-Z
				couponBuf.append( (char) ( (int) ( random.nextInt(26) ) + 97 ) );
				break;
			case 2:		// 0-9
				couponBuf.append(random.nextInt(10));
				break;
			default:
				break;
			}
			
			if( (i + 1) % 4 == 0 && (i + 1) != couponLength ) couponBuf.append("-");
		}
		
		int check = 0;
		int a = 0;
		
		while(a == 0 && check < 100){
			a = dao.joinCoupon(new CouponVO(couponBuf.toString(), null, 12));
			check += 1;
			
			if(check == 100) log.info("\n\n :: check - userNick ::" + check + " / " + detailVO.getUserNick() + "\n\n");
		}
		
		return couponBuf.toString();
	}
	
	@Override
	public void sendCouponEmail(HttpServletRequest request, UserDetailVO detailVO, String coupon) throws Exception {
		// TODO 쿠폰발송용
		detailVO.restoreCode(detailVO);
		
//		log.info("\n\n :: mail :: " + detailVO.getUserMail() + "\n\n");
		
		new EmailSender().sendEmail(detailVO.getUserMail(), "Holic 가입을 축하드립니다.", 
				"<div style='display: table;width: 100%;margin: 0;'>"
				+ "<div style='display: table;float: left;width: 25%;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;'></div>"
				+ "<div style='display: table;float: left;width: 50%;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;'>"
					+ "<h2 style='font-size: 40px;font-weight: bold;display: inline-block;margin: 0;'>Holic</h2>"
					+ "<p style='font-size: 12px;float: right;margin: 23px 0 0 0;'>한국에서 배우는 한국어 공부의 선구자 Holic</p>"
					+ "<hr style='margin-top: 0;'/>"
					+ "<div style='margin-bottom: 60px;padding: 15px;border: 4px solid black;'>"
						+ "<p>※ 사이트 오픈 기념 <b style='color: red;'>12개월 무료</b> 이용 쿠폰을 발송해 드립니다.</p>"
						+ "<p style='padding-left: 10px;'>Holic 사이트 로그인을 한 뒤 쿠폰을 입력해 주시면 즉시 사용가능합니다.</p>"
						+ "<hr style='margin-top: 0;'/>"
						+ "<p style='padding-left: 20px;'>☆ 쿠폰번호 : " + coupon + "</p>"
						+ "<p style='padding-left: 20px;'>☆ 사이트 : " + request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + "</p>"
						+ "<hr style='margin: 0 0 10px 0;'/>"
						+ "<p style='font-size: 12px;text-align: right;margin: 0;'>회원님들의 한국어 능력발전에 Holic이 함께하겠습니다.</p>"
						+ "</div>"
					+ "</div>"
				+ "<div style='display: table;float: left;width: 25%;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;'></div>"
				+ "</div>");
		
//		new EmailSender().sendEmail(dto.getUserMail(), "GShop - 귀하의 비밀번호 입니다.", 
//				"<html><body>Global Shopping 의 시작<br/>GShop 을 이용해 주셔서 감사합니다.<br/><br/>"
//				+ "요청하신 계정의 비밀번호는 " + password + "로 변경되었습니다.<br/>"
//				+ "하단의 url을 통해 로그인 하여 비밀번호를 변경해 주시기 바랍니다.<br/><hr/>"
//				+ "<a href='" + request.getServerName() + request.getContextPath()  + "/jsp/user/loginUI'>로그인</a>"
//				+ "<br/></body></html>");
	}

	@Override
	public void upDateUserDetaiVO(UserDetailVO userDetailVO) throws Exception {
		// TODO Auto-generated method stub
		
		dao.upDateUserDetaiVO(userDetailVO);
	}

	@Override
	public TeacherDetailVO getTeacherDetailVO(String userKey) throws Exception {
		// TODO Auto-generated method stub
		
		return dao.getTeacherDetailVO(userKey);
	}

	@Override
	public TimgVO getTimgVO(String userKey) throws Exception {
		// TODO Auto-generated method stub
		
		return dao.getTimgVO(userKey);
	}
	
	@Transactional
	@Override
	public void updateTeaCher(String userKey,TeacherDetailVO teacherDetailVO, TimgVO timgVO) throws Exception {
		// TODO Auto-generated method stub
		
		teacherDetailVO.setUserKey(userKey);
		timgVO.setUserKey(userKey);
		dao.updateTeacherDetailVO(teacherDetailVO);
		dao.updateTimg(timgVO);
		
	}
}