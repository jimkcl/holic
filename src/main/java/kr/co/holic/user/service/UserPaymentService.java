package kr.co.holic.user.service;

import javax.servlet.http.HttpSession;

import kr.co.holic.user.VO.UserPaymentVO;
import kr.co.holic.util.CouponVO;

public interface UserPaymentService {
	
	/* - HanJungIl - 170610 ~ 170630
	 * 결제/안내 페이 관련 service
	 */
	
	// TODO 회원 쿠폰 사용
	public void couponUsing(HttpSession session, CouponVO coupon) throws Exception;
	
	// TODO 유료 회원 결제정보 등록(기간등록)
	public void paymentChangeUser(HttpSession session, UserPaymentVO paymentVO, String payType) throws Exception;
}