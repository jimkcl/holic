package kr.co.holic.user.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.co.holic.user.DAO.UserPaymentDAO;
import kr.co.holic.user.VO.UserDetailVO;
import kr.co.holic.user.VO.UserPaymentVO;
import kr.co.holic.util.CouponVO;
import lombok.extern.java.Log;

@Log
@Service
public class UserPaymentServiceInfo implements UserPaymentService {
	
	/* - HanJungIl - 170610 ~ 170630
	 * 결제/안내 페이 관련 serviceInfo
	 */
	
	@Inject
	private UserPaymentDAO dao;
	
	@Transactional
	@Override
	public void couponUsing(HttpSession session, CouponVO coupon) throws Exception {
		// TODO 회원 쿠폰 사용
		
		// 쿠폰 사용
		int month = dao.couponUsing(coupon);
		
		// 유료 기간 설정
		Map<String, Object> map = (Map<String, Object>) session.getAttribute("LOGIN");
		
		UserPaymentVO payVO = new UserPaymentVO((String) map.get("userKey"), setData(month));
		
		dao.paymentChangeUser(payVO);
		
		// 유료회원 전환
		dao.paymentChangeUserType(payVO);
		
		// 쿠폰 만료 처리
		dao.couponExpiration(coupon);
		
		// 로그인 정보 변경
		map.put("userKey", map.get("userKey"));
		map.put("userNick", map.get("userNick"));
		map.put("userType", 2);
		
		session.removeAttribute("LOGIN");
		session.setAttribute("LOGIN", map);
	}
	
	@Transactional
	@Override
	public void paymentChangeUser(HttpSession session, UserPaymentVO vo, String payType) throws Exception {
		// TODO 유료 회원 결제정보 등록(기간등록)
		
		Map<String, String> map = (Map<String, String>) session.getAttribute("LOGIN");
		
		String userKey = map.get("userKey");
		int month = Integer.parseInt(vo.getPDate());
		
		// 기간설정
		vo.setPDate(setData(month));
		vo.setUserKey(userKey);
		
		try{
			dao.paymentChangeUser(vo);
		}catch(Exception e){
			// 회원 정보 확인
			UserDetailVO checkVO = dao.paymentChangeUserCheck(vo);
			
			if(checkVO == null){
				// 결제내역은 있으나 회원정보가 없는 경우 - 결제내역 삭제
				log.info("\n\n :::: error - detail is Null ::::\n\n");
				dao.paymentChangeUserDataDelete(vo);
			}
			
			if(checkVO.getUserType() != 2){
				// 회원 타입 변경
				log.info("\n\n :::: error - Type is Change ::::\n\n");
				dao.paymentChangeUserType(vo);
			}
			
			// 회원 정보 확인 후 남은기간 + 결제기간을 하기 위한 준비
			String[] dateSplit = checkVO.getUserPaymentVO().getPDate().split("-");
			
			int addMonth = Integer.parseInt(dateSplit[1]) + month;
			int addYear = 0;
			
			if(addMonth / 12 > 0){
				addMonth = addMonth % 12;
				addYear = addMonth / 12;
			}
			Calendar cal = new GregorianCalendar(Locale.KOREA);
			
			cal.add(Calendar.MONTH, addMonth + 1);
			cal.add(Calendar.YEAR , addYear);
			
			SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM-dd");
			vo.setPDate(fm.format(cal.getTime()));
			
			// 결제기간 연장
			dao.paymentChangeUserDate(vo);
		}
		dao.paymentChangeUserType(vo);
	}
	
	// 기간설정용
	private String setData(int month){
		// 기간설정
		Calendar cal = new GregorianCalendar(Locale.KOREA);
		
		cal.setTime(new Date());
		cal.add(Calendar.MONTH, month + 1);
		
		SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM-dd");
		
		return fm.format(cal.getTime());
	}
}