package kr.co.holic.user.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import kr.co.holic.teacher.VO.TeacherDetailVO;
import kr.co.holic.teacher.VO.TimgVO;
import kr.co.holic.user.VO.UserDetailVO;
import kr.co.holic.user.VO.UserLoginVO;

public interface UserService  {
	
	// TODO 아이디 찾기
	public String searchID(String userMail) throws Exception;
	
	// TODO 비밀번호 찾기
	public int searchPassword(HttpServletRequest request, String userMail, String userId) throws Exception;
	
	// TODO 회원탈퇴
	public int userDelete(HttpSession session) throws Exception;
	
	// TODO 쿠폰발송용
	public void sendCouponEmail(HttpServletRequest request, UserDetailVO detailVO, String coupon) throws Exception;
	
	public abstract UserDetailVO getDetail(String userKey)throws Exception;
	public abstract UserLoginVO getUserLoginVO(String userId)throws Exception;
	public abstract String userJoin(HttpServletRequest request, UserDetailVO detailVO, UserLoginVO loginVO)throws Exception;
	public abstract String userJoinCheck(String userKind, String KindName)throws Exception;
	public abstract void upDateUserDetaiVO(UserDetailVO userDetailVO)throws Exception;
	public abstract TeacherDetailVO getTeacherDetailVO(String userKey)throws Exception;
	public abstract TimgVO getTimgVO(String userKey)throws Exception;
	public abstract void updateTeaCher(String userKey, TeacherDetailVO teacherDetailVO, TimgVO timgVO)throws Exception;
}